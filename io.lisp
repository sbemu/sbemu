(in-package :sbemu)

(defvar *io-standard-input* nil)
(defvar *io-standard-output* nil)
(defvar *io-standard-error* nil)
(defvar *fd*  (make-array 1024 :initial-element nil))
(defvar *fdt* (make-array 1024 :initial-element nil)) ; stream type: {0=character, 8=(unsigned-byte 8)}
(defvar *fdi* (make-array 1024 :initial-element nil)) ; fd information: {filename,opt,type,position}

(defun read-some (s type)
  (if (= type 8)
    (read-byte s)
    (read-char s)))

(defun write-some (d s type)
  (if (= type 8)
    (write-byte d s)
    (write-char (code-char d) s))
  ; FIX: conditionally by a careful level
  (force-output s))

(defun io-handle-standard-input (action &rest args)
  (case action
    (:file-position
      (L "file-position~%" :io)
      0)
    (:file-length
      (L "file-length~%" :io)
      0)
    (:read-byte
      (L "read-byte~%" :io)
      (+ 32 (random 32)))
    (:read
      (let ((len (car args))
            lst x)
        (loop for i from 0 below len do
          (setf x (char-code (read-char *standard-input*)))
          (setf lst (nconc lst (list x)))
          (if (= x 10) (return)))
        #+nil(loop for i from 0 below len
                   ;for a across "(compile-file \"hej\")" do
                   for a across "(sb-ext:quit)" do
          (setf lst (nconc lst (list (char-code a)))))
        #+nil(setf lst (nconc lst (list 10)))
        (L "fake-read ~s len=~a/~a~%" lst len (length lst) :io)
        lst))))

(defun io-file-position (fd offset)
  (let ((s (svref *fd* fd)))
    (if (streamp s)
      (if offset
        (progn (file-position s offset)
               (file-position s))
        (file-position s))
      (funcall s :file-position offset))))

(defun io-file-length (fd)
  (let ((s (svref *fd* fd)))
    (if (streamp s)
      (file-length s)
      (funcall s :file-length))))

(defun io-read-byte (fd)
  (let ((s (svref *fd*  fd))
        (f (svref *fdt* fd)))
    (if (streamp s)
      (read-some s f)
      (funcall s :read-byte f))))

(defun io-open-file (file flags)
  (if (string= file "/dev/tty") (setf file "tty"))
  (loop for fd from 0 below 1024 do
    (when (not (svref *fd* fd))
      ;(if (logbitp flags 3) (error "cant support open file ~a in O_RDWR" file))
      (let ((rea (= (logand flags #xfff) 0))
            (app (logbitp flags 10))
            (opt (list :element-type '(unsigned-byte 8))))
        (if rea
          (setf opt (nconc opt (list :direction :input)))
          (setf opt (nconc opt (list :direction :output
                                     :if-does-not-exist :create
                                     :if-exists (if app :append :supersede)))))
        (setf (svref *fd*  fd) (apply 'open file opt))
        (setf (svref *fdt* fd) 8)
        (setf (svref *fdi* fd) (list file opt 8 0))
        (L "open [~s] as fd ~x with flags ~x using options ~a~%" file fd flags opt :io))
      (return-from io-open-file fd))))

(defun io-close-file (fd)
  (let ((s (svref *fd* fd)))
    (if (streamp s)
      (close s)
      (funcall s :close))
    (setf (svref *fd* fd) nil)))

(defun io-read-file (fd len)
  (let ((s (svref *fd*  fd))
        (f (svref *fdt* fd)))
    (if (streamp s)
      (delete nil (loop for i from 0 below len collect
                    (ignore-errors (read-some s f))))
      (funcall s :read len f))))

(defun io-write-file (fd buf len)
  (let ((s (svref *fd*  fd))
        (f (svref *fdt* fd)))
    (L "write fd=~a len ~x {" fd len :os)
    (loop for i from 0 below len do
      (write-some (mget-u8 (+ buf i)) s f))
    (L "}~%"))
  len)

(defun io-freeze ()
  (loop for i from 3 below 1024 do
    (let ((fi (svref *fdi* i))
          (ft (svref *fdt* i)))
      (when (and fi (< ft 128)) ; normal file types < 128
        (destructuring-bind (file opt type pos) fi
          (file-position (svref *fd* i) pos)
          (setf pos (file-position (svref *fd* i))) ; another CL gotcha
          (close (svref *fd* i))
          (setf (svref *fd* i) nil) ; set to nil to not confuse any save-lisp with non-open streams
          (setf (svref *fdi* i) (list file opt type pos)))))))

(defun io-init ()
  (setf (svref *fd* 0) #'io-handle-standard-input)
  (setf (svref *fd* 1) *standard-output*)
  (setf (svref *fd* 2) *standard-output*)
  (setf (svref *fdt* 0) 0)
  (setf (svref *fdt* 1) 0)
  (setf (svref *fdt* 2) 0)
  (setf (svref *fdi* 0) (list "/dev/stdin"))
  (setf (svref *fdi* 1) (list "/dev/stdout"))
  (setf (svref *fdi* 2) (list "/dev/stderr"))
  ; restore any old file-descriptors
  (loop for i from 3 below 1024 do
    (let ((fi (svref *fdi* i))
          (ft (svref *fdt* i)))
      (when (and fi (< ft 128)) ; normal file types < 128
        (destructuring-bind (file opt type pos) fi
          (format t "restoring file ~a~%" file)
          (setf (svref *fdt* i) type)
          (setf (svref *fd*  i) (apply 'open file opt))
          (setf pos (file-position (svref *fd* i) pos))
          (setf (svref *fdi* i) (list file opt type pos)))))))

