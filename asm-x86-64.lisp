
(in-package :sbemu)

(defmacro cpu-casetree ()
  (let ((casebody    (make-ecasebody *build-cpu-casetree*   '(o1 o2 o3)))
        (casebody-0f (make-ecasebody *build-cpu-casetree0f* '(o1 o2 o3))))
    `(if op2
       ,casebody-0f
       ,casebody)))

(defmacro cpu-casetree-dis ()
  (let ((casebody    (make-ecasebody *build-cpu-casetree-dis*   '(o1 o2 o3)))
        (casebody-0f (make-ecasebody *build-cpu-casetree0f-dis* '(o1 o2 o3))))
    `(if op2
       ,casebody-0f
       ,casebody)))

; http://ref.x86asm.net/geek64.html
; http://tom.bespin.org/src/low-level/opx86.html
(defun disasm-x86-64 (cpu stopoffset stop-pc decode-only &key quit-on-branch)
  (if (or (not stopoffset) (zerop stopoffset)) (setf stopoffset *runts*))
  ;(setf (svref cpu 0) addr)
  (let ((registers (cpu-regs cpu)) (pc (svref (cpu-regs cpu) 31)) cpc
        watchvalue (log *cpu-log*) (nolog *cpu-nolog*) (watch *cpu-watch*) (freeze *cpu-freeze*)
        (cpu-debug-lisp-fun *cpu-debug-lisp-fun*) (tn (cpu-tn cpu))
        flags quit cpulog beginlog branch bi
        o3 rex rexw mrm mrm-mod mrm-reg mrm-r/m sib sib-sca sib-idx sib-bas imm imms immd
        bas rs rd mul ind x y z q p iw ow dis dise
        (action (cons nil nil)))
    (L "disassemble at ~x~%" pc :6 :cpu)
    (setf flags (reg :flags))
    (macrolet (;(decode-flags () `(format nil "~x:~a~a~a~a" flags
               ;                          (if (zerop (logand flags 16)) "" "O")
               ;                          (if (zerop (logand flags 32)) "" "C")
               ;                          (if (zerop (logand flags 64)) "" "Z")
               ;                          (if (zerop (logand flags 128)) "" "S")))
               ; carry is arithmetically used as one by adc/sbc, so keep it at LSB
               (set-flags-c (b) `(setf flags (logior (logand flags (- #xffff   1)) ,b)))         (get-flags-c ()  `(logand flags   1))
               (set-flags-p (b) `(setf flags (logior (logand flags (- #xffff   4)) (ash ,b 2)))) (get-flags-p ()  `(logand flags   4))
               (set-flags-d (b) `(setf flags (logior (logand flags (- #xffff   8)) (ash ,b 3)))) (get-flags-d ()  `(logand flags   8))
               (set-flags-o (b) `(setf flags (logior (logand flags (- #xffff  16)) (ash ,b 4)))) (get-flags-o ()  `(logand flags  16))
               (set-flags-z (b) `(setf flags (logior (logand flags (- #xffff  64)) (ash ,b 6)))) (get-flags-z ()  `(logand flags  64))
               (set-flags-s (b) `(setf flags (logior (logand flags (- #xffff 128)) (ash ,b 7)))) (get-flags-s ()  `(logand flags 128))
               ;(load-mrm () `(progn (setf mrm (advance-u8))
               ;                     (setf mrm-mod (ash (logand mrm #b11000000) -6))
               ;                     (setf mrm-reg (ash (logand mrm #b00111000) -3))
               ;                     (setf mrm-r/m      (logand mrm #b00000111))))
               (load-sib () `(progn (setf sib (advance-u8))
                                    (setf sib-sca (ash (logand sib #b11000000) -6))
                                    (setf sib-idx (ash (logand sib #b00111000) -3))
                                    (setf sib-bas      (logand sib #b00000111))))
               (advance-mrm () `(progn (setf mrm (advance-u8)))) ;mget-u8 pc)) (incf pc)))
               (advance-u8  () `(prog1 (mget-u8 pc)  (incf pc)))
               (advance-u32 () `(prog1 (mget-u32 pc) (incf pc 4)))
               (advance-u64 () `(prog1 (mget-u64 pc) (incf pc 8)))
               (advance-s64 () `(prog1 (mget-s64 pc) (incf pc 8)))
               (advance-s8  () `(prog1 (progn (setf q (mget-u8 pc))
                                              (if (not (zerop (logand q #x80))) (- q #x100) q))
                                       (incf pc)))
               (advance-s16 () `(prog1 (progn (setf q (mget-u16 pc))
                                              (if (not (zerop (logand q #x8000))) (- q #x10000) q))
                                       (incf pc 2)))
               (advance-u16 () `(prog1 (mget-u16 pc) (incf pc 2)))
               (advance-s32 () `(prog1 (progn (setf q (mget-u32 pc))
                                              (if (not (zerop (logand q #x80000000))) (- q #x100000000) q))
                                       (incf pc 4)))
               (incpc (val) `(incf pc ,val)))
    (flet ((decode-flags () (format nil "~x:~a~a~a~a~a~a" flags
                                    (if (zerop (logand flags 1)) "" "C")
                                    (if (zerop (logand flags 4)) "" "P")
                                    (if (zerop (logand flags 8)) "" "D")
                                    (if (zerop (logand flags 16)) "" "O")
                                    (if (zerop (logand flags 64)) "" "Z")
                                    (if (zerop (logand flags 128)) "" "S")))
           (load-mrm () (progn (setf mrm (advance-u8))
                               (setf mrm-mod (ash (logand mrm #b11000000) -6))
                               (setf mrm-reg (ash (logand mrm #b00111000) -3))
                               (setf mrm-r/m      (logand mrm #b00000111))))
           (decode-mrm ()
             (setf rd (logior mrm-reg (ash (logand (or rex 0) 4) 1)))
             (cond
               ((and (< mrm-mod 3) (= mrm-r/m 4))
                 (setf ind t)
                 (setf sib (advance-u8))
                 (setf sib-sca (ash (logand sib #b11000000) -6))
                 (setf sib-idx (ash (logand sib #b00111000) -3))
                 (setf sib-bas      (logand sib #b00000111))
                 (setf bas (logior sib-bas (ash (logand (or rex 0) 1) 3)))
                 (setf rs  (logior sib-idx (ash (logand (or rex 0) 2) 2)))
                 (if (= rs 4) (setf rs nil)) ; rsp is not selectable by sib-index register selector
                 (if (> sib-sca 0) (setf mul (ash 1 sib-sca)))
                 (setf imm (case mrm-mod
                             (1 (advance-s8))
                             (2 (advance-s32))
                             (t 0)))
                 (when (= sib-bas 5)
                   (when (= mrm-mod 0)
                     (setf bas (setf sib-bas nil))
                     (setf imm (advance-s32)))))
               (t
                 (setf ind t)
                 (setf rs (logior mrm-r/m (ash (logand (or rex 0) 1) 3)))
                 (case mrm-mod
                   (0 (case mrm-r/m
                        (5 (setf ind t)
                           (setf rs 31) ; point to pc/ip-register
                           (setf imm (advance-s32)))
                        (t (setf imm nil))))
                   (1 (setf imm (advance-s8)))
                   (2 (setf imm (advance-s32)))
                   (3 (setf ind nil))))))
           (load-address ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if (logbitp 63 x) (setf x (- x (ash 1 64))))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm)))
           (load-indirect ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             ;(if (logbitp 63 x) (setf x (- x (ash 1 64))))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm))
             (unless decode-only (setf y (mget-u64 x)))) ; KLUDGEP conditionally load 32/64bit?
           (load-indirect2 ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if (logbitp 63 x) (setf x (- x (ash 1 64))))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm))
             (unless decode-only
               (setf y (case ow (1 (mget-u8 x)) ; FIX: shouldn't we use iw (input-width)?
                                (2 (mget-u16 x))
                                (4 (mget-u32 x))
                                (t (mget-u64 x))))))
           (load-indirect2-sign ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm))
             (unless decode-only
               (setf y (case ow (1 (mget-s8 x))
                                (2 (mget-s16 x))
                                (4 (mget-s32 x))
                                (t (mget-s64 x))))))
           (sign-extend (val sz)
             (cond
               ((and (= sz 1) (not (zerop (logand val #x80)))) (logior val (logxor (mask 64) (1- #x80))))
               ((and (= sz 2) (not (zerop (logand val #x8000)))) (logior val (logxor (mask 64) (1- #x8000))))
               ((not (zerop (logand val #x80000000))) (logior val (logxor (mask 64) (1- #x80000000))))
               (t val))))
      ;(L "stack registers: ~x,~x" rsp rbp)
      (loop for cpui from 0 below stopoffset do
        (setf branch nil)
        (incf *cpui*)
        (calc-ipscur)
        (when log
          (cond
            ((numberp log) (when (and log (>= *cpui* log)) (setf beginlog t) (setf cpulog t) (setf log nil)))
            ((listp log)
              (if (= pc (first log)) (setf cpulog t))
              (if (= pc (second log)) (setf cpulog nil)))))
        (when (and beginlog nolog)
          (setf x cpulog)
          (setf cpulog t)
          (loop for (a b) in nolog do
            (when (and (>= pc a) (<= pc b))
              (setf cpulog nil)
              (return)))
          (if (not (eq cpulog x))
            (L "~x *** log pruning changed ***~%" pc)))
        (when (and freeze (> *cpui* *cpu-freeze*) (not (get-cpu-state cpu)))
          (setf (reg :pc) pc)
          (setf (reg :flags) flags)
          (setf (reg :opc) cpc)
          ;(setf (cpu-regs cpu) registers)
          (freeze cpu)
          (setf *cpu-freeze* nil))
        (let* ((op (mget-u8 pc)) op2 o1 o2 prefix prefix-f2 prefix-f3 prefix-66 prefix-repnz prefix-repz tmp)
          ; we dont want to inherit values from previous instruction
          (setf o3 (setf rexw (setf rex (setf mrm (setf mrm-mod (setf mrm-reg (setf mrm-r/m (setf sib
           (setf sib-sca (setf sib-idx (setf sib-bas (setf imm (setf imms (setf immd nil))))))))))))))
          (setf bas (setf rs (setf rd (setf mul (setf ind nil)))))
          (fmt "CPU~a.~x.~x 0x~x ~a " tn *cpui* cpui pc (or (gethash pc *syms*) ""))
          ; pick out legacy prefixes (store in prefix)
          (setf cpc pc)
          (loop
            (setf tmp (case op
                        (#xf0 t) ; lock prefix
                        (#xf2 (setf prefix-repnz t) (setf prefix-f2 t)) ; Repeat
                        (#xf3 (setf prefix-f3 (setf prefix-repz t)))  ; |
                        ; (#xf4 t) ; ? (undocumented?) HLT instruction
                        (#x26 t) ; segment override prefix
                        (#x2e t) ; |
                        (#x36 t) ; |
                        (#x3e t) ; |
                        (#x64 t) ; |
                        (#x65 t) ; |
                        (#x66 (setf prefix-66 t)) ; operand-size override prefix
                        (#x67 t))) ; address-size override prefix
            (cond
              (tmp
                (push tmp prefix)
                (incf pc)
                (setf op (mget-u8 pc)))
              (t (return))))
          ;(if prefix (fmt " prefix: ~a" prefix))
          (when (or (= op #x40)
                    (= op #x41)
                    (= op #x42)
                    (= op #x43)
                    (= op #x44)
                    (= op #x45)
                    (= op #x46)
                    (= op #x47)
                    (= op #x48)
                    (= op #x49)
                    (= op #x4a)
                    (= op #x4b)
                    (= op #x4c)
                    (= op #x4d)
                    (= op #x4e)
                    (= op #x4f))
            (setf rex op)
            (if (not (zerop (logand rex 8))) (setf rexw t))
            (incf pc)
            (setf op (mget-u8 pc)))
          ;(if rex
          ;  (fmt "rex:~x" rex)
          ;  (fmt "      "))
          ; check if escape opcode
          (cond
            ((= op #x0f)
              (setf op2 op)
              ;(fmt " ~2x" op2)
              (incf pc)
              (setf op (mget-u8 pc)))
            (t ;(fmt "   ")
               ))
          (incf pc)
          (setf o1 (ash (logand op #b11000000) -6))
          (setf o2 (ash (logand op #b00111000) -3))
          (setf o3      (logand op #b00000111))
          ;(fmt " ~2x(~a~a~a)" op o1 o2 o3)
          (setf dise (length dis))
          (cpu-casetree)
          (if decode-only (if (/= (1- (length dis)) dise) (setf dis (nconc dis (list (list :nothing))))))
          (when watch
            (cond
              (watchvalue
                (when (/= watchvalue (or (mget-u8-soft watch) #xdead))
                  (L "CPU.~x.~x.~x: watch ~x changed from ~x to ~x~%" *cpui* cpui pc watch watchvalue (mget-u8-soft watch))
                  (setf watchvalue (mget-u8-soft watch))))
              (t (setf watchvalue (mget-u8-soft watch))))))
        (if (and bi quit-on-branch) (setf quit t))
        (when (and branch cpu-debug-lisp-fun)
          (cpu-debug-lisp-branch cpu pc cpu-debug-lisp-fun cpulog action)
          (case (car action)
            (:emulate
              (L "emucall requested~%" :8)
              (setf (reg :pc) pc)
              (setf (reg :opc) cpc)
              (setf (reg :flags) flags)
              (return-from disasm-x86-64 :emulate))))
        (fmt "~%")
        (when (or quit
                  *cpu-signal*
                  (get-cpu-state cpu)
                  (and stop-pc (= stop-pc pc)))
          (when *cpu-signal*
            (if (listp *cpu-signal*) (push-cpu-state cpu *cpu-signal*))
            (setf *cpu-signal* nil))
          (return)))))
        (setf (reg :pc) pc)
        (setf (reg :opc) cpc)
        (setf (reg :flags) flags)
        ;(setf (cpu-regs cpu) registers) ; FIX: needed?
        (if decode-only dis cpu)))

(defun disasm2 (addr stopoffset cpu &key stop-pc)
  (if (not addr) (setf (svref (svref cpu 3) 31) addr))
  (if (or (not stopoffset) (zerop stopoffset)) (setf stopoffset #xffffffff))
  (setf *ipsutc* (get-universal-time))
  (setf *ipscur* 0)
  (L "disassemble at ~x~%" addr :6 :cpu)
  ;(setf (svref cpu 0) addr)
  (let ((pc addr) cpc
        (registers (svref cpu 3))
        flags quit cpulog branch disstream
        o3 rex rexw mrm mrm-mod mrm-reg mrm-r/m sib sib-sca sib-idx sib-bas imm imms immd
        bas rs rd mul ind x y z q p iw ow)
    (setf flags (reg :flags))
    (macrolet (;(decode-flags () `(format nil "~x:~a~a~a~a" flags
               ;                          (if (zerop (logand flags 16)) "" "O")
               ;                          (if (zerop (logand flags 32)) "" "C")
               ;                          (if (zerop (logand flags 64)) "" "Z")
               ;                          (if (zerop (logand flags 128)) "" "S")))
               (set-flags-p (b) `(setf flags (logior (logand flags (- #xffff   4)) (ash ,b 2)))) (get-flags-p ()  `(logand flags   4))
               (set-flags-d (b) `(setf flags (logior (logand flags (- #xffff   8)) (ash ,b 3)))) (get-flags-d ()  `(logand flags   8))
               (set-flags-o (b) `(setf flags (logior (logand flags (- #xffff  16)) (ash ,b 4)))) (get-flags-o ()  `(logand flags  16))
               (set-flags-c (b) `(setf flags (logior (logand flags (- #xffff  32)) (ash ,b 5)))) (get-flags-c ()  `(logand flags  32))
               (set-flags-z (b) `(setf flags (logior (logand flags (- #xffff  64)) (ash ,b 6)))) (get-flags-z ()  `(logand flags  64))
               (set-flags-s (b) `(setf flags (logior (logand flags (- #xffff 128)) (ash ,b 7)))) (get-flags-s ()  `(logand flags 128))
               (load-sib () `(progn (setf sib (advance-u8))
                                    (setf sib-sca (ash (logand sib #b11000000) -6))
                                    (setf sib-idx (ash (logand sib #b00111000) -3))
                                    (setf sib-bas      (logand sib #b00000111))))
               (advance-mrm () `(progn (setf mrm (advance-u8)))) ;mget-u8 pc)) (incf pc)))
               (advance-u8  () `(prog1 (mget-u8 pc)  (incf pc)))
               (advance-u32 () `(prog1 (mget-u32 pc) (incf pc 4)))
               (advance-u64 () `(prog1 (mget-u64 pc) (incf pc 8)))
               (advance-s64 () `(prog1 (mget-s64 pc) (incf pc 8)))
               (advance-s8  () `(prog1 (progn (setf q (mget-u8 pc))
                                              (if (not (zerop (logand q #x80))) (- q #x100) q))
                                       (incf pc)))
               (advance-s16 () `(prog1 (progn (setf q (mget-u16 pc))
                                              (if (not (zerop (logand q #x8000))) (- q #x10000) q))
                                       (incf pc 2)))
               (advance-u16 () `(prog1 (mget-u16 pc) (incf pc 2)))
               (advance-s32 () `(prog1 (progn (setf q (mget-u32 pc))
                                              (if (not (zerop (logand q #x80000000))) (- q #x100000000) q))
                                       (incf pc 4)))
               (incpc (val) `(incf pc ,val))
               (bring (&body body) `(list 'progn (list 'setf 'imm imm)
                                                 (list 'setf 'mul mul)
                                                 (list 'setf 'bas bas)
                                                 (list 'setf 'rs  rs)
                                                 (list 'setf 'rd  rd)
                                                 (list 'setf 'imms  imms)
                                                 (list 'setf 'immd  immd)
                                                 (list 'setf 'mrm mrm)
                                                 (list 'setf 'sib sib)
                                                 (list 'setf 'mrm-mod mrm-mod)
                                                 (list 'setf 'mrm-reg mrm-reg)
                                                 (list 'setf 'mrm-r/m mrm-r/m)
                                                 (list 'setf 'x x)
                                                 (list 'setf 'y y)
                                                 (list 'setf 'z z)
                                                 (list 'setf 'q q)
                                      ,@body)))
    (flet ((decode-flags () (format nil "~x:~a~a~a~a~a~a" flags
                                    (if (zerop (logand flags 4)) "" "P")
                                    (if (zerop (logand flags 8)) "" "D")
                                    (if (zerop (logand flags 16)) "" "O")
                                    (if (zerop (logand flags 32)) "" "C")
                                    (if (zerop (logand flags 64)) "" "Z")
                                    (if (zerop (logand flags 128)) "" "S")))
           (load-mrm () (setf mrm (advance-u8))
                        (setf mrm-mod (ash (logand mrm #b11000000) -6))
                        (setf mrm-reg (ash (logand mrm #b00111000) -3))
                        (setf mrm-r/m      (logand mrm #b00000111))
                        (setf disstream (nconc disstream (list (list 'f 'load-mrm)))))
           (decode-mrm ()
             (setf rd (logior mrm-reg (ash (logand (or rex 0) 4) 1)))
             (cond
               ((and (< mrm-mod 3) (= mrm-r/m 4))
                 (setf ind t)
                 (setf sib (advance-u8))
                 (setf sib-sca (ash (logand sib #b11000000) -6))
                 (setf sib-idx (ash (logand sib #b00111000) -3))
                 (setf sib-bas      (logand sib #b00000111))
                 (setf bas (logior sib-bas (ash (logand (or rex 0) 1) 3)))
                 (setf rs  (logior sib-idx (ash (logand (or rex 0) 2) 2)))
                 (if (= rs 4) (setf rs nil)) ; rsp is not selectable by sib-index register selector
                 (if (> sib-sca 0) (setf mul (ash 1 sib-sca)))
                 (setf imm (case mrm-mod
                             (1 (advance-s8))
                             (2 (advance-s32))
                             (t 0)))
                 (when (= sib-bas 5)
                   (when (= mrm-mod 0)
                     (setf bas (setf sib-bas nil))
                     (setf imm (advance-s32)))))
               (t
                 (setf ind t)
                 (setf rs (logior mrm-r/m (ash (logand (or rex 0) 1) 3)))
                 (case mrm-mod
                   (0 (case mrm-r/m
                        (5 (setf rs 31) ; point to pc/ip register
                           (setf imm (advance-s32)))
                        (t (setf imm nil))))
                   (1 (setf imm (advance-s8)))
                   (2 (setf imm (advance-s32)))
                   (3 (setf ind nil)))))
             (setf disstream (nconc disstream (list (list 'f 'decode-mrm)))))
           (load-address ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm)))
           (load-indirect ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm))
             (setf y (mget-u64 x)))
           (load-indirect2 ()
             (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
             (if mul (setf x (* x mul)))
             (if bas (incf x (reg bas)))
             (if imm (incf x imm))
             (setf y (case ow (1 (mget-u8 x))
                              (2 (mget-u16 x))
                              (4 (mget-u32 x))
                              (t (mget-u64 x)))))
           (sign-extend (val sz)
             (cond
               ((and (= sz 1) (not (zerop (logand val #x80)))) (logior val (logxor (mask 64) (1- #x80))))
               ((and (= sz 2) (not (zerop (logand val #x8000)))) (logior val (logxor (mask 64) (1- #x8000))))
               ((not (zerop (logand val #x80000000))) (logior val (logxor (mask 64) (1- #x80000000))))
               (t val)))
           (eff (&rest effect-forms)
             (setf disstream (nconc disstream effect-forms))))
      ;(L "stack registers: ~x,~x" rsp rbp)
      (loop for cpui from 0 below stopoffset do
        (setf branch nil)
        (incf *cpui*)
        (incf *ipscur*)
        (when (> *ipscur* 100000)
          (let ((utc (get-universal-time)))
            (push (- utc *ipsutc*) *ips*)
            (setf *ipsutc* utc)
            (setf *ipscur* 0)))
        (let* ((op (mget-u8 pc)) op2 o1 o2 prefix prefix-66 prefix-repnz prefix-repz tmp)
          ; we dont want to inherit values from previous instruction
          (setf o3 (setf rexw (setf rex (setf mrm (setf mrm-mod (setf mrm-reg (setf mrm-r/m (setf sib
           (setf sib-sca (setf sib-idx (setf sib-bas (setf imm (setf imms (setf immd nil))))))))))))))
          (setf bas (setf rs (setf rd (setf mul (setf ind nil)))))
          (fmt "CPU.~x.~x 0x~x ~a " *cpui* cpui pc (or (gethash pc *syms*) ""))
          ; pick out legacy prefixes (store in prefix)
          (setf cpc pc)
          (loop
            (setf tmp (case op
                        (#xf0 t) ; lock prefix
                        (#xf2 (setf prefix-repnz t)) ; Repeat
                        (#xf3 (setf prefix-repz t))  ; |
                        ; (#xf4 t) ; ? (undocumented?) HLT instruction
                        (#x26 t) ; segment override prefix
                        (#x2e t) ; |
                        (#x36 t) ; |
                        (#x3e t) ; |
                        (#x64 t) ; |
                        (#x65 t) ; |
                        (#x66 (setf prefix-66 t)) ; operand-size override prefix
                        (#x67 t))) ; address-size override prefix
            (cond
              (tmp
                (push tmp prefix)
                (incf pc)
                (setf op (mget-u8 pc)))
              (t (return))))
          (if prefix (fmt " prefix: ~a" prefix))
          (when (or (= op #x40)
                    (= op #x41)
                    (= op #x42)
                    (= op #x43)
                    (= op #x44)
                    (= op #x45)
                    (= op #x46)
                    (= op #x47)
                    (= op #x48)
                    (= op #x49)
                    (= op #x4a)
                    (= op #x4b)
                    (= op #x4c)
                    (= op #x4d)
                    (= op #x4e)
                    (= op #x4f))
            (setf rex op)
            (if (not (zerop (logand rex 8))) (setf rexw t))
            (incf pc)
            (setf op (mget-u8 pc)))
          (if rex
            (fmt "rex:~x" rex)
            (fmt "      "))
          ; check if escape opcode
          (cond
            ((= op #x0f)
              (setf op2 op)
              (fmt " ~2x" op2)
              (incf pc)
              (setf op (mget-u8 pc)))
            (t (fmt "   ")))
          (incf pc)
          (setf o1 (ash (logand op #b11000000) -6))
          (setf o2 (ash (logand op #b00111000) -3))
          (setf o3      (logand op #b00000111))
          (fmt " ~2x(~a~a~a)" op o1 o2 o3)
          (eff (list :--------mark-------- cpc))
          (eff (list o1 o2 o3 rex rexw prefix-66 cpc
                     (cpu-casetree-dis))))
        (when branch
          (when (and cpulog *cpu-debug-lisp-fun*)
            (cpu-debug-lisp-branch cpu pc t)))
        (fmt "~%")
        (if (or quit (get-cpu-state cpu)
                (and stop-pc (= stop-pc pc)))
          (return)))))
        (setf (reg :pc) pc)
        (setf (reg :opc) cpc)
        (setf (reg :flags) flags)
        (setf (svref cpu 3) registers)
        disstream))

