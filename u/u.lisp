(proclaim '(optimize (safety 0) (debug 0) (speed 3)))


(with-open-file (s "/dev/stdin" :external-format :iso-8859-1)
(let ((h (make-hash-table :test 'equal))
      (i 0) lst)
  (loop for line = (read-line s nil) while line do
    (when (search "cpu branch" line)
      (setf line (subseq line (+ 7 (search "new pc " line))))
      (setf line (subseq line (1+ (search " " line))))
      (setf line (subseq line 0 (search " " line)))
      (let ((x (gethash line h)))
        (if x
          (setf (gethash line h) (list (first x) (incf (second x))))
          (setf (gethash line h) (list (incf i) 0))))))
  (maphash (lambda (k v)
             (push (nconc v (list k)) lst))
           h)
  (setf lst (sort lst (lambda (a b) (< (car a) (car b)))))
  (loop for (ord call name) in lst do
    (format t "~a ~a~%" call name))))

#+clisp (ext:quit)
#+sbcl (sb-ext:quit)
