#!/usr/bin/perl -w

use strict;

my %h = ();
my $i = 0;

while(my $line = <STDIN>){
  chomp $line;
  if($line =~ /cpu branch/){
    $line =~ s/.* cpu branch new pc \w+ (.*]) \w+.*/$1/;
    if(!defined($h{$line})){
      $h{$line}{num} = $i;
      $i++;
    }
    $h{$line}{cnt}++;
  }
}

my @lst = ();

while(my($name, $v) = each %h){
  push(@lst, [$$v{num}, $$v{cnt}, $name]);
}

@lst = sort {$$a[0] <=> $$b[0]} @lst;

foreach my $item (@lst){
  print "$$item[1] $$item[2]\n";
}

