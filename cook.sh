#!/bin/sh

BIN=/usr/local/bin/sbcl
BIN=sbcl/src/runtime/sbcl

objdump -d $BIN |\
grep @plt |\
grep -e jmpq -e callq |\
perl -Wpe '$_=~s/.*[calqjmp]+\s+(\w+)\s+<([@\w]+).*/(#x$1 :$2)/' |\
sort | uniq > temp-stubs.lisp

objdump -t $BIN |\
grep -v GLIBC |\
grep -v '\@PLT' |\
grep '\.text' |\
perl -Wpe '$_=~s/(\w+)\s+.*\.text\s+\w+\s+(.*)/\(#x$1 \"$2\"\)/' > temp-syms.lisp

objdump --syms $BIN |\
grep ' g ' |\
grep '\.bss' |\
perl -Wpe '$_=~s/(\w+)\s+.*/#x$1/' > temp-pages.lisp

