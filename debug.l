;
;  Debugging
; ------------
;

 8.1 Debugging overview
 ------------------------
   The debugging possibilities are good, and are focused on giving a programmable interface.
   Easy debugging is also a reason why the whole sbemu system is running single-threaded.
   We want to interrupt at anytime and be sure no other thread modifies the sbemu state.

 8.2 Debugging implementation
 ------------------------------

   |$(defvar *lisp-fun-count* (make-hash-table :test 'equal))
   |
   |$(defgeneric cpu-debug-lisp-fun (cpu pc how cpulog name count action))
   |
   |$(defmethod cpu-debug-lisp-fun (cpu pc how cpulog name count action)
   |   (if (or cpulog (and (eq how :maid) (not count)))
   |     (L "~x cpu branch new pc ~x [~a] ~a" *cpui* pc name count)))

   |; FIX: how do we properly identify ascii32?
   |$(defun cpu-debug-lisp-branch-arch (cpu pc how cpulog action endian wz lowtagz instance-lowtag)
   |   (flet ((mget-word (addr)
   |            (cond ;((and endian (= wz 8)) (mget-u64b-soft)) ; big-endian-64bit not such arch yet
   |                  ((and endian (= wz 4)) (mget-u32b-soft addr)) ; mips,
   |                  ((and (not endian) (= wz 8)) (mget-u64-soft addr))))) ; x86-64,
   |     (let ((obj (mget-word (- pc (* 4 wz))))
   |           (hz (* *heap-size* PAGESZ))
   |           (name (make-array 0 :element-type 'character :fill-pointer 0 :adjustable t))
   |           obj2 count)
   |       (when (and obj (= (ldb (byte lowtagz 0) obj) instance-lowtag) (< obj hz))
   |         (setf obj2 (mget-word (+ obj 1 (- wz) (* wz 3))))
   |         (when (and obj2 (= (ldb (byte lowtagz 0) obj2) instance-lowtag) (< obj2 hz))
   |           (let ((n (+ obj2 1)) x type)
   |             (setf type (mget-u8-soft (+ n 1))) ; get the second character in the string (if ascii32 this is zero)
   |             (when type ; if no page-fault
   |               (setf type (if (zerop type) t))
   |               (loop
   |                 (setf x (mget-u8-soft n))
   |                 (if (or (not x) (zerop x)) (return))
   |                 (vector-push-extend (code-char x) name)
   |                 (incf n (if type 4 1)))))
   |           (when name
   |             (setf count (gethash name *lisp-fun-count*))
   |             (if (not count)
   |               (setf (gethash name *lisp-fun-count*) 1)
   |               (setf (gethash name *lisp-fun-count*) (1+ count)))
   |             (cpu-debug-lisp-fun cpu pc how cpulog name count action)
   |             (fmt "~%")
   |             (when (and *monitor-lisp-function-break*
   |                      (string= *monitor-lisp-function-break* name))
   |               (setf *monitor-lisp-function-break* cpu)
   |               (setf *cpu-signal* t))))))))
   |
   |$(defun cpu-debug-lisp-branch (cpu pc how cpulog action)
   |   (case (cpu-arch cpu)
   |     (0 (cpu-debug-lisp-branch-arch cpu pc how cpulog action nil 8 4 15)) ; x86-64
   |     (1 (cpu-debug-lisp-branch-arch cpu pc how cpulog action t   4 3  7)) ; mips
   |     ))

 8.3 Hook cpu-debug-lisp-fun
 ------------------------------
   Everytime the cpu enters a lisp function it will call this hook.
   Example of how to extend this hook:

   | (defmethod cpu-debug-lisp-fun :after (cpu pc how cpulog name count action)
   |   (logf (list "At cpu iteration: " *cpui* " cpu branched to pc " pc
   |               " which is a lisp function of name: " name
   |               " and has been called " count " times"
   |               " current logging is: " cpulog
   |               " how we want to debug is: " how)))

 8.4 PIE function replacement
 --------------------------------
   Code that are called often and doesn't get moved around in
   the memory, can be replaced by an host-lisp function.
   This would speed up execution and also enable functionallity modification.

   Below we will begin defining all stuff that is needed to finally replace
   the guest lisp function EQL.

   All replacement support is currently only for x86-64.

 8.4.1 Function insert-stub-call
 ---------------------------------
   |$(defun insert-stub-call (name addr)
   |   (let* ((pn (pageno addr))
   |          (pb (aref *memw* pn))
   |          (page (get-page-num pn)))
   |     (cond
   |       ((not page) (error "cant install stub, no underlying page"))
   |       (t (setf (aref *memw* pn) OS-PROT-WRITE)))
   |     (let ((stubaddr (+ *stub-vector* (incf *stub-load-index* 8)))
   |           (code (gethash name *stubs-name*)))
   |       (assert code)
   |       (setf (gethash stubaddr *stubs*) code)
   |       (asm-stub stubaddr)
   |       ; we need to write down a jmpq-<stubaddr> to the lisp-function entry address
   |       ; the instruction looks like: jmpq-32bitoffset+pc
   |       ; first we write down the jmp operand and the mrm byte
   |       (mset-u8     addr    #xff) ; JMP/CALL operator
   |       (mset-u8  (+ addr 1) #x25) ; mrm byte
   |       (mset-u8  (+ addr 2) 0)    ; offset where jump should search for its jumpaddress
   |       (mset-u8  (+ addr 3) 0)
   |       (mset-u8  (+ addr 4) 0)
   |       (mset-u8  (+ addr 5) 0)
   |       (mset-u64 (+ addr 6) stubaddr)) ; write down the jumpaddress
   |     (setf (aref *memw* pn) pb)))      ; restore old page bits

 8.4.2 Macro add-stub-function
 ---------------------------------
   |$(defmacro add-stub-function ((name) &body body)
   |   `(progn
   |      (assert (keywordp ,name))
   |      (assert (not (gethash ,name *stubs-name*)))
   |      (setf (gethash ,name *stubs-name*)
   |            ,@body)))

 8.4.3 Example of replacing EQL
 --------------------------------
   Put the following form in your site.lisp file.
   Go through each FIX line and change depending on the sbcl runtime and core being emulated.

   | (macrolet
   |   ((maybe-get-widetag (a) `(and (= (logand ,a #xf) #xf) (mget-u8-soft (logxor ,a #xf)))))
   | (defun eql-real (x y)
   |   (let ((a (logand x 7))
   |         (b (logand y 7)))
   |     (cond
   |       ((= x y) t) ; same identity
   |       ((and (= a b) (= a #xf)) ; same lowtag of other-pointer type
   |         (setf a (maybe-get-widetag x))
   |         (setf b (maybe-get-widetag y))
   |         (cond
   |           ((and a (= a b))
   |             (error "NI: eql-real must check widetag type and compare number data"))))))))
   |
   | (macrolet
   |   ((x86-rax () `(svref registers 0))
   |    (x86-rcx () `(svref registers 1))
   |    (x86-rdx () `(svref registers 2))
   |    (x86-rbx () `(svref registers 3))
   |    (x86-rsp () `(svref registers 4))
   |    (x86-rbp () `(svref registers 5))
   |    (x86-rsi () `(svref registers 6))
   |    (x86-rdi () `(svref registers 7))
   |    (x86-r8  () `(svref registers 8))
   |    (x86-r12 () `(svref registers 12))
   |    (x86-rpc () `(svref registers 31))
   |    (x86-ropc () `(svref registers 32))
   |    (x86-rflags () `(svref registers 33))
   |    (cvar-core-address () #x62D568) ; FIX: this depends on sbcl version/build FIX: can we ask ELF?
   |    (cvar-page-table-address () #x635230) ; FIX: same here
   |    (page-size () #x1000)) ; PAGESZ is only available at CT, make this a RT avail function
   |
   | (install-tick-trigger #x400000 ; FIX: we want to install somewhere around call to lisp function REINIT
   |
   | ; replace core's EQL
   | (add-stub-function (:eql@core)
   |       (macrolet
   |         ((true ()  `(setf (svref registers 2) #x2010004f))
   |          (false () `(setf (svref registers 2) #x20100017))
   |          (maybe-get-widetag (a) `(and (= (logand ,a #xf) #xf) (mget-u8-soft (logxor ,a #xf)))))
   |         (lambda (cpu registers ret) ; EQL implementation
   |           (declare (ignore cpu))
   |           (L "EQL stub~%")
   |           (let ((x (svref registers 2)) (y (svref registers 7)) z a b)
   |             (L "  (EQL ~x ~x)~%" x y)
   |             (cond
   |               ((= x y) (true)) ; same identity
   |               (t
   |                 (cond
   |                   ((zerop (logand y 7)) ; even-fixnum?
   |                     (false))
   |                   (t
   |                     (cond
   |                       ((= (logand y #xff) #x1a) ; what is 1a?
   |                         (error "eql-drop-out-3"))
   |                       (t
   |                         (setf z (maybe-get-widetag y))
   |                         (cond
   |                           (z
   |                             (L "  eql widetag=~x~%" z)
   |                             (cond
   |                               ((<= z #x16) (error "eql-drop-out-5"))
   |                               ((< z #x1e)  (error "eql-drop-out-6"))
   |                               ((> z #x2a) (false))
   |                               (t ; possible complex number
   |                                 (cond
   |                                   ((= z #x1e) (error "widetag 1e not handled"))
   |                                   ((= z #x12) (error "widetag 12 not handled"))
   |                                   ((< z #x22)
   |                                     ; check first argument has same widetag
   |                                     (setf z (maybe-get-widetag x))
   |                                     (cond
   |                                       ((and z (= z #x22))
   |                                         ; do REALPART on arguments
   |                                         (setf a (mget-u64 (+ x -7)))
   |                                         (setf b (mget-u64 (+ y -7)))
   |                                         (cond
   |                                           ((eql-real a b)
   |                                             (setf a (mget-u64 (+ x -7 8)))
   |                                             (setf b (mget-u64 (+ y -7 8)))
   |                                             (if (eql-real a b) (true) (false)))
   |                                           (t (false))))))
   |                                   (t (error "eql-drop-out-4"))))))
   |                           (t (false)))))))))
   |             ; POP off return value assert it is equal to ret
   |             (setf z (mget-u64 (svref registers 4)))
   |             (assert (= z ret))
   |             (incf (svref registers 4) 8)
   |             (mset-u64 (+ 8 (svref registers 5)) z) ; write ret back
   |             (L "pop is writing ~x to ~x~%" z (+ 8 (svref registers 5)))
   |             ; make frame size (LEA rbp-30 to rsp
   |             (setf (svref registers 4) (- (svref registers 5) #x30))
   |             (L "after LEA, rbp/rsp is ~x/~x~%" (svref registers 5) (svref registers 4))
   |             (setf (svref registers 4) (svref registers 5))
   |             (L "after doing mov rbp to rsp, bp/rsp is ~x/~x~%" (svref registers 5) (svref registers 4))
   |             ; POP to RBP
   |             (setf (svref registers 5) (mget-u64 (svref registers 4)))
   |             (incf (svref registers 4) 8)
   |             (L "after pop to rbp, rbp/rsp is ~x/~x~%" (svref registers 5) (svref registers 4))
   |             ; RET
   |             ;(incf (svref registers 4) 8)
   |             ;(L "after ret, rbp/rsp is ~x/~x~%" (svref registers 5) (svref registers 4))
   |             ;
   |             (decf (svref registers 4) 8) ; compensate for call/stub return
   |             ;;;
   |             (L "return rbp/rsp is ~x/~x~%" (svref registers 5) (svref registers 4))
   |             ret))))
   | (insert-stub-call :eql@core #x1000016A18) ; FIX: run the emulator with logging to get the address to EQL.
   | (format t "**** replaced guest-core-function with host-lisp-functions ****~%")))




