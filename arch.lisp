
(in-package :sbemu)

(defvar *arch* (make-array 2))
(defvar *arch-regs* (make-array 2))

(defvar *config-x86-64* nil)
(defvar *config-mips* nil)

(defvar *config-x86-64-regs* (make-array 33))
(defvar *config-mips-regs* (make-array 33))

(defvar *build-cpu-casetree* nil)
(defvar *build-cpu-casetree0f* nil)
(defvar *build-cpu-casetree-dis* nil)
(defvar *build-cpu-casetree0f-dis* nil)
(defvar *runop2* (make-hash-table))

(defmacro add-casetree (operoct name)
  (let* ((o1 (first  operoct))
         (o2 (second operoct))
         (o3 (third  operoct))
         (lst (cond (o3 (list o1 o2 o3))
                    (o2 (list o1 o2))
                    (o1 (list o1))
                    (t (error "bad octet triple")))))
    `(progn
       (setf *build-cpu-casetree*
             (update-casetree *build-cpu-casetree* ',lst
                              (list ',(symbolicate "cpu-runop-" name))))
       (setf *build-cpu-casetree-dis*
             (update-casetree *build-cpu-casetree-dis* ',lst
                              (list ',(symbolicate "cpu-disop-" name)))))))

(defmacro add-casetree0f (operoct name)
  (let* ((o1 (first  operoct))
         (o2 (second operoct))
         (o3 (third  operoct))
         (lst (cond (o3 (list o1 o2 o3))
                    (o2 (list o1 o2))
                    (o1 (list o1))
                    (t (error "bad octet triple")))))
    `(progn
       (setf *build-cpu-casetree0f*
             (update-casetree *build-cpu-casetree0f* ',lst
                              (list ',(symbolicate "cpu-runop-" name))))
       (setf *build-cpu-casetree0f-dis*
             (update-casetree *build-cpu-casetree0f-dis* ',lst
                              (list ',(symbolicate "cpu-disop-" name)))))))

(defmacro define-cpu-runop ((name) &body body)
  (let ((rname (symbolicate "cpu-runop-" name))
        (dname (symbolicate "cpu-disop-" name)))
    `(progn
       (defmacro ,dname ()
         (quote ,(list 'quote (append '(progn) body))))
       (defmacro ,rname ()
         (quote ,(append '(progn) body))))))

; for now disable the separate disassembler operations
(defmacro define-cpu-runop2 ((name) letbody loadbody runbody &optional disbody)
  (let ((rname (symbolicate "cpu-runop-" name))
        ;(dname (symbolicate "cpu-disop-" name))
        )
    `(progn
       ;(defmacro ,dname ()
       ;     ,(list 'quote (list 'progn (append (list 'progn) loadbody)
       ;                                (list 'bring
       ;                                      (append (list 'quote)
       ;                                              (list (append (list 'progn)
       ;                                                            runbody)))))))
       (defmacro ,rname ()
             ,(list 'quote (list 'progn
                                 `(let (,@letbody)
                                    ,@loadbody
                                    (if decode-only
                                      (progn ,@disbody)
                                      (progn ,@runbody))))))
       (assert (not (gethash ',name *runop2*)))
       (setf (gethash ',name *runop2*)
             (quote (();(,@letbody)
                     ();(,@loadbody)
                     (,@runbody)
                     ))))))

(defmacro define-cpu-oper ((name oper &optional oper2) &body body)
  `(progn
     (defmacro ,(symbolicate "cpu-runop-" name) ()
       (quote ,(append '(progn) body)))
     (format t "add oper ~a in casetree~%" ',name)
     (if ,oper2
       (setf *build-cpu-casetree0f* (update-casetree *build-cpu-casetree0f* ',oper ',body))
       (setf *build-cpu-casetree*   (update-casetree *build-cpu-casetree*   ',oper ',body)))
     #+nil(if ,oper2
       (setf *build-cpu-casetree0f-dis* (update-casetree *build-cpu-casetree0f-dis* ',oper ',body))
       (setf *build-cpu-casetree-dis*   (update-casetree *build-cpu-casetree-dis*   ',oper ',body)))
     ))

(defun page-size () PAGESZ)

; also see cpu.lisp for stuff that also is architecture generic.
; we put it there, for dependency reasons

(defmacro cpu-tn    (cpu) `(svref ,cpu 0))
(defmacro cpu-context (cpu) `(svref ,cpu 1))
(defmacro cpu-state (cpu) `(svref ,cpu 2))
(defmacro cpu-regs  (cpu) `(svref ,cpu 3))
(defmacro cpu-fpucw (cpu) `(svref ,cpu 4))
(defmacro cpu-pthread (cpu) `(svref ,cpu 5))
(defmacro cpu-futex (cpu) `(svref ,cpu 6))
(defmacro cpu-sigmask (cpu) `(svref ,cpu 7))
(defmacro cpu-arch    (cpu) `(svref ,cpu 8))
(defmacro cpu-misc    (cpu) `(svref ,cpu 9))

(defmacro push-cpu-state (cpu state)
  `(push ,state (cpu-state ,cpu)))

(defmacro pop-cpu-state (cpu)
  `(pop (cpu-state ,cpu)))

(defmacro get-cpu-state (cpu)
  `(car (cpu-state ,cpu)))

; translate a register-keyword-name to the
; arch-independent-index (macro-index-at-compile-time)
(defun arch-reg-name (name)
  (ecase name
    (:arg1 1)
    (:arg2 2)
    (:arg3 3)
    (:arg4 4)
    (:arg5 5)
    (:arg6 6)
    (:rsp  7)
    (:rfp  8)
    (:rpc  9)
    (:ropc 10)
    (:rflags 11)))

(defun load-arch-config ()
  (setf (svref *arch* 0) *config-x86-64*)
  (setf (svref *arch* 1) *config-mips*)
  (setf (svref *arch-regs* 0) *config-x86-64-regs*)
  (setf (svref *arch-regs* 1) *config-mips-regs*)
  ; setup the register-translation going from a
  ; macro-index-at-compile-time (mict)
  ; to the real-cpu-register-index
  ; this mapping is only found here and in the macro AREG
  ; The reason for having this macro is to simplify AREGF
  ; operation during runtime. If not using this mapping
  ; AREGF would have to do assoc through the whole config
  ; to get the real-cpu-register-index.
  (let ((regs (cdr (assoc :regs *config-x86-64*))))
    (loop for reg in '(:rpc :ropc :rflags :rfp :rsp :arg1 :arg2 :arg3 :arg4 :arg5 :arg6) do
      (if (not (cdr (assoc reg regs))) (error "register nickname ~s not found in config" reg))
      (setf (svref *config-x86-64-regs* (arch-reg-name reg)) (cdr (assoc reg regs)))))
  (let ((regs (cdr (assoc :regs *config-mips*))))
    (loop for reg in '(:rpc :ropc :rflags :rfp :rsp :arg1 :arg2 :arg3 :arg4) do
      (if (not (cdr (assoc reg regs))) (error "register nickname ~s not found in config" reg))
      (setf (svref *config-mips-regs* (arch-reg-name reg)) (cdr (assoc reg regs))))))

; give a mapping from the elf-symbol-name to the index into *arch* array
(defun get-arch-enum (elf-name)
  (ecase elf-name
    (x86-64 0)
    (mips 1)))

(defmacro archname (name)
  (ecase name
    (x86-64 0)
    (mips 1)))

(defun archf (cpu name)
  (cdr (assoc name (svref *arch* (cpu-arch cpu)))))

(defun archf-set (cpu name value)
  (setf (cdr (assoc name (svref *arch* (cpu-arch cpu)))) value))

(defsetf archf archf-set)

(defun aregf (cpu mict)
  (assert (not (numberp (svref *arch-regs* (cpu-arch cpu))))) ; if this assertion fails, load-arch-config may not have been called
  (let ((real-reg-index (svref (svref *arch-regs* (cpu-arch cpu)) mict)))
    (svref (cpu-regs cpu) real-reg-index)))

(defun aregf-set (cpu mict value)
  (let ((real-reg-index (svref (svref *arch-regs* (cpu-arch cpu)) mict)))
    (setf (svref (cpu-regs cpu) real-reg-index) value)))

(defsetf aregf aregf-set)

; This is needed to enable usage of keyword symbol in source code.
; Otherwise we would need to put in mict numbers as is.
; For all-arch-psuedo-registers (not in a physical cpu register-file for any supported arch)
; we could macroexpand directly to the cpu-regs and not do the mict translation mambo
(defmacro areg (cpu sym-or-mict)
  (cond
    ((keywordp cpu) ; this clause works differently than the default clause, here the sym-or-mict
                    ; is either a arch-dependent symbol or an arch-dependent number. Not a mict.
                    ; mict is going away, and areg will be the prefered accessor.
                    ; This will not make it possible to runtime change which registers libc uses as arguments,
                    ; but it will cleanup the code everywhere alot.
      (ecase cpu
        (:x86-64 `(register-index ,sym-or-mict))
        (:mips   `(regnum ,sym-or-mict))))
    (t
      (let ((mict (if (keywordp sym-or-mict) (arch-reg-name sym-or-mict) sym-or-mict)))
        `(aregf ,cpu ,mict)))))

