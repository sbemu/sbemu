(asdf:defsystem "sbemu"
  :description "sbcl emulator"
  :version "20110701"
  :author "Larry Valkama"
  :serial t
  :depends-on ()
  :components ((:file "packages")
               ;(:file "config-loader") ; definitions usable by compile-time configuration
               ;(:file "config")        ; compile-time configuration
               ;(:file "config-cli")    ; get compile-time configuration from command-line
               (:file "logger")
               (:file "portable") ; routines that differs between CL-implementation
               (:file "portable-aux") ;
               (:file "common")   ; routines that are general and is not sbemu specific
               (:file "arch")     ; architecture support common
               (:file "local")    ; local routines to sbemu that are used here and there
               (:file "asm")      ; common assembler/disassembler
               (:file "x86-64")   ; arch specific instructions
               (:file "asm-x86-64") ; x86-64 converter
               (:file "mips-util"); tools for simplifying instruction description
               (:file "mips")     ; instruction description for mips
               (:file "asm-mips") ; mips converter
               (:file "cpu")      ; cpu handling
               (:file "os")       ; OS stubs and such
               (:file "os-aux")   ;
               (:file "heap")     ; heap manipulation
               (:file "io")       ; io-handling
               (:file "libc")     ; system calls
               (:file "libc-aux") ;
               (:file "elf")      ; executable linking format
               (:file "ice")      ; thaw/freeze emulator cores
               (:file "emu")      ; misc emulator stuff
               (:file "debug")    ; debug support
               (:file "mon")      ; monitor
               (:file "mon-aux")  ; monitor commands
               (:file "lisp-code") ; lisp-code analyser
               (:file "frag")     ; frag compiler
               (:file "help")))

