(in-package :sbemu)

; we could take the cond clause and wrap it into
; a let body with the const and call eval and see what happens
; but we try to be more efficient by parsing each cond-conditional clause
(defun shunt-code-cond (src const)
  (let ((cc (car src))) ; cond conditional
    (if (eq cc t) (return-from shunt-code-cond)) ; dont bother with always-match cond clause condition
    (cond
      ((symbolp cc) ; cond condition clause is a simple symbol (which is checked for nullity)
        (loop for (nam . val) in const do
          (cond
            ((and (eq cc nam) val) (return-from shunt-code-cond :found))
            ((and (eq cc nam) (not val)) (return-from shunt-code-cond :remove))))))))

(defun shunt-code-case (src const)
  (let ((cc (second src))
        (cases (cddr src)))
    (cond
      ((symbolp cc)
        (loop for (nam . val) in const do
          (when (eq nam cc)
            (loop for cas in cases do
              (when (or (equal (car cas) val)
                        (eq (car cas) t))
                (return-from shunt-code-case (list :found t (cdr cas)))))
            (return-from shunt-code-case (list nil t src))))))
    (list nil nil src)))

(defun shunt-code (src const)
  (cond
    ((and (listp src) (eq (car src) 'fmt)) nil)
    ((and (listp src) (eq (car src) 'cond))
      (let (taken newcond)
        (setf newcond (append (list 'cond)
                              (delete nil (loop for conds in (cdr src) collect
                                            (case (shunt-code-cond conds const)
                                              (:found (setf taken (list* (car conds) (shunt-code (cdr conds) const)))
                                                      (return))
                                              (:remove nil)
                                              (t (shunt-code conds const)))))))
        (if taken (setf newcond (list 'cond taken)))
        (cond
          ((zerop (length (cdr newcond))) nil) ; no cond-path ie a (cond) was found
          ((= 1 (length (cdr newcond))) ; a single cond path was found, return the cond-path contents wrapped into a progn
            (setf (car (second newcond)) 'progn)
            (if (equal (second newcond) '(progn nil))
              nil
              (second newcond)))
          (t newcond)))) ; multi cond path, dont touch the beast
    ((and (listp src) (or (eq (car src) 'case)
                          (eq (car src) 'ecase)))
      (let ((cn (car src))) ; case or ecase
        (destructuring-bind (found id src) (shunt-code-case src const)
          (cond
            (found
              (let ((body (shunt-code src const)))
                (if (= (length body) 1) ; avoid unnecessary progn
                  (car body)
                  (append (list 'progn) body))))
            ((and (eq cn 'case) id) ; variable found but didn't match case-condition, drop whole case
              nil)
            (t (append (list cn (second src))
                       (loop for cas in (cddr src) collect
                         (shunt-code cas const))))))))
    ((and (listp src) (eq (car src) 'if))
      (let ((cc (second src))) ; condition-clause
        (cond
          ((symbolp cc)
            (let ((con (assoc cc const)))
              (cond
                ((and (eq (car con) cc) (cdr con)) ; condition-clause name matches and value is true
                  (shunt-code (third src) const))
                ((and (eq (car con) cc) (not (cdr con))) ; condition-clause name matches and value is false
                  (shunt-code (fourth src) const))
                (t (append (list 'if) (shunt-code (cdr src) const))))))
          (t
            (loop for item in src collect (shunt-code item const))))))
    ((listp src)
      (loop for item in src collect (shunt-code item const)))
    ((symbolp src)
      (let ((con (find src const :test (lambda (a b) (eq a (car b))))))
        (if con
          (cdr con)
          src)))
    (t src)))

