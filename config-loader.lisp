(in-package :sbemu)

; collection of late configuration (not build time)
(defvar *config-late* nil)
(defvar *config-features* (make-hash-table))

(defun feature (name)
  (gethash name *config-features*))

; the pagesize is semi-bound to the emulated operating-system pagesize
(defmacro define-pagesize (value)
  (let ((mask (1- value))
        (bitn (loop for i from 0 below 32 ; a bit convoluted
                    finally (return i)
                    while (not (logbitp i value)))))
    `(progn
       (define-symbol-macro PAGESZ ,value)
       (define-symbol-macro PAGEMS ,mask)
       (define-symbol-macro PAGEBN ,bitn))))

; paranoid is used here and there to assert stuff,
; for more performance set paranoid to 0
(defmacro define-paranoid (value)
  `(define-symbol-macro PARANOID-LIMIT ,value))

(defmacro define-malloc-max (value) `(define-symbol-macro MALLOC-MAX ,value))

(defmacro define-direct-array-access (&optional value) `(setf (gethash :daa *config-features*) ,value))

(defmacro define-os-prot-read (value) `(define-symbol-macro OS-PROT-READ ,value))
(defmacro define-os-prot-write (value) `(define-symbol-macro OS-PROT-WRITE ,value))
(defmacro define-os-prot-exec (value) `(define-symbol-macro OS-PROT-EXEC ,value))
(defmacro define-os-prot-lazy (value) `(define-symbol-macro OS-PROT-LAZY ,value))

(macrolet ((deflate (name form)
             `(defmacro ,name (value) `(push (append ',',form (list ,value)) *config-late*))))
  (deflate define-heap-size (setf *heap-size*))
  (deflate define-log-level (set-log-level)))

(defmacro define-use-cl-store (&optional value) `(setf (gethash :cl-store *config-features*) ,value))

; lib

(defmacro define-lib-readlink-kludge (value) `(define-symbol-macro +lib-readlink-kludge+ ,value))
(defmacro define-lib-env-home        (value) `(define-symbol-macro +lib-env-home+ ,value))
(defmacro define-lib-env-sbcl-home   (value) `(define-symbol-macro +lib-env-sbcl-home+ ,value))

(defmacro define-lib-args-x86-64     (value) `(define-symbol-macro +lib-args-x86-64+ ,value))
(defmacro define-lib-args-mips       (value) `(define-symbol-macro +lib-args-mips+ ,value))
(defmacro define-lib-ret-x86-64      (value) `(define-symbol-macro +lib-ret-x86-64+ ,value))
(defmacro define-lib-ret-mips        (value) `(define-symbol-macro +lib-ret-mips+ ,value))

