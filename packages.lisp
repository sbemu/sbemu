(defpackage :common
  (:use :cl)
  (:export :L
           :log-open
           :log-close))

(defpackage :sbemu
  (:use :cl :common))

