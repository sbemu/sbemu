(asdf:defsystem "sbemu-test"
  :description "sbcl emulator test"
  :version "20110701"
  :author "Larry Valkama"
  :serial t
  :depends-on ()
  :components ((:file "test")
               (:file "test-common")))

