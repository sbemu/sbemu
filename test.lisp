(in-package :sbemu)

(defvar *debugger* t) ; should a error in a test will reach the debugger
(defvar *curfile* nil)
(defvar *tests* nil)
(defvar *suite* (make-hash-table :test 'equal))

(defmacro deftest ((&rest args) &body body)
  (let ((name (car args)))
    `(let ((name ',name)
           (suite (gethash *curfile* *suite*)))
       (L "adding test ~a/~a" *curfile* name)
       (setf suite (nconc suite (list (list name (lambda ()
                                                   ,@body)))))
       (setf (gethash *curfile* *suite*) suite))))

(defun list-tests ()
  (L "list tests.")
  (let ((tests (directory (make-pathname :name :wild :type :wild :defaults "./t/"))))
    (setf tests (loop for test in tests collect (format nil "~a" test)))
    (setf tests (delete nil (loop for test in tests collect
                              (progn
                                (when (= (- (length test) 5) (or (search ".lisp" test :from-end t) 0))
                                  (subseq test (1+ (search "/" test :from-end t))))))))
    (setf *tests* (sort tests 'string<))))

(defun load-tests ()
  (loop for test in *tests* do
    (L "loading test ~a" test)
    (in-package :sbemu)
    (setf *curfile* test)
    (if (search "/" test)
      (load test)
      (load (format nil "./t/~a" test)))))

(defun run-test (fun)
  (cond
    (*debugger* (funcall fun))
    (t
      (let (str res)
        (setf str (with-output-to-string (s)
                    (let ((*standard-output* s))
                      (setf res (multiple-value-list 
                                  (ignore-errors (funcall fun)))))))
        (car res)))))

(defun run-tests (s)
  (format s "running tests~%")
  (let ((good 0)    (total 0)
        (subgood 0) (subtotal 0))
    (loop for file in *tests* do
      (let ((suite (gethash file *suite*)))
        (format s "suite: ~a~%" file)
        (loop for test in suite do
          (let (res str)
            (destructuring-bind (name fun) test
              (setf str (format nil "  test ~a> " name))
              (format s "~a" str)
              (setf res (run-test fun)))
            (loop for x from 0 below (- 40 (length str)) do (format t "."))
            (cond
              ((and (numberp res) (= res 123))
                (format s " ok")
                (incf good))
              ((and (listp res) (numberp (first res)) (= (first res) 123))
                (let ((pass (second res))
                      (fail (third res)))
                  (cond
                    ((zerop fail)
                      (format s " ok ~a" pass)
                      (incf good))
                    (t
                      (format s " FAIL ~a/~a (~a%)" pass (+ pass fail) (* 100 (/ pass (+ pass fail))))))
                  (incf subtotal (+ pass fail))
                  (incf subgood pass)))
              (t
                (format s " FAIL")))
            (incf total)
            (format s "~%")))))
    (format s "~%result: ~a/~a tests, ~a/~a subtests~%" good total subgood subtotal)))

(defun test ()
  (load-arch-config)
  (setf *cpu-debug-lisp-fun* nil)
  (setf *cpu-log* 0)
  (list-tests)
  (if *cmdarg* (setf *tests* *cmdarg*))
  (load-tests)
  (if (= *verbose* 0) (setf *debugger* nil))
  (run-tests t))

