
(in-package :sbemu)

;(defmacro define-cpu-runop ((name) &body body)
;  (let ((mname (intern (concatenate 'string "CPU-RUNOP-" (string name)))))
;    `(defmacro ,mname ()
;       (quote ,(append '(progn) body)))))

; currently we only store the cpu context in the threads array
(defvar *threads* (make-array 8 :initial-element nil))

(defvar *interrupt-depth* 0)
(defvar *interrupt-stack*  #x19000000) ;#x53d9c78  ; FIX: use a static place for now to mimic gdb
(defvar *cpu-context-area* #x19000100) ;#x53d9c40

(defvar *tick-triggers* nil)

(defmacro install-tick-trigger (ticks &body body)
  `(progn
     (if (not *tick-triggers*) (setf *tick-triggers* (make-hash-table)))
     (setf (gethash ,ticks *tick-triggers*) (lambda () ,@body))))

(defun make-thread (arch)
  (L "making thread of arch ~s~%" arch)
  (let (sloti cpu ok)
    (loop
      (loop for i from 0 below (length *threads*) do
        (unless (svref *threads* i)
          (setf sloti i)
          (setf ok t)
          (return)))
      (if ok (return))
      (L "grow threads" :cpu)
      (let ((threads (make-array (+ (length *threads*) 8) :initial-element nil)))
        (loop for i from 0 below (length *threads*) do
          (setf (svref threads i) (svref *threads* i)))))
    (L "creating thread ~a~%" sloti :cpu)
    (setf (svref *threads* sloti) (setf cpu (make-cpu arch)))
    (setf (cpu-tn cpu) sloti)
    (setf (cpu-state cpu) nil)
    (setf (cpu-arch cpu) arch)
    cpu))

(defun get-thread (tn)
  (svref *threads* tn))

;FIX: put these on stack so we can do interrupts recursively
(defun setup-cpu-context (cpu context caa)
  (declare (ignore context))
  (cond
    ((= (cpu-arch cpu) (archname x86-64))
      ; if we were an int3 caused interrupt, pc would point to the byte after int3 instruction here
      (mset-u64 (+ caa #xa0) (areg cpu :rsp)) ; accessed by sbcl:os_context_sp_addr
      (mset-u64 (+ caa #xa8) (areg cpu :rpc)) ; accessed by sbcl:os_context_pc_addr to get 'int3 code'
      caa)
    ((= (cpu-arch cpu) (archname mips))
      ; FIX: check how the mips-abi wants things
      (mset-u32 (+ caa #xa0) (areg cpu :rsp)) ; accessed by sbcl:os_context_sp_addr
      (mset-u32 (+ caa #xa4) (areg cpu :rpc)) ; accessed by sbcl:os_context_pc_addr to get 'int3 code'
      caa)))

(defun setup-info (cpu info caa)
  (cond
    ((= (cpu-arch cpu) (archname x86-64))
      (mset-u64 (+ caa #x110) (ash info PAGEBN))
      (+ caa #x100))
    ((= (cpu-arch cpu) (archname mips))
      (mset-u32 (+ caa #x110) (ash info PAGEBN))
      (+ caa #x100))))

(defun pop-interrupt-context (cpu)
  (decf *interrupt-depth*)
  (let ((context (pop (cpu-context cpu))))
    (when (not context)
      (error "no context")
      #+nil(return-from pop-interrupt-context))
    (destructuring-bind (old-sigmask registers info sig) context
      (setf (cpu-sigmask cpu) old-sigmask)
      (setf (cpu-regs cpu) registers)
      (cond
        ((not info)         (setf (areg cpu :rpc) (mget-u64 (+ *cpu-context-area* #xa8))))
        ((not (zerop info)) (setf (areg cpu :rpc) (areg cpu :ropc)))) ; rewind pc only on memory interrupts
      (cond
        ((= sig 11) ; heavy-duty interrupts uses their own stack
          (incf *interrupt-stack* #x458))
        (t
          ; the rsp is already restored, nothing to do here
          ))
      (decf *cpu-context-area* #x100)
      (L "return from signal handler, pc=~x~%" (areg cpu :rpc) :os :4))))

(defun push-interrupt-context (cpu data)
  (incf *interrupt-depth*)
  (let ((sa (first data))
        (sig (second data))
        (info (third  data))
        (context (fourth data))
        (registers (copy-seq (cpu-regs cpu)))
        (arch (cpu-arch cpu))
        v)
    (incf *cpu-context-area* #x100) ; FIX: not sure what is stored here and what size this area should be
    (destructuring-bind (fun new-sigmask) sa
      (L "signal-handler: ~x blocked: ~x, current pc=~x~%" sa new-sigmask (reg :pc) :os :4)
      (push (list (cpu-sigmask cpu) (cpu-regs cpu) info sig) (cpu-context cpu)) ; save old cpu state
      (setf (cpu-regs cpu) registers)
      (setf (cpu-sigmask cpu) new-sigmask)
      (cond
        (sa
          (setf (areg cpu :arg1) sig) ; setup the interrupt handler arguments
          (setf (areg cpu :arg2) (setup-info cpu (or info 0) *cpu-context-area*))
          (setf (areg cpu :arg3) (setup-cpu-context cpu context *cpu-context-area*))
          (cond
            ((= sig 11) ; FIX: why different stack on signal-11?
              (L "  new rsp is interrupt-stack at ~x~%" *interrupt-stack* :os :4)
              (setf (areg cpu :rsp) *interrupt-stack*)
              (decf *interrupt-stack* #x458))
            (t
              ;(setf (reg :rsp) (- (reg :rsp) #x458))
              (L "  old rsp = ~x~%" (areg cpu :rsp) :4)
              ;(setf (reg :rsp) (- (reg :rsp) #x448))
              (setf (areg cpu :rsp) (- (areg cpu :rsp) #x470)) ; FIX: can we grovel this value instead?
              (L "  new rsp (old-rsp - offset) = ~x~%" (reg :rsp) :4)))
          (decf (areg cpu :rsp) 8) ; setup a fake stack frame that we can recognize on return
          (setf v (areg cpu :rsp))
          (L "storing interrupt-return vector at address ~x~%" v :os :8)
          (setf (areg cpu :rpc) fun) ; set the interrupt handler to call
          (cond
            ((eq arch (archname x86-64))
              (mset-u64 v #xcafe0000))   ; store the end-of-ccall-marker (FIX: jump to an vector with escape/break?)
            ((eq arch (archname mips))
              (setf (svref registers 31) #xcafe0000) ; set ra (return from interrupt)
              (setf (svref registers 25) fun) ; set t9 to function
              )))
        (t ; FIX: dont die on all signals
          (L "got signal ~a but no handler~%" sig)
          (push-cpu-state cpu (list :exit nil)))))))

(defun loop-cpu (ci cpu maxrunts)
  (L "switch to thread ~a[~a]~%" ci (cpu-tn cpu) :cpu)
  (if (not (numberp maxrunts)) (setf maxrunts nil))
  (let ((run t)
        (state (get-cpu-state cpu)) l1 l2)
      (when state
        (destructuring-bind (reason data) state
          (cond
            ((eq reason :tickwait)
              (cond
                ((> *cpui* data)
                  (pop-cpu-state cpu)
                  (L "thread ~x is done tickwait~%" ci))
                (t (return-from loop-cpu :wait))))
            ((eq reason :futex) (return-from loop-cpu :wait))
            ((eq reason :signal)
              (L "got cpu-exit ~s by interrupt, pushing context, current depth ~a stack=~x~%" state *interrupt-depth* *interrupt-stack* :os)
              ; FIX: maybe we dont want to run the signal handler straight away but
              ; instead resume execution and after a random amount of time/insts do it
              (pop-cpu-state cpu)
              (push-cpu-state cpu nil) ; clear the current state so the cpu can run (ie, if we are in a futex)
              (setf l1 (length (cpu-state cpu))) ; a bit kludgy but only for internal paranoia
              (push-interrupt-context cpu data)
              (setf l2 (length (cpu-state cpu)))
              (when (/= l1 l2)
                (L "ERROR: got cpu-exit by interrupt: ~s~%" (get-cpu-state cpu) :os)
                (error "not-sure-how-to-proceed")))
            ((eq reason :interrupt-exit)
              (L "got cpu-exit by interrupt, popping context at depth ~a~%" *interrupt-depth* :os :8)
              (pop-cpu-state cpu) ; pop away the current signal
              (assert (not (get-cpu-state cpu))) ; assert we are in clear-state
              (pop-cpu-state cpu) ; pop away the clear-state needed to run the signal
              (pop-interrupt-context cpu))
            ((eq reason :thread-exit)
              (return-from loop-cpu :kill)))))
      (when run
        (L "calling disasm with cpu-state ~s~%" (cpu-state cpu))
        (maybe-inhibit-gc
          (case (disasm cpu maxrunts)
            (:emulate ; disasm didn't run becase it stumbled upon a lisp-function that wants to be emucalled
              (L "~%emucall at pc=~x~%" (areg cpu :rpc) :8)
              (emulate-call cpu)
              (L "pc after emucall=~x flags=~b~%" (areg cpu :rpc) (areg cpu :rflags) :8)
              ))))
      ; After disasm has been run, the cpu may have ended up in a signal state.
      ; if so let the next call to LOOP-CPU handle it
      nil))

(defun loop-cpus (maxrunts)
  (let (res num)
    (loop
      (os-call)
      (if *tick-triggers*
        (maphash (lambda (ticks fun)
                   (when (> *cpui* ticks)
                     (L "install-late triggered~%")
                     (funcall fun)
                     (remhash ticks *tick-triggers*)))
                 *tick-triggers*))
      (setf num 0)
      (loop for i from 0
            for cpu across *threads* do
        (when cpu
          (setf res (loop-cpu i cpu (or maxrunts #x4000)))
          (cond
            ((not res) (incf num)) ; cpu has runned instructions
            ((eq res :kill)
              (L "removing thread ~a~%" i)
              (setf (svref *threads* i) nil)))))
      (when (zerop num) ; no cpu has runned
        (L "no cpu has runned~%")
        (sleep 0.1)
        (incf *cpui* 100000))
      (if maxrunts (return)))))

