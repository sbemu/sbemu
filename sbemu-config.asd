(asdf:defsystem "sbemu-config"
  :description "sbcl emulator config"
  :version "20110701"
  :author "Larry Valkama"
  :serial t
  :depends-on ()
  :components ((:file "packages")
               (:file "config-loader") ; definitions usable by compile-time configuration
               (:file "config")        ; compile-time configuration
               (:file "config-cli")))  ; get compile-time configuration from command-line
