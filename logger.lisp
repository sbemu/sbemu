(in-package :common)

(defvar *log-level* 0)        ; any log-usage below or equal to this level is reported in the logfile
(defvar *log-level-default* 3)
(defvar *log-dont-compile* 0) ; any log-usage with levels above this is replaced by an PROGN
                              ; example if level 4: (if x (L :5 "x is")) becomes (if x (progn))
(defvar *log-filename* nil)
(defvar *log-stream* t)

(defgeneric log-open (&key filename))
(defgeneric log-close ())

; example of logging usage:
; (L "1234")    ; use default log category and default log level
; (L "1234" :4) ; use default log category, only log at level 4
; (L "1234" :4 :cpu) ; categorize as CPU, only log at level 4
; this is supposed to be a macro, but is a fun for now
(defun L (&rest args)
  (if (not *log-stream*) (return-from l))
  (let (v (level *log-level-default*) cat)
    (setf args (reverse args))
    (loop repeat 2 do
      (when (keywordp (car args))
        (setf v (string (car args)))
        (cond
          ((and (= (length v) 1) (< (char-code (aref v 0)) 58)) (setf level (- (char-code (aref v 0)) 48)))
          (t (setf cat v)))
        (setf args (cdr args))))
    (setf args (reverse args))
    (when (<= level *log-level*)
      (if cat (format *log-stream* "~a: " cat))
      (apply 'format *log-stream* args))))

(defmethod log-open (&key filename)
  (let ((file (or filename *log-filename*)))
    (when (streamp *log-stream*) ; maybe close previous log-stream
      (log-close))
    (when (and (stringp file) (not (streamp *log-stream*)))
      (setf *log-stream* (open file :direction :output :if-exists :supersede :if-does-not-exist :create)))))

(defmethod log-close ()
  (when (streamp *log-stream*)
    (finish-output *log-stream*)
    (close *log-stream*)
    (setf *log-stream* nil)))

