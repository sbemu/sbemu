(in-package :sbemu)

(defmacro disadd (&rest items)
  `(setf dis (nconc dis (list (list cpc pc ,@items)))))

(defmacro fmt (&rest args)
  `(if cpulog
     (L ,@args)))

; registers:

; RFLAGS {32bit | EFLAGS }
; bits description
; 11   OF overflow
; 10   DF Direction
;  7   SF Sign
;  6   ZF Zero
;  4   AF Aux Carry
;  2   PF Parity
;  0   CF Carry
; RIP    {32bit | EIP}

(defun register-name (register-index)
  (ecase register-index
    (0  'rax)
    (1  'rcx)
    (2  'rdx)
    (3  'rbx)
    (4  'rsp)
    (5  'rbp)
    (6  'rsi)
    (7  'rdi)
    (8  'r8)
    (9  'r9) ; Isn't r9 used?
    (10 'r10)
    (11 'r11) ; Isn't r11 used?
    (12 'r12)
    (13 'r13)
    (14 'r14)
    (15 'r15)
    (16 'xmm0)
    (17 'xmm1)
    (18 'xmm2)
    (19 'xmm3)
    (20 'xmm4)
    (21 'xmm5)
    (22 'xmm6)
    (23 'xmm7)
    (31 'pc)
    (32 'opc)
    (33 'flags)))

(defun register-index (register-name)
  (ecase register-name
    (rax 0) (:rax 0)
    (rcx 1) (:rcx 1)
    (rdx 2) (:rdx 2)
    (rbx 3) (:rbx 3)
    (rsp 4) (:rsp 4)
    (rbp 5) (:rbp 5)
    (rsi 6) (:rsi 6)
    (rdi 7) (:rdi 7)
    (r8  8) (:r8  8)
    (r9  9) (:r9  9) ; Isn't r9 used?
    (r10 10) (:r10 10)
    (r11 11) (:r11 11) ; Isn't r11 used?
    (r12 12) (:r12 12)
    (r13 13) (:r13 13)
    (r14 14) (:r14 14)
    (r15 15) (:r15 15)
    (xmm0 16) (:xmm0 16)
    (xmm1 17) (:xmm1 17)
    (xmm2 18) (:xmm2 18)
    (xmm3 19) (:xmm3 19)
    (xmm4 20) (:xmm4 20)
    (xmm5 21) (:xmm5 21)
    (xmm6 22) (:xmm6 22)
    (xmm7 23) (:xmm7 23)
    (pc   31) (:pc   31)
    (opc  32) (:opc  32)
    (flags 33) (:flags 33)))

; instruction decoding:
;
; operator source -> destination
;
; Legacy-prefix, REX-prefix, Opcode, ModRM, SIB, Displacement, Immediate
;
; * a 2-byte opcode begins with #x0F
; * REX = 48 means 64bit operand size
;
; Legacy prefixes:
; Operand-size override 66
; Address-size override 67
; Segment-override      2e,3e,26,64,65,36,
; Lock                  f0
; repeat                f3,f2

; REX: 0100abcd
; a (bit3): 0 = default operand size, 1 = 64bit operand size
; b (bit2): 1 bit extension of MRM.REG (giving total size of 4bits, which spans 16 regs)
; c (bit1): 1 bit extension of SIB.IDX (giving total size of 4bits, which spans 16 regs)
; d (bit0): 1 bit extension of MRM-R/M, SIB, or opcode-reg field

; ModRM byte: consist of Mod,Reg and R/M field:
;   Mod: (2bits) 00 -- no-disp, 01 -- disp8, 10 -- disp16/32, 11 -- reg
;   Reg: Register or extra opcode-bits
;   R/M:

;(defmacro rget-u64 (i) `(svref registers ,i))

(defmacro rset-u8 (reg val)
  (if (keywordp reg)
    (let ((reg (register-index (intern (string reg)))))
      `(setf (svref registers ,reg) (logand #xff ,val)))
    `(setf (svref registers ,reg) (logand #xff ,val))))

(defmacro rset-u16 (reg val)
  (if (keywordp reg)
    (let ((reg (register-index (intern (string reg)))))
      `(setf (svref registers ,reg) (logand #xffff ,val)))
    `(setf (svref registers ,reg) (logand #xffff ,val))))

(defmacro rset-u32 (reg val)
  (if (keywordp reg)
    (let ((reg (register-index (intern (string reg)))))
      `(setf (svref registers ,reg) (logand #xffffffff ,val)))
    `(setf (svref registers ,reg) (logand #xffffffff ,val))))

(defmacro rset-u64 (reg val)
  (if (keywordp reg)
    (let ((reg (register-index (intern (string reg)))))
      `(setf (svref registers ,reg) ,val))
    `(setf (svref registers ,reg) ,val)))

(defmacro rget-u64 (reg)
  (if (keywordp reg)
    (let ((reg (register-index (intern (string reg)))))
      `(svref registers ,reg))
    `(svref registers ,reg)))

(defmacro rget-u32 (i) `(logand (svref registers ,i) #xffffffff))
(defmacro rget-u16 (i) `(logand (svref registers ,i) #xffff))
(defmacro rget-u8  (i) `(logand (svref registers ,i) #xff))

(defmacro reg (name/num &optional what)
  (case what
    (:name
      (cond
        ((keywordp name/num) `(register-name ,(register-index (intern (string name/num)))))
        (t                   `(register-name ,name/num))))
    (:name-xmm
      (cond
        ((keywordp name/num) `(register-name ,(register-index (intern (string name/num)))))
        (t                   `(register-name (+ 16,name/num)))))
    (:xmm
      (cond
        ((keywordp name/num) `(svref registers ,(register-index (intern (string name/num)))))
        (t                   `(svref registers (+ 16 ,name/num)))))
    (t
      (cond
        ((keywordp name/num) `(svref registers ,(register-index (intern (string name/num)))))
        (t                   `(svref registers ,name/num))))))

(defmacro to-xmm (reg) `(incf ,reg 16))

(defmacro mask (bits) `(1- (ash 1 ,bits)))

(defmacro rex-extend (what bit)
  `(if rex (setf ,what (logior ,what (ash (logand rex ,bit) (- 4 ,bit))))))
;  `(setf ,what (logior ,what (ash (logand (or rex 0) ,bit) (- 4 ,bit))))

(defun fmt-reg (registers r)
  (if r
    (concatenate 'string
                 (string (register-name r))
                 "("
                 (if (= r 31) "" (format nil "~x" (reg r)))
                 ")")
    ""))

(defun fmt-ind (registers x y mul bas imm rs &optional dis)
  (if dis
    (list :ind
      (if bas (reg bas :name))
      (if rs (if (= rs 31)
               :pc
               (reg rs :name)))
      mul
      imm)
    (concatenate 'string
                 "["
                 (cond
                   (bas (format nil "~a(~x)" (reg bas :name) (reg bas)))
                   (t ""))
                 (cond
                   ((not rs) "")
                   ((= rs 31) "+pc")
                   (t (format nil "+~a(~x)" (reg rs :name) (reg rs))))
                 (cond
                   (mul (format nil "*~x" mul))
                   (t ""))
                 (cond
                   (imm (format nil "+~x" imm))
                   (t ""))
                 (format nil "=~x]=~x" x y))))

(defmacro fmtreg (r) `(fmt-reg registers ,r))
(defmacro fmtind (&optional dis)
  (if dis
    `(fmt-ind registers x y mul bas imm rs t)
    `(fmt-ind registers x y mul bas imm rs)))

;        C Z  O
; d > s  0 0  S
; d = s  0 1  0
; d < s  1 0 !S
(defmacro set-flags2 ()
  `(cond
     ((> q 0)
       (set-flags-c 0)
       (set-flags-z 0)
       (set-flags-s 0)
       (set-flags-o 0))
     ((zerop q) (set-flags-c 0) (set-flags-z 1) (set-flags-s 0) (set-flags-o 0))
     ((< q 0)
       (set-flags-c 1)
       (set-flags-z 0)
       (set-flags-s 1)
       (set-flags-o 1))))

(defmacro set-flags ()
  `(cond
     ((> v1 0)
       (set-flags-c 0)
       (set-flags-z 0)
       (set-flags-s 0)
       (set-flags-o 0))
     ((zerop v1) (set-flags-c 0) (set-flags-z 1) (set-flags-s 0) (set-flags-o 0))
     ((< v1 0)
       (set-flags-c 1)
       (set-flags-z 0)
       (set-flags-s 1)
       (set-flags-o 1))))

(defmacro set-flags3 (ow)
  `(cond
     ((zerop (ldb (byte (* ,ow 8) 0) y)) (set-flags-c 0) (set-flags-z 1) (set-flags-o 0))
     ((zerop (logand y (ash 1 (1- (* ,ow 8)))))
       (set-flags-c 0)
       (set-flags-z 0)
       (set-flags-o (if (zerop (get-flags-s)) 0 1)))
     (t
       (set-flags-c 1)
       (set-flags-z 0)
       (set-flags-o (if (zerop (get-flags-s)) 1 0)))))

(defmacro set-flags4 (ow)
  `(cond
     ((zerop q) (set-flags-c 0) (set-flags-z 1) (set-flags-o 0))
     ((zerop (logand q (ash 1 (1- (* ,ow 8)))))
       (set-flags-c 0)
       (set-flags-z 0)
       (set-flags-o (if (zerop (get-flags-s)) 0 1)))
     (t
       (set-flags-c 1)
       (set-flags-z 0)
       (set-flags-o (if (zerop (get-flags-s)) 1 0)))))

(defmacro define-simple-mrm (name &key indirect direct (write t) (direct-rd-as-xmm t))
  `(define-cpu-runop2 (,name)
     ()
     ((load-mrm)
      (decode-mrm)
      (unless ind
        (to-xmm rs)
        (if ,direct-rd-as-xmm (to-xmm rd))))
     ((cond
        (ind
          (load-indirect)
          ,@indirect
          ,@(if write '((setf (reg rd) y)))
          (fmt "~a ~a -> ~a(~x)" ',name (fmtind) (reg rd :name) y))
        (t
          ,@direct
          ,@(if write '((setf (reg rd) y)))
          (fmt "~a ~a -> ~a(~x)" ',name (reg rs :name) (reg rd :name) y))))
     ((disadd ',name (if ind (fmtind :dis) (reg rs :name)) (reg rd :name)
              `(:const (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (rs . ,rs) (rd . ,rd))
              `(:macro load-indirect)))))

(defun asm-stub (addr)
  (mset-u8 addr #xc4)) ; x86 HLT

; instruction operations.
; variables available to each operation is the cpu-lambdas LET bindings (cpu-closures) and
; also the arguments passed down here through runop macro

(define-cpu-runop (stub-escape)
  (setf x (call-stub-escape (1- pc) cpu registers))
  (cond
    ((consp x)
      (setf pc (car x))
      (setf quit (cdr x)))
    (t
      (setf pc x)))
  (if (= pc 0) (setf quit 0)))

(define-cpu-runop2 (nop)
  ()
  ((setf mrm (advance-u8))
   (cond
     ((= #b01000100 mrm) (incf pc 2))
     ((= #b01000000 mrm) (incf pc 1))
     ((= #b00000000 mrm) (incf pc 0))
     ((= #b10000000 mrm) (incf pc 4))
     (t (incf pc 5))))
  ((fmt "nop"))
  ((fmt "nop") (disadd 'nop `(:const (mrm . ,mrm)))))

(define-cpu-runop (xchg/nop)
  (paranoid (zerop o3))
  (fmt "xchg/nop"))

; call into interrupt vector
(define-cpu-runop2 (call-int3)
  ()
  ()
  ((fmt "interrupt call 3") ; used by lisp condition handling cerror/error
   (L "int3, generate a sigtrap" :2)
   (push-cpu-state cpu (list :signal (list (get-signal-handler 5) 5 nil 0))))
  ((disadd 'call-int3 'int3)))

(define-cpu-runop2 (push)
  ()
  ((setf rs (logior o3 (or (if rex 8) 0))))
  ((let (reg-name reg-val)
      (ecase rs
        (#x0 (setf reg-name :rax) (setf reg-val (rget-u64 :rax)))
        (#x1 (setf reg-name :rcx) (setf reg-val (rget-u64 :rcx)))
        (#x2 (setf reg-name :rdx) (setf reg-val (rget-u64 :rdx)))
        (#x3 (setf reg-name :rbx) (setf reg-val (rget-u64 :rbx)))
        (#x4 (setf reg-name :rsp) (setf reg-val (rget-u64 :rsp)))
        (#x5 (setf reg-name :rbp) (setf reg-val (rget-u64 :rbp)))
        (#x6 (setf reg-name :rsi) (setf reg-val (rget-u64 :rsi)))
        (#x7 (setf reg-name :rdi) (setf reg-val (rget-u64 :rdi)))
        (#x8 (setf reg-name :r8)  (setf reg-val (rget-u64 :r8)))
        (#x9 (setf reg-name :r9)  (setf reg-val (rget-u64 :r9)))
        (#xa (setf reg-name :r10) (setf reg-val (rget-u64 :r10)))
        (#xb (setf reg-name :r11) (setf reg-val (rget-u64 :r11)))
        (#xc (setf reg-name :r12) (setf reg-val (rget-u64 :r12)))
        (#xd (setf reg-name :r13) (setf reg-val (rget-u64 :r13)))
        (#xe (setf reg-name :r14) (setf reg-val (rget-u64 :r14)))
        (#xf (setf reg-name :r15) (setf reg-val (rget-u64 :r15))))
      (assert (and reg-name reg-val))
      (decf (reg :rsp) 8)
      (fmt "~x push ~a(~x) to stack ~x" op reg-name reg-val (reg :rsp))
      (mset-u64 (reg :rsp) reg-val)
      (fmt " new rbp/rsp: ~x ~x" (reg :rbp) (reg :rsp))))
  ((fmt "push") (disadd 'push `(:const (rs . ,rs)))))

(define-cpu-runop (pushi)
  (decf (reg :rsp) 8)
  (setf imm (advance-u8))
  (mset-u64 (reg :rsp) imm)
  (fmt " push.1 ~x" imm))

(define-cpu-runop (pushiw)
  (decf (reg :rsp) 8)
  (setf imm (advance-u32))
  (mset-u64 (reg :rsp) imm)
  (fmt " push.4 ~x" imm))

(define-cpu-runop2 (pop)
  ()
  ((setf rs (logior o3 (or (if rex 8) 0))))
  ((let (reg-name reg-val)
      (ecase rs
        (#x0 (setf reg-name :rax))
        (#x1 (setf reg-name :rcx))
        (#x2 (setf reg-name :rdx))
        (#x3 (setf reg-name :rbx))
        (#x4 (setf reg-name :rsp))
        (#x5 (setf reg-name :rbp))
        (#x6 (setf reg-name :rsi))
        (#x7 (setf reg-name :rdi))
        (#x8 (setf reg-name :r8))
        (#x9 (setf reg-name :r9))
        (#xa (setf reg-name :r10))
        (#xb (setf reg-name :r11))
        (#xc (setf reg-name :r12))
        (#xd (setf reg-name :r13))
        (#xe (setf reg-name :r14))
        (#xf (setf reg-name :r15)))
      (setf reg-val (mget-u64 (reg :rsp)))
      (fmt "~x pop ~a(new value ~x) from stack ~x" op reg-name reg-val (reg :rsp))
      (incf (reg :rsp) 8)
      (setf (reg rs) reg-val)
      (fmt " new rbp/rsp: ~x ~x" (reg :rbp) (reg :rsp))))
  ((fmt "pop") (disadd 'pop (reg rs :name)
                       `(:const (rs . ,rs)))))

(define-cpu-runop2 (pop-mrm)
  ()
  ((load-mrm)
   (decode-mrm))
  ((load-address)
   (setf z (mget-u64 (reg :rsp))) ; pop into z
   (incf (reg :rsp) 8)
   (cond
     (ind
       (fmt " pop ~a ~x" (fmtind) z)
       (mset-u64 x z))
     (t
       (fmt " pop ~a=~x" (fmt-reg registers rs) z)
       (rset-u64 rs z))))
  ((fmt "pop")
     (disadd 'pop-mrm
             (if ind
               (fmtind :dis)
               (reg rs :name))
             `(:const (ind . ,ind) (imm . ,imm) (mul . ,mul) (bas . ,bas) (rs . ,rs))
             '(:macro load-address))))

(defmacro bitset (num) `(not (zerop ,num)))
(defmacro bitclr (num) `(zerop ,num))

(defmacro jno () `(zerop (get-flags-o)))
(defmacro jl ()  `(or (and (bitclr (get-flags-s)) (bitset (get-flags-o)))
                      (and (bitset (get-flags-s)) (bitclr (get-flags-o)))))
(defmacro jb ()  `(not (zerop (get-flags-c))))
(defmacro jae () `(zerop (get-flags-c)))
(defmacro jz ()  `(not (zerop (get-flags-z))))
(defmacro jnz () `(zerop (get-flags-z)))
(defmacro jbe () `(or (not (zerop (get-flags-c)))
                      (not (zerop (get-flags-z)))))
(defmacro ja ()  `(and (zerop (get-flags-c))
                       (zerop (get-flags-z))))
(defmacro jp ()  `(not (zerop (get-flags-p))))
(defmacro js ()  `(not (zerop (get-flags-s))))
(defmacro jns () `(zerop (get-flags-s)))
(defmacro jge () `(or (and (bitclr (get-flags-s)) (bitclr (get-flags-o)))
                      (and (bitset (get-flags-s)) (bitset (get-flags-o)))))
(defmacro jle () `(or (not (zerop (get-flags-z)))
                      (and (bitclr (get-flags-s)) (bitset (get-flags-o)))
                      (and (bitset (get-flags-s)) (bitclr (get-flags-o)))))
(defmacro jg ()  `(and (zerop (get-flags-z))
                       (or (and (bitclr (get-flags-s)) (bitclr (get-flags-o)))
                           (and (bitset (get-flags-s)) (bitset (get-flags-o))))))
(defmacro jeans () `(or (jae) (jns))) ;  jump if equal above or not signed

(defun flag-number-to-name (number)
  (ecase number
    (2 :b)
    (3 :ae)
    (4 :z)
    (5 :nz)
    (6 :be)
    (7 :a)
    (9 :jns)
    (#xa :p)
    (#xc :jl)
    (#xd :jge)
    (#xf :jg)))

(define-cpu-runop2 (cmov)
  ()
  ((load-mrm)
   (decode-mrm))
  ((if ind (load-indirect))
   (unless ind (setf y (reg rs)))
   (fmt "cmov~a ~a -> ~a" (flag-number-to-name o3) (if ind (fmtind) (reg rs :name)) (reg rd :name))
   (setf z (ecase o3
             (2 (if (jb) y))
             (3 (if (jae) y))
             (4 (if (jz) y))
             (5 (if (jnz) y))
             (6 (if (jbe) y))
             (7 (if (ja) y))))
   (when z
     (setf (reg rd) z)
     (fmt " moved =~x " z)))
  ((disadd 'cmov (flag-number-to-name o3) (if ind (fmtind :dis) (reg rs :name)) (reg rd :name)
           `(:const (ind . ,ind) (rs . ,rs) (rd . ,rd) (o3 . ,o3))
           '(:macro load-indirect))))

(define-cpu-runop2 (cmov2)
  ()
  ((load-mrm)
   (decode-mrm)
   (setf o3 (logior #b1000 o3)))
  ((if ind
     (load-indirect)
     (setf y (reg rs)))
   (fmt "cmov~a ~a -> ~a" (flag-number-to-name o3) (if ind (fmtind) (reg rs :name)) (reg rd :name))
   (setf z (ecase o3
             (9   (if (jns) y))
             (#xa (if (jp) y))
             (#xc (if (jl) y))
             (#xd (if (jge) y))
             (#xf (if (jg) y))))
   (when z
     (setf (reg rd) z)
     (fmt " moved =~x " z)))
  ((disadd 'cmov2 (flag-number-to-name o3) (if ind (fmtind :dis) (reg rs :name)) (reg rd :name)
           `(:const (o3 . ,o3) (rs . ,rs) (rd . ,rd) (ind . ,ind))
           '(:macro load-indirect))))

(define-cpu-runop (movb)
  (setf imms (advance-s8))
  (setf (reg o3) (dpb imms (byte 8 0) (reg o3)))
  (fmt "mov.1 ~x -> ~a" imms (reg o3 :name)))

; FIX: merge mov and movii runop
(define-cpu-runop2 (movii)
  (dir)
  ((setf iw (if (= (logand o3 1) 1) 4 1))
   (setf dir (logand o3 2))
   (load-mrm)
   (decode-mrm)
   (if prefix-66 (setf iw 2)) ; FIX: also prefix-67 can make 16bit
   (setf ow iw)
   (setf imms (ecase iw
                (1 (advance-u8))
                (2 (advance-u16))
                (4 (advance-u32))))
   (when rexw
     (setf ow 8)
     (setf imms (sign-extend imms iw))))
  ((let (v1)
    (cond
      (ind
        (cond
          ((zerop dir)
            (if rs
              (setf v1 (if (= rs 31) pc (reg rs)))
              (setf v1 0))
            (if mul (setf v1 (* v1 mul)))
            (if bas (incf v1 (reg bas)))
            (if imm (incf v1 imm))
            ;(setf v1 (mget-u64 v1))
            (setf v1 (case ow
                       (1 (mget-u8  v1))
                       (2 (mget-u16 v1))
                       (4 (mget-u32 v1))
                       (t (mget-u64 v1))))
            (fmt "mov.~a [~a+~a~a~a~a~x=~x] -> ~a(~x)" ow
                 (if bas (register-name bas) "")
                 (if rs (register-name rs) "")
                 (if mul "*" "")
                 (if mul mul "")
                 (if imm "+" "")
                 (if imm imm "")
                 v1 (if rd (register-name rd)) rd)
            (error "nothing is moved!"))
          (t
            (if rs
              (setf v1 (if (= rs 31) pc (reg rs)))
              (setf v1 0))
            (if mul (setf v1 (* v1 mul)))
            (if bas (incf v1 (reg bas)))
            (if imm (incf v1 imm))
            ; fix: use dpb with proper size
            (case ow
              (1 (mset-u8  v1 imms))
              (2 (mset-u16 v1 imms))
              (4 (mset-u32 v1 imms))
              (t (mset-u64 v1 imms)))
            (fmt "mov.~a ~x -> [~a+~a~a~x~a~x=~x]" ow
                 imms (if bas (register-name bas) "")
                 (if rs (register-name rs) "")
                 (if mul "*" "")
                 (if mul mul "")
                 (if imm "+" "")
                 (if imm imm "") v1))))
      (t
        (cond
          ((zerop dir)
            (rset-u64 rd imms)
            (fmt "mov.~a ~x -> ~a(~x)" ow imms (register-name rd) (reg rd)))
          (t
            (setf v1 (reg rd))
            (rset-u64 rs imms)
            (fmt "mov.~a ~x -> ~a(~x)" ow imms (register-name rs) (reg rs))))))))
  ((fmt "movii") (disadd 'movii
                         `(:const (ind . ,ind) (dir . ,dir) (iw . ,iw) (ow . ,ow) (mul . ,mul) (bas . ,bas) (imm . ,imm)
                                  (rs . ,rs) (rd . ,rd) (imms . ,imms)))))

(define-cpu-runop2 (mov)
  ((wsz (if (= (logand o3 1) 1) 4 1)))
  ((load-mrm)
   (decode-mrm)
   (setf dir (logand o3 2))
   (if rexw (setf wsz 8))
   (if prefix-66 (setf wsz 2))) ; FIX: also prefix-67 can make 16bit
  ((let (v1 v2)
    (if ind (load-address))
    (cond
      (ind
        (cond
          ((zerop dir)
            (setf y (reg rd))
            (case wsz
              (1 (mset-u8 x (logand y #xff)))
              (2 (mset-u16 x (logand y #xffff)))
              (4 (mset-u32 x (logand y #xffffffff)))
              (t (mset-u64 x y)))
            (fmt "mov.~a ~a(~x) -> ~a" wsz
                 (reg rd :name) y
                 (fmtind)))
          (t
            (setf y (reg rd))
            (setf y (case wsz
                      (1 (setf (reg rd) (dpb (mget-u8  x) (byte 8  0) y)))
                      (2 (setf (reg rd) (dpb (mget-u16 x) (byte 16 0) y)))
                      (4 (setf (reg rd)      (mget-u32 x)))
                      (t (setf (reg rd)      (mget-u64 x)))))
            (fmt "mov.~a ~a -> ~a" wsz (fmtind) (register-name rd)))))
      (t
        (cond
          ((zerop dir)
            (setf v1 (reg rd))
            (setf z rs)
            (fmt "mov.~a ~a(~x) -> ~a" wsz (register-name rd) v1 (register-name rs)))
          (t
            (setf v1 (reg rs))
            (setf z rd)
            (fmt "mov.~a ~x(~x) -> ~a" wsz (register-name rs) v1 (register-name rd))))
        (case wsz
          (1 (rset-u64 z (logand v1 #xff)))
          (2 (rset-u64 z (logand v1 #xffff)))
          (4 (rset-u64 z (logand v1 (mask 32))))
          (t (rset-u64 z v1)))))))
  ((fmt "mov") (disadd 'mov (if ind
                              (if (zerop dir)
                                (list (reg rd :name) (fmtind :dis))
                                (list (fmtind :dis) (reg rd :name)))
                              (if (zerop dir)
                                (list (reg rd :name) (reg rs :name))
                                (list (reg rs :name) (reg rd :name))))
                            `(:const (o3  . ,o3)  (rex . ,rex) (rexw . ,rexw) (rd . ,rd) (rs . ,rs)
                                     (dir . ,dir) (ind . ,ind) (prefix-66 . ,prefix-66) (mul . ,mul) (bas . ,bas) (imm . ,imm)
                                     (wsz . ,wsz))
                            '(:macro load-address))))

(define-cpu-runop2 (mov8) ; 8bit move
  ()
  ((load-mrm)
   (decode-mrm))
  ((let ((dir (logand o3 2))
        (rex-r (if (and rex (/= (logand rex 4) 0)) 8 0))
        (rex-b (if (and rex (/= (logand rex 1) 0)) 8 0))
        ereg erm
        v1 v2)
    (setf ereg (logior rd rex-r))
    (setf erm (logior (or rs 0) rex-b))
    ;(setf imms 0)
    (cond
      (ind
        (cond
          ((zerop dir)
            (setf v1 (if rs (if (= rs 31) pc (reg rs)) 0))
            (if mul (setf v1 (* v1 mul)))
            (if bas (incf v1 (reg bas)))
            (if imm (incf v1 imm))
            (setf v2 (reg rd))
            (mset-u8 v1 (logand v2 #xff))
            (fmt "mov.1 ~a(~x) -> [~a+~a~a~a+~x=~x]"
                 (reg rd :name) v2 (if bas (register-name bas))
                 (if rs (register-name rs) "")
                 (if mul "*" "")
                 (if mul mul "")
                 imm v1))
          (t
            (setf v1 (if rs (if (= rs 31) pc (reg rs)) 0))
            (if mul (setf v1 (* v1 mul)))
            (if bas (incf v1 (reg bas)))
            (if imm (incf v1 imm))
            (setf v1 (logand (mask 64) v1))
            (setf x (reg rd))
            (setf v2 (mget-u8  v1))
            (setf (reg rd) (dpb v2 (byte 8 0) x))
            (fmt "mov.1 [~a+~a~a~a+~x=~x]=~x -> ~a"
                 (if bas (register-name bas) "")
                 (if rs (register-name rs) "")
                 (if mul "*" "")
                 (if mul mul "")
                 imm v1 v2 (if rd (register-name rd))))))
      (t
        (cond
          ((zerop dir)
            (setf v1 (reg rd))
            (setf x rs)
            (fmt "mov.1 ~a(~x) -> ~a" (register-name rd) v1 (register-name rs)))
          (t
            (setf v1 (reg rs))
            (setf x rd)
            (fmt "mov.1 ~x(~x) -> ~a" (register-name rs) v1 (register-name rd))))
        (setf y (reg x))
        (setf (reg x) (dpb v1 (byte 8 0) y))))))
  ((fmt "mov8") (disadd 'mov8)))

(define-cpu-runop2 (movrimm4)
  ((rex-q (if (and rex (/= (logand rex 8) 0)) 8 0))
   (rex-b (if (and rex (/= (logand rex 1) 0)) 8 0)))
  ((setf imms (if (zerop rex-q) (advance-u32) (advance-u64)))
   (setf rs (logior rex-b o3)))
  ((setf (reg rs) imms)
   (fmt "mov ~x -> ~a" imms (fmtreg rs)))
  ((fmt "movrimm") (disadd 'movrimm4 imms (reg rs :name)
                           `(:const (o3 . ,o3) (imms . ,imms) (rs . ,rs))
                           )))

(define-cpu-runop2 (lea)
  ()
  ((load-mrm)
   (decode-mrm))
  ((load-address)
   (setf (reg rd) (logand (mask 64) x))
   (fmt "lea ~a -> ~a(~x)" (fmtind) (reg rd :name) (reg rd)))
  ((fmt "lea ~x" rs) (disadd 'lea (fmtind :dis) (reg rd :name)
                             `(:const (mul . ,mul) (ind . ,ind) (bas . ,bas) (imm . ,imm) (rs . ,rs) (rd . ,rd))
                             '(:macro load-address))))

(define-cpu-runop2 (movzx)
  ()
  ((load-mrm)
   (decode-mrm)
   (setf ow (if (= (logand o3 1) 1) 2 1))
   (paranoid (not (zerop (logand o3 2))))) ; direction
  ((cond
     (ind
       (load-indirect)
       (if (= 1 ow)
         (setf (reg rd) (logand y #xff))
         (setf (reg rd) (logand y #xffff)))
       (fmt "movzx.~a ~a -> ~a=~x " ow (fmtind) (register-name rd) (reg rd)))
     (t
       (paranoid (not imm))
       (setf x (reg rs))
       (if (= ow 1)
         (setf z (logand x #xff))
         (setf z (logand x #xffff)))
       (setf (reg rd) z)
       (fmt "movzx.~a ~a -> ~a" ow (fmt-reg registers rs) (fmt-reg registers rd)))))
  ((fmt "movzx") (disadd 'movzx
                         `(:const (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (ow . ,ow) (rs . ,rs) (rd . ,rd))
                         `(:macro load-indirect))))

(define-cpu-runop (movsx)
  (let ((wsz (if (= (logand o3 1) 1) 2 1)))
    (load-mrm)
    (decode-mrm)
    ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
    (paranoid (not (zerop (logand o3 2)))) ; direction
    (cond
      (ind
        (load-indirect)
        (setf (reg rd) (if (= 1 wsz)
                         (logand y #x00ff)
                         (logand y #xffff)))
        (fmt "movsx.~a ~a -> ~a=~x" wsz (fmtind) (register-name rd) (reg rd)))
      (t
        (paranoid (not imm))
        (setf x (reg rs))
        (if (= wsz 1)
          (setf z (logand x #xff))
          (setf z (logand x #xffff)))
        (setf (reg rd) z)
        (fmt "movsx.~a ~a -> ~a" wsz (fmt-reg registers rs) (fmt-reg registers rd))))))

(define-cpu-runop (movs)
  (setf q (if (or prefix-repz prefix-repnz) (logand (reg :rcx) #xff) 1))
  (fmt "movs.~a count=~x [rsi=~x] -> [rdi=~x]" (if rex 8 4) q
       (reg :rsi) (reg :rdi))
  (cond
    (rex
      (setf z (if (zerop (get-flags-d)) 8 -8))
      (loop repeat q do
        (setf x (reg :rsi))
        (setf y (mget-u64 x))
        (mset-u64 (reg :rdi) y)
        (incf (reg :rsi) z)
        (incf (reg :rdi) z))
      (if (or prefix-repz prefix-repnz) (setf (reg :rcx) 0)))
    (t
      (setf z (if (zerop (get-flags-d)) 4 -4))
      (loop repeat q do
        (setf x (reg :rsi))
        (setf y (mget-u32 x))
        (mset-u32 (reg :rdi) y)
        (incf (reg :rsi) z)
        (incf (reg :rdi) z))
      (if (or prefix-repz prefix-repnz) (setf (reg :rcx) 0))))
  (fmt " ... [rsi=~x] -> [rdi=~x]" (reg :rsi) (reg :rdi)))

(define-cpu-runop (lods)
  (paranoid (not (or prefix-repz prefix-repnz)))
  (setf z (if (zerop (get-flags-d)) 8 -8))
  (setf imm (mget-u64 (reg :rsi)))
  (setf (reg :rax) imm)
  (incf (reg :rsi) z)
  (fmt "lods [rsi] -> rax=~x" imm))

(define-cpu-runop (stos)
  (paranoid (not (zerop (logand o3 2))))
  (paranoid (not (zerop (logand o3 1))))
  (setf q (if (or prefix-repz prefix-repnz) (logand (reg :rcx) #xff) 1))
  (fmt "stos.~a count=~x rax=~x -> [rdi=~x]" (if rex 8 4) q
       (reg :rax) (reg :rdi))
  (setf y (reg :rax))
  (cond
    (rex
      (setf z (if (zerop (get-flags-d)) 8 -8))
      (loop repeat q do
        (mset-u64 (reg :rdi) y)
        (incf (reg :rdi) z))
      (if (or prefix-repz prefix-repnz) (setf (reg :rcx) 0)))
    (t
      (setf z (if (zerop (get-flags-d)) 4 -4))
      (loop repeat q do
        (mset-u32 (reg :rdi) y)
        (incf (reg :rdi) z))
      (if (or prefix-repz prefix-repnz) (setf (reg :rcx) 0))))
  (fmt " ... [rdi=~x]" (reg :rdi)))

(define-cpu-runop (cmpsb)
  (setf q (if (or prefix-repz prefix-repnz) (logand (reg :rcx) #xff) 1))
  (fmt "cmps.1 count=~x [rsi=~x] -> [rdi=~x]" q
       (reg :rsi) (reg :rdi))
  (setf z (if (zerop (get-flags-d)) 1 -1))
  (loop repeat q do
    (setf q (- (mget-u8 (reg :rdi)) (mget-u8 (reg :rsi))))
    (if (and prefix-repz (/= q 0)) (return))
    (if (and prefix-repnz (= q 0)) (return))
    (incf (reg :rsi) z)
    (incf (reg :rdi) z))
  (set-flags2)
  ;(if (or prefix-repz prefix-repnz) (incf (reg :rcx) q))
  (fmt " ... [rsi=~x] -> [rdi=~x]" (reg :rsi) (reg :rdi)))

(define-cpu-runop2 (movslq)
  ()
  ((load-mrm)
   (decode-mrm)
   (paranoid (not prefix-66)) ; if supported, deposit 16bit into destination (not touching upper 48bits)
   (paranoid (not (zerop (logand o3 1)))) ; not valid in 64bitmode
   (paranoid (not (zerop (logand o3 2)))) ; not valid in 64bitmode
   (setf ow (if rexw 8 4)))
  ((if ind (load-address))
   (cond
     (ind
       (setf y (mget-u32 x))
       (if (= ow 8)
         (if (not (zerop (logand y #x80000000)))
           (setf y (logior #xffffffff00000000 y))))
       (setf (reg rd) y)
       (fmt "movslq ~a -> ~a" (fmtind) (fmtreg rd)))
     (t
       (setf y (rget-u32 rs))
       (if (= ow 8)
         (if (not (zerop (logand y #x80000000)))
           (setf y (logior #xffffffff00000000 y))))
       (setf (reg rd) y)
       (fmt "movslq ~x(~x) -> ~a" (register-name rs) y (register-name rd)))))
  ((fmt "movslq") (disadd 'movslq
                          (if ind (list (fmtind :dis) (reg rd :name))
                                  (list (reg rs :name) (reg rd :name)))
                          `(:const (ow . ,ow) (ind . ,ind) (rs . ,rs) (rd . ,rd))
                          '(:macro load-address))))


; FIX: * what rex bit to use? * sign-bit not used
; arimix should be named bitwise-i-e
; opcodes 80 i8->e8 ,81 iv->ev, 83 i8s->ev
(define-cpu-runop2 (arimix)
  ()
  ((load-mrm)
   (decode-mrm)
   (ecase o3
     (0 (setf iw 1) (setf ow 1))
     (1 (setf iw 4) (setf ow 4))
     (3 (setf iw 1) (setf ow 4)))
   (if rexw (setf ow 8))
   (when prefix-66 (setf ow 2) (if (/= iw 1) (setf iw 2)))
   (setf imms (case iw
                (1  (advance-s8))
                (2  (advance-s16))
                (t  (advance-s32)))))
  ((if ind (load-address))
   (let (v v1 imms2)
    ;(fmt "{~x:~a->~a,$~x,*~a,~x}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind pc)
    ;(fmt "pc=~x, iw=~x " pc iw)
    (cond
      (ind
        (setf y (case ow (1 (mget-u8 x))
                         (2 (mget-u16 x))
                         (4 (mget-u32 x))
                         (t (mget-u64 x)))))
      (t
        (setf y (reg rs))
        (setf y (case ow (1 (logand #xff y))
                         (2 (logand #xffff y))
                         (4 (logand (mask 32) y))
                         (t y)))))
    (case mrm-reg
      (0 ; add
        (setf v1 (+ y imms))
        (cond
          (ind
            ; FIX: dpb v1 on y width bits, then store y
            (case ow (1 (mset-u8  x (logand #xff v1)))
                     (2 (mset-u16 x (logand #xffff v1)))
                     (4 (mset-u32 x (logand (mask 32) v1)))
                     (t (mset-u64 x (logand (mask 64) v1))))
            (fmt "add.~a ~x -> ~a (arimix)" ow imms (fmtind)))
          (t
            (case ow (1 (setf v1 (setf (reg rs) (logand #xff v1))))
                     (2 (setf v1 (setf (reg rs) (logand #xffff v1))))
                     (4 (setf v1 (setf (reg rs) (logand (mask 32) v1))))
                     (t (setf v1 (setf (reg rs) (logand (mask 64) v1)))))
            (fmt "add.~a ~x -> ~a (arimix)" ow imms (fmtreg rs)))))
      (1 ; or
        (cond
          (ind
            (setf y (logior y imms))
            (case ow (1 (mset-u8  x (logand #xff y)))
                     (2 (mset-u16 x (logand #xffff y)))
                     (t (mset-u64 x y)))
            ;(cond ((= zs 1)
            ;       (setf y (logand y #xff))
            ;       (mset-u8 x y))
            ;     (t (mset-u64 x y)))
            (fmt "or.~a ~a -> ~a" ow (fmtreg rd) (fmtind))
            (setf v1 y))
          (t
            ;(setf imms (logand imms (mask 64)))
            (setf v1 (logior y imms))
            (setf (reg rs) v1)
            (fmt "or.~a ~x -> ~a" ow imms (fmtreg rs)))))
      (2 ; adc
        (setf v1 (+ y imms (get-flags-c)))
        (cond
          (ind
            ; FIX: dpb v1 on y width bits, then store y
            (case ow (1 (mset-u8  x (logand #xff v1)))
                     (2 (mset-u16 x (logand #xffff v1)))
                     (4 (mset-u32 x (logand (mask 32) v1)))
                     (t (mset-u64 x (logand (mask 64) v1))))
            (fmt "adc.~a ~x -> ~a (arimix)" ow imms (fmtind)))
          (t
            (case ow (1 (setf v1 (setf (reg rs) (logand #xff v1))))
                     (2 (setf v1 (setf (reg rs) (logand #xffff v1))))
                     (4 (setf v1 (setf (reg rs) (logand (mask 32) v1))))
                     (t (setf v1 (setf (reg rs) (logand (mask 64) v1)))))
            (fmt "adc.~a ~x -> ~a (arimix)" ow imms (fmtreg rs)))))
      (3 ; sbb -- subtract with borrow
        (fmt "sbb.~a $~x -> ~a (~x," ow imms (register-name rs) (reg rs))
        (paranoid (not ind))
        (setf v1 (decf (reg rs) imms))
        (if (not (zerop (get-flags-c)))
          (decf (reg rs)))
        (fmt "~x)" (reg rs)))
      (4 ; and
        ;(if (= zs 1)
        ;  (if (< imms 0) (setf imms (+ imms #x100))) ; invert the sign operation
        ;  (if (< imms 0) (setf imms (+ imms #x100000000)))) ; invert the sign operation
        (cond
          (ind
            ;(load-address)
            (setf y (logand y imms))
            ;(if (= ow 1)
            ;  (mset-u8 x (logand #xff y))
            ;  (mset-u64 x y))
            (case ow (1 (mset-u8  x (logand #xff y)))
                     (2 (mset-u16 x (logand #xffff y)))
                     (4 (mset-u32 x (logand (mask 32) y)))
                     (t (mset-u64 x y)))
            (fmt "and.~a ~a -> ~a" ow imms (fmtind))
            (setf v1 y))
          (t
            (setf imms2 (logand imms (mask 64)))
            (setf v1 (logand y imms2))
            (setf (reg rs) v1)
            (fmt "and.~a ~x -> ~a" ow imms2 (fmtreg rs)))))
      (5 ; sub
        (setf v1 (- y imms))
        (cond
          (ind
            (paranoid (/= ow 1))
            ;(load-address)
            ;(setf y (mget-u64 x))
            (case ow (1 (mset-u8  x (logand #xff v1)))
                     (2 (mset-u16 x (logand #xffff v1)))
                     (4 (mset-u32 x (logand (mask 32) v1)))
                     (t (mset-u64 x v1)))
            (fmt "sub.~a ~x -> ~a" ow imms (fmtind)))
          (t
            (case ow (1 (setf v1 (setf (reg rs) (logand #xff v1))))
                     (2 (setf v1 (setf (reg rs) (logand #xffff v1))))
                     (4 (setf v1 (setf (reg rs) (logand (mask 32) v1))))
                     (t (setf v1 (setf (reg rs) v1))))
            (fmt "sub.~a ~x -> ~a" ow imms (fmtreg rs)))))
      (6 ; xor
        (cond
          (ind
            (setf imms2 (logand imms (mask 64)))
            (setf v1 (setf y (logxor y imms2)))
            (case ow
              (1 (mset-u8 x (logand #xff y)))
              (2 (mset-u16 x (logand #xffff y)))
              (4 (mset-u32 x (logand (mask 32) y)))
              (t (mset-u64 x y)))
            (fmt "xor.~a ~x -> ~a" ow imms2 (fmtind)))
          (t
            (setf imms2 (logand imms (mask 64)))
            (setf x (reg rs))
            (setf v1 (logxor x imms2)) ; FIX: check the bits depending on ow to recognize flags
            (setf x (dpb v1 (byte (case ow (1 8)
                                           (2 16)
                                           (4 32)
                                           (t 64)) 0) x))
            (setf (reg rs) x)
            (fmt "xor.~a ~x -> ~a" ow imms2 (fmtreg rs)))))
      (7 ; cmp
        (setf imms2 (case ow (1 (logand #xff imms))
                             (2 (logand #xffff imms))
                             (4 (logand (mask 32) imms))
                             (t (logand (mask 64) imms))))
        (setf v y)
        (cond
          (ind
            (fmt "cmp.~a ~a : ~x" ow (fmtind) imms2))
          (t
            (fmt "cmp.~a ~a : ~x" ow (fmtreg rs) imms2)))
        (setf v1 (- v imms2)))
      (t (error "arimix doesnt recognize subop ~x~%" mrm-reg)))
    (setf y v1)
    (case ow
      (1 (setf q (if (or (> y #xff) (< y 0)) t))
         (setf y (logand y #xff))
         (setf z (ash y -7)))
      (2 (setf q (if (or (> y #xffff) (< y 0)) t))
         (setf y (logand y #xffff))
         (setf z (ash y -15)))
      (4 (setf q (if (or (> y (mask 32)) (< y 0)) t))
         (setf y (logand y (mask 32)))
         (setf z (ash y -31)))
      (t (setf q (if (or (> y (mask 64)) (< y 0)) t))
         (setf y (logand y (mask 64)))
         (setf z (ash y -63))))
    (set-flags-s z)
    (set-flags3 ow)
    (set-flags-c (if q 1 0))
    (fmt " fr:~a " (decode-flags))))
  ((fmt "arimix") (disadd 'arimix
                          (ecase mrm-reg (0 'add)
                                         (1 'or)
                                         (2 'adc)
                                         (3 'sbb)
                                         (4 'and)
                                         (5 'sub)
                                         (6 'xor)
                                         (7 'cmp))
                          (if ind
                            (fmtind :dis)
                            (reg rs :name))
                          imms
                          `(:const (mrm-reg . ,mrm-reg) (ow . ,ow) (iw . ,iw) (rexw . ,rexw) (o3 . ,o3)
                                   (ind . ,ind) (imms . ,imms) (rs . ,rs) (prefix-66 . ,prefix-66)
                                   (mul . ,mul) (bas . ,bas) (imm . ,imm))
                          '(:macro load-address))))


(define-cpu-runop (arimix2)
  (load-mrm)
  (let (;(subop (logand #b00111000 mrm))
        (wsz (logior (ash (logand o3 1) 2)
                     (logand 1 (lognot (logand o3 1)))))
        (reg   (logior (logand #b00000111 mrm) (if (and rex (/= (logand rex 1) 0)) 8 0)))
        ;(immsz (if (= (logand 1 o3) 1) t))
        v1 v2)
    (decode-mrm)
    (setf imms (if (= wsz 4) (advance-u32) (advance-u8)))
    (ecase mrm-mod
      (0
        (fmt "add.~a ~x ~a" wsz imm (register-name reg)))
      (2
        (cond
          (ind
            (setf v1 (+ (reg rd) imm))
            (setf v2 (mget-u64 v1))
            (incf v2 (+ imms))
            (mset-u64 v1 v2)
            (fmt "addc.~a ~x [~a+~x=~x]=~x" wsz imms (register-name rd) imm v1 v2))
          (t
            (fmt "addc.~a ~x ~a / ~a -> ~a , ~x,~a" wsz imm (register-name reg)
             (register-name rs) (register-name rd) imm ind
             )
            (error "fix: not implemented"))))
      (7
        (fmt "cmp.~a ~a ~a" wsz imm (register-name reg))
        (error "fix: not implemented")))))

(defmacro logmask-rexw (&body body)
  `(logand (if rexw (mask 64) (mask 32)) ,@body))

(define-cpu-runop2 (arimix3)
  ()
  ((setf iw (logior (ash (logand o3 1) 2)
                    (logand 1 (lognot (logand o3 1)))))
   (load-mrm)
   (decode-mrm)
   (case mrm-reg
      (0 ; test
        (setf imms (if (= iw 4) (advance-s32) (advance-s8))))
      (2 ; one-complement negate
        (paranoid (not ind)))
      (3 ; two-complement negate
        (paranoid (not ind)))
      (5 ; IMUL rdx rax mrm
        (setf ow 8)))) ; FIX: imul has other widths
  ((ecase mrm-reg
      (0 ; test
        (cond
          (ind
            (load-indirect)
            (setf q (logand imms y))
            (fmt "test.~a ~a and ~a" iw (fmtind) imms))
          (t
            (setf q (logand imms (reg rs)))
            (fmt "test.~a ~a and ~a" iw (register-name rs) imms)))
        (set-flags2))
      (2 ; one-complement negate
        (setf (reg rs) (logand (if rexw (mask 64) (mask 32)) (lognot (reg rs))))

        (fmt "not: ~a=~x" (register-name mrm-r/m) (reg rs)))
      (3 ; two-complement negate
        (setf x (reg rs))
        (setf y (logand (lognot x) (mask 64)))
        (setf z (logbitp 63 y)) ; check for overflow
        (incf y)
        (set-flags-o (if (or (zerop x) (eq z (logbitp 63 y))) 0 1)) ; OK, how is overflow really checked?
        (setf y (logand (if rexw (mask 64) (mask 32)) y))
        (setf (reg rs) y) ; (logand (if rexw (mask 64) (mask 32)) y))
        (set-flags-c (if (zerop y) 0 1))
        (set-flags-z (if (zerop y) 1 0))
        (fmt "neg: ~a=~x" (reg rs :name) (reg rs)))
      (4 ; mul AL*MRM-> RDX
        (if ind (load-indirect))
        (cond
          (ind
            (setf z y)
            (fmt "mul.~a ~a*rax -> " iw (fmtind)))
          (t
            (setf z (reg rs))
            (fmt "mul.~a ~a(~x)*rax -> " iw (register-name rs) z)))
        (cond
          (rex
            (setf z (* z (logand (reg :rax) (mask 64))))
            (cond
              ((zerop (ash z -64))
                (set-flags-o 0) (set-flags-c 0))
              (t (set-flags-o 1) (set-flags-c 1)))
            (setf (reg :rax) (logand z (mask 64)))
            (setf (reg :rdx) (ash (logand z (ash (mask 64) 64)) -64)) ; get the 64 MSB
            (fmt "rdx:rax=~x:~x" (reg :rdx) (reg :rax)))
          ((= iw 4)
            (setf z (* z (logand (reg :rax) (mask 32))))
            (cond
              ((zerop (ash z -32))
                (set-flags-o 0) (set-flags-c 0))
              (t (set-flags-o 1) (set-flags-c 1)))
            (setf (reg :rax) (logand z (mask 32))) ; get the 32 LSB
            (setf (reg :rdx) (ash (logand z (ash (mask 32) 32)) -32)) ; get the 32 MSB
            (fmt "rdx:rax=~x:~x" (reg :rdx) (reg :rax)))
          (t
            (setf z (* z (logand (reg :rax) #xff)))
            (cond
              ((zerop (ash z -32))
                (set-flags-o 0) (set-flags-c 0))
              (t (set-flags-o 1) (set-flags-c 1)))
            (setf (reg :rax) (logand z (mask 32))) ; get the 32 LSB
            (fmt "rax=~x" (reg :rax)))))
      (5 ; IMUL rdx rax mrm
        (if ind (load-indirect2-sign)) ; FIX: there is other imms
        (unless ind
          (setf y (reg rs))
          (setf y (sign-extend64 y)))
        ;(setf z (logior (ash (logand (mask 32) (reg :rdx)) 32) (logand (mask 32) (reg :rax))))
        (setf x (sign-extend64 (reg :rax)))
        (fmt "imul rax(~x) by ~x" (reg :rax) y)
        (setf y (* x y))
        (cond
          ((and (>= y 0) (not (zerop (ash y -63)))) ; FIX: {overflow = check if ash -64 is nonzero} ?
            (set-flags-o 1) (set-flags-c 1))
          (t
            (set-flags-o 0) (set-flags-c 0)))
        (setf (reg :rax) (logand y (mask 64)))
        (setf (reg :rdx) (logand (ash y -64) (mask 64)))
        (fmt " = (rdx=~x, rax=~x)" (reg :rdx) (reg :rax)))
      (6 ; div
        (if ind (load-indirect))
        (unless ind
          (setf y (reg rs))
          (fmt "div rdx:rax -> ~a" (reg rs :name)))
        (cond
          (rexw
            (if (zerop (reg :rdx))
              (setf z (reg :rax))
              (setf z (logior (ash (reg :rdx) 64) (reg :rax)))))
          (t
            (if (zerop (reg :rdx))
              (setf z (logand (reg :rax) (mask 32)))
              (setf z (logior (logand (ash (reg :rdx) 32) (mask 32)) (logand (reg :rax) (mask 32)))))))
        (multiple-value-bind (quotient remainder) (truncate z y)
          (setf (reg :rax) quotient)
          (setf (reg :rdx) remainder))
        ;(setf (reg :rax) (truncate (/ (reg :rax) y)))
        )
      (7 ; idiv divide rdx:rax by mrm, put result in rax:rdx
        (if ind (load-indirect))
        (unless ind
          (setf y (reg rs)))
        (cond
          (rexw
            (if (zerop (reg :rdx))
              (setf z (reg :rax))
              (setf z (logior (ash (reg :rdx) 64) (reg :rax)))))
          (t
            (if (zerop (reg :rdx))
              (setf z (logand (reg :rax) (mask 32)))
              (setf z (logior (logand (ash (reg :rdx) 32) (mask 32)) (logand (reg :rax) (mask 32)))))))
        ;(setf z (logior (ash (logand (mask 32) (reg :rdx)) 32) (logand (mask 32) (reg :rax))))
        (fmt "idiv rdx:rax(~x,~x) by ~x" (reg :rdx) (reg :rax) y)
        (cond
          (rexw
            (if (logbitp 63 y) ; the denominator is negative, reverse two-complements
              (setf y (- y (ash 1 64)))))
          (t
            (if (logbitp 31 y) ; the denominator is negative, reverse two-complements
              (setf y (- y (ash 1 32))))))
        (cond
          (rexw
            (if (logbitp 127 z) ; the numerator is negative, reverse two-complements
              (setf z (- z (ash 1 128)))))
          (t
            (if (logbitp 63 z) ; the numerator is negative, reverse two-complements
              (setf z (- z (ash 1 64))))))
        (multiple-value-bind (quotient remainder) (truncate z y)
          (setf (reg :rax) quotient)
          (setf (reg :rdx) remainder))
        (fmt " = (rdx=~x, rax=~x)" (reg :rdx) (reg :rax)))))
  ((fmt "arimix3") (disadd 'arimix3
                           (ecase mrm-reg
                             (0 'test
                               (cond
                                 (ind (list 'test :iw iw (fmtind :dis) imms))
                                 (t   (list 'test :iw iw (reg rs :name) imms))))
                             (2 'neg1c)
                             (3 'neg2c)
                             (4 'mul-AL*MRM->RDX)
                             (5 'IMUL-rdx-rax)
                             (6 'div)
                             (7 'idiv))
                           `(:const (ind . ,ind) (mrm-reg . ,mrm-reg) (imms . ,imms) (rs . ,rs)
                                    (mul . ,mul) (bas . ,bas) (imm . ,imm) (rexw . ,rexw))
                           '(:macro load-indirect))))

(define-cpu-runop (imul)
  (load-mrm)
  (decode-mrm)
  (ecase o3
    (4
      (paranoid rexw) ; else we must restict count to lower five bits
      (setf imms (logand (advance-u8) #b11111)) ; if rexw #b111111
      (cond
        (ind
          (fmt "shrd ~x ~a:cl -> ~a" imms (reg rs :name) (fmtind))
          (error "not implemented"))
        (t
          (when (not (zerop imms))
            (setf x (reg rd))
            (setf y (reg rs))
            (setf z 0)
            (loop repeat imms do
              (setf z (logand y 1))
              (setf y (ash y -1))
              (setf y (logior y (ash (logand x 1) 63)))
              (setf x (ash x -1)))
            (setf (reg rs) y)
            (set-flags-c z))
          (fmt "shrd ~x ~a -> ~a" imms (reg rd :name) (reg rs :name)))))
    ; 5 => shrd but CL instead of imm8
    (6 ; sse stuff
      (ecase mrm-reg
        (2 (fmt "ldmxcsr"))
        (3
          (load-address)
          (mset-u32 x #x1f80) ; not sure what to set.
          (fmt "stmxcsr ~x" x))
        (6 (fmt "mfence"))))
    (7
      (fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
      (cond
        (ind
          (load-indirect)
          (setf z (logand (mask 64) (* y (reg rd))))
          (fmt "imul ~a * ~a -> ~a" (fmtind) (register-name rd) (register-name rd)))
        (t (setf z (logand (mask 64) (* (reg rs) (reg rd))))
           (fmt "imul ~a * ~a -> ~a" (register-name rs) (register-name rd) (register-name rd))))
      (cond
        ((zerop (ash z -64))
          (set-flags-o 0) (set-flags-c 0))
        (t
          (set-flags-o 1) (set-flags-c 1)))
      (setf (reg rd) z)
      (fmt "=~x" z))))

(define-cpu-runop2 (imul3) ; GR, mrm, Imm8
  ()
  ((setf iw (logand o3 2))
   (load-mrm)
   (decode-mrm)
   (if (zerop iw)
     (setf imms (advance-s32))
     (setf imms (advance-s8))))
  ((let ((s 0) n o h)
    ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
    (cond
      (ind
        (setf z (+ (if (= rs 31) pc (reg rs)) (or imm 0)))
        (setf x (mget-u32 z))
        (fmt "imul [~a+~x](=~x)*~x -> ~a" (register-name rs) imm x imms (register-name rd)))
      (t
        (setf x (reg rs))
        (fmt "imul ~a * ~x -> ~a" (fmt-reg registers rs) imms (register-name rd))))
    ; get result sign
    (setf s (logxor s (ash x -63)))
    (setf s (logxor s (ash imms -63)))
    (setf y (* x imms))
    (set-flags-o 0) (set-flags-c 0)
    (cond
      ((not (zerop s)) ; result should be negative, we have a overflow if 63 is clear or any bits over 63 have holes.
        (setf o (ash y -63)) ; when checking for holes, include bit 63 too.
        (loop for i from 63 downto 0 do
          (cond
            ((logbitp i o) (setf n t))
            (t
              (if n (setf h t)))))
        (when (or h (not n))
          (set-flags-o 1) (set-flags-c 1)))
      ((not (zerop (ash y -63))) ; result is positive, we have overflow if any bit above 62 is set.
        (set-flags-o 1) (set-flags-c 1))
      (t
        (set-flags-o 0) (set-flags-c 0)))
    (setf (reg rd) (logand (mask 64) y))
    (fmt "=~x" (reg rd))))
  ((fmt "imul3") (disadd 'imul3 `(:const (ind . ,ind) (iw . ,iw) (imms . ,imms) (imm . ,imm) (rs . ,rs) (rd . ,rd)))))

(define-cpu-runop (test-al)
  (let (v2)
    (setf v2 (logand (reg :rax) #xff))
    (setf imm (advance-s8))
    (setf q (logand v2 imm))
    (set-flags2)
    (fmt "test-al ~x ~x" v2 imm)))

(define-cpu-runop (test-rax)
  (let (v2)
    (setf v2 (reg :rax))
    (setf imm (advance-s32))
    (setf q (logand v2 imm))
    (set-flags2)
    (fmt "test-rax ~x ~x" v2 imm)))

(define-cpu-runop2 (callimm4)
  ()
  ((setf bi t)
   (setf branch t)
   (setf imms (advance-s32)))
  ((fmt "call.w $~x Rpc=~x" imms pc)
   (decf (reg :rsp) 8)
   (fmt " new-rsp: ~x" (reg :rsp))
   (mset-u64 (reg :rsp) pc)
   ;(fmt " read-back: ~x" (mget-u64 (reg :rsp)))
   (incf pc imms)
   (fmt " new pc: $~x, return stored at ~x rbp=~x" pc (reg :rsp) (reg :rbp)))
  ((fmt "callimm4") (disadd 'callimm4 'c
                            `(:const (imms . ,imms)))))

(define-cpu-runop2 (call*)
  ()
  ((setf bi t)
   (load-mrm)
   (decode-mrm)
   (setf imm (advance-s32)))
  ((setf branch t)
   (fmt "call *($~x+Rpc=~x)=(~x)" imm pc (mget-u64 (+ imm pc)))
   (setf imm (mget-u64 (+ imm pc)))
   (decf (reg :rsp) 8)
   (mset-u64 (reg :rsp) pc)
   ;(fmt " read-back: ~x" (mget-u64 (reg :rsp)))
   (setf pc imm)
   (fmt " new pc: $~x, return stored at ~x" pc (reg :rsp)))
  ((fmt "call*") (disadd 'call* 'c imm)))

(define-cpu-runop2 (testr)
  (reg0 reg1)
  ((advance-mrm)
   (setf reg0 (logior (ash (logand #b00111000 mrm) -3) (if (and rex (/= (logand rex 4) 0)) 8 0)))
   (setf reg1 (logior (logand #b00000111 mrm) (if (and rex (/= (logand rex 1) 0)) 8 0)))
   (setf ow (if rexw 8 4))
   (if (not (logbitp 0 o3)) (setf ow 1))
   (if prefix-66 (setf ow 2)))
  ((let (v sign)
     (fmt "test.~a ~a, ~a (~x AND ~x)" ow (register-name reg0) (register-name reg1) (reg reg0) (reg reg1))
     (setf y (logand (reg reg0) (reg reg1)))
     (setf sign (logand 1 (ash y (- 1 (* ow 8)))))
     (set-flags-s sign)
     (set-flags3 ow)
     (fmt " =~x new flags: ~a" v (decode-flags))))
  ((fmt "call*") (disadd 'testr `(:const (ow . ,ow) (reg0 . ,reg0) (reg1 . ,reg1)))))

(define-cpu-runop2 (jc1)
  ()
  ((setf bi t)
   (setf imm (advance-s8)))
  ((ecase o3
     (1 (fmt "jno.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jno) (incf pc imm) (setf branch t)))
     (2 (fmt "jb.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jb) (incf pc imm) (setf branch t)))
     (3 (fmt "jae.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jae) (incf pc imm) (setf branch t)))
     (4 (fmt "jz.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jz) (incf pc imm) (setf branch t)))
     (5 (fmt "jnz.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jnz) (incf pc imm) (setf branch t)))
     (6 (fmt "jbe.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (jbe) (incf pc imm) (setf branch t)))
     (7 (fmt "ja.b ~x f:~a" (+ pc imm) (decode-flags))
        (when (ja) (incf pc imm) (setf branch t)))))
  ((fmt "jc1") (disadd 'jc1 'jc
                       (ecase o3
                         (1 'no)
                         (2 'b)
                         (3 'ae)
                         (4 'z)
                         (5 'nz)
                         (6 'be)
                         (7 'a))
                       :pc imm 2
                       `(:const (o3 . ,o3) (imm . ,imm)))))

(define-cpu-runop2 (jc4)
  ()
  ((setf bi t)
   (setf imm (advance-s32)))
  ((ecase o3
    (1 (fmt "jno.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jno) (incpc imm) (setf branch t)))
    (2 (fmt "jb.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jb) (incpc imm) (setf branch t)))
    (3 (fmt "jae.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jae) (incpc imm) (setf branch t)))
    (4 (fmt "jz.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jz) (incpc imm) (setf branch t)))
    (5 (fmt "jnz.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jnz) (incpc imm) (setf branch t)))
    (6 (fmt "jbe.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jbe) (incpc imm) (setf branch t)))
    (7 (fmt "ja.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (ja) (incpc imm) (setf branch t)))))
  ((fmt "jc4") (disadd 'jc4 'jc (ecase o3
                                  (1 'no)
                                  (2 'b)
                                  (3 'ae)
                                  (4 'z)
                                  (5 'nz)
                                  (6 'be)
                                  (7 'a))
                                :pc imm 6
                                `(:const (o3 . ,o3) (imm . ,imm)))))

(define-cpu-runop2 (jx4) ()
  ((setf bi t)
   (setf imm (advance-s32)))
  ((ecase o3
    (0 (fmt "js.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (js) (incpc imm) (setf branch t)))
    (1 (fmt "jns.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jns) (incpc imm) (setf branch t)))
    (2 (fmt "jp.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jp) (incpc imm) (setf branch t)))
    (4 (fmt "jl.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jl) (incpc imm) (setf branch t)))
    (5 (fmt "jge.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jge) (incpc imm) (setf branch t)))
    (6 (fmt "jle.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jle) (incpc imm) (setf branch t)))
    (7 (fmt "jg.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jg) (incpc imm) (setf branch t)))))
  ((fmt "jx4") (disadd 'jx4 'jc (ecase o3
                                  (0 's)
                                  (1 'ns)
                                  (2 'p)
                                  (4 'l)
                                  (5 'ge)
                                  (6 'le)
                                  (7 'g)) :pc imm 6
                                `(:const (o3 . ,o3) (imm . ,imm)))))

(define-cpu-runop2 (jx1) ()
  ((setf bi t)
   (setf imm (advance-s8)))
  ((ecase o3
    (0 (fmt "js.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (js) (incpc imm) (setf branch t)))
    (1 (fmt "jns.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (zerop (get-flags-s)) (incpc imm) (setf branch t)))
    (2 (fmt "jp.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jp) (incpc imm) (setf branch t)))
    (4 (fmt "jl.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jl) (incpc imm) (setf branch t)))
    (5 (fmt "jge.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jge) (incpc imm) (setf branch t)))
    (6 (fmt "jle.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jle) (incpc imm) (setf branch t)))
    (7 (fmt "jg.w ~x f:~a" (+ pc imm) (decode-flags))
       (when (jg) (incpc imm) (setf branch t)))))
  ((fmt "jx1") (disadd 'jx1 'jc  (ecase o3
                                   (0 's)
                                   (1 'ns)
                                   (2 'p)
                                   (4 'l)
                                   (5 'ge)
                                   (6 'le)
                                   (7 'g)) :pc imm 3
                                `(:const (o3 . ,o3) (imm . ,imm)))))

(define-cpu-runop2 (jmp1)
  ()
  ((setf imm (advance-s8))
   (setf bi t))
  ((setf branch t)
   (incf pc imm)
   (fmt "jmp.b ~x" pc))
  ((fmt "jmp.b") (disadd 'jmp1 'j imm
                         `(:const (imm . ,imm)))))


(define-cpu-runop2 (jmp4)
  ()
  ((setf bi t)
   (setf imm (advance-s32)))
  ((setf branch t)
   (incf pc imm)
   (fmt "jmp.w ~x" pc))
  ((fmt "jmp.w") (disadd 'jmp4 'j imm
                         `(:const (imm . ,imm)))))

; misc
(define-cpu-runop2 (call/jmp/push)
  ()
  ((load-mrm)
   (decode-mrm)
   (if (or (= mrm-reg 2) (= mrm-reg 4)) (setf bi t)))
  ((ecase mrm-reg
      (0 ; inc
        (cond
          (ind
            (load-address)
            (setf y (logand (mask 64) (1+ (mget-u64 x))))
            (mset-u64 x y)
            (fmt "inc ~a" (fmtind)))
          (t
            (setf (reg rs) (logand (mask 64) (1+ (reg rs))))
            (fmt "inc ~a" (fmt-reg registers rs)))))
      (1 ; dec
        (cond
          (ind
            (error "indirect inc not implemented"))
          (t
            (setf y (logand (mask 64) (1- (reg rs))))
            (setf (reg rs) y)
            (set-flags-z (if (zerop y) 1 0))
            (cond
              (rex (set-flags-s (if (logbitp 63 y) 1 0)))
              (t   (set-flags-s (if (logbitp 31 y) 1 0))))
            (fmt "dec ~a" (fmt-reg registers rs)))))
      (2 ; call
        (setf branch t)
        (decf (reg :rsp) 8)
        (mset-u64 (reg :rsp) pc)
        (cond
          (ind
            (load-indirect)
            (fmt "call [~a*~a+~x=~x]=~x" (fmt-reg registers rs) mul imm x y)
            (setf pc y))
          (t
            (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
            (if imm (incf x imm))
            (fmt "call ~a+~x=~x" (fmt-reg registers rs) imm x)
            (setf pc x)))
        (fmt " new pc: $~x, return stored at ~x" pc (reg :rsp)))
      (4 ; jmp
        (setf branch t)
        (cond
          (ind
            (load-indirect)
            (fmt "jmp ~a" (fmtind))
            (setf pc y))
          (t
            (setf x (if (= rs 31) pc (reg rs)))
            (setf y (if imm (+ x imm) x))
            (fmt "jmp ~a(~x)+~x=~x" (register-name rs) x imm y)
            (setf pc y))))
      (6 ; push
        (cond
          (ind
            (load-indirect)
            (decf (reg :rsp) 8)
            (mset-u64 (reg :rsp) y)
            (fmt "push [~a+~a*~a+~x=~x]=~x" (fmt-reg registers bas) (fmt-reg registers rs) mul imm x y))
          (t
            (fmt "push ~a+~x=~x" (fmt-reg registers rs) imm v))))))
  ((fmt "call/jmp/push") (disadd 'call/jmp/push
                                 (ecase mrm-reg
                                   (0 'inc)
                                   (1 'dec)
                                   (2 'c)
                                   (4 'j)
                                   (6 'push))
                                 `(:const (mrm-reg . ,mrm-reg) (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (rs . ,rs))
                                 '(:macro load-address load-indirect))))

(define-cpu-runop (inc/dec)
  (load-mrm)
  (decode-mrm)
  (setf z (ecase mrm-reg
            (0 1) ; inc
            (1 -1))) ; dec
  (cond
    (ind
      (load-address)
      (setf z (logand #xff (+ z (mget-u8 x))))
      (mset-u8 x z)
      (fmt "~a.1 ~a" (ecase mrm-reg (0 "inc") (1 "dec")) (fmtind)))
    (t
      (setf z (logand #xff (+ z (reg rs))))
      (setf (reg rs) z)
      (fmt "~a.1 ~a" (ecase mrm-reg (0 "inc") (1 "dec")) (reg rs :name))))
  (set-flags-s (if (logbitp 7 z) 1 0)))


;00 Eb <- Gb     000
;01 Ew <- Gw     001
;02 Gb <- Eb     010
;03 Gw <- Ew     011
;04 AL <- Ib     100
;05 rAX <- Iw    101
; convention:
; all bodies must put result for flag-eval in y
; FIX: some operations is strict modular and wont need result width truncate
(defmacro type-1 ((name) &key operation bitwise indirect direct rax (flags t) (load t) (write t))
  `(define-cpu-runop2 (,name)
     ((axd (logand o3 4))
      (dir (logbitp 1 o3))
      )
     ((let ()
        (setf iw (1+ (* (logand o3 1) 3)))
        (setf ow iw)
        (if rexw (setf ow 8))
        (if prefix-66 (setf iw (setf ow 2)))
        (paranoid (if prefix ; FIX: also prefix-67 can make 16bit
                    (and prefix-66 (= (length prefix) 1))
                    t))
        (cond
          ((zerop axd)
            (load-mrm)
            (decode-mrm))
          (t
            (setf imms (cond
                         ((= iw 1) (advance-u8))
                         ((= iw 2) (advance-u16))
                         (t        (advance-s32))))))))
     ((let (carry sign rs2 rd2 u p)
       (cond
         ((zerop axd)
           (cond
             (ind
               ;(paranoid (not (zerop dir))) ; fix try to handle swapping (dir), meanwhile, be paranoid about it
               ,@(cond
                   (load ; load values into z and y (z is destination)
                     '((load-indirect2)
                       ;(fmt "LI:~x " y)
                       (setf z (case ow
                                 (1 (rget-u8  rd))
                                 (2 (rget-u16 rd))
                                 (4 (rget-u32 rd))
                                 (t (reg rd))))
                       (if (not dir) (psetf z y y z))))
                   (t '((load-address))))
               ; the body will do its thing using x and rd and leave the result in q
               ;(if (zerop dir) (fmt "[RY: ~x]" (mget-u64 x)))
               ;(fmt "{ ~x op ~x " z y)
               ,@(cond
                   (indirect indirect)
                   (operation `((setf q (,operation z y))))
                   (t (error "must specify either indirect body or operation")))
               ;,@(if operation
               ;    `((setf y (,operation z y)))
               ;     indirect)
               ;(fmt "= ~x } " y)
               ,(if write '(if (not dir)
                             (case ow
                               (1 (mset-u8  x (logand q #xff)))
                               (2 (mset-u16 x (logand q #xffff)))
                               (4 (mset-u32 x (logand q (mask 32))))
                               (t (mset-u64 x (logand q (mask 64)))))
                             (setf (reg rd) (logand q (mask 64)))))
               ;(if (not dir) (fmt "RB: ~x  " (mget-u64 x)))
               (if (not dir)
                 (fmt "~a.~a/~a ~a -> ~a" ',name iw ow (fmt-reg registers rd) (fmtind))
                 (fmt "~a.~a/~a ~a -> ~a" ',name iw ow (fmtind) (fmt-reg registers rd))))
             (t
               (setf rs2 rs) (setf rd2 rd)
               (if (not dir) (psetf rs2 rd2 rd2 rs2)) ; swap rs <-> rd
               (setf y (reg rs2)) (setf z (reg rd2))  ; when :load t ? (because NOT doesnt use y?)
               (case ow
                 (1 (setf y (logand y #xff))      (setf z (logand z #xff)))
                 (2 (setf y (logand y #xffff))    (setf z (logand z #xffff)))
                 (4 (setf y (logand y (mask 32))) (setf z (logand z (mask 32))))
                 (t (setf y (logand y (mask 64))) (setf z (logand z (mask 64)))))
               ,@(cond
                   (direct direct)
                   (operation `((setf q (,operation z y))))
                   (t (error "must specify either direct body or operation")))
               (setf p q) ; please fmt
               ,@(if write
                   '((setf u (reg rd2))
                     (setf p (case ow
                               (1 (dpb q (byte 8 0) u))  ;  al
                               (2 (dpb q (byte 16 0) u)) ; eax
                               ;(4 (dpb q (byte 32 0) u)) ; is maybe (logand y (mask 32))) enough (zero-extend) ?
                               (4 (logand q (mask 32)))
                               (t (logand q (mask 64)))));rax, make sure not overflow 64bits
                     ;(fmt "nrd=~x " q)
                     (setf (reg rd2) p)))
               (fmt "~a.r ~a(~x) -> ~a(~x) = ~x" ',name (reg rs2 :name) y (reg rd2 :name) z p)))) ;FIX: wrong print of src,dst
         (t
           (setf y imms)
           (paranoid (not dir)) ; only support rs OP rax -> rd
           (setf z (reg :rax))
           (case ow
             (1 (setf z (logand z #xff)))
             (2 (setf z (logand z #xffff)))
             (4 (setf z (logand z (mask 32)))))
           ;(fmt " z/y: ~x,~x " z y)
           ,@(cond
               (rax rax)
               (operation `((setf q (,operation z y))))
               (t (error "no rax body or operation specified")))
           ,@(if write
               '((setf u (reg :rax))
                 (setf (reg :rax) (case ow
                                    (1 (dpb q (byte 8 0) u))
                                    (2 (dpb q (byte 16 0) u))
                                    (4 (logand q (mask 32)))
                                    (t (logand q (mask 64))))
                                  #+nil(dpb q (byte (case ow
                                                 (1 8)
                                                 (2 16)
                                                 (4 32)
                                                 (t 64)) 0) u))))
           (fmt "~a.~a ~x -> rax(~x)" ',name ow y (reg :rax))))
       (case ow
         (1 (if (or (> q #xff) (< q 0)) (setf carry t))
            (setf q (logand q #xff))
            (setf sign (ash q -7)))
         (2 (if (or (> q #xffff) (< q 0)) (setf carry t))
            (setf q (logand q #xffff))
            (setf sign (ash q -15)))
         (4 (if (or (> q (mask 32)) (< q 0)) (setf carry t))
            (setf q (logand q (mask 32)))
            (setf sign (ash q -31)))
         (t (if (or (> q (mask 64)) (< q 0)) (setf carry t))
            (setf q (logand q (mask 64)))
            (setf sign (ash q -63))))
       ,@(cond
           ((listp flags) flags)
           ((eq flags :add-flags)
             '((set-flags-s sign)
               (set-flags4 ow)
               (set-flags-c (if carry 1 0))
               (setf p (not (zerop (logand z (ash 1 (1- (* ow 8))))))) ; FIX: replace with logbitp
               (setf u (not (zerop (logand y (ash 1 (1- (* ow 8))))))) ; FIX: replace with logbitp
               (setf q (not (zerop (logand q (ash 1 (1- (* ow 8))))))) ; FIX: replace with logbitp
               (set-flags-o (cond ((and p u (not q)) 1)
                                  ((and (not p) (not u) q) 1)
                                  (t 0)))))
           ((eq flags :sub-flags)
             '((setf p (not (zerop (logand z (ash 1 (1- (* ow 8))))))) ; FIX: replace with logbitp
               (setf u (not (zerop (logand y (ash 1 (1- (* ow 8))))))) ; FIX: replace with logbitp
               (set-flags-s sign)
               (setf p (cond ((and (not p) u) nil)
                             ((and p (not u)) t)
                             ((and p (> z y)) nil)
                             ((and p (< z y)) t)
                             ((< z y) t)
                             (t nil))) ; FIX-perf: probably the probable scenario is last
               (cond
                 ((zerop q) (set-flags-c 0) (set-flags-z 1) (set-flags-o 0))
                 ((not p)
                   (set-flags-c 0)
                   (set-flags-z 0)
                   (set-flags-o (if (zerop (get-flags-s)) 0 1)))
                 (t
                   (set-flags-c 1)
                   (set-flags-z 0)
                   (set-flags-o (if (zerop (get-flags-s)) 1 0))))
               (set-flags-c (if carry 1 0))))
           (bitwise
                  '((set-flags-s sign)
                    (set-flags4 ow)
                    (set-flags-c 0)
                    (set-flags-o 0)))
           (flags '((set-flags-s sign)
                    (set-flags4 ow)
                    (set-flags-c (if carry 1 0)))))
       (fmt " fr:~a " (decode-flags))))
       ((fmt "~a" ',name) (disadd ',name
                                  (cond
                                    ((zerop axd)
                                      (cond
                                        (ind
                                          (if (not dir)
                                            (list :iw/ow iw ow (reg rd :name) (fmtind :dis))
                                            (list :iw/ow iw ow (fmtind :dis) (reg rd :name))))
                                        (t
                                          (list (reg rs :name) (reg rd :name)))))
                                    (t
                                      (list :iw/ow iw ow y)))
                                  `(:const (ind . ,ind) (dir . ,dir) (axd . ,axd) (ow . ,ow) (rs . ,rs) (rd . ,rd)
                                           (mul . ,mul) (imm . ,imm) (bas . ,bas) (imms . ,imms))
                                  '(:macro load-indirect2)))))

(type-1 (or)
  :bitwise t
  :operation logior
  :indirect  ((setf q (logior z y)))
  :direct    ((setf q (logior z y))))

(type-1 (and)
  :bitwise t
  :operation logand
  :indirect  ((setf q (logand z y)))
  :direct    ((setf q (logand z y))))

(type-1 (xor)
  :bitwise t
  :operation logxor
  :indirect  ((setf q (logxor z y)))
  :direct    ((setf q (logxor z y))))

(type-1 (add) :operation + :flags :add-flags)
(type-1 (sub) :operation - :flags :sub-flags)

(type-1 (cmp)
  :indirect ((setf q (- z y)))
  :direct   ((setf q (- z y)))
  :rax      ((setf q (- z y)))
  :write nil
  :flags :sub-flags)

(type-1 (sbb)
  :indirect ((setf q (- z y (get-flags-c)))) ; here is the reason why carry must be LSB
  :direct   ((setf q (- z y (get-flags-c))))
  :rax      ((setf q (- z y (get-flags-c)))))

(type-1 (adc)
  :indirect ((setf q (+ z y (get-flags-c)))) ; here is the reason why carry must be LSB
  :direct   ((setf q (+ z y (get-flags-c))))
  :rax      ((setf q (+ z y (get-flags-c)))))

(define-cpu-runop (sub/cmp)
  (load-mrm)
  (decode-mrm)
  (cond
    (ind
      (setf x (reg rs))
      (setf z (reg rd))
      (setf y (mget-u64 x))
      (setf (reg rd) y)
      (mset-u64 x z)
      (fmt "xchg [~a] <-> ~a" (reg rs :name) (reg rd :name)))
    (t
      (setf x (reg rs))
      (setf (reg rs) (reg rd))
      (setf (reg rd) x)
      (fmt "xchg ~a <-> ~a" (reg rs :name) (reg rd :name)))))

; FIX: take care of both opcode C0 and C1
(define-cpu-runop2 (shf/ror)
  ()
  ((load-mrm)
   (decode-mrm)
   ; obscure feature, count(cl/imm) is masked x-bits and whats worse, sbcl take advantage of this
   (setf imm (logand (advance-u8) (if rexw #b00111111 #b00011111)))
   (setf ow 4)
   (if rexw (setf ow 8))
   ;(if prefix-66 (setf ow 2))
   ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
   (paranoid (= mrm-mod 3))) ; no operation support indirect yet
  ((ecase mrm-reg
     (0 (setf z "rol")
        (setf y (reg rs))
        (case ow
          (1 (loop repeat imm do (setf y (logior (ldb (byte 1  7) y) (ash (ldb (byte  7 0) y) 1)))))
          (4 (loop repeat imm do (setf y (logior (ldb (byte 1 31) y) (ash (ldb (byte 31 0) y) 1)))))
          (8 (loop repeat imm do (setf y (logior (ldb (byte 1 63) y) (ash (ldb (byte 63 0) y) 1)))))))
     (4 (setf z "shl")
        (setf y (logand (1- (ash 1 (* ow 8)))
                        (ash (reg rs) imm))))
     (5 (setf z "shr")
        (setf y (logand (1- (ash 1 (* ow 8))) ; FIX: is the logand overflowprotection? needed for shr?
                        (ash (reg rs) (- imm)))))
     (7 (setf z "sar")
        (setf y (reg rs))
        (setf x (case ow
                  (1 (if (logbitp  7 y) (ash (ash      #xff (- imm  8)) (- 8  imm)) 0))
                  (2 (if (logbitp 15 y) (ash (ash    #xffff (- imm 16)) (- 16 imm)) 0))
                  (4 (if (logbitp 32 y) (ash (ash (mask 32) (- imm 32)) (- 32 imm)) 0))
                  (t (if (logbitp 63 y) (ash (ash (mask 64) (- imm 64)) (- 64 imm)) 0))))
        (setf y (logior (ash y (- imm)) x)))
     (t (error "not implemented ~x" mrm-reg)))
   (setf (reg rs) y)
   (fmt "~a.~a ~a ~x=~x" z ow (reg rs :name) imm y)
   (set-flags-z (if (zerop (reg rs)) 1 0)))
  ((fmt "shf/ror") (disadd 'shf/ror `(:const (imm . ,imm) (ow . ,ow) (rexw . ,rexw) (mrm-reg . ,mrm-reg) (rs . ,rs)))))

(define-cpu-runop2 (bitsar) ; bit-shift-and-rotate
  ((wsz (logand o3 1))
   (clr (logand o3 2)))
  ((load-mrm)
   (decode-mrm)
   (paranoid (not prefix-66))
   (setf ow (1+ (* wsz 3)))
   (if rexw (setf ow 8)))
  ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  ((cond
     (ind (load-indirect))
     (t (setf y (reg rs))))
   (setf z (if (zerop clr) 1 (logand (reg :rcx) (if rexw #b00111111 #b00011111))))
   (ecase mrm-reg
     (0 ;(setf ow (1+ (* (logand o3 1) 3)))
        ;(if rexw (setf ow 8))
        ;(setf m (1- (* ow 8)))
        ;(loop repeat z do
        ;  (setf y (logior (ash (ldb (byte 1 m) y) (- m)) (ash (ldb (byte m 0) y) 1))))
        (case ow
          (1 (loop repeat z do (setf y (logior (ldb (byte 1  7) y) (ash (ldb (byte  7 0) y) 1)))))
          (4 (loop repeat z do (setf y (logior (ldb (byte 1 31) y) (ash (ldb (byte 31 0) y) 1)))))
          (8 (loop repeat z do (setf y (logior (ldb (byte 1 63) y) (ash (ldb (byte 63 0) y) 1))))))
        (fmt "rol.~a ~a by ~x = ~x " ow (reg rs :name) z y))
     (1
        (case ow
          (1 (loop repeat z do (setf y (logior (ash (ldb (byte 1 0) y)  7) (ldb (byte  7 1) y)))))
          (4 (loop repeat z do (setf y (logior (ash (ldb (byte 1 0) y) 31) (ldb (byte 31 1) y)))))
          (8 (loop repeat z do (setf y (logior (ash (ldb (byte 1 0) y) 63) (ldb (byte 63 1) y))))))
        (fmt "ror.~a ~a =~x by ~x" ow (reg rs :name) y z))
     (3 ; shift in carry and set new carry
       (cond
         (rexw (setf y (logior (ash (get-flags-c) 64) y)))
         (t    (setf y (logior (ash (get-flags-c) 32) y))))
       (set-flags-c (logand 1 (ash y (- z 1))))
       (setf y (ash y (- z)))
       (fmt "rcr.~a ~a =~x by ~x" ow (reg rs :name) y z))
     (4 (loop repeat z do (setf y (ash y 1)))
        (fmt "shl ~a =~x by ~x" (reg rs :name) y z))
     (5 (loop repeat z do (setf y (ash y -1)))
        (fmt "shr ~a =~x by ~x" (reg rs :name) y z))
     (7 (setf x (case ow ; sar
                  (1 (logand y #x80))
                  (2 (logand y #x8000))
                  (4 (logand y (ash 1 31)))
                  (t (logand y (ash 1 63)))))
        (loop repeat z do ; FIX: instead of loop, logior a mask of z msb bits if x is negative
          (setf y (logior (ash y -1) x)))
        (fmt "sar.~a ~a =~x by ~x" ow (reg rs :name) y z)))
   (setf (reg rs) y)
   (set-flags-z (if (zerop y) 1 0)))
  ((fmt "~a" 'bitsar) (disadd 'bitsar
                              `(:const (ow . ,ow) (rexw . ,rexw) (mrm-reg . ,mrm-reg) (ind . ,ind) (rs . ,rs)
                                       (wsz . ,wsz) (clr . ,clr))
                              '(:macro load-indirect))))

(define-cpu-runop (bitsar2)
  (load-mrm)
  (decode-mrm)
  (paranoid (not ind))
  ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (setf imm (logand (advance-u8) (if rexw #b00111111 #b00011111)))
  (ecase mrm-reg ;mrm-r/m
;    (0 (setf (reg rs) (ash (reg rs) imm))
;       (fmt "shr ~a ~x =~x" (register-name rs) imm (reg rs))
       ; fix: isn't this ROL?, fix, rotate in the bytes, also only affect 8lsb
       ; (erro
;       )
    (5 (setf x (reg rs))
       (setf (reg rs) (dpb (ash (logand x #xff) (- imm)) (byte 8 0) x))
       (fmt "shr ~a ~x =~x" (register-name rs) imm (reg rs)))
;    (5
;      (setf x (reg rs))
;      (setf y (ash (logand x #xff) (- imm)))
;      (setf (reg rs) (dpb y (byte 8 0) x)))
    ))

(define-cpu-runop2 (bsr) ; bit-scan-reverse, not branch-subroutine
  ()
  ((load-mrm)
   (decode-mrm))
  ((let (v v1 v2)
    (cond
      (ind
        (setf v1 (if rs (if (= rs 31) pc (reg rs)) 0))
        (if mul (setf v1 (* v1 mul)))
        (if bas (incf v1 (reg bas)))
        (if imm (incf v1 imm))
        (setf v2 (mget-u64 v1)))
      (t
        (setf v2 (reg rs))))
    ;
    (set-flags-z (if (zerop v2) 1 0))
    (setf v 0)
    (loop for i from 63 downto 0 do
      (when (not (zerop (logand v2 (ash 1 i))))
        (setf v i)
        (return)))
    (setf (reg rd) v)
    (fmt "bsr")
    ))
  ((fmt "bsr") (disadd 'bsr
                       `(:const (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (rs . ,rs) (rd . ,rd)))))

(define-cpu-runop (enter)
  (setf x (advance-u8))  ; size of stack frame
  (setf y (advance-u16)) ; nesting level
  (decf (reg :rsp) 8)
  (mset-u64 (reg :rsp) (reg :rbp))
  (setf (reg :rbp) (reg :rsp))
  (decf (reg :rsp) x)
  (fmt "enter new rsp=~x" (reg :rsp)))

(define-cpu-runop (leave)
  (setf (reg :rsp) (reg :rbp)) ; mov rbp -> rsp
  (let ((val (mget-u64 (reg :rsp))))
    (setf (reg :rbp) val) ; pop rbp
    (fmt "leave to ~x using stack ~x" val (reg :rsp))
    (incf (reg :rsp) 8)
    (fmt " new rbp/rsp ~x ~x" (reg :rbp) (reg :rsp))))

(define-cpu-runop2 (ret) ()
  ((setf bi t))
  ((setf pc (mget-u64 (reg :rsp)))
   ; FIX: make these proper vectors, ie instead of having the return address mean something
   ;      let the cpu jump to the vector which would contain a special-instruction
   (cond
     ((= pc #xcafe0000) ; interrupt-exit vector, used to return from interrupt-handler
       (setf quit t)
       (push-cpu-state cpu (list :interrupt-exit nil)))
     ((= pc 0)
       (setf quit t)
       (push-cpu-state cpu (list :thread-exit nil))))
   (fmt "ret ~x stack ~x" pc (reg :rsp))
   (incf (reg :rsp) 8)
   (fmt " new rbp/rsp ~x ~x" (reg :rbp) (reg :rsp)))
  ((fmt "ret") (disadd 'ret 'ret)))

(define-cpu-runop (setflag)
  (load-mrm)
  (decode-mrm)
  (paranoid (not ind))
  (ecase o2
    (2
      (ecase o3
        (2 (fmt "setb ~a"  (register-name rs)) (setf (reg rs) (if (not (zerop (get-flags-c))) 1 0)))
        (3 (fmt "setae ~a" (register-name rs)) (setf (reg rs) (if (zerop (get-flags-c)) 1 0)))
        (4 (fmt "sete ~a"  (register-name rs)) (setf (reg rs) (if (not (zerop (get-flags-z))) 1 0)))
        (5 (fmt "setne ~a" (register-name rs)) (setf (reg rs) (if      (zerop (get-flags-z))  1 0)))
        (7 (fmt "seta ~a"  (register-name rs)) (setf (reg rs) (if (and (zerop (get-flags-z)) (zerop (get-flags-c))) 1 0)))))
    (3
      (ecase o3
        (4 (fmt "setl ~a"  (register-name rs)) (setf (reg rs) (if (or (and (zerop (get-flags-s)) (not (zerop (get-flags-o))))
                                                                      (and (not (zerop (get-flags-s))) (zerop (get-flags-o))))
                                                                1 0)))
        (5 (fmt "setge ~a" (register-name rs)) (setf (reg rs) (if (= (get-flags-s) (get-flags-o)) 1 0)))
        (7 (fmt "setg ~a"  (register-name rs)) (setf (reg rs) (if (and (zerop (get-flags-z)) (= (get-flags-s) (get-flags-o))) 1 0)))))))

(define-cpu-runop2 (misc1)
  ()
  ((setf bi t)
   (setf imms (ecase o3
                (3 (setf bi t)
                   (advance-s8))
                (5 (advance-u8)))))
  ((ecase o3
     (3 ; jRCXz jump if rcx=zero
       (if (zerop (reg :rcx))
         (incf pc imms))
       (fmt "jrcxz ~x" (+ pc imms)))
     (5 ; in
       (fmt "in ~x eax" imms))))
  ((fmt "misc1") (disadd 'misc1 'jc `(:const (o3 . ,o3) (imms . ,imms)))))

(define-cpu-runop2 (cltq)
  ()
  ()
  ((case o3
     (0
       (setf y (reg :rax))
       (if (not (zerop (logand y #x80000000)))
         (setf (reg :rax) (logior y (logxor (mask 64) #xffffffff))))
       (fmt "cltq/cdqe rax=~x" (reg :rax)))
     (1
       (if (zerop (ash (reg :rax) -63))
         (setf (reg :rdx) 0)
         (setf (reg :rdx) (mask 64)))
       (fmt "cltq/cqo rdx=~x" (reg :rdx)))))
  ((fmt "cltq") (disadd 'cltq `(:const (o3 . ,o3)))))

(define-cpu-runop2 (clc) () () ((set-flags-c 0) (fmt "clc")) ((fmt "clc") (disadd 'clc)))
(define-cpu-runop2 (stc) () () ((set-flags-c 1) (fmt "stc")) ((disadd 'stc)))
(define-cpu-runop2 (cmc) () () ((set-flags-c (logxor 1 (get-flags-c))) (fmt "cmc")) ((disadd 'cmc)))
(define-cpu-runop2 (cld) () () ((set-flags-d 0) (fmt "cld")) ((disadd 'cld)))
(define-cpu-runop2 (std) () () ((set-flags-d 1) (fmt "std")) ((disadd 'std)))

; not host-thread-safe
(define-cpu-runop (cmpxchg)
  (let (v v2)
    (load-mrm)
    (decode-mrm)
    ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
    (assert (not *cpu-signal*))
    (cond
      (ind
        (load-indirect)
        (setf v (reg :rax))
        (setf v2 (reg rd))
        (check-write-page (ash x (- PAGEBN))) ; this will raise *cpu-signal* if page isn't realized
        (fmt "cmpxchg ~a -> rax(~x) : ~a/~x" (fmtreg rd) v (fmtind) y))
      (t
        (setf y imm)
        (setf v (reg :rax))
        (setf v2 (reg rd))
        (fmt "cmpxchg ~a -> rax(~x) : ~x" (fmtreg rd) v y)))
    (fmt " [cpustate: ~s] " *cpu-signal*)
    (unless *cpu-signal* ; little kludgy, we just want to know if we are going to rerun this instruction
      (cond
        ((= v y)
          (fmt " ! (~x == ~x)" v y)
          (cond
            (ind (mset-u64 x v2))
            (t   (mset-u64 imm v2)))
          (set-flags-z 1))
        (t
          (fmt " ! (~x /= ~x)" v y)
          (rset-u64 :rax y)
          (set-flags-z 0))))))

; floating point

(define-cpu-runop (f-arith)
  (load-mrm)
  (decode-mrm)
  ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (ecase rd
    (4 (fmt "fisub")
      )))

(define-cpu-runop (f-stw)
  (load-mrm)
  (decode-mrm)
  ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (load-address)
  (mset-u32 x 0)
  (fmt "fnstsw"))

(define-cpu-runop (f-nstenv)
  (load-mrm)
  (decode-mrm)
  ;(fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (ecase rd
    (4
      (load-address)
       (setf (cpu-fpucw cpu) (mget-u32 x))
      (fmt "fldenv ~x" (mget-u32 x)))
    (6 (load-address)
       (mset-u32 x (cpu-fpucw cpu))
       (fmt "fnstenv ~x" (cpu-fpucw cpu)))))

; sse

; floatcache, a cons that holds the float-as-bits, and possibly translated bits into double/single float
;(defun set-floatcons-bits (bits)
;  (cons bits 
;        (bits-to-single-float y)))
;
;(defun set-floatcons-float (float)
;  (cons (single-float-to-bits (coerce float 'single-float))
;        (truncate float)))
;
;(defun set-doublecons-bits (bits)
;  (error "not implemented"))
;
;(defun set-doublecons-float (float)
;  (cons (double-float-to-bits float)
;        (truncate float)))
;
;(defun set-doublecons-int (int)
;  (cons (double-float-to-bits float)
;        (truncate float)))

;(defun get-floatcons-bits (fc)
;  (car fc))
;
;(defun get-floatcons-float (fc)
;  (cdr fc))

;-----

(defun get-bits (fc)
  (car fc))

(defun get-single (fc)
  (if (cdr fc)
    (cdr fc)
    (bits-to-single-float (car fc))))

(defun get-double (fc)
  (if (cdr fc)
    (cdr fc)
    (bits-to-double-float (car fc))))

(defmacro make-doublefloat (from &optional value)
  (ecase from
    (:int
      `(let ((x ,value))
         (if (logbitp 63 x) (setf x (- x (ash 1 64))))
         (setf x (coerce x 'double-float))
         (cons (double-float-to-bits x) x)))
    (:single `(cons (double-float-to-bits ,value) (coerce ,value 'double-float)))
    (:double `(cons (double-float-to-bits ,value) ,value))
    (:bits   `(cons ,value (bits-to-double-float ,value)))
    (:bitsonly `(cons ,value nil))
    (:nan    `(cons #xfff8000000000000 nil))))

(defmacro make-singlefloat (from &optional value)
  (ecase from
    (:int
      `(let ((x ,value))
         (if (logbitp 63 x) (setf x (- x (ash 1 64))))
         (setf x (coerce x 'single-float))
         (cons (single-float-to-bits x) x)))
    (:single `(cons (single-float-to-bits ,value) ,value))
    (:double `(cons (single-float-to-bits ,value) (coerce ,value 'double-float)))
    (:bits   `(cons ,value (bits-to-single-float ,value)))
    (:bitsonly `(cons ,value nil))
    (:nan    `(cons #xfff80000 nil))))

(define-cpu-runop2 (cvtsi2sdq)
  ()
  ((paranoid (or prefix-repnz prefix-repz))
   (load-mrm)
   (decode-mrm)
   (to-xmm rd)
   (setf ow (if rexw 8 4)) ; from 8 or 4 bytes
   (setf iw prefix-f2))    ; to double or single float
  ((cond
     (ind
       (load-indirect2-sign)
       (if iw
         (setf (reg rd) (make-doublefloat :int y))
         (setf (reg rd) (make-singlefloat :int y)))
       (fmt "cvtsi2~a.~a ~a -> ~a" (if iw "sd" "ss") ow (fmtind) (reg rd :name)))
     (t
       (if iw
         (setf (reg rd) (make-doublefloat :int (reg rs)))
         (setf (reg rd) (make-singlefloat :int (reg rs))))
       (fmt "cvtsi2~a.~a ~a -> ~a(~x)" (if iw "sd" "ss") ow (reg rs :name) (reg rd :name) (reg rd)))))
  ((fmt "cvtsi2sdq") (disadd 'cvtsi2sdq `(:const (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (ow . ,ow) (iw . ,iw)
                                                 (rs . ,rs) (rd . ,rd))
                                        '(:macro load-indirect2-sign))))

(define-simple-mrm cvttss2si
  :indirect ((setf y (truncate (bits-to-single-float y))))
  :direct   ((setf y (truncate (get-single (reg rs)))))
  :direct-rd-as-xmm nil)

#+nil(define-cpu-runop (cvttss2si)
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (cond
    (ind
      (load-indirect)
      (setf (reg rd) y)
      (fmt "cvttss2si ~a -> ~a" (fmtind) (reg rd :name)))
    (t
      (to-xmm rs)
      (setf (reg rd) (truncate (get-single (reg rs))))
      (fmt "cvttss2si ~a -> ~a" (reg rs :name) (reg rd :name)))))

(defmacro single-unary-oper (oper x)
  `(cond
     ((integerp ,x)
       (make-doublefloat :bitsonly ,x))
     (t
       (setf y (,oper ,x))
       (make-doublefloat :double y))))

(define-cpu-runop (sqrtsd)
  (load-mrm)
  (decode-mrm)
  (to-xmm rs)
  (to-xmm rd)
  (setf x (get-double (reg rs)))
  (setf y (single-unary-oper sqrt x))
  ; for some reason sign bit is preserved, and sbcl depends on -0
  (if (and (car (reg rd)) (logbitp 63 (car (reg rd))))
    (setf (car (reg rd)) (logior (car (reg rd)) (ash 1 63))))
  (setf (reg rd) y)
  (fmt "sqrtsd ~a(~f) = sqrt(~a(~f))" (reg rd :name) y (reg rs :name) x))

(define-cpu-runop (cvtsx2sx)
  (load-mrm)
  (decode-mrm)
  (to-xmm rs)
  (to-xmm rd)
  (cond
    (prefix-f3 ; single-float to double-float
      (setf x (get-single (reg rs)))
      (cond
        ((integerp x) ; infinity detected, convert to double infinity
          (setf (reg rd) (make-doublefloat :bitsonly
                                           (logior (ash (logand x (ash 1 31)) 32)
                                                   (ash (1- (ash 1 11)) 52)))))
        (t (setf (reg rd) (make-doublefloat :single x))))
      (fmt "cvtss2sd ~f -> ~a(~x)" x (reg rd :name) (reg rd)))
    (t
      (setf x (get-double (reg rs)))
      (cond
        ((integerp x) ; infinity detected, convert to double infinity
          (setf (reg rd) (make-singlefloat :bitsonly
                                           (logior (ash (logand x (ash 1 63)) -32)
                                                   (ash (1- (ash 1 8)) 23)))))
        (t
          (setf (reg rd) (make-singlefloat :double x))))
      (fmt "cvtsd2ss ~f -> ~a(~x)" x (reg rd :name) (reg rd)))))

; movd:
;       0f 6e movd.4/8 rm -> mm
;       0f 7e movd.4/8 mm -> rm
; 66    0f 6e movd.4/8 rm -> xmm
; 66    0f 7e movd.4/8 xmm -> rm
; movq:
;       0f 6f movq.8 mm/mm64 -> mm
;       0f 7f movq.8 mm/mm64 -> mm
;    f3 0f 7e movq.8 xmm/m64 -> xmm
;    66 0f d6 movq.8 xmm -> xmm/m64
(define-cpu-runop2 (sse-mov-from)   ; 7e
  ((dir (cond
               (prefix-f3 t)
               (t nil))))
  ((setf ow (if rexw 8 4))
   (load-mrm)
   (decode-mrm)
   (to-xmm rd)
   (if (and dir (not ind)) (to-xmm rs)))
  ((if ind (if dir (load-indirect) (load-address)))
   (fmt "{~x:~a->~a,$~x,*~a,~a} " mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind dir)
   (cond
     (dir
       (cond
         (ind
           (setf (reg rd) (if rexw (make-doublefloat :bitsonly y)
                                   (make-singlefloat :bitsonly y)))
           (fmt "sse-mov-from.~a ~a -> ~a(~x)" ow (fmtind) (reg rd :name) (reg rd)))
         (t
           (setf z (get-bits (reg rs)))
           (setf (reg rd) (copy-list (reg rs)))
           (fmt "sse-mov-from.~a ~a(~x) -> ~a" ow (reg rs :name) z (reg rd :name)))))
     (t
       (setf z (get-bits (reg rd)))
       (cond
         (ind
           (if rexw (mset-u64 x z)
                    (mset-u32 x z))
           (fmt "sse-mov-from.~a ~a(~x) -> ~a" ow (reg rd :name) z (fmtind)))
         (t
           (setf (reg rs) z)
           (fmt "sse-mov-from.~a ~a(~x) -> ~a" ow (reg rd :name) z (reg rs :name)))))))
  ((fmt "mov-xmm") (disadd 'sse-mov-from
                           (if dir
                             (if ind
                               (list :ow ow (fmtind :dis) (reg rd :name))
                               (list :ow ow (reg rd :name) (reg rs :name)))
                             (if ind
                               (list :ow ow (reg rd :name) (fmtind :dis))
                               (list :ow ow (reg rd :name) (reg rs :name))))
                           `(:const (rs . ,rs) (rd . ,rd) (mul . ,mul) (bas . ,bas) (imm . ,imm) (ind . ,ind) (dir . ,dir)
                                    (rexw . ,rexw))
                           '(:macro load-address load-indirect))))

(define-cpu-runop (sse-mov-to)    ; 6e
  (paranoid (not (logbitp 0 o3))) ; mov from aligned reg/mem 128bit to xmm register
  (setf ow (if rexw 8 4))
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a} " mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (to-xmm rd)
  (cond
    (ind
      (load-indirect)
      (if rexw
        (setf (reg rd) (make-doublefloat :bitsonly y)) ; sbcl is probably using this operation in DF mode, when it should be SF
        (setf (reg rd) (make-singlefloat :bitsonly y)))
      (fmt "sse-mov-to.~a ~a -> ~a(~x)" (if ow 8 4) (fmtind) (reg rd :name) (reg rd)))
    (t
      (if rexw
        (setf (reg rd) (make-doublefloat :bitsonly (reg rs))) ; sbcl ... dito
        (setf (reg rd) (make-singlefloat :bitsonly (reg rs))))
      (fmt "sse-mov-to.~a ~a -> ~a(~x)" (if ow 8 4) (reg rs :name) (reg rd :name) (reg rd)))))

(define-simple-mrm pcmpeqd
  ;:write nil pcmpeqd doesn't use flags, it write the comparsion result in corresponding xmm word in destination
  :indirect ((error "pcmpeqd indirect is not implemented yet"))
  :direct ((if (= (get-bits (reg rs)) (get-bits (reg rd)))
             (setf y #xffffffff)
             (setf y #x00000000))
           (setf y (make-singlefloat :bitsonly y))))

(define-simple-mrm movmskps
  :indirect ((error "movmskps has no indirect"))
  :direct ((setf y (get-bits (reg rs)))
           (setf y (if (logbitp 31 (get-bits (reg rs))) #xf 0)))
  :direct-rd-as-xmm nil)

;    0f 28 single vps wps
; 66 0f 28 double vpd wpd
;    0f 29 single wps vps
; 66 0f 29 double wpd vpd
(define-cpu-runop (movapx)
  (let ((dir (if (logbitp 0 o3) t)))
  (setf ow prefix-66)
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a} " mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (cond
    (ind
      (load-address)
      (to-xmm rd)
      (cond
        (dir
          (if ow
            (mset-u64 x (get-bits (reg rd)))
            (mset-u32 x (logand (mask 32) (get-bits (reg rd)))))
          (fmt "movap~a ~a -> ~a" (if ow "d" "s") (reg rd :name) (fmtind)))
        (t
          (cond
            (ow
              (setf y (mget-u64 x))
              (setf (reg rd) (make-doublefloat :bitsonly y)))
            (t
              (setf y (mget-u32 x))
              (setf (reg rd) (make-singlefloat :bitsonly y))))
          (fmt "movap~a ~a -> ~a" (if ow "d" "s") (fmtind) (reg rd :name)))))
    (t
      (to-xmm rs)
      (to-xmm rd)
      (cond
        (dir
          (fmt "movap~a ~a -> ~a" (if ow "d" "s") (reg rd :name) (reg rs :name))
          (setf (reg rs) (reg rd)))
        (t
          (fmt "movap~a ~a -> ~a" (if ow "d" "s") (reg rs :name) (reg rd :name))
          (setf (reg rd) (reg rs))))))))

(define-cpu-runop (comisx)
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (to-xmm rd)
  (cond ; load source
    (ind
      (load-indirect)
      (fmt " ind[~x]=~x " x y)
      (setf z y)
      (cond
        (prefix-66
          (setf p (logbitp 63 z))
          (setf z (bits-to-double-float z)))
        (t
          (setf p (logbitp 31 z))
          (setf z (bits-to-single-float z)))))
    (t
      (to-xmm rs)
      (cond
        (prefix-66
          (setf z (get-double (reg rs)))
          (if (integerp z) (setf p (logbitp 63 z))))
        (t
          (setf z (get-single (reg rs)))
          (if (integerp z) (setf p (logbitp 31 z)))))))
  (cond ; load destination
    (prefix-66
      (setf x (get-double (reg rd)))
      (if (integerp x) (setf q (logbitp 63 x))))
    (t
      (setf x (get-single (reg rd)))
      (if (integerp x) (setf q (logbitp 31 x)))))
  ;(fmt " [~x,~a ~x,~a] " x q z p)
  (setf y (cond
            ((and (integerp x) (integerp z))
              (cond
                ((eq q p) 0)
                ((and q (not p)) -1)
                ((and p (not q)) 1)))
            ((integerp x) (if q -1 1))
            ((integerp z) (if p 1 -1))
            ((and x z) (setf y (cond ((> x z) 1) ((< x z) -1) (t 0))))))
  ;         Z P C
  ; nan     1 1 1
  ; rd > rs 0 0 0
  ; rd < rs 0 0 1
  ; rd = rs 1 0 0
  (cond
    ((numberp y)
      (set-flags-p 0)
      (set-flags-c (if (< y 0) 1 0))
      (set-flags-z (if (= y 0) 1 0)))
    (t
      (set-flags-p 1)
      (set-flags-c 1)
      (set-flags-z 1)))

  (if ind
    (fmt "comis~a ~a -> ~a(~f) fr:~a"     (if prefix-66 "d" "s") (fmtind)         (reg rd :name) x (decode-flags))
    (fmt "comis~a ~a(~f) -> ~a(~f) fr:~a" (if prefix-66 "d" "s") (reg rs :name) z (reg rd :name) x (decode-flags))))

; how portable is it to rely on the CL-type real/float vs non-real/float? like (assert (not (integerp 1.0)))
(defmacro float-oper (oper x z makefun)
  `(let ((xs (if (integerp ,x) ; xs/zs shows signs of operands x/z
               (logbitp ,(if (eq makefun 'make-singlefloat) 31 63) ,x)
               (minusp ,x)))
         (zs (if (integerp ,z)
               (logbitp ,(if (eq makefun 'make-singlefloat) 31 63) ,z)
               (minusp ,z))))
     (cond
       ((and (integerp ,x) (integerp ,z)) ; both operands are infinity
         (cond
           ((and xs zs) (,makefun :bitsonly ; if both operands are negative create a positive infinity
                                  ,(if (eq makefun 'make-singlefloat)
                                     (ash (1- (ash 1 8)) 23) ; create a positive-single-float-infinity
                                     (ash (1- (ash 1 11)) 52)))) ; create a positive-double-float-infinity
           (xs (,makefun :bitsonly ,x))  ; one operand is negative infinity, choose that one
           (t (,makefun :bitsonly ,z)))) ; else choose the other operand (that can be negative)
       ((integerp ,x)
         ,@(cond
             ((or (eq oper '*) (eq oper '/))
               `((if zs ; the other operand is negative, flip the infinity direction
                   (,makefun :bitsonly (logxor ,(if (eq makefun 'make-singlefloat) (ash 1 31) (ash 1 63)) ,x))
                   (,makefun :bitsonly ,x))))
             (t `((,makefun :bitsonly ,x)))))
       ((integerp ,z)
         ,@(cond
             ((or (eq oper '*) (eq oper '/))
               `((if xs ; the other operand is negative, flip the infinity direction
                   (,makefun :bitsonly (logxor ,(if (eq makefun 'make-singlefloat) (ash 1 31) (ash 1 63)) ,z))
                   (,makefun :bitsonly ,z))))
             (t `((,makefun :bitsonly ,z)))))
       (t ; no operand contains infinity
         ; it is enough if the float-cache is a non-complex number (ie, assert realp is too tight here)
         ;(assert (realp y)) ; KLUDGE: we trust each oper (a CL function) keeps float-type
         (handler-case (,makefun :double (,oper ,x ,z))
           (arithmetic-error () ; FIX: we dont produce NaNs for all masked arithmetic-errors?
             (cond
               ((zerop (logand (cpu-fpucw cpu) 63)) :sigfpe)
               (t ; produce a quiet NaN
                 (,makefun :nan)))))))))

(macrolet
  ((oper (name bitwise oper)
     `(define-cpu-runop2 (,name)
        ()
        ((load-mrm)
         (decode-mrm)
         (to-xmm rd)
         (unless ind (to-xmm rs))
         )
        (
         (setf z (cond (ind ; load source operand
                         (load-indirect)
                         ,@(cond
                             (bitwise `(y))
                             (t
                               `((cond
                                   ((or prefix-f2 prefix-66) (bits-to-double-float y))
                                   (t                        (bits-to-single-float y)))))))
                       (t ;(to-xmm rs)
                          ,@(cond
                              (bitwise `((get-bits (reg rs))))
                              (t `((cond
                                     ((or prefix-f2 prefix-66) (get-double (reg rs)))
                                     (t (get-single (reg rs))))))))))
         ; load destination operand and perform operator
         ,@(cond
             (bitwise
               `((setf x (get-bits (reg rd)))
                 (setf y (,oper x z))
                 (cond
                   (prefix-f2 (error "sse-xxx with prefix-f2 is unimplemented"))
                   (prefix-66 (setf y (make-singlefloat :bitsonly y)))
                   (t         (setf y (make-doublefloat :bitsonly y))))))
             (t
               `((cond
                   ((or prefix-f2 prefix-66)
                     (setf x (get-double (reg rd)))
                     (setf y (float-oper ,oper x z make-doublefloat)))
                   (t
                     (setf x (get-single (reg rd)))
                     (setf y (float-oper ,oper x z make-singlefloat)))))))
         ; check for inf/nan
         (if (keywordp y)
           (push-cpu-state cpu (list :signal (list (get-signal-handler 8) 8 nil 0)))
           (setf (reg rd) y))
         (fmt "~a~a ~a(~x) -> ~a(~x) = ~x" ',name
              (cond (prefix-f2 "SD") (prefix-f3 "SS") (prefix-66 "PD") (t "PS"))
              (reg rs :name) z (reg rd :name) x (list y)))
        ((fmt "~a" ',name) (disadd ',name `(:const (ind . ,ind) (mul . ,mul) (bas . ,bas) (imm . ,imm) (rd . ,rd) (rs . ,rs)
                                                   (prefix-f2 . ,prefix-f2) (prefix-66 . ,prefix-66) (prefix-f3 . ,prefix-f3))
                                          '(:macro load-indirect))))))
  (oper sse-and t logand)
  (oper sse-xor t logxor)
  (oper sse-mul nil *)
  (oper sse-div nil /)
  (oper sse-add nil +)
  (oper sse-sub nil -))

; auto casetree (define-cpu-oper (movss (0 2 0) t)
(define-cpu-runop2 (movss)
  ((dir (logbitp 0 o3)))
  ((load-mrm)
   (decode-mrm)
   (to-xmm rd)
   (unless ind (to-xmm rs)))
   ; MAYBE-FIX: treating no-prefix(movups) and prefix-66(movupd) as normal movss/movsd
  ((if ind
     (if dir (load-address)
             (load-indirect)))

   (cond
     (ind
       (cond
         (dir
           (setf y (get-bits (reg rd)))
           (cond
             (prefix-f3
               (mset-u32 x y)
               (fmt "movss ~a -> ~a" (reg rd :name) (fmtind)))
             (t
               (mset-u64 x y)
               (fmt "movsd ~a -> ~a" (reg rd :name) (fmtind)))))
         (t
           ; FIX: rd must be an xmm register (because we store a float), does the operation assures this?
           (cond
             (prefix-f3
               (setf (reg rd) (make-singlefloat :bits y))
               (fmt "movss ~a -> ~a" (fmtind) (reg rd :name)))
             (t
               (setf (reg rd) (make-doublefloat :bits y))
               (fmt "movsd ~a -> ~a" (fmtind) (reg rd :name)))))))
     (t
       (cond
         (dir
           (setf (reg rs) (reg rd))
           (fmt "movss ~a -> ~a" (reg rd :name) (reg rs :name)))
         (t
           (setf (reg rd) (reg rs))
           (fmt "movss ~a -> ~a" (reg rs :name) (reg rd :name)))))))
  ((fmt "movss") (disadd 'movss (if ind
                                  (if dir
                                    (if prefix-f3
                                      (list :s (reg rd :name) (fmtind :dis))
                                      (list :d (reg rd :name) (fmtind :dis)))
                                    (if prefix-f3
                                      (list :s (fmtind :dis) (reg rd :name))
                                      (list :d (fmtind :dis) (reg rd :name))))
                                  (if dir
                                    (list :s (reg rd :name) (reg rs :name))
                                    (list :s (reg rs :name) (reg rd :name))))
                                `(:const (ind . ,ind) (dir . ,dir) (rd . ,rd) (rs . ,rs) (prefix-f3 . ,prefix-f3)
                                         (mul . ,mul) (imm . ,imm) (bas . ,bas))
                                '(:macro load-address load-indirect))))

(define-cpu-runop (movntq)
  (load-mrm)
  (decode-mrm)
  (to-xmm rd)
  (paranoid ind)
  (load-address)
  (mset-u64 x (get-bits (reg rd)))
  (incf x 8)
  (mset-u64 x 0) ; FIX: is this "upper" double-float in xmm register?
  (fmt "movnt~aq ~a -> ~a" (if prefix-66 "d" "") (reg rd :name) (fmtind)))

; this instruction is a good exercise in LDB/DPB
(define-cpu-runop2 (shufps)
  ()
  ((load-mrm)
   (decode-mrm)
   (to-xmm rs)
   (to-xmm rd)
   (paranoid (not ind))
   (setf imm (advance-u8)))
  ((setf x (get-bits (reg rs)))
   (setf y (get-bits (reg rd)))
   (let ((ny y) z)
     (setf z (logand imm #b11))
     (setf (ldb (byte 32  0) ny) (ldb (byte 32 (* 32 z)) y))
     (setf z (ash (logand imm #b1100) -2))
     (setf (ldb (byte 32 32) ny) (ldb (byte 32 (* 32 z)) y))
     (setf z (ash (logand imm #b110000) -4))
     (setf (ldb (byte 32 64) ny) (ldb (byte 32 (* 32 z)) x))
     (setf z (ash (logand imm #b11000000) -6))
     (setf (ldb (byte 32 96) ny) (ldb (byte 32 (* 32 z)) x))
     (setf (reg rd) (make-doublefloat :bitsonly ny))
     (fmt "shufps ~x:~a(~x) -> ~a(~x->~x)" imm (reg rs :name) x (reg rd :name) y ny)))
  ((disadd 'shufps imm (reg rs :name) (reg rd :name)
           `(:const (rs . ,rs) (rd . ,rd) (ind . ,ind) (imm . ,imm)))))
 
(define-cpu-runop (unpack)
  (load-mrm)
  (decode-mrm)
  (to-xmm rs)
  (to-xmm rd)
  (fmt "unpack ~a -> ~a" (reg rs :name) (reg rd :name)))

#+nil(define-cpu-runop (divss)
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (to-xmm rs)
  (to-xmm rd)
  (setf x (reg rd))
  (setf z (reg rs))
  (setf y (/ x z))
  (setf (reg rd) y)
  (fmt "divss ~a -> ~a=~f" (reg rs :name) (reg rd :name) y))

(define-cpu-runop (cpuid)
  (case (reg :rax)
    (0 (setf (reg :rax) 1)
       (setf (reg :rbx) #x68747541)
       (setf (reg :rcx) #x444d4163)
       (setf (reg :rdx) #x69746e65)))
  (fmt "cpuid"))

(define-cpu-runop (bt)
  (load-mrm)
  (decode-mrm)
  (fmt "{~x:~a->~a,$~x,*~a}" mrm (if rs (reg rs :name)) (if rd (reg rd :name)) imm ind)
  (paranoid (not ind))
  (setf y (logand (if rex 63 31) (reg rd)))
  (set-flags-c (if (logbitp y (reg rs)) 1 0))
  (fmt "bt ~a,~a =~a" (reg rd :name) (reg rs :name) (get-flags-c)))

(define-cpu-runop (rdtsc)
  (setf (reg :rax) 0)
  (setf (reg :rdx) 0)
  (fmt "rdtsc"))

(define-cpu-runop (prefetch)
  (load-mrm)
  (decode-mrm)
  (fmt "prefetch"))

(add-casetree0f (0 2 0) movss)
(add-casetree0f (0 2 1) movss)
(add-casetree0f (0 2 4) unpack)
(add-casetree0f (0 3 0) prefetch)
(add-casetree0f (0 3 7) nop)
(add-casetree0f (0 5 2) cvtsi2sdq)
(add-casetree0f (0 5 4) cvttss2si)
(add-casetree0f (0 5 6) comisx) ; ucomisx
(add-casetree0f (0 5 7) comisx)
(add-casetree0f (0 5 0) movapx)
(add-casetree0f (0 5 1) movapx)

(add-casetree0f (1 0)   cmov)
(add-casetree0f (1 1)   cmov2)
(add-casetree0f (1 2 0) movmskps)
(add-casetree0f (1 2 1) sqrtsd)
(add-casetree0f (1 2 4) sse-and)
(add-casetree0f (1 2 7) sse-xor)

(add-casetree0f (1 3 0) sse-add)
(add-casetree0f (1 3 1) sse-mul)
(add-casetree0f (1 3 2) cvtsx2sx)
(add-casetree0f (1 3 4) sse-sub)
(add-casetree0f (1 3 6) sse-div)
(add-casetree0f (1 5)   sse-mov-to)
(add-casetree0f (1 6)   pcmpeqd)
(add-casetree0f (1 7)   sse-mov-from)
(add-casetree0f (2 0 1) jc4)
(add-casetree0f (2 0 2) jc4)
(add-casetree0f (2 0 3) jc4)
(add-casetree0f (2 0 4) jc4)
(add-casetree0f (2 0 5) jc4)
(add-casetree0f (2 0 6) jc4)
(add-casetree0f (2 0 7) jc4)
(add-casetree0f (2 1 0) jx4)
(add-casetree0f (2 1 1) jx4)
(add-casetree0f (2 1 2) jx4)
(add-casetree0f (2 1 4) jx4)
(add-casetree0f (2 1 5) jx4)
(add-casetree0f (2 1 6) jx4)
(add-casetree0f (2 1 7) jx4)
(add-casetree0f (2 2)   setflag)
(add-casetree0f (2 3)   setflag)
(add-casetree0f (2 4 2) cpuid)
(add-casetree0f (2 4 3) bt)
(add-casetree0f (0 6 1) rdtsc)
(add-casetree0f (2 5)   imul)
(add-casetree0f (2 6 1) cmpxchg)
(add-casetree0f (2 6 6) movzx)
(add-casetree0f (2 6 7) movzx)
(add-casetree0f (2 7 5) bsr)
(add-casetree0f (2 7 6) movsx)
(add-casetree0f (2 7 7) movsx)
(add-casetree0f (3 0 6) shufps)
(add-casetree0f (3 2 6) sse-mov-from)
(add-casetree0f (3 4 7) movntq)


(add-casetree (0 0) add)
(add-casetree (0 1) or)
(add-casetree (0 2) adc)
(add-casetree (0 3) sbb)
(add-casetree (0 4) and)
(add-casetree (0 5) sub)
(add-casetree (0 6) xor)
(add-casetree (0 7) cmp)

(add-casetree (1 2) push)
(add-casetree (1 3) pop)
(add-casetree (1 4) movslq)
(add-casetree (1 5 0) pushiw)
(add-casetree (1 5 1) imul3)
(add-casetree (1 5 2) pushi)
(add-casetree (1 5 3) imul3)
(add-casetree (1 6) jc1)
(add-casetree (1 7) jx1)

(add-casetree (2 0 0) arimix)
(add-casetree (2 0 1) arimix)
(add-casetree (2 0 2) arimix2)
(add-casetree (2 0 3) arimix)
(add-casetree (2 0 4) testr)
(add-casetree (2 0 5) testr)
(add-casetree (2 0 6) arimix2)
(add-casetree (2 0 7) sub/cmp)
(add-casetree (2 1 0) mov8)
(add-casetree (2 1 1) mov)
(add-casetree (2 1 2) mov8)
(add-casetree (2 1 3) mov)
(add-casetree (2 1 4) mov)
(add-casetree (2 1 5) lea)
(add-casetree (2 1 6) mov)
(add-casetree (2 1 7) pop-mrm)
(add-casetree (2 2)   xchg/nop)
(add-casetree (2 3)   cltq)
(add-casetree (2 4 5) movs)
(add-casetree (2 4 6) cmpsb)
(add-casetree (2 5 0) test-al)
(add-casetree (2 5 1) test-rax)
(add-casetree (2 5 3) stos)
(add-casetree (2 5 5) lods)
(add-casetree (2 6)   movb)
(add-casetree (2 7)   movrimm4)

(add-casetree (3 0 0) bitsar2)
(add-casetree (3 0 1) shf/ror)
(add-casetree (3 0 3) ret)
(add-casetree (3 0 4) stub-escape) ; psuedo
(add-casetree (3 0 6) movii)
(add-casetree (3 0 7) movii)
(add-casetree (3 1 0) enter)
(add-casetree (3 1 1) leave)
(add-casetree (3 1 4) call-int3)
(add-casetree (3 2)   bitsar) ; shift-and-rotate
(add-casetree (3 3 1) f-nstenv)
(add-casetree (3 3 5) f-stw)
(add-casetree (3 3 6) f-arith)
(add-casetree (3 4)   misc1)
(add-casetree (3 5 0) callimm4)
(add-casetree (3 5 1) jmp4)
(add-casetree (3 5 3) jmp1)
(add-casetree (3 6 6) arimix3)
(add-casetree (3 6 7) arimix3)
(add-casetree (3 6 5) cmc)
(add-casetree (3 7 0) clc)
(add-casetree (3 7 1) stc)
(add-casetree (3 7 4) cld)
(add-casetree (3 7 5) std)
(add-casetree (3 7 6) inc/dec)
(add-casetree (3 7 7) call/jmp/push)

