;
;  Emulator start
; -----------------
;

 11.1 Emulator start overview
 ------------------------------
  What happens when the emulator starts?
  That question is looked closer at in this chapter. We will also define
  the two "main" functions that setup everything and start the emulation.

  11.2 Emulator Parameters
  --------------------------
  The emulator parameters describes what can be adjusted during runtime. Note this is only
  a partial total list of adjustable parameters, each module has its own set of parameters.

  The executable file that is being emulated (PIE).

  |$(defvar *maxrun* nil) ; default run cpu forever
  |$(defvar *runtime* nil)
  |$(defvar *resume-file* nil)

  11.3 Emulator startup
  -----------------------
  A short overview of what happens when the emulator start.
  After input parameters are handled (like command line and site.lisp), the START function
  is entered. That function will load the core and the executable runtime.
  It will also setup memory, logging, initialize OS and finally call RUN-EMULATED-SBCL.

  RUN-EMULATED-SBCL will do the ELF dance which setup stubs etc. Create the CPU and initial thread.
  And then enter the C-init function (that function in turn will enter C-main).

  11.3.1 Function set-c-stack
  -----------------------------
  A native OS will have some prefered values for what a good stack are. So we need
  to make that information architecture dependently. Concretely it will take the
  configuration parameters :C-STACK-BP and :C-STACK-SP and put them in the architecture dependent
  stack and base registers.
  See @ref{Site Configuration for X86-64} or @ref{Site Configuration for MIPS} for how the :C-STACK-BP and :C-STACK-SP are
  setup in the site configuration file.

  |$(defun set-c-stack (cpu)
  |   ; FIX: move the stack to it's own bank (upper part in malloc bank?)
  |   ; set the base and stack pointer to the address minus the size
  |   (L "changing stack using ~s, ~s~%" (archf cpu :c-stack-bp) (archf cpu :c-stack-sp) :emu)
  |   (setf (areg cpu :rfp) (- (first (archf cpu :c-stack-bp)) (second (archf cpu :c-stack-bp))))
  |   (setf (areg cpu :rsp) (- (first (archf cpu :c-stack-sp)) (second (archf cpu :c-stack-sp)))))

  Essentially what we are doing is STACK-BEGIN-ADDRESS minus STACK-SIZE.
  FIX: Not sure this is correct: stack grows-lower-address on mips/x86-64.
  FIX: This is the correct place to initialize the stack pages, but that is instead done in RUN-EMULATED-SBCL.

  11.3.2 Function emu-set-pc
  ----------------------------
  Same treatment as the stack: store the address in the architecture dependent PC registers.

  |$(defun emu-set-pc (cpu address)
  |   (setf (areg cpu :rpc) address))

  11.3.3 Function run-emulated-sbcl
  -----------------------------------
  This function focuses on the runtime events that begins with ELF poking around in the executable file,
  upto the point of calling the C-init function. Which where we leave control over to the the emulated user-code.

  |$(defun run-emulated-sbcl (run)
  |   (format t "run-emulated-sbcl 0~%")

  The function begins with calling the ELF loader that will take bits and pieces from the executable runtime and
  move it into the user-memory.
  FIX: It could be advantageous to let ELF take a filename parameter instead and skip the *runtime* altogheter.
  This would save us some memory if the runtime was big (because the ELF could instead traverse the runtime using a filehandle).

  |   (multiple-value-bind (main arch) (elf-parse *runtime*
  |                                               #'make-page
  |                                               (lambda (addr name type arch)
  |                                                 (declare (ignore type))
  |                                                 (let ((sym (intern (format nil "~a@PLT" (string-upcase name)) :keyword)))
  |                                                   (make-stub-arch addr sym arch))))
  |     (let* ((cpu (make-thread (get-arch-enum arch)))
  |            (registers (cpu-regs cpu))
  |            (arch (cpu-arch cpu)))

  Following section should be done in @ref{Function set-c-stack}.

  |       ; make enough room for the c-stack
  |       (loop for i from 0 below 128 do
  |         (make-page (- #x1fffe000 (* i PAGESZ))))

  This hardcoded address is something that should be fixed ( the site-config tells us what page interrupt context is using ).

  |       (make-page #x53D9C70) ; FIX: mimic gdb interrupt-stack

  |       (set-c-stack cpu)
  |       ; set arguments to main(int argc, char **argv) (used by __libc_start_main)
  |       (make-page *argv*)
  |       (make-page (+ *argv* 8192))
  |       (cond
  |         ((eq arch (archname mips))
  |           (make-page #x4315cc)
  |           (mset-u32b #x4315cc *libc-stdout*)
  |           (make-page #x431788)
  |           (mset-u32b #x431788 *libc-stderr*)))
  |       ; if the emulator begins at c-main, just set rdi+rsi, but here we begin one step lower
  |       ;(rset-u64 :rsi *argv*)
  |       ;(rset-u64 :rdi (length *c-args*))
  |       (let ((z 0) (n 512)) ; room for 512/8 args
  |         (loop for arg in *c-args* do
  |           (cond
  |             ((eq arch (archname x86-64))
  |               (mset-u64 (+ *argv* z) (+ *argv* n)) (incf z 8))
  |             ((eq arch (archname mips))
  |               (mset-u32b (+ *argv* z) (+ *argv* n)) (incf z 4)))
  |           (L "writing c-argument ~s at ~x pointed to by ~x" arg (+ *argv* n) (+ *argv* z) :4 :os)
  |           (loop for a across arg do
  |             (mset-u8 (+ *argv* n) (char-code a)) (incf n))
  |           (mset-u8 (+ *argv* n) 0) (incf n))
  |         )
  |       (L "registers: ~s~%" registers)

  FIX: this section is x86-64 stuff and should go away or atleast not be hardcoded.
  |       ; stuff initialized by libc probably (when calling entry point _start)
  |       (make-page #x40a6e8) ; FIX: make-page using info from elf instead
  |       (make-page #x6347a0) ; symbol: interrupt_handlers :FIX: temp-pages takes care of this now

  |       (L "**** setting pc to ~x~%" main)
  |       (L "**** first byte at ~x is ~x~%" main (mget-u8 main))
  |       (emu-set-pc cpu main)
  |       (assert (not *cpu-signal*)) ; ensure any prior actions didn't raise a signal
  |       (L "start emulation at address ~x~%" main)
  |       (if run (loop-cpus nil))
  |       (L "emulator exit.~%")
  |       (format t "**** emulator exit. ****~%")
  |       cpu)))

  11.3.4 Function load-runtime
  ------------------------------

  The function LOAD-RUNTIME and LOAD-CORE is utilized by the ELF loader.
  It uses a general slurp function such as:

  |$(defun load-file-as-binary (file)
  |   (let ((arr (make-array 0 :element-type '(unsigned-byte 8) :initial-element 0 :fill-pointer 0 :adjustable t)))
  |     (with-open-file (s file :direction :input :element-type '(unsigned-byte 8))
  |       (loop for c = (read-byte s nil) while c do
  |         (vector-push-extend c arr)))
  |     arr))

  To load the runtime file:

  |$(defun load-runtime (file)
  |   (L "loading runtime file ~a~%" file :emu)
  |   (setf *runtime* (load-file-as-binary file)))

  11.3.5 Function load-core
  ---------------------------
  The SBCL-Core isn't loaded manually anymore, but instead is loaded by the user-code
  (the normal mmap a file routine).

  |$(defun load-core (file)
  |   (L "loading core file ~a~%" file :emu)
  |   (setf *core* (load-file-as-binary file)))

  11.3.6 Function start
  -----------------------
  START can be called in two main scenarios: starting fresh over, or starting with an thawed ICE file.

  |$(defun start (run)
  |   (log-open)
  |   (cond
  |     (*ice*
  |       (run-thawed-sbcl (thaw)))
  |     (t
  |       (load-arch-config)
  |       ;(load-runtime "/usr/local/bin/sbcl")
  |       (load-runtime (first *c-args*))
  |       ;(load-core "/usr/local/lib/sbcl/sbcl.core")
  |       ;(setf *mem* (make-array 65536 :initial-element nil :fill-pointer 0 :adjustable t))
  |       ;(setf *memsz* 65536)

  It is in os-init where we setup the user-memory.

  |       (os-init *heap-size*) ; FIX: put that in config.lisp
  |       ;(os-init (* #xa000000 2)) ; this will make sbcl 1.0.28 go nuts
  |       (run-emulated-sbcl run)
  |       (log-close))))

  11.4 Core and ICE handling
  ----------------------------

  11.4.1 Function run-thawed-sbcl
  ---------------------------------
  |$(defun run-thawed-sbcl (cpu)
  |    (declare (ignore cpu))
  |    (let ()
  |      (loop-cpus #x4000)))


  11.4.2 Function core
  ----------------------
  |$(defun core ()
  |   (format t "run PIE for a little while to have something to dump~%")
  |   (let ((cpu (start *maxrun*)))
  |     (format t "PIE has been run for ~x instructions, time to dump.~%" *maxrun*)
  |     (format t "relazy pages~%")
  |     (heap-relazy)
  |     (format t "freezing io~%")
  |     (io-freeze)
  |     (format t "call lisp dump-core~%")
  |     (force-output common::*log-stream*)
  |     #+sbcl (sb-ext:save-lisp-and-die "sbemu.core" :executable nil :toplevel (lambda ()
  |                                                                               (handler-bind
  |                                                                                 ((end-of-file (lambda (c) (declare (ignore c)) (exit-all))))
  |                                                                                 (resume-sbcl cpu)))))
  |   (exit-emulator))

  11.4.3 Function resume-sbcl
  ------------------------------------
  |$(defun resume-sbcl (cpu)
  |   (declare (ignore cpu))
  |   (format t "resuming from sbcl-core~%")
  |   (log-open)
  |   (lisp-chdir :pwd)
  |   (io-init)
  |   (format t "CWD: ~s,~s~%" *default-pathname-defaults* (truename "."))
  |   (if (and *resume-file* (probe-file *resume-file*))
  |     (load *resume-file*))
  |   (loop-cpus #x4000))


  11.5 Functions that exit the emulator
  -----------------------------------------

  11.5.1 Function exit-emulator
  -------------------------------
  |$(defun exit-emulator ()
  |   (log-close)
  |   0)

  11.5.2 Function exit-all
  ---------------------------
  |$(defun exit-all ()
  |   (exit-emulator)
  |   (lisp-exit))

