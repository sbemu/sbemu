; check SUB instructions

(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x10000))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize stacks
          (my-make-page cmem #xa0c0e0c)
          (my-make-page cmem #x5060808)
          (my-make-page cmem #x50586d5)
          (my-make-page cmem #x883d)
          (my-make-page cmem #x22886B)
          (my-make-page cmem #x50606cd) (my-mset-u8 cmem #x50606cd #xff)
          (format t "done setting page~%")
          ;(make-page #x1fffe000) ; ???
          ; initialize memory
          ;(loop for i from 1960 to 1980 do (my-mset-u8 cmem i (- i 1959)))
          ; initialize registers
          ; create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000005060708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  (oper sub-8-rsp 8 1 (#x48 #x83 #xec #x08) ((:reg= pc 12) (:reg= :rsp #x5060704)))
  (oper add-$50000004-eax 8 1 (#x05 #x04 #x00 #x00 #x50) ((:reg= pc 13) (:reg= :rax #x5506070c)))
  (oper add-esi-[rdi+$4a] 8 1 (#x01 #x77 #x4a) ((:reg= pc 11) (:mem= #x5060759 14 7 6 5)))
  (oper add-eax-[rbp-4] 8 1 (#x01 #x45 #xfc) ((:reg= pc 11) (:mem= #x5060709 8 7 6 5)))
  (oper addb-$d0-[rax+$00000100] 8 1 (#x80 #x80 #x00 #x01 #x00 #x00 #xd0) ((:reg= pc 15) (:mem= #x5060808 208)))
  (oper sbb-3-esi 8 1 (#x83 #xde #x03) ((:reg= pc 11) (:reg= :rsi #x506070a)))
  (oper add-$1-r13 8 1 (#x49 #x83 #xc5 #x01) ((:reg= pc 12) (:reg= :r13 #x5060706)))
  (oper add-rcx-rcx 8 1 (#x48 #x01 #xc9) ((:reg= pc 11) (:reg= :rcx #xa0c0e12)))
  (oper add-[rip+$22885c]-rcx 8 1 (#x48 #x03 #x0d #x5c #x88 #x22 #x00) ((:reg= pc 15)))
  (oper mulq-[rbp-$8038] 8 1 (#x48 #xf7 #xa5 #xc8 #x7f #xff #xff) ((:reg= pc 15) (:reg= :rdx 0) (:reg= :rax 0)))
  (oper imul-[rip+$00882d]-rsi 8 1 (#x48 #x0f #xaf #x35 #x2d #x88 #x00 #x00) ((:reg= pc 16) (:reg= :rsi 0)))
  (oper idiv-rbx 8 1 (#x48 #xf7 #xfb) ((:reg= pc 11) (:rax #xffffffcd0a3b80c9) (:rdx #x23bff65)))
  (oper imul-8-rax-rdx 8 1 (#x48 #x6b #xd0 #x08) ((:reg= pc 12) (:rdx #x28303840)))
  (oper imul-rdi 8 1 (#x48 #xf7 #xef) ((:reg= pc 11) (:rax #x193c6ac7bba178) (:rdx 0)))
  (oper div-rdi 8 1 (#x48 #xf7 #xf7) ((:reg= pc 11) (:rdx #x395ce05) (:rax #xffffff01332a4ecd)))
  (oper neg-rdi 8 1 (#x48 #xf7 #xdf) ((:reg= pc 11) (:reg= :rdi #xfffffffffaf9f8f1)))
  (oper inc-rdx 8 1 (#x48 #xff #xc2) ((:reg= pc 11) (:reg= :rdx #x506070b)))
  (oper dec-rsi 8 1 (#x48 #xff #xce) ((:reg= pc 11) (:rsi #x506070d)))
  (oper inc-al  8 1 (#xfe #xc0) ((:reg= pc 10) (:rax 9)))
  (oper inc-[rbp+38] 8 1 (#x48 #xff #x45 #xc8) ((:reg= pc 12) (:mem= #x50606d5 1)))
  (oper add-rax-rcx 8 1 (#x48 #x01 #xc1) ((:reg= pc 11) (:reg= :rcx #xa0c0e11)))
  (oper sub-[r12+60]-rbx 8 1 (#x49 #x2b #x5c #x24 #x60) ((:reg= pc 13)))
  (oper add-[pc+0x22885c]-rcx 8 1 (#x48 #x03 #x0d #x5c #x88 #x22 #x00) ((:reg= pc 15)))
  (oper sub-[rbp-40]-rdx 8 1 (#x48 #x2b #x55 #xc0) ((:reg= pc 12) (:rdx #x506060b)))
  (oper sub-90-[r12+rax] 8 1 (#x49 #x81 #x2c #x04 #x90 #x00 #x00 #x00) ((:reg= pc 16) (:mem= #xa0c0e0c #x70 #xff #xff #xff #xff #xff #xff #xff)))
  (oper imul-3e8*rbx-rax 8 1 (#x48 #x69 #xc3 #xe8 #x03 #x00 #x00) ((:reg= pc 15) (:rax #x139f8b82f8)))
  (oper adc-0-rdx 8 1 (#x48 #x83 #xd2 #x00) ((:reg= pc 12) (:rdx #x506070b))))

(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x10000))
          (my-make-page cmem 0)
          (my-make-page cmem #x50c8)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          (my-make-page cmem #x50606cd) (my-mset-u8 cmem #x50606cd #xff)
          (format t "done setting page~%")
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000123 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  (oper sub-r14-[rsi*8+47a0] 8 1 (#x4c #x29 #x34 #xf5 #xa0 #x47 #x00 #x00) ((:reg= pc 16) (:mem= #x50c8 #xd3 254 255 255 255 255 255 255 255))))

 

