; check MOV instruction

(defun mytest-make-pages (cmem &rest addrs)
  (loop for addr in addrs do
    (my-make-page cmem addr)))

(macrolet
  ((oper-mov (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x10000))
          (mytest-make-pages cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))

          ;(loop for i from 0 below (length (cpu-registers cpu2)) do
          ;  (setf (svref (cpu-registers cpu) i) i)
          ;  (setf (svref (cpu-registers cpu2) i) i))
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000405060708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper-mov mov-rsi-r13 8 1 (#x49 #x89 #xf5) ((:reg= pc 11) (:reg= :r13 #x40506070E)))
  (oper-mov mov-$424d32-edi 8 1 (#xbf #x32 #x4d #x42 #x00) ((:reg= pc 13) (:reg= :rdi #x424d32)))
  (oper-mov mov-rsp-rbp 8 1 (#x48 #x89 #xe5) ((:reg= pc 11) (:reg= :rbp #x40506070c)))
  (oper-mov mov-rsp-rbx 8 1 (#x48 #x89 #xe3) ((:reg= pc 11) (:reg= :rbx #x40506070c)))
  (oper-mov mov-rax-r12 8 1 (#x49 #x89 #xc4) ((:reg= pc 11) (:reg= :r12 #x405060708)))
  (oper-mov mov-r12-rsi 8 1 (#x4c #x89 #xe6) ((:reg= pc 11) (:reg= :rsi #x405060704)))
  (oper-mov mov-r13-rdx 8 1 (#x4c #x89 #xea) ((:reg= pc 11) (:reg= :rdx #x405060705))))

(macrolet
  ((oper-mov (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x40000))
          (my-make-page cmem ,code-offset)
          ; initialize stacks
          (mytest-make-pages cmem (- #x1fffe000 1) ; stack mapping
                                  (- #x1fffe000 8191)
                                  (- #x1fffe000 8192)
                                  (- #x1fffe000 8193) #x50606B5 #x506063c #x62C270 #xa0bfdf5 #x21de82 #xa0c0e0e #x50586c1 #x21ff53 #x5060000 #x505F000 #x505E000 #x2d363f52 #x211d86 #xd6c6 #x636071)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          (loop for i from 0 below 8 do (my-mset-u8 cmem (+ #x506063c i) i))
          (loop for i from 0 below 8 do (my-mset-u8 cmem (+ #x506073c i) i))
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000005060708 i)))))
          (my-mset-u8 cmem #x5060708 1) (my-mset-u8 cmem #x5060709 2)
          (my-make-page cmem #x1b0) (my-mset-u8 cmem #x1b0 123)
          (my-mset-u8 cmem #x5060732 8)
          (loop for i from 0 below 8 do (my-mset-u8 cmem (+ #x5060711 i) 10))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          ;(my-make-page cmem ,code-offset)
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  ; MOV 64 bit
  (oper-mov mov-rbx-rdx 8 1 (#x48 #x89 #xda) ((:reg= pc 11) (:reg= :rdx #x506070b)))
  (oper-mov mov-1-r12 8 1 (#x41 #xbc #x01 #x00 #x00 #x00) ((:reg= pc 14) (:reg= :r12 1)))
  (oper-mov mov-rax-[rbp-$50] 8 1 (#x48 #x89 #x45 #xb0) ((:reg= pc 12) (:mem= #x50606bd 8 7 6 5 4)))
  (oper-mov mov-rdx-[rbp-$58] 8 1 (#x48 #x89 #x55 #xa8) ((:reg= pc 12) (:mem= #x50606b5 10 7 6 5 9))) ; FIX: it isn't 9, not detected?
  (oper-mov mov-rdx-[rcx-7]   8 1 (#x48 #x89 #x51 #xf9) ((:reg= pc 12) (:mem= #x5060702 10 7 6 5 0 0 0 0)))
  (oper-mov mov-rdx-[rbp-8]   8 1 (#x48 #x89 #x55 #xf8) ((:reg= pc 12) (:mem= #x5060705 10 7 6 5 0 0 0 0)))
  (oper-mov mov-rax-[rbp-$a0]  8 1 (#x48 #x89 #x85 #x60 #xff #xff #xff) ((:reg= pc 15) (:mem= #x506066d 8 7 6 5 4)))
  (oper-mov mov-[r12+$38]-rax  8 1 (#x49 #x8b #x44 #x24 #x38) ((:reg= pc 13) (:reg= :rax #x706050403020100)))
  (oper-mov mov-[rax]-rsi      8 1 (#x48 #x8b #x30) ((:reg= pc 11) (:reg= :rsi 513)))
  (oper-mov mov-rax-[rcx+8]    8 1 (#x48 #x89 #x41 #x08) ((:reg= pc 12) (:mem= #x5060711 8 7 6 5 0 0 0 0)))
  (oper-mov mov-[$000001b0]-rax  8 1 (#x48 #x8b #x04 #x25 #xb0 #x01 #x00 #x00) ((:reg= pc 16) (:reg= :rax 123)))
  ;;;;(oper-mov mov-[$20100ab0]-r8 8 1 (#x4c #x8b #x04 #x25 #xb0 #x0a #x10 #x20) ((:reg= pc 16)))
  ;(oper-mov mov-[r12+rax+1]-rax 8 1 (#x49 #x8b #x04 #x04) ((:reg= pc 12)))
  (oper-mov mov-[$00000a70]-rcx  8 1 (#x48 #x8b #x0c #x25 #x70 #x0a #x00 #x00) ((:reg= pc 16) (:reg= :rcx 0)))
  ;(oper-mov mov-[r12+rcx+1]-rcx 8 1 (#x49 #x8b #x0c #x0c) ((:reg= pc 12)))
  (oper-mov mov-rax-[rbp-$98]  8 1 (#x48 #x89 #x85 #x68 #xff #xff #xff) ((:reg= pc 15) (:mem= #x5060675 8 7 6 5 4)))
  (oper-mov mov-rcx-[rbp-$90]  8 1 (#x48 #x89 #x8d #x70 #xff #xff #xff) ((:reg= pc 15) (:mem= #x506067d 9 7 6 5 4)))
  (oper-mov mov-rsp-[rbp-$a8]  8 1 (#x48 #x89 #xa5 #x58 #xff #xff #xff) ((:reg= pc 15) (:mem= #x5060665 12 7 6 5 4)))
  ; FIX: maybe rip-$8bb dont point right
  (oper-mov mov-[rip-$8bb]-rdx #x5061870 1 (#x48 #x8b #x15 #xcc #xed #xff #xff) ((:reg= pc #x5061877) (:reg= :rdx #x7)))
  (oper-mov mov-[rip+$21b6aa]-eax #x410bc0 1 (#x8b #x05 #xaa #xb6 #x21 #x00) ((:reg= pc #x410bc6) (:reg= :rax 0)))
  (oper-mov mov-rax-[rip+$21de73] 8 1 (#x48 #x89 #x05 #x73 #xde #x21 #x00) ((:reg= pc 15) (:mem= #x21de82 8 7 6 5)))
  (oper-mov mov-[rdx+rcx*8]-rbx 8 1 (#x48 #x8b #x1c #xca) ((:reg= pc 12) (:reg= :rbx 0)))
  ; MOV 32 bit
  (oper-mov mov-[rbp-$804c]-edi 8 1 (#x8b #xbd #xb4 #x7f #xff #xff) ((:reg= pc 14) (:reg= :rdi 0)))
  (oper-mov mov-$1000-edx 8 1 (#xba #x00 #x10 #x00 #x00) ((:reg= pc 13) (:reg= :rdx #x1000)))
  (oper-mov mov-$1000000000-rax 8 1 (#x48 #xb8 0 0 0 0 #x10 0 0 0) ((:reg= pc 18) (:reg= :rax #x1000000000)))
  (oper-mov movzbl-al-edx 8 1 (#x0f #xb6 #xd0) ((:reg= pc 11) (:reg= :rdx 8)))
  (oper-mov movzbl-[rdx+$a]-eax 8 1 (#x0f #xb6 #x42 #x0a) ((:reg= pc 12) (:reg= :rax 10)))
  (oper-mov movzbl-[r13+0]-eax 8 1 (#x41 #x0f #xb6 #x45 #x00) ((:reg= pc 13) (:reg= :rax 0)))
  (oper-mov movzbl-[rip+$21ff44]-eax 8 1 (#x0f #xb6 #x05 #x44 #xff #x21 #x00) ((:reg= pc 15) (:reg= :rax 0)))
  (oper-mov movzbl-[rax+$a]-edx 8 1 (#x0f #xb6 #x50 #x0a) ((:reg= pc 12) (:reg= :rdx 10)))
  (oper-mov movzbl-[rax]-ecx 8 1 (#x0f #xb6 #x08) ((:reg= pc 11) (:reg= :rcx 1)))
;(oper-mov movzwl-[rax+8]-edi 8 1 (#x0f #xb7 #x78 #x08) ((:reg= pc 12) (:reg= :rdi #x0a0a0a00))) ; only move 32 bit
(oper-mov movzwl-[rax+8]-edi 8 1 (#x0f #xb7 #x78 #x08) ((:reg= pc 12) (:reg= :rdi #xA00))) ; only move 32 bit
  (oper-mov movzwl-di-edx      8 1 (#x0f #xb7 #xd7) ((:reg= pc 11) (:reg= :rdx #x70f)))
  (oper-mov movslq-[rip+$211d77]-rax 8 1 (#x48 #x63 #x05 #x77 #x1d #x21 #x00) ((:reg= pc 15) (:reg= :rax 0)))
  (oper-mov movslq-eax-rbx 8 1 (#x48 #x63 #xd8) ((:reg= pc 11) (:rbx #x5060708)))
  (oper-mov movsbl-[rip+00d6b7]-eax 8 1 (#x0f #xbe #x05 #xb7 #xd6 #x00 #x00) ((:reg= pc 15) (:rax 0)))
  (oper-mov movzbl-[r10+30]-r13 8 1 (#x45 #x0f #xb6 #x6a #x30) ((:reg= pc 13) (:r13 8)))
  ; LEA 64 bit
  (oper-mov lea-rbp-$68-rcx  8 1 (#x48 #x8d #x4d #x98) ((:reg= pc 12) (:reg= :rcx #x50606a5)))
  (oper-mov lea-rbp-$b0-rsp  8 1 (#x48 #x8d #xa5 #x50 #xff #xff #xff) ((:reg= pc 15) (:reg= :rsp #x506065d)))
  (oper-mov lea-rbp-$140-r12 8 1 (#x4c #x8d #xa5 #xc0 #xfe #xff #xff) ((:reg= pc 15) (:reg= :r12 #x50605cd)))
  (oper-mov lea-rbp-$1e0-r13 8 1 (#x4c #x8d #xad #x20 #xfe #xff #xff) ((:reg= pc 15) (:reg= :r13 #x506052d)))
  (oper-mov lea-[rdx*4+0]-rax 8 1 (#x48 #x8d #x04 #x95 #x00 #x00 #x00 #x00) ((:reg= pc 16) (:reg= :rax #x14181c28)))
  (oper-mov lea-[rsp+$f]-rbx 8 1 (#x48 #x8d #x5c #x24 #x0f) ((:reg= pc 13) (:reg= :rbx #x506071b)))
  (oper-mov lea-[r14+rax*8]-rdx 8 1 (#x49 #x8d #x14 #xc6) ((:reg= pc 12) (:reg= :rdx #x2d363f46)))
  (oper-mov lea-[rip+169]-rax 8 1 (#x48 #x8d #x05 #x69 #x01 #x00 #x00) ((:reg= pc 15) (:rax #x178)))
  (oper-mov lea-[r13+rax*8]-r8 8 1 (#x4d #x8d #x44 #xc5 #x00) ((:reg= pc 13) (:r8 #x2d363f45)))
  ; LEA 32 bit
  (oper-mov lea-[rdx+rax*1]-eax 8 1 (#x8d #x04 #x02) ((:reg= pc 11) (:reg= :rax #xa0c0e12)))
  ; cmove
  (oper-mov cmove-rax-rsi 8 1 (#x48 #x0f #x44 #xf0) ((:reg= pc 12)))
  (oper-mov cmovbe-r15-rdx 8 1 (#x49 #x0f #x46 #xd7) ((:reg= pc 12) (:rdx #x5060707)))
  (oper-mov cmovne-eax-edx 8 1 (#x0f #x45 #xd0) ((:reg= pc 11) (:reg= :rdx #x5060708)))
  (oper-mov cmovg-r11-rcx  8 1 (#x49 #x0f #x4f #xcb) ((:reg= pc 12) (:rcx #x5060703)))
  (oper-mov cmovns-r11-rdx 8 1 (#x49 #x0f #x49 #xd3) ((:reg= pc 12) (:rdx #x5060703)))
  ; lods
  (oper-mov lods-[rsi]-rax 8 1 (#x48 #xad) ((:reg= pc 10) (:reg= :rax #xa0a0a0a0a000000) (:reg= :rsi #x5060706)))
  ; sbcl
  (oper-mov mov-rax-[rcx+10] 8 1 (#x48 #x89 #x41 #x10) ((:reg= pc 12) (:mem= #x5060719 8 7 6 5)))
  (oper-mov mov-[rdx-f]-al 8 1 (#x8a #x42 #xf1) ((:reg= pc 11) (:rax #x5060700)))
  (oper-mov cmovb-rbx-rsp 8 1 (#x48 #x0f #x42 #xe3) ((:reg= pc 12) (:rsp #x506070b)))
  (oper-mov cmovz-[rdi]-r9 8 1 (#x4c #x0f #x44 #x0f) ((:reg= pc 12)))
  (oper-mov cmovp-r11-rbx 8 1 (#x49 #x0f #x4a #xdb) ((:reg= pc 12) ))
  (oper-mov cmovae-r11-rbx 8 1 (#x49 #x0f #x43 #xdb) ((:reg= pc 12)))
  (oper-mov movzbq-[rdx+rax*1+1]-rsi 8 1 (#x48 #x0f #xb6 #x74 #x02 #x01) ((:reg= pc 14) (:rsi 0)))
  (oper-mov movsq-ds.rsi-es.rdi 8 1 (#x48 #xa5) ((:reg= pc 10) (:rsi #x5060706) (:rdi #x5060707) (:mem= #x5060711 0)))
  (oper-mov mov-cl-[rdx+a] 8 1 (#x88 #x4a #x0a) ((:reg= pc 11) (:mem= #x5060714 9)))
  (oper-mov mov-[rcx-f]-al 8 1 (#x8a #x41 #xf1) ((:reg= pc 11) (:rax #x5060700))) ; 8-bit moves: 88 8a b0
  (oper-mov movzbl-[r12+b]-edi 8 1 (#x41 #x0f #xb6 #x7c #x24 #x0b) ((:reg= pc 14) (:rdi 0)))
  (oper-mov movswq-[rbx+6]-rax 8 1 (#x48 #x0f #xbf #x43 #x06) ((:reg= pc 13) (:rax #xa0a))) ; movsx move and sign-extend
  ;(oper-mov mov-[rdx-f]-al 8 1 (#x48 #xff #xc0) ((:reg= pc 11)))
  (oper-mov rep-movsq-[rsi]-[rdi] 8 1 (#xf3 #x48 #xa5) ((:reg= pc 11) (:rcx 0)
                                                                      (:rsi #x50606c6)
                                                                      (:rdi #x50606c7)
                                                                      (:mem= #x5060711 0)
                                                                      (:mem= #x5060708 0 1 2)
                                                                      ;(:mem= #x5060719 10)
                                                                      ;(:mem= #x5060732 0 8)
                                                                      ;(:mem= #x506073d 0 1 1 3 4 5 6 7)
                                                                      )))

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              cmem)
          (setf checks (nconc checks (list `(:reg= pc ,,(+ 8 (length code))))))
          (setf cmem (my-make-memory #x1))
          ; initialize stacks
          ; initialize code memory to both real and test memory
          (my-make-page cmem 8)
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i 8) x))
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x1122334455667788 i)))))
          (setf cpu2 (run-cpu cpu2 8 1))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper mov-eax-eax (#x89 #xc0) ((:rax #x55667788)))
  (oper mov-1-al (#xb0 #x01) ((:rax #x1122334455667701))))

