; float and sse

(defun test-set-cpu (cpu cpu2)
  (loop for i from 0 below (length (cpu-registers cpu2)) do
    (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                              (cond
                                                ((< i 16) (logxor (logxor #x0000000000000123 i)))
                                                ((< i 29) (make-singlefloat :int (logxor (logxor #x0000000000000123 i))))
                                                (t 0))))))

(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x100))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize stacks
          (my-make-page cmem #x100)
          (test-set-cpu cpu cpu2)
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  (oper fnstenv-[rbp-20] 8 1 (#xd9 #x75 #xe0) ((:reg= pc 11)))
  (oper fldenv-[rbp-20] 8 1 (#xd9 #x65 #xe0) ((:reg= pc 11)))
  (oper ldmxcsr-[rbp-4] 8 1 (#x0f #xae #x55 #xfc) ((:reg= pc 12))))

(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              (code ',code)
              cmem)
          (setf checks (nconc checks (list `(:reg= pc ,(+ 8 (length code))))))

          (setf cmem (my-make-memory #x100))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize stacks
          (my-make-page cmem #x100)
          (test-set-cpu cpu cpu2)
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
   (oper movq-xmm1-xmm3 8 1 (#xf3 #x0f #x7e #xd9) ((:xmm3 #x43990000)))
   (oper movq-xmm0-xmm4 8 1 (#xf3 #x0f #x7e #xe0) ((:xmm4 #x43998000)))
   (oper movq-rdx-xmm0 8 1 (#x66 #x48 #x0f #x6e #xc2) ((:xmm0 #x121)))
   (oper movq-rax-xmm1 8 1 (#x66 #x48 #x0f #x6e #xc8) ((:xmm1 #x123)))
   (oper comiss-xmm1-xmm0 8 1 (#x0f #x2f #xc1) ())
   (oper movq-rdx-xmm0 8 1 (#x66 #x48 #x0f #x6e #xc2) ((:xmm0 #x121)))
   (oper cvtsi2sdq-[rbp-0x8]-xmm0 8 1 (#xf2 #x48 #x0f #x2a #x45 #xf8) ((:xmm0 0)))
   (oper sqrtsd-xmm0-xmm0 8 1 (#xf2 #x0f #x51 #xc0) ((:xmm0 #x4031857b80000000)))
   (oper cvtsd2ss-xmm0-xmm0 8 1 (#xf2 #x0f #x5a #xc0) ((:xmm0 #x43998000)))
   (oper cvtss2sd-xmm0-xmm1 8 1 (#xf3 #x0f #x5a #xc8) ((:xmm1 #x4073300000000000)))
   (oper movq-xmm0-rdx 8 1 (#x66 #x48 #x0f #x7e #xc2) ((:rdx #x43998000)))
   (oper andps-xmm0-xmm1 8 1 (#x0f #x54 #xc8) ((:xmm1 #x43990000)))
   (oper xorps-xmm1-xmm1 8 1 (#x0f #x57 #xc9) ((:xmm1 0)))
   (oper xorps-xmm2-xmm2 8 1 (#x0f #x57 #xd2) ((:xmm2 0)))
   (oper andpd-[rip+102]-xmm0 8 1 (#x66 #x0f #x54 #x05 #x02 #x01 #x00 #x00) ((:xmm0 0)))
   (oper movss-[rbp-10]-xmm0 8 1 (#xf3 #x0f #x10 #x45 #xf0) ((:xmm0 0)))
   (oper movss-xmm1-[rbp-8]  8 1  (#xf3 #x0f #x11 #x4d #xf8) ((:mem= #x120 #x99 #x43 #xff #xff #xff #xff)))
   (oper movss-xmm0-[rcx+97] 8 1 (#xf3 #x0f #x11 #x81 #x97 #x00 #x00 #x00) ((:mem= #x1ba #x80 #x99 #x43 #xff #xff #xff #xff)))
   (oper movss-xmm0-xmm2     8 1 (#xf3 #x0f #x10 #xd0) ((:xmm2 #x43998000)))
   (oper movsd-[rbp-10]-xmm0 8 1 (#xf2 #x0f #x10 #x45 #xf0) ((:xmm0 0)))
   (oper divss-xmm2-xmm1 8 1 (#xf3 #x0f #x5e #xca) ((:xmm1 #x3f806b70)))
   (oper mulss-xmm2-xmm1 8 1 (#xf3 #x0f #x59 #xca) ((:xmm1 #x47b64900)))
   (oper mulsd-xmm2-xmm1 8 1 (#xf2 #x0f #x59 #xca) ((:xmm1 #x40f6c92000000000)))
   (oper subss-xmm2-xmm1 8 1 (#xf3 #x0f #x5c #xca) ((:xmm1 #x3f800000)))
   (oper addsd-xmm2-xmm1 8 1 (#xf2 #x0f #x58 #xca) ((:xmm1 #x4083180000000000)))
   (oper movups-xmm7-[rsp-10] 8 1 (#x0f #x11 #x7c #x24 #xf0) ((:mem= #x119 #x9a #x43)))
   (oper movntdq-xmm7-[rdi] 8 1 (#x66 #x0f #xe7 #x3f) ((:mem= #x126 #x9a #x43)))
   (oper movapd-xmm1-xmm0 8 1 (#x66 #x0f #x28 #xc1) ((:xmm0 #x43990000)))
   (oper movaps-xmm0-[rbp+a0] 8 1  (#x0f #x29 #x85 #x60 #xff #xff #xff) ((:mem= #x87 #x80 #x99 #x43)))
   (oper ucomisd-[rax]-xmm0 8 1 (#x66 #x0f #x2e #x00) ())
   (oper unpcklps-xmm1-xmm0 8 1 (#x0f #x14 #xc1) ()))

