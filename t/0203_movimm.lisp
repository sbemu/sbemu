; check MOV instruction

(defun mytest-make-pages (cmem &rest addrs)
  (loop for addr in addrs do
    (my-make-page cmem addr)))


(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x40000))
          ; initialize code memory to both real and test memory
          (my-make-page cmem ,code-offset)
          (mytest-make-pages cmem #x1000 #x700 #x800 #x62c910 #x246e)
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000001234 i)))))
          (loop for i from 0 below 255 do (my-mset-u8 cmem (+ #x100 i) i))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          ;(my-make-page cmem ,code-offset)
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper movb-84-[rax+a] 8 1 (#xc6 #x40 #x0a #x84) ((:reg= pc 12) (:mem= #x123e #x84)))
  (oper movq-$0-[rbp-$48]  8 1 (#x48 #xc7 #x45 #xb8 #x00 #x00 #x00 #x00) ((:reg= pc 16)))
  (oper movq-$4109d0-[rip+$21a549] #x4123bc 1 (#x48 #xc7 #x05 #x49 #xa5 #x21 #x00 #xd0 #x09 #x41 #x00) ((:reg= pc #x4123c7) (:mem= #x62c910 #xd0 9 #x41)))
  (oper mov-$410bf0-[rbp-$140] 8 1 (#x48 #xc7 #x85 #xc0 #xfe #xff #xff #xf0 #x0b #x41 #x00) ((:reg= pc #x13) (:mem= #x10f1 #xf0 11 #x41)))
  (oper mov-$fffffffffffffff8-rsi 8 1 (#x48 #xc7 #xc6 #xf8 #xff #xff #xff) ((:reg= pc 15) (:reg= :rsi #xfffffffffffffff8)))
  (oper movq-$0-[r14+rax*1] 8 1 (#x49 #xc7 #x04 #x06 #x00 #x00 #x00 #x00) ((:reg= pc 16)))
  (oper movq-0-[rax]        8 1 (#x48 #xc7 #x00 #x00 #x00 #x00 #x00) ((:reg= pc 15)))
  (oper movq-$40e080-[rax] 8 1 (#x48 #xc7 #x00 #x80 #xe0 #x40 #x00) ((:reg= pc 15) (:mem= #x1234 128 224 64)))
  (oper movq-0-[r12+$18] 8 1 (#x49 #xc7 #x44 #x24 #x18 #x00 #x00 #x00 #x00) ((:reg= pc 17)))
  (oper movq-$30-[$00000230] 8 1 (#x48 #xc7 #x04 #x25 #x30 #x02 #x00 #x00 #x30 #x00 #x00 #x00) ((:reg= pc 20) (:mem= #x230 #x30)))
  (oper mov-$40000004-[rbp-$b8]  8 1 (#xc7 #x85 #x48 #xff #xff #xff #x04 #x00 #x00 #x40) ((:reg= pc 18) (:mem= #x1179 4 64 0 64)))
  (oper movb-$7f-[rbp+rax-$1020] 8 1 (#xc6 #x84 #x05 #xe0 #xef #xff #xff #x7f) ((:reg= pc 16) (:mem= #x1445 127)))
  (oper movw-$0-[rdx+8] 8 1 (#x66 #xc7 #x42 #x08 #x00 #x00) ((:reg= pc 14)))
  (oper movw-1000-[rax+8] 8 1 (#x66 #xc7 #x40 #x08 #x00 #x10) ((:reg= pc 14) (:mem= #x123d #x10))))

