; misc stuff

(macrolet
  ((oper (name code-offset runlength code &optional checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              (code   ',code)
              cmem)
          (setf checks (maybe-add-pc-check checks code ,code-offset))
          (setf cmem (my-make-memory #x800000))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize stacks
          ;(my-make-page #x0fffe000) ; ???
          ;(my-make-page cmem (- #x1fffe000 1)) ; stack mapping
          ;(my-make-page cmem (- #x1fffe000 8191))
          ;(my-make-page cmem (- #x1fffe000 8192))
          ;(my-make-page cmem (- #x1fffe000 8193))
          (my-make-page cmem #x50606b5)
          (my-make-page cmem #x100b48)
          (my-make-page cmem #xc0e1c)
          (my-make-page cmem #x405060704)
          (my-make-page cmem #x60708)
          (loop for i from #x405060700 to #x405060730 do (my-mset-u8 cmem i (logxor (logand #xff i) #xaa)))
          ;(make-page #x1fffe000) ; ???
          ; initialize memory
          ;(loop for i from 1960 to 1980 do (my-mset-u8 cmem i (- i 1959)))
          ; initialize registers
          ; create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000060708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  (oper in-$53-eax 8 1 (#xe5 #x53) ())
  (oper xchg.2 8 1 (#xf0 #x48 #x87 #x18) ((:rbx 0) (:mem= #x60708 11 7 6)))
  (oper lock-cmpxchg-rdx-$00100b48 8 1 (#xf0 #x48 #x0f #xb1 #x14 #x25 #x48 #x0b #x10 #x00) ((:rax 0)))
  (oper lock-cmpxchg-rcx-[rdx-rbx*1+7] 8 1 (#xf0 #x48 #x0f #xb1 #x4c #x1a #x07) ((:rax 0)))
  (oper prefetch0-[rcx] 8 1 (#x0f #x18 #x09) ()))

(macrolet
  ((oper (name code-offset runlength code &optional checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              (code   ',code)
              cmem)
          (setf checks (maybe-add-pc-check checks code ,code-offset))
          (setf cmem (my-make-memory 128))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          ; initialize stacks
          ; create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000060708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  (oper rdtsc  8 1 (#x0f #x31) ((:rax 0) (:rdx 0)))
  (oper mfence 8 1 (#x0f #xae #xf0) ())
  (oper cpuid  8 1 (#x0f #xa2) ())
  (oper cmc    8 1 (#xf5) ())
  (oper xchg   8 1 (#x48 #x87 #xd1) ((:rcx #x6070a) (:rdx #x60709))))

