
(defun compile-subtest-function (&key code checks state vars)
  (let ((initvals (loop repeat (length vars) collect nil))
        c)
    (setf c
          `(lambda (oper cpu cpu2 &key vals)
               (ecase oper
                 (:asm
                   (destructuring-bind ,vars vals
                     (declare (ignorable ,@vars))
                     ,@code))
                 (:state
                   (destructuring-bind ,vars (list ,@initvals)
                     (declare (ignorable ,@vars))
                     ,@state
                     (list ,@vars)))
                 (:check-regs
                   (destructuring-bind ,vars vals
                     (declare (ignorable ,@vars))
                     ;(format t "about to check, x=~x, y=~x~%" x y)
                     (check-regs cpu cpu2
                                 ; this filter is needed when we have an redundant check
                                 ; which happens when the test computes to initial register value
                                 (delete nil (loop for (name value) in (list ,@checks) collect
                                               (unless (zerop value)
                                                 (list name value))))))))))
    ;(format t "~%compiling subtest: ~s~%" c)
    (compile nil c)))

(defun test-sign-extend-64 (x)
  (logior (ash (logand (- (ash x -31)) (1- (ash 1 32))) 32) x))

(defun random-int8 ()
  (case (random 32)
    (0 0)
    (1 1)
    (2 2)
    (3 (1- (ash 1 7))) ; largest signed 8bit
    (4 (ash 1 7))      ; most negative signed 8bit
    (5 (1+ (ash 1 7))) ; second most negative signed 8bit
    (6 (1- (ash 1 8))) ; largest unsigned 8bit (negative), or smallest negative
    (7 (1- (1- (ash 1 8)))) ; second largest unsigned 8bit, or second smallest negative
    (t (random 256))))

(defun random-int32 ()
  (case (random 32)
    (0 0)
    (1 1)
    (2 2)
    (3 (1- (ash 1 31))) ; largest signed 32bit
    (4 (ash 1 31))      ; most negative signed 32bit
    (5 (1+ (ash 1 31))) ; second most negative signed 32bit
    (6 (1- (ash 1 32))) ; largest unsigned 32bit (negative), or smallest negative
    (7 (1- (1- (ash 1 32)))) ; second largest unsigned 32bit, or second smallest negative
    (t (random (ash 1 32)))))

(defun random-int64 ()
  (case (random 32)
    (0 0)
    (1 1)
    (2 2)
    (3 (1- (ash 1 63))) ; largest signed 64bit
    (4 (ash 1 63))      ; most negative signed 64bit
    (5 (1+ (ash 1 63))) ; second most negative signed 64bit
    (6 (1- (ash 1 64))) ; largest unsigned 64bit (negative), or smallest negative
    (7 (1- (1- (ash 1 64)))) ; second largest unsigned 64bit, or second smallest negative
    (t (test-sign-extend-64 (random (ash 1 32))))))

(defun test-imul-overflow (x z y)
  (let ((s 0) p n h)
    (setf s (logxor s (ash x -63)))
    (setf s (logxor s (ash z -63)))
    (cond
      ((not (zerop s))
       ;(setf n (1+ (ash y -64)))
       ;(loop while (zerop (logand n 1)) do (setf n (ash n -1))) ; shrink to rightmost bit
       ;(not (zerop (logxor 1 n))) ; more than one bit in q

        (setf p (ash y -63)) ; when checking for holes, include bit 63 too.
        (loop for i from 63 downto 0 do
          (cond
            ((logbitp i p) (setf n t))
            (t
              (if n (setf h t)))))
        (if (or h (not n)) t))
      ((not (zerop (ash y -63))) t))))

(defun x86-random-mrm-direct (rex)
  (declare (ignore rex))
  (flet ((name (n)
           (case n
             (0 :rax)
             (1 :rcx)
             (2 :rdx)
             (3 :rbx)
             (4 :rsp)
             (5 :rbp)
             (6 :rsi)
             (7 :rdi))))
  (let ((src (random 8))
        (dst (random 8)))
    (loop while (= src dst) do (setf dst (random 8)))
    (list (logior #b11000000
                  (ash dst 3)
                  src)
          (name src)
          (name dst)))))

(macrolet
  ((oper (name &key code checks state vars)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ,checks)
              (runlength (length ',code))
              cmem fun vals (num 100) (pass 0))
          (setf cmem (my-make-memory 1))
          (my-make-page cmem 0)
          (setf checks (loop for pair in checks collect `(list ,@pair))) ; convert to an constructor-list
          (setf fun (compile-subtest-function :code ',code
                                              :checks checks
                                              :state ',state
                                              :vars ',vars))
          (flet
            ((run ()
               (asm-init cmem 8) ; start assembly at 8
               (loop for i from 0 below (length (cpu-registers cpu2)) do
                 (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i) 0)))
               (setf vals (funcall fun :state cpu cpu2))
               (L "subtest using state ~x~%" vals :2)
               (funcall fun :asm cpu cpu2 :vals vals)
               (setf cpu2 (run-cpu cpu2 8 runlength))
               ; check what differs
               (funcall fun :check-regs cpu cpu2 :vals vals)
               (check-mems cmem checks)
               (incf pass)))
            (loop repeat num do
              (if *debugger* (run)
                             (ignore-errors (run)))))
          (list 123 pass (- num pass))))))
  (oper asm-imul :code ((asm :mov x src)
                        (asm :raw #x48 #x6b mrm z)  ; imul i-src-dst
                        (asm :raw #x71 #x7f))       ; jno
                 :vars (x y z o fpc src dst mrm)
                 :state ((destructuring-bind (m s d) (x86-random-mrm-direct nil)
                           (setf mrm m)
                           (setf src s)
                           (setf dst d)
                           (setf z (random 128)) ; 7bit FIX: use random-int8, bug in imul
                           (setf x (test-sign-extend-64 (random-int32))) ; make a 32bit integer, sign extend it to 64bit
                           (setf y (* x z)) ; do the imul operation
                           (setf o (test-imul-overflow x z y))
                           (setf y (logand (1- (ash 1 64)) y))
                           (setf fpc (if o #x15 #x94))))
                 :checks `((:pc fpc) (src x) (dst y)))
  (oper asm-add  :code ((asm :mov8 x src)
                        (asm :mov8 z dst)
                        (asm :raw #x48 #x01 mrm)  ; add-src-dst
                        (asm :raw #x71 #x79))     ; jno (FIX: more interesting to check carry?)
                 :vars (x y z o fpc src dst mrm)
                 :state ((destructuring-bind (m d s) (x86-random-mrm-direct nil)
                           (setf mrm m)
                           (setf src s)
                           (setf dst d)
                           (setf x (random-int64))
                           (setf z (random-int64))
                           (setf y (+ x z)) ; do the add operation
                           (let ((parity 0))
                             (if (logbitp 63 x) (incf parity))
                             (if (logbitp 63 z) (incf parity))
                             (if (and (evenp parity) (not (eq (logbitp 63 y) (logbitp 63 x))))
                               (setf o t)))
                           (setf y (logand (1- (ash 1 64)) y))
                           (setf fpc (if o #x21 #x9a))))
                 :checks `((:pc fpc) (src x) (dst y))))

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              (runlength (length ',code))
              cmem)
          (setf cmem (my-make-memory 1000))
          (my-make-page cmem 0)
          (asm-init cmem 8) ; start assembly at 8
          ,@code
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000023 i)))))
          (my-mset-u8 cmem #x97 #x01)
          (my-mset-u8 cmem #x9f #x40)
          (my-mset-u8 cmem #x36 #x00)
          ; run code
          (setf cpu2 (run-cpu cpu2 8 runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper asm-imul0 ((asm :raw #x48 #xba #xfd #xff #xff #xff #xff #xff #xff #xff)
                   (asm :raw #x48 #x6b #xc2 #x08)  ; imul-8-rdx
                   (asm :raw #x71 #x0e))           ; jno x
                  ((:reg= pc #x26) (:rdx #xfffffffffffffffd) (:rax #xffffffffffffffe8)))
  (oper asm-imul1 ((asm :mov #x1f40 :rdi)
                   (asm :raw #x48 #xb8 #x00 #x15 #x3d #x90 #x89 #x3d #x6 0) ; set rax
                   (asm :raw #x48 #xf7 #xef)      ; imul-rdi (should overflow)
                   (asm :raw #x71 #x0e))          ; jno x
                  ((:reg= pc #x1e) (:rdi #x1f40) (:rdx 0) (:rax #xc3030adb74d04000)))
  (oper asm-imul2 ((asm :mov #x40 :rdi)
                   (asm :raw #x48 #xb8 #xf4 #xff #xff #xff #xff #xff #xff #xff) ; rax=fffffffffffffff4 (-12)
                   (asm :raw #x48 #xba #xa0 #xff #xff #xff #xff #xff #xff #xff) ; rdx=ffffffffffffffa0 (-96)
                   (asm :raw #x48 #xf7 #xef)                 ; imul-rdi
                   (asm :raw #x0f #x81 #x95 #x00 #x00 #x00)) ; jno should jump
                  ((:reg= pc #xc1) (:rdi #x40) (:rax #xfffffffffffffd00) (:rdx #xffffffffffffffff)))
  (oper asm-imul3 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #xff #x7f)
                   (asm :raw #x48 #x6b #xd0 #x08)  ; imul 8-rax-rdx
                   (asm :raw #x71 #x0e))           ; jno x
                  ((:reg= pc #x18) (:rax #x7fffffffffffffff) (:rdx #xfffffffffffffff8)))
  (oper asm-imul4 ((asm :mov 6 :rax)
                   (asm :raw #x48 #x6b #xd0 #x08)  ; imul 8-rax-rdx
                   (asm :raw #x71 #x7f))           ; jno x
                  ((:reg= pc #x94) (:rax 6) (:rdx #x30)))
  (oper asm-imul5 ((asm :mov 1 :r12)
                   (asm :mov 1 :r13)
                   (asm :raw #x4d #x0f #xaf #x6c #x24 #x30))  ; imul-[r12+30]-r13
                  ((:reg= pc #x1c) (:r12 1) (:r13 0)))
  (oper asm-mul6 ((asm :mov 1 :rax)
                  (asm :mov 0 :rdx)
                  (asm :raw #x48 #xb8 #xcd #xcc #xcc #xcc #xcc #xcc #xcc #xff)
                  (asm :raw #x48 #xf7 #x25 #x70 #x00 #x00 #x00)) ; mul-[rip+70]
                 ((:reg= pc #x27) (:rax #xffcccccccccccccd) (:rdx 0)))
  (oper asm-idiv0 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #xff #xff)
                   (asm :raw #x48 #xba #xff #xff #xff #xff #xff #xff #xff #xff)
                   (asm :mov 4 :rcx)
                   (asm :raw #x48 #xf7 #xf9)) ; idiv-rcx
                  ((:reg= pc #x26) (:rax 0) (:rdx -1) (:rcx 4)))
  (oper asm-idiv1 ((asm :raw #x48 #xb8 #xfb #xff #xff #xff #xff #xff #xff #xff)
                   (asm :raw #x48 #xba #xff #xff #xff #xff #xff #xff #xff #xff)
                   (asm :raw #x48 #xb9 #xfc #xff #xff #xff #xff #xff #xff #xff)
                   (asm :raw #x48 #xf7 #xf9)) ; idiv-rcx
                  ((:reg= pc #x29) (:rax 1) (:rdx -1) (:rcx #xfffffffffffffffc)))
  (oper asm-div2 ((asm :mov 64 :rbp) ; write at some unused area
                  (asm :raw #x48 #xb8 #x50 #x42 #x0a #xa0 #xb0 #xa8 #x2c #x42)
                  (asm :raw #x48 #x89 #x45 #x40)           ; mov rax-[rbp]
                  (asm :raw #x48 #xb8 #xdc #x9e #x26 #xfd #xe7 #xdb #x2d #x62) ; set rax
                  (asm :raw #x48 #xba #x5a #xf6 #xe5 #xc0 #x01 #x00 #x00 #x00) ; set rdx
                  (asm :raw #x48 #xf7 #x75 #x40)) ; div-[rbp-50]
                 ((:reg= pc #x35) (:rbp 64) (:rdx #x23b36bd4d393d2cc) (:rax #x6c897556d) (:mem= #x80 #x50 #x42 #x0a #xa0 #xb0 #xa8 #x2c #x42)))
  (oper asm-sub0 ((asm :raw #x48 #xbf #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80)
                  (asm :mov 24 :rdx)
                  (asm :raw #x48 #x29 #xfa) ; sub rdi-rdx
                  (asm :raw #x71 #x77))     ; jno should not jump
                 ((:reg= pc #x1e) (:rdi #x8000000000000000) (:rdx #x8000000000000018)))
  (oper asm-sbb0 ((asm :raw #x48 #xb8 #x00 #xff #xff #xff #xff #xff #xff #xff)
                  (asm :raw #xf9)            ; set-carry
                  (asm :raw #x83 #xd8 #x03)) ; sbb-3-esi
                 ((:reg= pc #x16) (:rax #xfffffffffffffefc)))
  (oper asm-add0 ((asm :raw #x48 #x01 #xfa)  ; add-rdi-rdx (should not set flags SO)
                  (asm :raw #x71 #x77))      ; jno-x should jump
                 ((:reg= pc #x84) (:rdx #x45)))
  (oper asm-add1 ((asm :raw #x48 #xba #xe0 #xff #xff #xff #xff #xff #xff #x7f) ; rdx 7fffffffffffffe0
                  (asm :raw #x48 #xbf #xf8 #x01 #x00 #x00 #x00 #x00 #x00 #x00) ; rdi 1f8
                  (asm :raw #x48 #x01 #xfa)  ; add-rdi-rdx (should set flags SO)
                  (asm :raw #x71 #x77))      ; jno-x should not jump
                 ((:reg= pc #x21) (:rdx #x80000000000001d8) (:rdi #x1f8)))
  (oper asm-add2 ((asm :raw #x48 #xb8 #x00 #x00 #x00 #x01 #x00 #x00 #x00 #xff) ; rax fffffff000000000
                  (asm :raw #x48 #x03 #x45 #xe0)) ; add-[rbp+08]-rax, rax shouldn't be larger than 64bit(> 2^64-1)
                 ((:reg= pc #x16) (:rax #xb9480000)))
  (oper asm-add3 ((asm :mov #x-b8 :rdx) ; FFFFFFFFFFFFFF48
                  ;(asm :raw #x48 #xba #x48 #xff #xff #xff #xff #xff #xff #xff) ; rdx
                  (asm :raw #x48 #xbf #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80) ; rdi
                  (asm :raw #x48 #x01 #xfa)  ; add-rdi-rdx (should set flags SO)
                  (asm :raw #x73 #x44))       ; jae should not jump
                 ((:reg= pc #x1e) (:rdx #x7fffffffffffff48) (:rdi #x8000000000000000)))
  (oper asm-add4 ((asm :mov -1 :rax)
                  (asm :raw #x48 #x39 #xc8) ; cmp-rcx-rax just to set the S flag
                  (asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x40) ; rax
                  (asm :mov #x48e96 :rsi)
                  (asm :raw #x48 #x01 #xf0)  ; add-rsi-rax should clear the S flag
                  (asm :raw #x0f #x88 #xf4 #x0 #x0 #x0)) ; js should not jump (because S is not set)
                 ((:reg= pc #x2c) (:rax #x4000000000048e96) (:rsi #x48e96)))
  (oper asm-neg0 ((asm :mov 0 :rdx)
                  (asm :raw #x48 #xf7 #xda)  ; neg-rdx
                  (asm :raw #x75 #x44)       ; jae should not jump
                  (asm :raw #x71 #x77))      ; jno should jump
                 ((:reg= pc #x8d) (:rdx 0)))
  (oper asm-neg1 ((asm :raw #x48 #xba #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80)
                  (asm :raw #x48 #xf7 #xda)  ; neg-rdx
                  (asm :raw #x71 #x77)       ; jno should not jump
                  (asm :raw #x73 #x77))      ; jae should not jump
                 ((:reg= pc #x19) (:rdx #x8000000000000000)))
  (oper asm-neg2 ((asm :mov #x180 :rdx)
                  (asm :raw #x48 #xf7 #xda)  ; neg-rdx
                  (asm :raw #x73 #x44)       ; jae should not jump
                  (asm :raw #x71 #x77))      ; jo  should not jump
                 ((:reg= pc #x8d) (:rdx #xfffffffffffffe80)))
  (oper asm-dec0 ((asm :mov 0 :rbx)
                  (asm :raw #xff #xcb)  ; dec-ebx
                  (asm :raw #x79 #x7f)) ; jns-x should not jump
                 ((:reg= pc #x13) (:rbx #xffffffffffffffff))))

