; assemble, run and verify
; note: we are eating our own food. For a more real-life testing see 'r' directory

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              (runlength (length ',code))
              cmem)
          (setf cmem (my-make-memory 1000))
          (my-make-page cmem 0)
          (asm-init cmem 8) ; start assembly at 8
          ,@code
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below #x1d do ; registers 0-29 is real (not psuedo)
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000023 i)))))
          (my-mset-u8 cmem #x97 #x01)
          (my-mset-u8 cmem #x9f #x40)
          (my-mset-u8 cmem #x36 #x00)
          ; run code
          (setf cpu2 (run-cpu cpu2 8 runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  ; cmp rdx=1000000010 [...]=10 sets flags CS
  (oper asm-cmp1 ((asm :raw #x49 #x39 #x54 #x24 #x68) ; cmp-rdx-[r12+68]
                  (asm :raw #x76 #x0c))               ; jbe-x
                 ((:reg= pc #x1b)))                   ; should branch
  (oper asm-cmp2 ((asm :raw #x49 #x39 #x54 #x24 #x70) ; cmp-rdx-[r12+68]
                  (asm :raw #x76 #x0c))               ; jbe-x
                 ((:reg= pc #xf)))                    ; should not branch
  (oper asm-cmp3 ((asm :raw #x48 #x3b #xb5 #x10 #x00 #x00 #x00) ; cmp-[rbp+10]-rsi
                  (asm :raw #x0f #x8f #x63 #x01 #x00 #x00))     ; jg-x
                 ((:reg= pc #x178)))                            ; should branch
  (oper asm-cmp4 ((asm :raw #x48 #xb9 #x00 #x00 #x00 #x00 #xfc #xff #xff #xff) ; movabs-fffffffc00000000-rcx
                  (asm :mov #x10 :rax)
                  (asm :raw #x48 #x39 #xc8) ; cmp-rcx-rax
                  (asm :raw #x7c #x64))     ; jl +64 should not jump
                 ((:reg= pc #x1e) (:rax 16) (:rcx #xfffffffc00000000)))
  (oper asm-cmp5 ((asm :mov 3 :r10)
                  (asm :mov 2 :r11)
                  (asm :raw #x45 #x39 #xd3) ; cmp-r10-r11
                  (asm :raw #x78 #x69))     ; js should jump
                 ((:reg= pc #x84) (:r10 3) (:r11 2)))
  (oper asm-cmp6 ((asm :mov #x333bca2 :rax)
                  (asm :raw #x3c #x16)        ; cmp-16-rax
                  (asm :raw #x0f #x86 #x89))  ; jbe should not jump
                 ((:reg= pc #x17) (:rax #x333bca2)))
  (oper asm-cmp7 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #x00 #x00 #x00 #x00) ; eax = -1 (32bit)
                  (asm :raw #x85 #xc0)  ; test-eax-eax
                  (asm :raw #x78 #x6d)) ; js should jump
                 ((:reg= pc #x83) (:rax #xffffffff)))
  (oper asm-cmp8 ((asm :raw #x48 #xb8 #xff #xff #x00 #x00 #x00 #x00 #x00 #x00) ; eax = -1 (16bit)
                  (asm :raw #x66 #x85 #xc0)  ; test-eax-eax
                  (asm :raw #x78 #x6d)) ; js should jump
                 ((:reg= pc #x84) (:rax #xffff)))
  (oper asm-cmp9 ((asm :mov 0 :rbx)
                  (asm :raw #x48 #x81 #xfb #x00 #xfc #xff #xff)  ; test-eax-eax
                  (asm :raw #x7c #x11)) ; jl should not jump, flags OS should be cleared
                 ((:reg= pc #x18) (:rbx 0)))
  (oper asm-cmp10 ((asm :mov #x60 :rbx)
                   (asm :raw #x48 #x81 #xfb #xf8 #x01 #x00 #x00)  ; cmp-1f8-rbx
                   (asm :raw #x7f #x11)) ; jl should not jump, SC is set, O is cleared
                  ((:reg= pc #x18) (:rbx #x60)))
  (oper asm-cmp11 ((asm :mov #x1a000 :r15)
                   (asm :raw #x49 #x81 #xff #x00 #x10 #x00 #x00)  ; cmp-1000,r15
                   (asm :raw #x0f #x86 #x89))  ; jbe should not jump
                  ((:reg= pc #x1c) (:r15 #x1a000)))
  (oper asm-cmp12 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #x00 #x00 #x00 #x00) ; eax = -1 (32bit)
                   (asm :raw #x83 #xf8 #xff)  ; cmp-ffffffffffffffff-eax
                   (asm :raw #x0f #x84 #x8c #x00 #x00 #x00)) ; je should jump (cmp is 4 bytes only)
                  ((:reg= pc #xa7) (:rax #xffffffff)))
  (oper asm-cmp13 ((asm :raw #x48 #xbf #xff #xff #xff #xff #xff #xff #xff #x7f) ; movabs-7ffffffffffffff8-rdi
                   (asm :raw #x48 #xba #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80) ; movabs-8000000000000000-rdx
                   (asm :raw #x48 #x39 #xfa) ; cmp-rdi-rdx (should set O-flag)
                   (asm :raw #x7f #x7f))     ; jg should not jump
                  ((:reg= pc #x21) (:rdx #x8000000000000000) (:rdi #x7fffffffffffffff)))
  (oper asm-cmp14 ((asm :raw #x48 #xba #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80) ; rdx=8000000000000000
                   (asm :raw #x48 #xbf #xf8 #xff #xff #xff #xff #xff #xff #xff) ; rdi=fffffffffffffff8
                   (asm :raw #x48 #x39 #xfa) ; cmp-rdi-rdx (should set CS-flag)
                   (asm :raw #x7c #x7f))     ; jl should jump
                  ((:reg= pc #xa0) (:rdx #x8000000000000000) (:rdi #xfffffffffffffff8)))
  (oper asm-cmp15 ((asm :mov #xfe0 :rax)
                   (asm :raw #x66 #x3d #xff #x0f) ; cmp-fff-ax
                   (asm :raw #x77 #x8b))          ; ja should not jump
                  ((:reg= pc #x15) (:rax #xfe0)))
  (oper asm-cmp16 ((asm :mov #xc18 :rax)
                   (asm :raw #x48 #x3d #x00 #x00 #x00 #xfc) ; cmp-fffffffffc000000-rax
                   (asm :raw #x7c #x7f))     ; jl should not jump
                  ((:reg= pc #x17) (:rax #xc18)))
  (oper asm-testb ((asm :mov #xc4 :rdx)
                   (asm :raw #x84 #xd2)  ; test-dl-dl
                   (asm :raw #x79 #x7f)) ; jns should not jump
                  ((:reg= pc #x13) (:rdx #xc4)))
  (oper asm-tests ((asm :mov #xff0000 :r11)
                   (asm :raw #x66 #x45 #x85 #xdb) ; test-r11w-r11w
                   (asm :raw #x75 #x7f))          ; je should not jump
                  ((:reg= pc #x15) (:r11 #xff0000)))
  (oper asm-testw ((asm :mov #x10000000 :rax)
                   (asm :raw #x48 #x85 #xc0) ; test-rax-rax
                   (asm :raw #x74 #x7f))     ; je should not jump
                  ((:reg= pc #x14) (:rax #x10000000)))
  (oper asm-bt ((asm :mov 8 :rax) ; first operand is bit string
                (asm :mov 3 :rdx) ; second operand is bit index
                (asm :raw #x48 #x0f #xa3 #xd0) ; bt-rdx-rax
                (asm :raw #x72 #x55))          ; jb-x  should/not jump
               ((:reg= pc #x71) (:rax 8) (:rdx 3))))

