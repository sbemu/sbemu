; check memory read/write

(macrolet
  ((mem-read-write (name roper woper bytes)
     `(deftest (,name)
        (setf *mem* (make-array 256 :initial-element nil :fill-pointer 0 :adjustable t))
        (setf *memw* (make-array 256 :initial-element 7))
        (setf *memsz* 256)
        (make-page 0)
        (loop for i from 0 below 8 do ; check basic read-write
          (,woper i i)
          (assert (= (,roper i) i)))
        (loop for i from 0 below 8 do ; check basic read-write
          (,woper (* i ,bytes) i)
          (assert (= (,roper (* i ,bytes)) i)))
        (,woper (* 0 ,bytes) 9) ; write interleaving, to check we dont overwrite between
        (,woper (* 2 ,bytes) 9)
        (,woper (* 4 ,bytes) 9)
        (,woper (* 6 ,bytes) 9)
        (loop for i from 0 below 8
              for x in '(9 1 9 3 9 5 9 7) do
          (assert (= (,roper (* i ,bytes)) x)))
        123)))
  (mem-read-write mem-u8  mget-u8  mset-u8  1)
  (mem-read-write mem-u16 mget-u16 mset-u16 2)
  (mem-read-write mem-u32 mget-u32 mset-u32 4)
  (mem-read-write mem-u64 mget-u64 mset-u64 8))

(macrolet
  ((oper (name roper woper value)
     `(deftest (,name)
        (setf *mem* (make-array 256 :initial-element nil :fill-pointer 0 :adjustable t))
        (setf *memw* (make-array 256 :initial-element 7))
        (setf *memsz* 256)
        (make-page 0)
        (loop for i from 0 below 8 do ; check basic read-write
          (,woper i ,value)
          (assert (= (,roper i) ,value)))
        123)))
  (oper mem-u8  mget-u8  mset-u8  #xff)
  (oper mem-u16 mget-u16 mset-u16 #xffff)
  (oper mem-u32 mget-u32 mset-u32 #xffffffff)
  (oper mem-u64 mget-u64 mset-u64 #xffffffffffffffff)
  (oper mem-u8  mget-u8  mset-u8  #x87)
  (oper mem-u16 mget-u16 mset-u16 #x8765)
  (oper mem-u32 mget-u32 mset-u32 #x87654321)
  (oper mem-u64 mget-u64 mset-u64 #x9876543210abcdef))

