;
; mips float instruction test
; we dont check the functionallity in depth but more the 'around stuff' like instruction decoding.
;

(defun test-mips-set-cpu (cpu cpu2)
  (loop for i from 0 below (length (cpu-registers cpu2)) do
    (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                              (logxor (logxor #x0000000000000123 i))))))

(defun test-mips-regular-test (memsize checks code)
  (let ((code-offset 8)
        (*print-pretty* nil)
        (cpu  (setf *cpu1* (make-cpu-mips)))
        (cpu2 (setf *cpu2* (make-cpu-mips)))
        cmem)
    (setf checks (maybe-add-pc-check checks code code-offset))
    (setf cmem (my-make-memory memsize))
    (my-make-page cmem 0)
    ; initialize code memory to both real and test memory
    (loop for i from 0 for x in code do (my-mset-u8 cmem (+ i code-offset) x))
    ; initialize stacks
    (my-make-page cmem #x100)
    (test-mips-set-cpu cpu cpu2)
    (run-cpu cpu2 code-offset 1) ; run code
    (check-regs cpu cpu2 checks) ; check what differs
    (check-mems cmem checks)
    123))

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (test-mips-regular-test #x100 ',checks ',code))))
   (oper mtc1-a0-$f12 (#x44 #x84 #x60 #x00) ((:f12 #x127)))
   (oper mfc1-f0-a1   (#x44 #x05 #x00 #x00) ((:a1 #x101)))
   (oper cvt.d.w-f12-f12 (#x46 #x80 #x63 #x21) ((:f12 0) (:f13 #x4070d000)))
   (oper cvt.s.d-f0-f0   (#x46 #x20 #x00 #x20) ((:f0 257)))
   (oper lwc1-[t3-3]-f0 (#xc5 #x60 #xff #xfd) ((:f0 0)))
   (oper swc1-f0-[t0-3] (#xe5 #x00 #xff #xfd) ((:mem= #x12a 1 1)))
   (oper div.d-f0-f2-f0 (#x46 #x22 #x00 #x03) ((:f0 #x9e4a4271) (:f1 #x3fef4465)))
   (oper mul.s-f0-f2-f0 (#x46 #x02 #x00 #x02) ((:f0 0)))
   (oper mul.s-f0-f2-f0 (#x46 #x02 #x00 #x02) ((:f0 0)))
   (oper mov.s-f0-f4   (#x46 #x00 #x01 #x06) ((:f4 #x101)))
   (oper abs.s-f0-f0   (#x46 #x00 #x00 #x05) ((:f0 0)))
   (oper abs.d-f0-f0   (#x46 #x20 #x00 #x05) ((:f0 #x80) (:f1 #x80)))
   (oper c.lt.s-f2-f0  (#x46 #x02 #x00 #x3c) ())
   (oper c.lt.d-f2-f0  (#x46 #x22 #x00 #x3c) ())
   (oper neg.d-f0-f0   (#x46 #x20 #x00 #x07) ((:f1 #x80000100)))
   (oper c.seq.s-f2-f0 (#x46 #x02 #x00 #x3a) ())
   (oper c.ngt.d-f2-f0 (#x46 #x22 #x00 #x3f) ())
   (oper bc1t-x        (#x45 #x01 #x00 #x04) ())
   (oper bc1f-x        (#x45 #x00 #x00 #x4b) ())
   )
