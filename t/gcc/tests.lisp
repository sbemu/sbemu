; 1) generate a c-code file that contains a function named foo that will return 123
; 2) extract the dissasembly and bytes of the foo function using an external tool
; 3) run the bytes through the emulator and check that it also returns 123
;
; if a test fails it is possible to compare it side by side with gdb.
;
; TEST-FORM
; test name
; code-blit-address
; run-max-instructions
; argument-1
; argument-2
; c-function-call-string
; c-function-body-list)

(in-package :sbemu)

; a simple and static test to begin with
(test-form :a0 40
  "foo(100,23, 'A', \"hej\")"
  '("int foo (int a, int b, char c, char *d)
    {
      return a + b;
    }"))

(let* ((a (random 100))
       (b (random 100))
       (c (+ 123 a b)))
  (test-form :a1 40
    (format nil "foo(~a,~a, 'A', \"hej\")" a b)
    (list (format nil "int foo (int a, int b, char c, char *d)
                       { return ~a - a - b; }" c))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :a2 40
    (format nil "foo(~a)" a)
    (list (format nil "int bar (int a) { return 0; }
                       int foo (int a)
                       { bar(a); return 123; }"))))

(let* ((n (1+ (random 10)))
       (a (- (random (ash 1 32)) (ash 1 31)))
       (b (- (random (ash 1 32)) (ash 1 31))))
  (test-form :a3 4000
    (format nil "foo(~a, ~a)" a b)
    (list (format nil "int min (int a, int b) { return a - b; }
                       int plu (int a, int b) { return a + b; }
                       int dii (int a, int b) { return a / b; }
                       int mul (int a, int b) { return a * b; }
                       int bar (int a) {
                         int i, x = 0;
                         for(i = 0; i < ~a; i++){
                           x = min(x, a);
                           x = plu(x, i);
                           x = mul(x, i);
                           x = dii(x, 2);
                         }
                         return x;
                       }
                       int foo (int a, int b)
                       {
                         int x = 0;
                         x += bar(a);
                         x -= bar(b);
                         x -= bar(a);
                         x += bar(b);
                         return x + 123;
                       }" n))))

; come up with a nice scheme to autogenerate c-code, to avoid having to write c manually
;(cfun bar (a)
;  (creturn (c+ (c- 'a a) 123)))
; =>
;("int" "bar" "(" "a" ")"
;  ("return" (("a" "-" a) "+" 123)))
; =>
; int bar ( a ) 
;    return (((a - 5678) + 123));
(let (ret)
(labels ((addstr (str) (setf ret (concatenate 'string ret str)))
         ;(emit (&rest args) (addstr (apply 'format nil args)))
         (symstr (sym) (string-downcase (string sym)))
         (funi1  (name arg body) (addstr (format nil "int ~a (int ~a){~%~a}~%"
                                                     (symstr name) (symstr arg) body))))
  (setf ret "")
  (let ((a (- (random (ash 1 32)) (ash 1 31))))
    (funi1 'bar 'a (format nil "return a - ~a + 123;" a))
    (test-form :a4 40
      (format nil "bar(~a)" a)
      (list ret)))

  (setf ret "")
  (let ((a (- (random (ash 1 8)) (ash 1 7))))
    (funi1 'bar 'a (format nil "return ((a ^ ~a) + 123);" a))
    (test-form :xor1 40
      (format nil "bar(~a)" a)
      (list ret)))))

; come up with a nice scheme to autogenerate c-code, to avoid having to write c manually
;(cfun bar (a)
;  (creturn (c+ (c- 'a a) 123)))
; =>
;("int" "bar" "(" "a" ")"
;  ("return" (("a" "-" a) "+" 123)))
; =>
; int bar ( a ) 
;    return (((a - 5678) + 123));

(defun symstr (sym) (if (symbolp sym) (string-downcase (string sym)) sym))
(defmacro c (str) `(list ,str))
(defmacro c+ (x y) `(list "(" ,x " + " ,y ")"))
(defmacro c- (x y) `(list "(" ,x " - " ,y ")"))
(defmacro cend () ";~%")
(defmacro cxor (x y) `(list "(" ,x " ^ " ,y ")"))
(defmacro cand (x y) `(list "(" ,x " & " ,y ")"))
(defmacro cor  (x y) `(list "(" ,x " | " ,y ")"))

(defmacro creturn (form) `(list "return (" ,form ");"))
(defmacro cfun (name args &body body)
  `(list "int" ,(symstr name) "(" ,@(loop for i from 0 for arg in args collect `(list ,(if (not (zerop i)) "," "") "int" (symstr ,arg)))
                              "){~%" ,@body "}~%"))

(defun flatten (code acc)
  (cond
    ((and code (listp code) (listp (car code)))
      (setf acc (flatten (car code) acc))
      (flatten (cdr code) acc))
    ((and code (listp code))
      ;(format t "flatten: ~s~%" (car code))
      (flatten (cdr code) (concatenate 'string acc " " (format nil "~a" (or (car code) "")))))
    (t
      ;(format t "final flatten: ~s~%" (car code))
      (concatenate 'string acc " " (format nil "~a" (or (car code) ""))))))

(defmacro emit-c (code)
  ;(format t "unflattened code: ~s~%" code)
  `(string-downcase (format nil (flatten ,code ""))))


(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :xor2 40 (format nil "foo(~a)" a)
             (list (emit-c (cfun foo ('a)
                             "int x = " a (cend)
                             "x = " (cxor 'x 123) (cend)
                             "x = " (cxor 'x a) (cend)
                             (creturn 'x))))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :branch0 80 (format nil "foo(~a)" a)
             (list "int f0 (a){ return a + 1;}
                    int f1 (a){ return f0(a) + 1;}
                    int f2 (a){ return f1(a) + 1;}
                    int f3 (a){ return f2(a) + 1;}"
                    (format nil "int foo(a){ return f3(a) - ~a + 119;}" a))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :addsub0 80 (format nil "foo(~a)" a)
             (list (format nil "int foo(a){
                                int x = a;
                                x = x + 0x7fffffff;
                                x = x - a;
                                x = x - 0x7fffffff;
                                return x + 123;}"))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :addsub1 80 (format nil "foo(~a)" a)
             (list (format nil "long int foo(a){
                                long int x = a;
                                x = x + 0xffffffff;
                                x = x - a;
                                x = x - 0xffffffff;
                                return x + 123;}"))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :addsub2 80 (format nil "foo(~a)" a)
             (list (format nil "uint64_t foo(a){
                                uint64_t x = a;
                                x = x + 0x7fffffffffffffff;
                                x = x - a;
                                x = x - 0x7fffffffffffffff;
                                return x + 123;}"))))

(let ((a (- (random (ash 1 32)) (ash 1 31)))
      (b (- (random (ash 1 32)) (ash 1 31))))
  (test-form :xor3 80 (format nil "foo(~a, ~a)" a b)
             (list (format nil "uint64_t foo(a,b){
                                uint64_t x = a, y = 0;
                                x++; x++; x++; x++;
                                x+= b;
                                x = x ^ x;
                                y += x;
                                return y + 123;}"))))

(let ((a (- (random (ash 1 32)) (ash 1 31))))
  (test-form :arith0 80 (format nil "foo(~a)" a)
             (list (format nil "int bar(x){ return x;}
                                int foo(int a){
                                int x = a * 2;
                                //x += a & 0xffff;
                                //x = bar(x);
                                //x -= a & 0xffff;
                                x /= 2;
                                x -= a;
                                return (x & 0xffffff) + 123;}"))))

(let ((a (- (random (ash 1 32)) (ash 1 31)))
      (b (- (random (ash 1 32)) (ash 1 31)))
      (c (- (random (ash 1 32)) (ash 1 31)))
      (d (- (random (ash 1 32)) (ash 1 31))))
  (test-form :arith1 80 (format nil "foo(~a, ~a, ~a, ~a)" a b c d)
             (list (format nil "int bar(x){ return x;}
                                uint64_t foo(a,b,c,d){
                                uint64_t x = 0;
                                x -= a * 2;
                                x -= b * 4;
                                x -= c * 8;
                                x -= d * 16;
                                x = bar(x);
                                x += a * 2;
                                x += b * 4;
                                x += c * 8;
                                x += d * 16;
                                return (x & 0xffffff) + 123;}"))))

(let* ((a (1+ (random 8)))
       (b (1+ (random 8)))
       (c (1+ (random 8)))
       (d (1+ (random 8)))
       (s (* d c b a)))
  (test-form :loop0 #x8000 (format nil "foo(~a, ~a, ~a, ~a)" a b c d)
             (list (format nil "uint64_t foo(a,b,c,d){
                                int x = 0,i,j,k,l;
                                for(i = 0; i < a; i++){
                                for(j = 0; j < b; j++){
                                for(k = 0; k < c; k++){
                                for(l = 0; l < d; l++){
                                  x++;}}}}
                                x -= ~a;
                                return x + 123;}" s))))

(let ((y (1+ (random 8)))
      (len (+ 4 (random 64))))
  (test-form :call0 #x400 (format nil "foo()")
             (list (let ((str "int f0(int x){return x;}"))
                     (loop for i from 1 below (1+ len) do
                       (setf str (format nil "~a~%int f~a(int x){return f~a(x - ~a);}" str i (1- i) y)))
                     (setf str (format nil "~%~a~%int foo(){return f~a(0) + 123 + (~a * ~a);}~%" str len len y))
                     str))))

