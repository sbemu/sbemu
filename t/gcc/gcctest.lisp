
(in-package :sbemu)

(setf *random-state* (make-random-state t))

(defvar *foo-s* "t/gcc/foo.c")
(defvar *foo* "t/gcc/foo")

(defun make-c-file (call code)
  (with-open-file (s *foo-s* :direction :output :if-exists :supersede :if-does-not-exist :create)
    (format s "#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>~%~%")
    (loop for line in code do (write-line line s))
    (format s "~% int main (int argc, char **argv) { uint64_t x = ~a; if(x == 123){return 123;}else{return 0;}}" call)))

(defun compile-c ()
  (if (probe-file *foo*) (delete-file *foo*))
  (sb-ext:run-program "/usr/bin/gcc" `("-Wall" "-O0" ,*foo-s* "-o" ,*foo*) :wait t)
  (let ((proc (sb-ext:run-program *foo* nil :wait t)))
    (if (/= (sb-ext:process-exit-code proc) 123)
      (error "The test case in t/gcc/foo.c doesn't return 123 (atleast the program foo doesn't return 123 or may not even exist)"))))

(defun test-form (name max-gcc call-string function-body)
  (format t "****** run random-test ~a ******~%" name)
  (let ((*print-pretty* nil))
    (format t ">>> compile source~%")
    (make-c-file call-string function-body)
    (compile-c)
    (format t ">>> setup memory~%")
    ;; setup environment and call disasm, collect return value in register rax
    (setf *memw* (make-array #x20000 :initial-element 15))
    (format t ">>> load runtime~%")
    (load-runtime *foo*)
    (format t ">>> init emulator~%")
    (let* ((cpu (run-emulated-sbcl nil))
           (registers (svref cpu 3)))
      (format t "~%>>> run disasm~%")
      (setf *cpu-watch* nil)
      (setf *cpu-nolog* nil)
      (setf *cpu-log* 0) ; if a test-case fail, set this to zero and rerun
      (setf cpu (disasm (svref cpu 0) max-gcc cpu :stop-pc 0))
      (format t "pc: ~x, rax=~x (~a)~%" (svref cpu 0) (reg :rax) (reg :rax))
      (assert (= (reg :rax) 123)))))

(defun gcctest ()
  (in-package :sbemu)
  ; initialize as much as possible and let the test
  ; run in a dirty environment
  (setf *heap-size* #x20000)
  (os-init *heap-size*)
  (load "t/gcc/tests"))

