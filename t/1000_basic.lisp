; basic assembly, just verify tiny-test-assembler

; first just pot down various bytes, making sure nothing breaks
(macrolet
  ((oper (name code)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              cmem)
          (setf cmem (my-make-memory 1))
          (asm-init cmem 8) ; start assembly at 8
          (my-make-page cmem 0)
          ,code
          123))))
  (oper asm-basic-mov (asm :mov #xbeefcafe :rax))
  (oper asm-basic-raw (asm :raw 0 1 2 4)))

