; check NOP instruction

(macrolet
  ((oper (name code finalpc)
     `(deftest (,name)
        (let ((cpu (make-cpu-x86-64))
              (*print-pretty* nil))
          (setf *mem* (make-array #x20000 :initial-element nil :fill-pointer 0 :adjustable t))
          (setf *memw* (make-array #x20000 :initial-element 7))
          (setf *memsz* #x20000)
          (make-page 0)
          (loop for i from 0 for x in ',code do
            (mset-u8 i x))
          ;(make-page #x0fffe000) ; ???
          (make-page (- #x1fffe000 1)) ; stack mapping
          (make-page (- #x1fffe000 8191))
          (make-page (- #x1fffe000 8192))
          (make-page (- #x1fffe000 8193))
          (setf cpu (disasm cpu 1))
;          (let ((registers (svref cpu 3)))
          (if (/= (areg cpu :rpc) ,finalpc) (format t "error: pc=~x expected, got pc=~x~%" ,finalpc (areg cpu :rpc)))
          (assert (= (areg cpu :rpc) ,finalpc))
;            (assert (= (reg :pc) 3))) ; FIX: use length of code+1 instead
          ; initially the registers should be zero, but make-cpu doesnt do that (no reason really not to).
          ;(loop for x across (svref cpu 3) do ; registers
          ;  (if (numberp x)
          ;    (assert (zerop x))))
          123))))
  (oper nopw  (     #x0f #x1f) 3)
  (oper nopw2 (#x66 #x0f #x1f #x84) 9))
