;
; mips float instruction test
; we dont check the functionallity in depth but more the 'around stuff' like instruction decoding.
;

(defun test-mips-set-cpu (cpu cpu2)
  (loop for i from 0 below (length (cpu-registers cpu2)) do
    (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                              (logxor (logxor #x0000000000000123 i)))))
  (setf (svref (cpu-registers cpu) 0) 0)
  (setf (svref (cpu-registers cpu2) 0) 0))

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu  (setf *cpu1* (make-cpu-mips)))
              (cpu2 (setf *cpu2* (make-cpu-mips)))
              (checks ',checks)
              (runlength (length ',code))
              (code-offset 8)
              (cmem (my-make-memory 10)))
          (my-make-page cmem 0)
          (asm-init cmem code-offset)
          ,@code
          (test-mips-set-cpu cpu cpu2)
          (loop for i from 0 for b in '(#x3f #xa6 #x33 #xdc) do (my-mset-u8 cmem (+ #x12d i) b)) ; store a float at #x12d
          (loop for i from 0 for b in '(#x40 #x10 #x00 #x00) do (my-mset-u8 cmem (+ #x140 i) b)) ; store a float at #x140
          (loop for i from 0 for b in '(#x40 #x00 #x00 #x00) do (my-mset-u8 cmem (+ #x148 i) b)) ; store a float at #x148
          (run-cpu cpu2 code-offset runlength) ; run code
          (check-regs cpu cpu2 checks) ; check what differs
          (check-mems cmem checks)
          123))))
  (oper asm-cvt.s.w ((asm :raw #x44 #x84 #x00 #x00)
                     (asm :raw #x46 #x80 #x00 #x20))
                    ((pc #x10) (:f0 #x43938000)))
  (oper asm-cvt.w.d ((asm :raw #xc5 #x00 #x00 #x17)  ; lwc1-[t0+15]-f0  =0
                     (asm :raw #xc5 #x01 #x00 #x15)  ; lwc1-[T0+15]-f1  =40000000
                     (asm :raw #x46 #x20 #x00 #x24)) ; cvt.w.d-f0-f0
                    ((pc #x14) (:f0 4) (:f1 #x40100000)))
  (oper asm-lwc1 ((asm :raw #xc6 #x60 #xff #xfd)  ; lwc1-[s3-3]-f0
                  (asm :raw #x46 #x00 #x00 #x05)  ; abs.s-f0-f0
                  (asm :raw #x44 #x04 #x00 #x00)) ; mfc1-f0-a0
                  ((pc #x14) (:f0 1.2984576) (:a0 #x3fa633dc)))
  (macrolet ((arith-oper (name off f0 f1 f2 f3 bytes)
               `(oper ,name ((asm :raw #xc5 #x00 #x00 (+ ,off #x17)) ; lwc1-[T0+17]-f0  =0
                             (asm :raw #xc5 #x01 #x00 #x15)          ; lwc1-[T0+15]-f1  =40100000
                             (asm :raw #xc5 #x02 #x00 (+ ,off #x17)) ; lwc1-[T0+17]-f2  =0
                             (asm :raw #xc5 #x03 #x00 #x1d)          ; lwc1-[T0+1d]-f3  =40000000
                             (asm :raw ,@bytes))                     ; oper-f0-f2-f0
                            ((pc #x1c) (:f2 ,f2) (:f3 ,f3) (:f0 ,f0) (:f1 ,f1)))))
    (arith-oper asm-add.d 0 0 #x40180000 0 #x40000000 (#x46 #x22 #x00 #x00))
    (arith-oper asm-div.d 0 0 #x40000000 0 #x40000000 (#x46 #x22 #x00 #x03))
    (arith-oper asm-mul.d 0 0 #x40200000 0 #x40000000 (#x46 #x22 #x00 #x02))
    (arith-oper asm-sub.d 0 0 #x40000000 0 #x40000000 (#x46 #x22 #x00 #x01))
    (arith-oper asm-add.s -2 #x40900000 #x40100000 #x40100000 #x40000000 (#x46 #x02 #x00 #x00)))
  (oper asm-bc1t ((asm :raw #x45 #x01 #x00 #x04) ; bc1t-x, which needs pipelined, is therefor delayd
                  (asm :raw 0 0 0 0) ; meanwhile this nop is executed
                  ; here inbetween these nops, our bc1t will be executed
                  (asm :raw 0 0 0 0) ; and finally this nop will be executed
                  ) ((pc #x14)))
  (oper asm-bc1f ((asm :raw #x45 #x00 #x00 #x4b)
                  (asm :raw 0 0 0 0)
                  (asm :raw 0 0 0 0)) ((pc #x13c)))
  (oper asm-slti ((asm :raw #x3c #x05 #x80 #x00)
                  (asm :raw #x28 #xa4 #x01 #xa4)
                  (asm :raw #x10 #x80 #x00 #xfc)
                  (asm :raw 0 0 0 0)
                  (asm :raw 0 0 0 0)) ((pc #x1c) (:a0 1) (:a1 #x80000000)))
  (oper asm-sll ((asm :raw #x3c #x04 #x80 #x00) ; lu 8000-a0
                 (asm :raw #x00 #x04 #x20 #x40) ; sll-1-a0-a0
                 (asm :raw 0 0 0 0)) ((pc #x14) (:a0 0))))
