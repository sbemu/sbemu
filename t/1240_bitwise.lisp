; assemble, run and verify

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              (runlength (length ',code))
              cmem)
          (setf cmem (my-make-memory 1000))
          (my-make-page cmem 0)
          (asm-init cmem 8) ; start assembly at 8
          ,@code
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000023 i)))))
          (my-mset-u8 cmem #xdf #x26)
          ; run code
          (setf cpu2 (run-cpu cpu2 8 runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  ; cmp rdx=1000000010 [...]=10 sets flags CS
  (oper asm-xorz ((asm :raw #x49 #x31 #xac #x24 #xb0 #x00 #x00 #x00) ; xor-rbp-[r12+b0]
                  (asm :raw #x74 #x14))                              ; je-x
                 ((:reg= pc #x26) (:mem= #xdf 0)))                   ; should branch
  (oper asm-sar0 ((asm :raw #x48 #xb9 #x00 #xff #xff #xff #xff #xff #xff #xff)
                  (asm :raw #x48 #xc1 #xf9 #x03)) ; sar 3-rcx
                 ((:reg= pc #x16) (:rcx #xffffffffffffffe0)))
  (oper asm-not0 ((asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00)
                  (asm :raw #x48 #xf7 #xd0))                      ; not-rax
                 ((:reg= pc #x15) (:rax #xffffffffffffffff)))
  (oper asm-ror.8a((asm :mov 1 :rdx)
                   (asm :mov 1 :rcx)
                   (asm :raw #x48 #xd3 #xca))                      ; ror rdx by cl
                  ((:reg= pc #x19) (:rcx 1) (:rdx #x8000000000000000)))
  (oper asm-ror.8b((asm :raw #x48 #xba #xe8 #xff #xff #xff #xff #xff #xff #x7f)
                   (asm :raw #x48 #xb9 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80)
                   (asm :raw #xf9)            ; set carry
                   (asm :raw #x48 #xd1 #xda)) ; ror rdx by cl
                  ((:reg= pc #x20) (:rcx #x8000000000000000) (:rdx #xbffffffffffffff4)))
  (oper asm-rol.8 ((asm :raw #x48 #xba #x01 #x00 #x00 #x00 #x00 #x00 #x00 #x80)
                   (asm :mov 1 :rcx)
                   (asm :raw #x48 #xd3 #xc2))                      ; rol rdx by cl
                  ((:reg= pc #x1c) (:rcx 1) (:rdx 3)))
  (oper asm-shl0 ((asm :mov #x41200000 :rdx)
                  (asm :raw #x48 #xc1 #xe2 #x20))
                 ((:reg= pc #x13) (:rdx #x4120000000000000))) ; shl-20-rdx
  (oper asm-shrd0 ((asm :raw #x48 #xb8 #x00 #x40 #xd0 #x74 #xdb #x0a #x03 #xc3)
                   (asm :mov 2 :rdx)
                   (asm :raw #x48 #x0f #xac #xd0 #x03))
                  ((:reg= pc #x1e) (:rdx 2) (:rax #x5860615b6e9a0800))) ; rdx=0x5860615b6e9a0800 rax=2 flags=APIO
  (oper asm-rcr0 ((asm :raw #x48 #xba #xd8 #x01 #x00 #x00 #x00 #x00 #x00 #x80)
                  (asm :raw #x48 #xd1 #xda)) ; rcr-rdx
                 ((:reg= pc #x15) (:rdx #x40000000000000ec)))
  (oper asm-or   ((asm :mov -32 :rcx)
                  (asm :raw #x48 #x09 #xc9)
                  (asm :raw #x79 #x7f))
                 ((:reg= pc #x14) (:rcx #xffffffffffffffe0)))
  (oper asm-and0 ((asm :raw #x48 #xba #xf8 #xff #xff #xff #xff #xff #xff #xff) ; -8
                  (asm :raw #x45 #x23 #x44 #x51 #x18)) ; and-[r9+rdx*2+18]-r8
                 ((:reg= pc #x17) (:r8 0) (:rdx #xfffffffffffffff8))))

