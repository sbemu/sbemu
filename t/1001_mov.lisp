; assemble, run and verify
; note: we are eating our own food. For a more real-life testing see 'r' directory

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              (runlength (length ',code))
              cmem)
          (setf cmem (my-make-memory 1000))
          (my-make-page cmem 0)
          (asm-init cmem 8) ; start assembly at 8
          ,@code
          (setf checks (nconc checks (list `(:reg= pc ,*asm-fillpointer*))))
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 8 runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper asm-mov1 ((asm :mov #x01020304 :rax)) ((:reg= :rax #x01020304)))
  (oper asm-mov2 ((asm :mov #x01020304 :r15)) ((:reg= :r15 #x01020304)))
  (oper asm-mov3 ((asm :raw #x48 #xbd #xff #xff #xff #xff #xff #xff #xff #x7f) ; mov--1-rbp
                  (asm :raw #x41 #x0f #xb6 #x68 #x30))    ; movzbl-[r8+30]-rbp
                 ((:rbp 0)))
  (oper asm-mov4 ((asm :raw #x48 #xba #xf8 #xff #xff #xff #xff #xff #xff #xff) ; -8
                  (asm :mov 64 :rdi)
                  (asm :raw #x4c #x89 #x44 #x57 #x10))    ; mov-r8-[rdi+rdx*2+10] ; 0x40bbea
                 ((:mem= #x41 7) (:rdi #x40) (:rdx #xfffffffffffffff8)))
  (oper asm-movslq0 ((asm :raw #x48 #xb8 #x67 #x45 #x23 #x81 #x00 #x00 #x00 #x00) ; eax = -1 (32bit)
                     (asm :raw #x48 #x63 #xd0))                                   ; movslq-eax-rdx
                    ((:rax #x81234567) (:rdx #xffffffff81234567)))
  (oper asm-movslq1 ((asm :raw #x48 #xb8 #x67 #x45 #x23 #x7f #x00 #x00 #x00 #x00) ; eax = -1 (32bit)
                     (asm :raw #x48 #x63 #xd0))                                   ; movslq-eax-rdx
                    ((:rax #x7f234567) (:rdx #x7f234567)))
  (oper asm-movslq2 ((asm :raw #x48 #xb8 #x00 #xd0 #x33 #x03 #x10 #x00 #x00 #x00) ; eax = -1 (32bit)
                     (asm :raw #x48 #x63 #xd0))                                   ; movslq-eax-rdx
                    ((:rax #x100333d000) (:rdx #x333d000)))

  (oper asm-rep-stos0 ((asm :mov 64 :rax)
                       (asm :mov 3 :rcx)
                       (asm :mov 128 :rdi)
                       (asm :raw #xf3 #x48 #xab))  ; rep stos rax rdi
                      ((:mem= #x70 64) (:mem= #x78 64) (:mem= #x80 64 0 0 0 0 0 0 0 64 0 0 0 0 0 0 0 64) (:rdi #x68) (:rcx 0) (:rax 64)))

  )

