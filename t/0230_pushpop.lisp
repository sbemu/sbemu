; check PUSH/POP instructions

(macrolet
  ((oper (name code-offset runlength code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (setf *cpu1* (make-cpu-x86-64)))
              (cpu2 (setf *cpu2* (make-cpu-x86-64)))
              (checks ',checks)
              cmem)
          (setf cmem (my-make-memory #x8000))
          (my-make-page cmem 0)
          ; initialize code memory to both real and test memory
          (loop for i from 0 for x in ',code do (my-mset-u8 cmem (+ i ,code-offset) x))
          (my-make-page cmem #x50606B5)
          (my-make-page cmem #x704)
          (loop for i from #x700 to #x730 do (my-mset-u8 cmem i (logxor (logand #xff i) #xaa)))
          ; create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (logxor (logxor #x0000000000000708 i)))))
          ; run code
          (setf cpu2 (run-cpu cpu2 ,code-offset ,runlength))
          ; check what differs
          (check-regs cpu cpu2 checks)
          (check-mems cmem checks)
          123))))
  ; push checks
  (oper push-r12 8 1 (#x41 #x54) ((:reg= pc 10) (:reg= :rsp #x704) (:mem= #x704 #x04 #x07 0 0 0 0 0 0)))
  (oper push-r13 8 1 (#x41 #x55) ((:reg= pc 10) (:reg= :rsp #x704) (:mem= #x704 #x05 #x07 0 0 0 0 0 0)))
  (oper push-r14 8 1 (#x41 #x56) ((:reg= pc 10) (:reg= :rsp #x704) (:mem= #x704 #x06 #x07 0 0 0 0 0 0)))
  (oper push-r15 8 1 (#x41 #x57) ((:reg= pc 10) (:reg= :rsp #x704) (:mem= #x704 #x07 #x07 0 0 0 0 0 0)))
  (oper push-rax 8 1 (#x50) ((:reg= pc 9) (:reg= :rsp #x704) (:mem= #x704 #x08 #x07 0 0 0 0 0 0)))
  (oper push-rdx 8 1 (#x52) ((:reg= pc 9) (:reg= :rsp #x704) (:mem= #x704 #x0a #x07 0 0 0 0 0 0)))
  (oper push-rbx 8 1 (#x53) ((:reg= pc 9) (:reg= :rsp #x704) (:mem= #x704 #x0b #x07 0 0 0 0 0 0)))
  (oper push-rsi 8 1 (#x56) ((:reg= pc 9) (:reg= :rsp #x704) (:mem= #x704 #x0e #x07 0 0 0 0 0 0)))
  (oper push-rdi 8 1 (#x57) ((:reg= pc 9) (:reg= :rsp #x704) (:mem= #x704 #x0f #x07 0 0 0 0 0 0)))
  (oper pushq-[r12+rax*1] 8 1 (#x41 #xff #x34 #x04) ((:reg= pc 12) (:reg= :rsp #x704) (:mem= #x704 0 0 0 0 0 0 0 0)))
  (oper push1-10 8 1 (#x6a #x10) ((:reg= pc 10) (:reg= :rsp #x704) (:mem= #x704 16 0 0 0 0 0 0 0)))
  (oper push4-10 8 1 (#x68 0 0 0 10) ((:reg= pc 13) (:reg= :rsp #x704) (:mem= #x704 0 0 0 10 0 0 0 0)))
  ; pop checks
  (oper pop-rax 8 1 (#x58) ((:reg= pc 9) (:reg= :rsp #x714) (:reg= :rax #xb9b8bbbaa5a4a7a6)))
  (oper pop-rcx 8 1 (#x59) ((:reg= pc 9) (:reg= :rsp #x714) (:reg= :rcx #xb9b8bbbaa5a4a7a6)))
  (oper pop-rbx 8 1 (#x5b) ((:reg= pc 9) (:reg= :rsp #x714) (:reg= :rbx #xb9b8bbbaa5a4a7a6)))
  (oper popq-[rcx-10] 8 1 (#x8f #x41 #xf0) ((:reg= pc 11) (:reg= :rsp #x714) (:mem= #x6f9 #xa6 #xa7 #xa4 #xa5 #xba #xbb #xb8 #xb9)))
  (oper enter 8 1 (#xc8 #x08 #x00 #x00) ((:reg= pc 12) (:rsp #x6fc) (:rbp #x704) (:mem= #x704 13 7 0 0 0 0 0 0))))


