; assemble, run and verify
; note: we are eating our own food. For a more real-life testing see 'r' directory

(macrolet
  ((oper (name code checks)
     `(deftest (,name)
        (let ((*print-pretty* nil)
              (cpu (make-cpu-x86-64))
              (cpu2 (make-cpu-x86-64))
              (checks ',checks)
              (runlength (length ',code))
              cmem)
          (setf cmem (my-make-memory 1000))
          (my-make-page cmem 0)
          (asm-init cmem 8) ; start assembly at 8
          ,@code
          ; initialize registers, create somewhat unique values that fills the whole register
          (loop for i from 0 below (length (cpu-registers cpu2)) do
            (setf (svref (cpu-registers cpu) i) (setf (svref (cpu-registers cpu2) i)
                                                      (cond
                                                        ((< i 16) (logxor (logxor #x000000000000023 i)))
                                                        ((< i 29) (make-singlefloat :int (logxor (logxor #x000000000000023 i))))
                                                        (t 0)))))
          (my-mset-u8 cmem #x97 #x01)
          (my-mset-u8 cmem #x9f #x40)
          ;(my-mset-u8 cmem #x36 #x00)
          ; run code
          (setf cpu2 (run-cpu cpu2 8 runlength))
          ; check what differs
          (check-mems cmem checks)
          (check-regs cpu cpu2 checks)
          123))))
  (oper asm-movq0    ((asm :mov #x41555555 :rdx)
                      (asm :raw #x66 #x48 #x0f #x6e #xc2))     ; movq-rdx-xmm0
                     ((:reg= pc #x14) (:rdx #x41555555) (:xmm0 #x41555555)))
  (oper asm-movq1    ((asm :mov #x41200000 :rax)
                      (asm :raw #x66 #x48 #x0f #x6e #xc8))     ; movq-rax-xmm1
                     ((:reg= pc #x14) (:rax #x41200000) (:xmm1 #x41200000)))
  (oper asm-movq2 ((asm :raw #x48 #xc7 #xc0 #xff #xff #xff #xff)
                   (asm :raw #x48 #xc1 #xe8 #x21)
                   (asm :raw #x66 #x48 #x0f #x6e #xc0))
                  ((:reg= pc #x18) (:rax #x7fffffff) (:xmm0 #x7fffffff)))
  (oper asm-movq3 ((asm :mov #x7f800000 :rdx)
                   (asm :raw #x66 #x0f #x6e #xc2) ; movq-rdx-xmm0
                   (asm :raw #x0f #x2f #xc1 #xba)) ; comiss-xmm1-xmm0
                  ((:reg= pc #x16) (:rdx #x7f800000) (:xmm0 #x7f800000)))
  (oper asm-movq4 ((asm :mov #x68 :rbp)
                   (asm :raw #xf3 #x0f #x7e #x55 #x98)) ; movq-[rbp+68]-xmm2
                  ((:reg= pc #x14) (:rbp #x68) (:xmm2 0)))
  (oper asm-movq5 ((asm :mov #x68 :rdx)
                   (asm :raw #x66 #x0f #xd6 #x42 #xf9)) ; movq-xmm0-[rdx-7]
                  ((:reg= pc #x14) (:rdx #x68) (:mem= #x63 #x4c #x42)))
  (oper asm-cvtsi2ss ((asm :mov 10 :rax) ; break 0x100008AB08
                      (asm :raw #x48 #x89 #x45 #xf8)           ; mov rax-[rbp-8]
                      (asm :raw #xf3 #x48 #x0f #x2a #x45 #xf8) ; cvtsi2ssq [rbp-8]-xmm0 ; xmm0=41800000
                      (asm :raw #x66 #x48 #x0f #x7e #xc2))     ; movd-xmm0-rdx (gdb says movq, but it's not)
                     ((:reg= pc #x1e)  (:mem= #x1e 10) (:rax 10) (:rdx #x41200000) (:xmm0 #x41200000)))
  (oper asm-cvttss2si ((asm :mov #x41555555 :rdx)
                       (asm :raw #x66 #x0f #x6e #xc2)  ; movq-rdx-xmm0
                       (asm :raw #xf3 #x48 #x0f #x2c #xd0))  ; cvttss2si-xmm0-rdx
                      ((:reg= pc #x18) (:rdx #xd) (:xmm0 #x41555555)))
  (oper asm-cvttss2si2 ((asm :mov #x100 :rbp)
                        (asm :raw #x48 #xb8 #x55 #x55 #x55 #x41 #x11 #x11 #x11 #x11) ; cvttss2si should ignore the #x11 bytes
                        (asm :raw #x48 #x89 #x45 #xf8)           ; mov rax-[rbp-8]
                        (asm :raw #xf3 #x48 #x0f #x2c #x55 #xf8))  ; cvttss2si-[rbp-8]-rdx
                       ((:reg= pc #x23) (:mem= #xf8 #x55 #x55 #x55 #x41 #x11 #x11 #x11 #x11) (:rbp #x100) (:rax #x1111111141555555) (:rdx 13)))
  (oper asm-cvtsi2sd ((asm :mov #x41555555 :rdx)
                       (asm :raw #xf2 #x48 #x0f #x2a #xca))  ; cvtsi2sd-rdx-xmm1
                     ((:reg= pc #x14) (:rdx #x41555555) (:xmm1 #x41d0555555400000)))
  (oper asm-cvtsi2sd2 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #xff #xff) ; rax = -1
                       (asm :raw #xf2 #x48 #x0f #x2a #xc0))  ; cvtsi2sd-rax-xmm0
                     ((:reg= pc #x17) (:rax #xffffffffffffffff) (:xmm0 #xbff0000000000000)))
  (oper asm-cvtsd2ss0 ((asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80)
                       (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                       (asm :raw #xf2 #x0f #x5a #xc0)         ; cvtsd2ss-xmm0-xmm0
                       (asm :raw #x66 #x48 #x0f #x7e #xc2))   ; movq-xmm0-rdx
                      ((:reg= pc #x20) (:rax #x8000000000000000) (:xmm0 #x-80000000) (:rdx #x-80000000)))
  (oper asm-comiss0 ((asm :mov 0 :rax)
                     (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rdx-xmm0
                     (asm :raw #x48 #xb8 #xff #xff #x7f #xff #x00 #x00 #x00 #x00)
                     (asm :raw #x66 #x48 #x0f #x6e #xc8)    ; movq-rax-xmm1
                     (asm :raw #x0f #x2f #xc1)              ; comiss-xmm1-xmm0
                     (asm :raw #x76 #x7f)                   ; jbe
                     (asm :raw #x7a #x7f)                   ; jp
                     (asm :raw #x74 #x14))                  ; je
                    ((:reg= pc #x2c) (:rax #xff7fffff) (:xmm0 0) (:xmm1 #xff7fffff)))
  (oper asm-comiss1 ((asm :mov #x3fc00000 :rax)
                     (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rdx-xmm0
                     (asm :mov #x3f800000 :rax)
                     (asm :raw #x66 #x48 #x0f #x6e #xc8)    ; movq-rax-xmm1
                     (asm :raw #x0f #x2f #xc1)              ; comiss-xmm1-xmm0
                     (asm :raw #x76 #x7f)                   ; jbe
                     (asm :raw #x7a #x7f)                   ; jp
                     (asm :raw #x74 #x14))                  ; je
                    ((:reg= pc #x29) (:rax #x3f800000) (:xmm0 #x3fc00000) (:xmm1 #x3f800000)))
  (oper asm-comiss2 ((asm :mov #x7f800000 :rdx)
                     (asm :raw #x66 #x48 #x0f #x6e #xca) ; movd-rdx-xmm1
                     (asm :raw #x48 #xbf #x00 #x00 #x80 #xff #x00 #x00 #x00 #x00)
                     (asm :raw #x66 #x48 #x0f #x6e #xc7) ; movd-rdi-xmm0
                     (asm :raw #x0f #x2f #xc1)           ; comiss-xmm1-xmm0
                     (asm :raw #x73 #x7f))               ; jae
                    ((:reg= pc #x28) (:rdx #x7f800000) (:rdi #xff800000) (:xmm1 #x7f800000) (:xmm0 #xff800000)))
  (oper asm-comiss3 ((asm :mov 0 :rdx)
                     (asm :raw #x66 #x48 #x0f #x6e #xca) ; movd-rdx-xmm1
                     (asm :raw #x48 #xbf #x00 #x00 #x80 #xff #x00 #x00 #x00 #x00)
                     (asm :raw #x66 #x48 #x0f #x6e #xc7) ; movd-rdi-xmm0
                     (asm :raw #x0f #x2f #xc1)           ; comiss-xmm1-xmm0
                     (asm :raw #x73 #x7f))               ; jae
                    ((:reg= pc #x28) (:rdx 0) (:rdi #xff800000) (:xmm1 0) (:xmm0 #xff800000)))
  (oper asm-comiss4 ((asm :raw #x48 #xba #xff #xff #x7f #x7f #x00 #x00 #x00 #x00) ; most-positive-single-float
                     (asm :raw #x66 #x48 #x0f #x6e #xca) ; movd-rdx-xmm1
                     (asm :raw #x48 #xbf #xff #xff #x7f #xff #x00 #x00 #x00 #x00) ; most-negative-single-float
                     (asm :raw #x66 #x48 #x0f #x6e #xc7) ; movd-rdi-xmm0
                     (asm :raw #x0f #x2f #xc1)           ; comiss-xmm1-xmm0
                     (asm :raw #x73 #x7f))               ; jae
                    ((:reg= pc #x2b) (:rdx #x7f7fffff) (:rdi #xff7fffff) (:xmm1 #x7f7fffff) (:xmm0 #xff7fffff)))
  (oper asm-comiss5 ((asm :raw #x48 #xbf #xff #xff #x7f #xff #x00 #x00 #x00 #x00) ; most-negative-single-float
                     (asm :raw #x66 #x48 #x0f #x6e #xc7) ; movd-rdi-xmm0
                     (asm :raw #x0f #x2f #x05 #xe8 #x00 #x00 #x00)           ; comiss-[pc+e8]-xmm0
                     (asm :raw #x73 #x7f))               ; jae
                    ((:reg= pc #x20) (:rdi #xff7fffff) (:xmm0 #xff7fffff)))
  (oper asm-addss0 ((asm :mov #x3f8ccccd :rdx) ; 1.1
                    (asm :mov #x400ccccd :rdi) ; 2.2
                    (asm :raw #x66 #x0f #x6e #xca)  ; movd-rdx-xmm1
                    (asm :raw #x66 #x0f #x6e #xd7)  ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x58 #xca)       ; addss-xmm2-xmm1
                    (asm :raw #x66 #x48 #x0f #x7e #xc9)) ; movq-xmm1-rcx
                   ((:reg= pc #x27) (:rdx #x3f8ccccd) (:rcx #x40533334) (:rdi #x400ccccd) (:xmm1 #x40533334) (:xmm2 #x400ccccd)))
  (oper asm-addss1 ((asm :mov #x7f800000 :rdx)
                    (asm :raw #x66 #x48 #x0f #x6e #xca)      ; movd-rdi-xmm1
                    (asm :raw #x48 #xbf #xcd #xcc #xcc #xbd #x00 #x00 #x00 #x00)
                    (asm :raw #x66 #x48 #x0f #x6e #xd7)      ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x58 #xca))          ; addss-xmm2-xmm1
                   ((:reg= pc #x27) (:rdx #x7f800000) (:rdi #xbdcccccd) (:xmm1 #x7f800000) (:xmm2 #xbdcccccd)))
  (oper asm-movsd0 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #xef #xff)
                    (asm :raw #x48 #x89 #x43 #x40)       ; mov rax-[rbx+40]
                    (asm :raw #xf2 #x0f #x10 #x43 #x40)) ; movsd-[rax+40]-xmm0
                   ((:reg= pc #x1b) (:rax #xffefffffffffffff) (:xmm0 #xffefffffffffffff) (:mem= #x60 #xff #xff #xff #xff #xff #xff #xef #xff)))
  (oper asm-movsd1 ((asm :raw #x48 #xb8 #xc0 #xff #xff #xff #xff #xff #xff #xff)
                    (asm :raw #x48 #x89 #x43 #x40)           ; mov rax-[rbx+40]
                    (asm :raw #xf2 #x48 #x0f #x2a #x43 #x40) ; cvtsi2sdq-[rax+40]-xmm0
                    (asm :raw #xf2 #x0f #x11 #x43 #x40))     ; movsd-xmm0-[rax+40]
                   ((:reg= pc #x21) (:rax #xffffffffffffffc0) (:xmm0 #xc3f0000000000000) (:mem= #x60 #x00 #x00 #x00 #x00 #x00 #x00 #xf0 #xc3)))
  (oper asm-sqrtsd0 ((asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x80) ; check that sqrtsd preserves -0d0
                     (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                     (asm :raw #xf2 #x0f #x51 #xc0)         ; sqrtsd xmm0
                     (asm :raw #x66 #x48 #x0f #x7e #xc2))   ; movq-xmm0-rdx
                    ((:reg= pc #x20) (:rax #x8000000000000000) (:rdx #x8000000000000000) (:xmm0 #x8000000000000000)))
  (oper asm-sqrtsd1 ((asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x10 #x40) ; check that sqrtsd(4) = 2
                     (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                     (asm :raw #xf2 #x0f #x51 #xc0)         ; sqrtsd xmm0
                     (asm :raw #x66 #x48 #x0f #x7e #xc2))   ; movq-xmm0-rdx
                    ((:reg= pc #x20) (:rax #x4010000000000000) (:rdx #x4000000000000000) (:xmm0 #x4000000000000000)))
  (oper asm-comisd0 ((asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #xef #xff)
                     (asm :raw #x48 #x89 #x43 #x40)      ; mov rax-[rbx+40]
                     (asm :raw #xf2 #x0f #x10 #x43 #x40) ; movsd-[rax-7]-xmm0
                     (asm :raw #x48 #xb8 #xff #xff #xff #xff #xff #xff #x3f #xc3)
                     (asm :raw #x48 #x89 #x43 #x40)      ; mov rax-[rbx+40]
                     (asm :raw #xf2 #x0f #x10 #x4b #x40) ; movsd-[rax-7]-xmm1
                     (asm :raw #x66 #x0f #x2f #xc1)      ; comisd-xmm1-xmm0
                     (asm :raw #x7a #x77)                ; jp should not jump
                     (asm :raw #x74 #x10))               ; je should not jump
                    ((:reg= pc #x36) (:rax #xc33fffffffffffff) (:xmm0 #xffefffffffffffff) (:xmm1 #xc33fffffffffffff) (:mem= #x60 #xff #xff #xff #xff #xff #xff #x3f #xc3)))
  (oper asm-comisd1 ((asm :raw #x48 #xba #x00 #x00 #x00 #x00 #x00 #x00 #xf0 #x7f)
                     (asm :raw #x66 #x48 #x0f #x6e #xca) ; movd-rdx-xmm1
                     (asm :raw #x48 #xbf #x00 #x00 #x00 #x00 #x00 #x00 #xf0 #xff)
                     (asm :raw #x66 #x48 #x0f #x6e #xc7) ; movd-rdi-xmm0
                     (asm :raw #x66 #x0f #x2f #xc1)      ; comiss-xmm1-xmm0
                     (asm :raw #x73 #x7f))               ; jae
                    ((:reg= pc #x2c) (:rdx #x7ff0000000000000) (:rdi #xfff0000000000000) (:xmm1 #x7ff0000000000000) (:xmm0 #xfff0000000000000)))
  (oper asm-subss0 ((asm :raw #x48 #xb8 #x40 #x00 #x00 #x00 #x00 #x00 #x00 #x00)
                    (asm :raw #x48 #x89 #x43 #x40)       ; mov rax-[rbx+40]
                    (asm :raw #xf2 #x48 #x0f #x2a #x43 #x40) ; cvtsi2sdq-[rbx+40]-xmm0
                    (asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00)
                    (asm :raw #x48 #x89 #x43 #x40)       ; mov rax-[rbx+40]
                    (asm :raw #xf2 #x0f #x10 #x4b #x40)  ; movsd-[rbx+40]-xmm1
                    (asm :raw #xf2 #x0f #x5c #xc8)       ; subss-xmm0-xmm1
                    (asm :raw #xf2 #x0f #x11 #x4b #x40)) ; movsd-xmm1-[rbx+40]
                   ((:reg= pc #x38) (:rax #x0) (:xmm0 #x4050000000000000) (:xmm1 #xc050000000000000) (:mem= #x66 #x50 #xc0)))
  (oper asm-subsd1 ((asm :raw #x48 #xb8 #x69 #xc3 #x10 #x81 #xf3 #x28 #xf8 #x3f)
                    (asm :raw #x66 #x48 #x0f #x6e #xc8)  ; movq-rax-xmm1
                    (asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #xf0 #x3f)
                    (asm :raw #x66 #x48 #x0f #x6e #xd0)  ; movq-rax-xmm2
                    (asm :raw #xf2 #x0f #x5c #xca)       ; subsd-xmm2-xmm1
                    (asm :raw #xf2 #x0f #x11 #x4b #x40)) ; movsd-xmm1-[rbx+40]
                   ((:reg= pc #x2f) (:rax #x3ff0000000000000) (:xmm1 #x3fe051e7022186d2) (:xmm2 #x3ff0000000000000) (:mem= #x60 #xd2 #x86 #x21 #x2 #xe7 #x51 #xe0 #x3f)))
  (oper asm-subsd2 ((asm :raw #x48 #xb8 #xf6 #x9e #xae #x22 #x37 #x12 #xfa #x3f)
                    (asm :raw #x66 #x48 #x0f #x6e #xc8)  ; movq-rax-xmm1
                    (asm :raw #x48 #xb8 #x00 #x00 #x00 #x00 #x00 #x00 #xf0 #x3f)
                    (asm :raw #x48 #x89 #x43 #x40)       ; mov-rax-[rbx+40]
                    (asm :raw #xf2 #x0f #x5c #x0d #x33 #x00 #x00 #x00))
                   ((:reg= pc #x2d) (:rax #x3ff0000000000000) (:xmm1 #x3fe4246e455d3dec) (:mem= #x66 #xf0 #x3f)))
  ; this test is about how to load xmm regs (SF/DF issues), divss is only used to verify correct values is loaded into xmm1/xmm2
  (oper asm-divss0 ((asm :raw #x48 #xb8 #x00 #x00 #x20 #x41 #x00 #x00 #x00 #x00)
                    (asm :raw #x66 #x48 #x0f #x6e #xc8)  ; movq-rax-xmm1 absurd: this happens in TWO-ARG-/, moving an apparent SF
                    (asm :raw #x48 #xb8 #x00 #x00 #x40 #x3f #x00 #x00 #x00 #x00) ; by using a DF operation
                    (asm :raw #x66 #x48 #x0f #x6e #xd0)  ; movq-rax-xmm2           so the DF operation (66 48 0f 63) instead of SF
                    (asm :raw #xf3 #x0f #x5e #xca)       ; divss-xmm2-xmm1         (66 0f 63) must take care not to cache any reals.
                    (asm :raw #xf2 #x0f #x11 #x4b #x40)) ; movsd-xmm1-[rbx+40]     and only store the float as bits
                   ((:reg= pc #x2f) (:rax #x3f400000) (:xmm1 #x41555555) (:xmm2 #x3f400000) (:mem= #x60 #x55 #x55 #x55 #x41)))
  (oper asm-divss1 ((asm :mov 1 :rdx)
                    (asm :raw #x66 #x48 #x0f #x6e #xca)      ; movd-rdi-xmm1
                    (asm :mov 1 :rdi)
                    (asm :raw #x66 #x48 #x0f #x6e #xd7)      ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x5e #xca))          ; divss-xmm2-xmm1
                   ((:reg= pc #x24) (:rdx #x1) (:rdi 1) (:xmm1 #x3f800000) (:xmm2 1)))
  (oper asm-divss2 ((asm :mov 1 :rdx)
                    (asm :raw #x66 #x48 #x0f #x6e #xca)      ; movd-rdi-xmm1
                    (asm :raw #x48 #xbf #x01 #x00 #x00 #x00 #xff #xff #xff #xff) ; check divss only uses lower 64bit
                    (asm :raw #x66 #x48 #x0f #x6e #xd7)      ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x5e #xca))          ; divss-xmm2-xmm1
                   ((:reg= pc #x27) (:rdx #x1) (:rdi #xffffffff00000001) (:xmm1 #x3f800000) (:xmm2 #xffffffff00000001)))
  (oper asm-mulss0 ((asm :mov 1 :rdx)
                    (asm :raw #xf2 #x48 #x0f #x2a #xca) ; cvtsi2sd-rdx-xmm1
                    (asm :mov #x7f800000 :rdi)          ; rdi is positive infinity
                    (asm :raw #x66 #x0f #x6e #xd7)      ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x59 #xca))     ; mulss-xmm2-xmm1
                   ((:reg= pc #x23) (:rdx 1) (:rdi #x7f800000) (:xmm1 #x7f800000) (:xmm2 #x7f800000)))
  (oper asm-mulsd0 ((asm :raw #x48 #xb8 #xf6 #x9e #xae #x22 #x37 #x12 #xfa #x3f)
                    (asm :raw #x66 #x48 #x0f #x6e #xc8) ; movq-rax-xmm1
                    (asm :raw #xf2 #x0f #x5c #x0d #xa5 #x00 #x00 #x00) ; subsd-[rip+a5]-xmm1
                    (asm :raw #x48 #xb8 #x18 #x2d #x44 #x54 #xfb #x21 #x09 #x40)
                    (asm :raw #x48 #x89 #x45 #x40)      ; mov rax-[rbx+40]
                    (asm :raw #xf2 #x0f #x10 #x55 #x40) ; movsd-[rbp-10]-xmm2
                    (asm :raw #xf2 #x0f #x59 #xca))     ; mulsd-xmm2-xmm1
                   ((:reg= pc #x36) (:rax #x400921fb54442d18) (:xmm1 #x401479ea9f7c047b) (:xmm2 #x400921fb54442d18)
                    (:mem= #x66 #x18 #x2d #x44 #x54 #xfb #x21 #x09 #x40)))
  (oper asm-mulss1 ((asm :mov #x7f800000 :rdx)
                    (asm :raw #x66 #x48 #x0f #x6e #xca)      ; movd-rdi-xmm1
                    (asm :raw #x48 #xbf #xcd #xcc #xcc #xbd #x00 #x00 #x00 #x00)
                    (asm :raw #x66 #x48 #x0f #x6e #xd7)      ; movd-rdi-xmm2
                    (asm :raw #xf3 #x0f #x59 #xca))          ; mulss-xmm2-xmm1
                   ((:reg= pc #x27) (:rdx #x7f800000) (:rdi #xbdcccccd) (:xmm1 #xff800000) (:xmm2 #xbdcccccd)))
  (oper asm-shufps ((asm :raw #x48 #xb8 #x1a #x00 #x00 #x00 #x00 #x00 #x40 #x3f)
                    (asm :raw #x66 #x48 #x0f #x6e #xd0)      ; movq-rax-xmm2
                    (asm :raw #x0f #xc6 #xd2 #xfd))          ; shufps-fd-xmm2-xmm2
                   ((:reg= pc #x1b) (:rax #x3f4000000000001a) (:xmm2 #x3f400000)))
  (oper asm-shufps2 ((asm :raw #x48 #xb8 #x1a #x00 #x00 #x00 #xff #xff #x7f #xff)
                     (asm :raw #x66 #x48 #x0f #x6e #xd0)      ; movq-rax-xmm2
                     (asm :raw #x0f #xc6 #xd2 #xfd))          ; shufps-fd-xmm2-xmm2
                   ((:reg= pc #x1b) (:rax #xff7fffff0000001a) (:xmm2 #xff7fffff)))
  (oper asm-pcmpeqd ((asm :raw #x48 #xb8 #xaa #xaa #xaa #xaa #xaa #xaa #xaa #xaa)
                     (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                     (asm :raw #x66 #x48 #x0f #x6e #xc8)    ; movq-rax-xmm1
                     (asm :raw #x66 #x0f #x76 #xc1))        ; pcmpeqd-xmm1-xmm0
                    ((:reg= pc #x20) (:rax #xaaaaaaaaaaaaaaaa) (:xmm1 #xaaaaaaaaaaaaaaaa) (:xmm0 #xffffffff)))
  (oper asm-pcmpeqd2 ((asm :mov 0 :rax)
                      (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                      (asm :raw #x66 #x48 #x0f #x6e #xc8)    ; movq-rax-xmm1
                      (asm :raw #x66 #x0f #x76 #xc1)         ; pcmpeqd-xmm1-xmm0
                      (asm :raw #x48 #x0f #x50 #xc0))        ; movmskps-xmm0-rax
                     ((:reg= pc #x21) (:rax #xf) (:xmm1 #x0) (:xmm0 #xffffffff)))
  (oper asm-movmskps ((asm :mov #xffffffff :rax)
                      (asm :raw #x66 #x48 #x0f #x6e #xc0)    ; movq-rax-xmm0
                      (asm :raw #x48 #x0f #x50 #xc0))        ; movmskps-xmm0-rax
                    ((:reg= pc #x18) (:rax #xf) (:xmm0 #xffffffffffffffff))))

