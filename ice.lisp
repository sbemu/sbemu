
(in-package :sbemu)

(defvar *ice* nil)

(defvar *icebox* '(*malloc-heap*
                   *mmap-heap*
                   *mmap-heap-head*
                   *argv*
                   *libc*
                   ;*stubs*
                   ;*stubs-name*
                   *signal-handlers*
                   *sigmask*
                   *malloc*
                   ;*sbcl-heap*
                   ;*c-stack*
                   *page-fault*
                   ;*fd*   need to restore streams
                   ;*fdt*  and functions
                   *fdi*
                   *syms*
                   *memsz*
                   *memw*
                   *mem*))

(defun thaw ()
  (let (cpu)
    (format t "thawing from file [~a]~%" *ice*)
    (with-open-file (s *ice* :direction :input :element-type '(unsigned-byte 8))
      (setf cpu (restore s))
      (let ((registers (svref cpu 3)))
        (format t "thawing cpu at pc=~x~%" (reg :pc)))
      (loop for item in *icebox* do
        (format t "thawing ~a~%" item)
        (setf (symbol-value item) (restore s)))
      (format t "done thawing from file [~a]~%" *ice*))
    (format t "recreating stubs~%")
    ; wait, no need to recreate the stubs, if we havn't made them dirty (PIE has overwritten ldso-trampoline)
    (load-stub)
    (load "libc") ; FIX: ugly hack to populate the stubs hash (we cant save them due to lambda support in cl-store)
    (io-init) ; restore standard streams
    (format t "thaw complete.~%")
    cpu))

(defun freeze (cpu)
  (let ((file *ice*))
    (if (not file) (setf file "core.ice"))
    (format t "freezing to [~a]~%" file)
    (with-open-file (s file :direction :output :if-exists :supersede :if-does-not-exist :create :element-type '(unsigned-byte 8))
      (store cpu s)
      (format t "freezing cpu at pc=~x cpui=~x~%" (svref cpu 0) *cpui*)
      (loop for item in *icebox* do
        (format t "freezing ~a~%" item)
        (store (symbol-value item) s))
      (format t "done freezing.~%"))
    #+sbcl(sb-ext:quit)
    #+clisp(ext:quit)))

