; a frag is a instruction sequence where the first instruction is
; something where the cpu will jump or call to.
; The last instruction is a branch/ret or interrupt.
; between the first and last instruction there must be no branch/ret or interrupt instructions.

(in-package :sbemu)

(defvar *frag-lambda* (make-hash-table :test #'equal))

; decode a single frag
(defun dis-disasm-until-branch (cpu)
  (let (f r inst type)
    (setf f (setf r (disasm cpu 8 :decode-only t :quit-on-branch t)))
    (loop
      (L "~%doing disasm~%")
      (L "  result:~%")
      (loop while r do
        (setf inst (car r))
        (L "  ~x~%" inst)
        (setf type (fourth inst))
        (cond
          ((eq type 'jc)
            (setf (cdr r) nil)
            (return-from dis-disasm-until-branch f))
          ((eq type 'ret)
            (setf (cdr r) nil)
            (return-from dis-disasm-until-branch f))
          ((eq type 'j)
            (setf (cdr r) nil)
            (return-from dis-disasm-until-branch f))
          ((eq type 'c)
            (setf (cdr r) nil)
            (return-from dis-disasm-until-branch f))
          ((eq type 'int3)
            (setf (cdr r) nil)
            (return-from dis-disasm-until-branch f)))
        (setf r (cdr r)))
      (setf r (disasm cpu 8 :decode-only t :quit-on-branch t))
      (setf f (append f r)))))

; decode the whole function tree (returning a hash of frags)
; this would be used to take advantage of intra-frag optimization
(defun dis-lispfun-inst-tree (cpu hash)
  (L "--------------------------------------------- hash: ~a~%" hash)
  (let ((h (or hash (make-hash-table :test #'equal))) r li addr fa)
    (flet ((dis-leaf (addr)
             (L "  dis-leaf ~x hash value ~a~%" addr (gethash addr h))
             (unless (gethash addr h)
               (setf (areg cpu :rpc) addr)
               (dis-lispfun-inst-tree cpu h)
               ;(setf (gethash addr h) leaf)
               ;(setf leafs (nconc leafs leaf))
               )))
    ; get initial leaf
    (setf fa (setf addr (areg cpu :rpc)))
    (setf r (dis-disasm-until-branch cpu))
    ;(setf leaf (list r))
    (assert (not (gethash addr h)))
    (setf (gethash addr h) r)
    (L "initial leaf is ~x: ~x~%" addr r)
    (setf li (car (last r))) ; get last instruction
    (cond
      ((eq (fourth li) 'c) ; call
        (let ((next (second li)))
          (L "call found returning to : ~x~%" next)
          (dis-leaf next)))
      ((eq (fourth li) 'jc)
        (L "branch found: ~x~%" li)
        (let ((a (+ (first li) (nth 7 li))) ;  next instruction if branch not taken
              (b (+ (first li) (nth 7 li) (nth 6 li)))) ; instruction if branch taken
          (L "~a branch not taken address: ~x~%" a)
          (dis-leaf a)

          (L "~a branch taken address: ~x~%" b)
          (dis-leaf b))))
    (list fa h)
    ;(list fa h leafs)
    )))

(defun inst-source-analyze (src const)
  (L "  analyzing instruction source (shunting code paths using constants ~s)~%" const)
  (setf src (shunt-code src const))
  src)

(defun inst-source-analyze-load (src ind)
  (cond
    ((and (listp src) (eq (car src) 'load-mrm)) nil)
    ((and (listp src) (eq (car src) 'decode-mrm)) nil)
    ((and (listp src) (eq (car src) 'if) (eq (second src) 'ind))
      (if ind (third src)
              (fourth src)))
    ((listp src)
      (loop for item in src collect
        (inst-source-analyze-load item ind)))
    (t src)))


(defun compile-frag-lambda (code)
  (compile nil `(lambda (cpu)
                  (let* ((registers (cpu-regs cpu))
                         (pc (reg :pc))
                         (opc pc)
                         (flags (reg :flags))
                         x y z q branch quit)
                    (declare (ignorable x y z q branch))
                    (macrolet ((maybe-setpc (inst-name val)
                                 `(progn
                                    (L "~a pc=~x(new ~x) ------ branch=~a, flags=~b~%" ',inst-name pc ,val branch flags)
                                    (if (eq ',inst-name 'call/jmp/push) (setf quit :call))
                                    (if (eq ',inst-name 'ret) (setf quit :return))
                                    ; FIX: add tail-call (jmpq*)
                                    (setf pc ,val)))
                               (incpc (off) `(setf pc (+ pc ,off)))
                               (myreg (num) `(svref registers ,num))
                               (set-flags-o (b) `(setf flags (logior (logand flags (- #xffff  16)) (ash ,b 4)))) (get-flags-o ()  `(logand flags  16))
                               (set-flags-c (b) `(setf flags (logior (logand flags (- #xffff   1)) ,b)))         (get-flags-c ()  `(logand flags   1))
                               (set-flags-s (b) `(setf flags (logior (logand flags (- #xffff 128)) (ash ,b 7)))) (get-flags-s ()  `(logand flags 128))
                               (set-flags-z (b) `(setf flags (logior (logand flags (- #xffff  64)) (ash ,b 6)))) (get-flags-z ()  `(logand flags  64))
                               (set-flags-d (b) `(setf flags (logior (logand flags (- #xffff   8)) (ash ,b 3)))) (get-flags-d ()  `(logand flags   8))

                               )
                    (L "begin frag at pc=~x~%" pc)
                    ,@code
                    (L "frag finished, setting pc to ~x, branch=~a, flags=~b~%" pc branch flags)
                    (setf (reg :flags) flags)
                    (setf (reg :pc) pc)
                    (setf (reg :opc) opc)
                    quit)))))

(defun compile-frag (cpu frag)
  (declare (ignorable cpu))
  (let (src code)
    (L "  compiling source frag ~s~%" frag)
    (loop for inst in frag do
      (L "  -----------------------------------------~%")
      (L "    ~x~%" inst)
      (let (ind indbody const macro)
        (loop for item in inst do
          (cond
            ((and (listp item) (eq (car item) :ind))
              (setf ind t)
              (setf indbody (cdr item)))
            ((and (listp item) (eq (car item) :const))
              (setf const (cdr item)))
            ((and (listp item) (eq (car item) :macro))
              (setf macro (cdr item)))))
        (setf src (gethash (third inst) *runop2*))
        (assert src)
        ;(L "IND: ~a~%" ind)
        ;(L "Const: ~a~%" const)
        ; because we have called disasm, we have full information about the result of the load-body, therefore
        ; the load-body is not needed anymore. And the result of the load-body is used to optimize the src-body
        (destructuring-bind (letbody loadbody runbody) src
          (progn letbody loadbody runbody)
          ;(setf load (inst-source-analyze-load loadbody ind))
          ; add macros that are requested by the runop
          (let ((macros (loop for m in macro collect
                          (cond
                            ((eq m 'load-indirect)
                              '(load-indirect ()
                                 '(progn
                                    (setf x (if rs (if (= rs 31) pc (myreg rs)) 0))
                                    (if mul (setf x (* x mul)))
                                    (if bas (incf x (myreg bas)))
                                    (if imm (incf x imm))
                                    (setf y (mget-u64 x)))))
                            ((eq m 'load-indirect2)
                              '(load-indirect2 ()
                                 '(progn
                                    (setf x (if rs (if (= rs 31) pc (myreg rs)) 0))
                                    (if (logbitp 63 x) (setf x (- x (ash 1 64))))
                                    (if mul (setf x (* x mul)))
                                    (if bas (incf x (myreg bas)))
                                    (if imm (incf x imm))
                                    (setf y (case ow (1 (mget-u8 x)) ; FIX: shouldn't we use iw (input-width)?
                                                     (2 (mget-u16 x))
                                                     (4 (mget-u32 x))
                                                     (t (mget-u64 x)))))))
                            ((eq m 'load-indirect2-sign)
                              '(load-indirect2-sign ()
                                 '(progn
                                    (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
                                    (if mul (setf x (* x mul)))
                                    (if bas (incf x (reg bas)))
                                    (if imm (incf x imm))
                                    (setf y (case ow (1 (mget-s8 x))
                                                     (2 (mget-s16 x))
                                                     (4 (mget-s32 x))
                                                     (t (mget-s64 x)))))))
                            ((eq m 'load-address)
                              '(load-address ()
                                 '(progn
                                    (setf x (if rs (if (= rs 31) pc (reg rs)) 0))
                                    (if (logbitp 63 x) (setf x (- x (ash 1 64))))
                                    (if mul (setf x (* x mul)))
                                    (if bas (incf x (reg bas)))
                                    (if imm (incf x imm)))))
                            ))))
            (setf runbody (if macros
                            `((macrolet
                                (,@macros)
                                ,@runbody))
                            runbody)))
          (L "analyzing runbody source: ~s~%" runbody)
          (setf runbody (inst-source-analyze runbody const))
          (L "Analyzed inst src: ~s~%" runbody)
          (setf code (nconc code
                            (list (list 'maybe-setpc (third inst) (second inst)))
                            runbody))
          ;(L "Analyzed inst load: ~s~%" load)
          ;(L "code is : ~s~%" `(macrolet ((load-address () `(setf x ',',indbody))) ,@load ,@src))
          ;(L "code is : ~s~%" src)
          ;(setf code (nconc code `((macrolet ((load-address () `(setf x ',',indbody))) ,@load ,@src))))
          ;(setf code (nconc code src (list (list 'maybe-setpc (second inst)))))
          )))
    (L "~%-----------------------------------------~%")
    (L "  final code for frag compilation: ~s~%" code)
    (compile-frag-lambda code)))

(defun exec-frag (addr cpu h)
  (let ((frag (gethash addr *frag-lambda*)))
    (L "found frag-lambda: ~s (pc=~x)~%" frag addr)
    (unless frag
      (setf (areg cpu :rpc) addr)
      ;(setf h (second (dis-lispfun-inst-tree cpu nil))) ; we compile the frag as we go, instead of compiling the whole lisp-function in one go.
      ;
      (setf frag (dis-disasm-until-branch cpu)) ; so here we request the disassembly for just a single frag
      ;
      ;(setf frag (gethash addr h)) ; get source for frag
      (if (not frag) (return-from exec-frag (list :error "no source")))
      (setf frag (compile-frag cpu frag))
      (setf (gethash addr *frag-lambda*) frag))
      (setf (areg cpu :rpc) addr)
    (L "calling frag with pc=~x~%" (areg cpu :rpc))
    (assert (= (areg cpu :rpc) addr))
    (funcall frag cpu)))

(defun emulate-call (cpu)
  (let (addr reason h)
    (loop for i from 0 do
      (setf addr (areg cpu :rpc))
      (L "************************************************ ~a emulating call on ~x~%" i addr)
      (setf reason (exec-frag addr cpu h))
      (cond
        ((and reason (listp reason)) ; error when executing frag
          (L "ERROR when executing frag: ~a~%" reason)
          (return))
        ((and reason (symbolp reason))
          (ecase reason
            (:return
              (L " * * * * return from frag * * * *~%")
              (return))
            (:call
              (L " * * * * frag call * * * *~%")
              (return))))
        (reason
          (L " unknown reason to prematurely exit frag (interrupt?): ~s~%" reason)
          (return)))
      (L "************************************************ ~a after frag exec, new pc is ~x~%" i (areg cpu :rpc))
      )))

