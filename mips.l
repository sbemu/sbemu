;
; 16 MIPS instruction set
;
  16.1 MIPS instruction overview
  --------------------------------
  The instructions are defined using the macro:

  | (define-inst (name arguments))

  The arguments can be :noload which is used to delay
  the effect of an instruction. This will make the cpu
  act as if it where an pipeline architecture.

  The body to this macro constitues of normal forms but
  the :formula form.
  This special form is used to compress the syntax of
  the instruction operation.

  |   (:formula (:= rd (+ rs rt)))

  Will effectively be expanded to:

  |   (setf rd (+ (mreg rs) (mreg rt)))

  16.2 Boolean operations
  -------------------------
  These instructions do boolean functions such as AND, XOR etc.
  Because of their similarity they are lumped together in an
  MACROLET.

  16.2.1 Instruction AND OR XOR NOR on registers
  ------------------------------------------------
  Input and output are registers.

  |$(macrolet
  |   ((inst (name oper)
  |      (let ((strname (string-downcase (string name))))
  |        `(define-inst (,name)
  |           (:formula (:= rd (,oper rs rt)))
  |           (fmt "~a ~a ~a ~a" ,strname :rs :rt :rd)))))
  |     (inst and logand)
  |     (inst or  logior)
  |     (inst xor logxor)
  |     (inst nor lognor))

  16.2.2 Instruction AND OR XOR on immediate
  --------------------------------------------
  Input is register and immediate, output are register.

  |$(macrolet
  |   ((inst (name oper)
  |      (let ((strname (string-downcase (string name))))
  |        `(define-inst (,name)
  |           (:formula (:= rt (,oper immu rs)))
  |           (fmt "~a ~x ~a ~a" ,strname immu :rs :rt)))))
  |   (inst ori  logior)
  |   (inst andi logand)
  |   (inst xori logxor))


  16.3 Arithmetic operations
  ----------------------------

    16.3.1 Instruction SUB
    ----------------------------
    |$(define-inst (sub) ; sub
    |   (:formula (:= rd (- rs rt)))
    |   (fmt "sub ~a ~a ~a" :rs :rt :rd))

    16.3.2 Instruction ADDU
    ----------------------------
    |$(define-inst (addu) ; add unsigned (no overflow)
    |   (:formula (:= rd (logand (mask 32) (+ rs rt))))
    |   (if (zerop rt)
    |     (fmt "move ~a ~a" :rs :rd)
    |     (fmt "addu ~a ~a ~a" :rs :rt :rd)))

    16.3.3 Instruction SUBU
    ----------------------------
    |$(define-inst (subu) ; sub unsigned
    |   (:formula (:= rd (logand (mask 32) (- rs rt))))
    |   (fmt "subu ~a ~a ~a" :rs :rt :rd))

    16.3.4 Instruction ADDI
    ----------------------------
    |$(define-inst (addi) ; add immediate unsigned
    |   (:formula (:= rt (+ imm rs)))
    |   (fmt "addi ~x ~a ~a" imm :rs :rt))

    16.3.5 Instruction ADDIU
    ----------------------------
    |$(define-inst (addiu) ; add immediate unsigned (no overflow)
    |   (:formula (:= rt (+ imm rs)))
    |   (fmt "addiu ~x ~a ~a" imm :rs :rt))

    16.3.6 Instruction MULT
    ----------------------------
    |$(define-inst (mult) ; multiply
    |   (:formula (let ((x (* rs rt)))
    |               (:= :lo (ldb (byte 32 0) x))
    |               (:= :hi (ldb (byte 32 32) x))))
    |   (fmt "mult ~a ~a ~a:~a" :rs :rt :hi :lo))

    16.3.7 Instruction MULTU
    ----------------------------
    |$(define-inst (multu) ; multiply unsigned
    |   (:formula (let ((x (* rs rt)))
    |               (:= :lo (ldb (byte 32 0) x))
    |               (:= :hi (ldb (byte 32 32) x))))
    |   (fmt "multu ~a ~a ~a:~a" :rs :rt :hi :lo))

    16.3.8 Instruction DIV
    ----------------------------
    |$(define-inst (div) ; divide
    |   (:formula (:= (values :lo :hi) (truncate rs rt)))
    |   (fmt "div ~a ~a ~a:~a" :rs :rt :hi :lo))

  16.4 Load and Store operations
  --------------------------------

    16.4.1 Instruction LHU
    ------------------------
    |$(define-inst (lhu) ; load halfword unsigned
    |   (:formula (:= rt (mget-u16b (+ rs immu))))
    |   (fmt "lhu ~x ~x ~x" immu :rs :rt))

    16.4.2 Instruction LUI
    ------------------------
    |$(define-inst (lui) ; load upper immediate
    |   (:formula (:= rt (ash immu 16)))
    |   (fmt "lui ~x ~a" immu :rt))

    16.4.3 Instruction LW
    ------------------------
    |$(define-inst (lw) ; load word
    |   (:formula (:= rt (mget-u32b (+ rs imm))))
    |   (fmt "lw ~x ~a ~a" imm :rs :rt))

    16.4.4 Instruction LB
    ------------------------
    |$(define-inst (lb) ; load byte FIX: sign extend result?
    |   (:formula (:= rt (mget-u8 (+ rs imm))))
    |   (fmt "lb ~x ~a ~a" imm :rs :rt))

    16.4.5 Instruction LBU
    ------------------------
    |$(define-inst (lbu) ; load unsigned byte
    |   (:formula (:= rt (mget-u8 (+ rs imm))))
    |   (fmt "lbu ~x ~a ~a" imm :rs :rt))

    16.4.6 Instruction SW
    ------------------------
    |$(define-inst (sw) ; store word
    |   (:formula (mset-u32b (+ rs imm) rt))
    |   (fmt "sw ~x ~a ~a" imm :rs :rt))

    16.4.7 Instruction SB
    ------------------------
    |$(define-inst (sb) ; store byte
    |   (:formula (mset-u8 (+ rs imm) rt))
    |   (fmt "sb ~x ~a ~a" imm :rs :rt))


  16.5 Branch operations
  ----------------------------

    16.5.1 Instruction BEQ BNE 
    -----------------------------
    Following instructions branches wheter zero.

    |$(macrolet
    |   ((inst (name oper)
    |      (let ((strname (string-downcase (string name))))
    |        `(define-inst (,name :noload)
    |           (:formula (if ,oper :set-pc-imm))
    |           (fmt "~a ~x ~x ~x" ,strname vs vt (ash imm 2))))))
    |   (inst beq (=  (logand (mask 32) vs) (logand (mask 32) vt))) ; branch if equal
    |   (inst bne (/= (logand (mask 32) vs) (logand (mask 32) vt)))) ; branch unless equal

    16.5.2 Instruction Bxx..
    ---------------------------
    Following instructions branches by doing an arithmetic compare.

    |$(macrolet
    |   ((inst (name link oper)
    |      (let ((strname (string-downcase (string name))))
    |        `(define-inst (,name :noload)
    |           (:formula (when ,oper :set-pc-imm ,(if link :link-ra)))
    |           (fmt "~a ~x ~x" ,strname vs (ash imm 2))))))
    |   (inst blez  nil (or (logbitp 31 vs) (= vs 0))) ; branch if less than or equal to zero
    |   (inst bluez nil (<= vs 0)) ; branch if unsigned less than or equal to zero
    |   (inst bgez  nil (and (not (logbitp 31 vs)) (>= vs 0))) ; branch if greater or equal-zero (psuedo-op b if vs is zero-register)
    |   (inst bgtz  nil (and (not (logbitp 31 vs)) (> vs 0))) ; branch if greater than zero
    |   (inst bltz  nil (logbitp 31 vs)) ; branch if less than zero
    |   (inst bltzal t  (logbitp 31 vs)) ; branch if not equal and link
    |   (inst bgezal t  (>= vs 0)))      ; branch if greater or equal-zero and link

    16.5.3 Instruction JALR
    -------------------------
    Unconditional branching

    |$(define-inst (jalr :noload) ; jump and link register
    |   (:formula (progn (:= rd (+ pc 8))
    |                    (:= pc (- vs 8))))
    |   (fmt "jalr ~a ~x" :rd vs))

    16.5.4 Instruction JR
    -------------------------
    Unconditional branching

    |$(define-inst (jr :noload) ; jump register
    |   (:formula (:= pc (- vs 8)))
    |   (fmt "jr ~x" vs))

  16.6 Bit shift and rotate operations
  --------------------------------------

    16.6.1 Instruction SLLV
    -------------------------
    |$(define-inst (sllv) ; shift left logical value
    |   (:formula (:= rd (logand (mask 32) (ash rt (logand #b11111 rs)))))
    |   (fmt "sllv ~a ~a ~a" :rs :rt :rd))

    16.6.2 Instruction SLL
    -------------------------
    |$(define-inst (sll) ; shift left logical
    |   (:formula (:= rd (logand (mask 32) (ash rt ih))))
    |   (cond
    |     ((zerop ih) (fmt "nop"))
    |     (t (fmt "sll ~x ~a ~a" ih :rt :rd))))

    16.6.3 Instruction SRL
    -------------------------
    |$(define-inst (srl) ; shift right logical
    |   (:formula (:= rd (ash rt (- ih))))
    |   (fmt "srl ~x ~a ~a" ih :rt :rd))

    16.6.4 Instruction SRLV
    -------------------------
    |$(define-inst (srlv) ; shift right logical value
    |   (:formula (:= rd (ash rt (- (logand #b11111 rs)))))
    |   (fmt "srlv ~x ~a ~a" rs :rt :rd))

    16.6.5 Instruction SRA
    -------------------------
    |$(define-inst (sra) ; shift right arithmetic
    |   (:formula (:= rd (if (logbitp 31 rt)
    |                      (logior (ash (1- (ash (ash rt -31) ih)) (- 32 ih))
    |                              (ash rt (- ih)))
    |                      (ash rt (- ih)))))
    |   (fmt "sra ~x ~a ~a" ih :rt :rd))

    16.6.6 Instruction SRAV
    -------------------------
    |$(define-inst (srav) ; shift right arithmetic value
    |   (setf x (logand #b11111 (mreg rs)))
    |   (:formula (:= rd (if (logbitp 31 rt)
    |                      (logior (ash (1- (ash (ash rt -31) x)) (- 32 rs))
    |                              (ash rt (- x)))
    |                      (ash rt (- x)))))
    |   (fmt "srav ~x ~a ~a" x :rt :rd))

  16.7 Bit conditional set operations
  -------------------------------------

    16.7.1 Instruction SLT SLTU
    -----------------------------
    |$(macrolet
    |   ((inst (name oper)
    |      (let ((strname (string-downcase (string name))))
    |        `(define-inst (,name)
    |           (:formula (:= rd (if ,oper 1 0)))
    |           (fmt "~a ~a ~a ~a" ,strname :rs :rt :rd)))))
    |   (inst slt  (< (sign32 rs) (sign32 rt))) ; set on less than
    |   (inst sltu (< rs rt))) ; set on less than unsigned

    16.7.2 Instruction SLTI
    ---------------------------
    |$(define-inst (slti) ; set on less than immediate
    |   (:formula (:= rt (if (< (sign32 rs) imm) 1 0)))
    |   (fmt "slti ~x ~a ~a" imm :rs :rt))

    16.7.3 Instruction SLTIU
    ---------------------------
    |$(define-inst (sltiu) ; set on less than unsigned immediate
    |   (:formula (:= rt (if (< rs immu) 1 0)))
    |   (fmt "sltiu ~x ~a ~a" immu :rs :rt))

  16.8 Special operations
  -------------------------------------

    16.8.1 Instruction MFHI
    ---------------------------
    |$(define-inst (mfhi)
    |   (:formula (:= rd :hi))
    |   (fmt "mfhi ~a ~a" :rd :hi))

    16.8.2 Instruction MFLO
    ---------------------------
    |$(define-inst (mflo)
    |   (:formula (:= rd :lo))
    |   (fmt "mflo ~a ~a" :rd :lo))

    16.8.3 Instruction BREAK
    ---------------------------
    |$(define-inst (break)
    |   (setf quit t)
    |   (push-cpu-state cpu (list :signal (list (get-signal-handler rt) rt nil 0)))
    |   (fmt "break ~x" :rt))

  16.9 Floating point operations
  -------------------------------------

    16.9.1 Control register access instructions
    --------------------------------------------
    |$(define-inst (ctc1) (fmt "ctc1"))
    |$(define-inst (cfc1) (fmt "cfc1"))

    16.9.2 Floating-point register access instructions
    ---------------------------------------------------
    |$(define-inst (mfc1)
    |   (:formula (progn
    |     (:= y (fpu :rd))
    |     (setf y (case (type-of y)
    |               (single-float (single-float-to-bits y))
    |               (double-float (double-float-to-bits (coerce y 'single-float)))
    |               (t            y)))
    |     (:= rt y)))
    |   (fmt "mfc1 f~a ~a" rd :rt))

    |$(define-inst (mtc1)
    |   (:formula (:= (fpu :rd) rt))
    |   (fmt "mtc1 ~a f~a " :rt rd))

    16.9.3 Conversion instructions
    --------------------------------
    |$(define-inst (cvt.s)
    |   (setf x (ecase rs
    |             (#x11 :d) ; double-float (64bit)
    |             (#x14 :w))) ; word
    |   (:formula
    |     (:= (fpu (ldb (byte 5 6) immu))
    |         (case x
    |           (:w (single-float-to-bits (coerce (fpu (ldb (byte 5 11) immu)) 'single-float)))
    |           (:d (coerce (fpu (ldb (byte 5 11) immu)) 'single-float)))))
    |   (fmt "cvt.s.~a src=~a dst=~a" x (fpu (ldb (byte 5 6) immu) :name) (fpu (ldb (byte 5 11) immu) :name)))

    |$(define-inst (cvt.d)
    |   (setf z (ecase rs
    |             (#x14 :w))) ; W-32 fixed-point
    |   (:formula (progn
    |     (case z
    |       (:w (setf x (fpu (ldb (byte 5 11) immu)))
    |           (assert (integerp x)) ; if fail, probably due to incorrect instruction usage
    |           (if (logbitp 31 x) (setf x (- x (ash 1 32)))) ; sign convert
    |           (setf x (double-float-to-bits (coerce x 'double-float)))))
    |     (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))
    |     (:= (fpu (ldb (byte 5 6) immu))      (logand (mask 32) x))))
    |   (fmt "cvt.d.~a src=~a dst=~a(~x)" z (fpu (ldb (byte 5 6) immu) :name) (fpu (ldb (byte 5 11) immu) :name) x))

    |$(define-inst (cvt.w)
    |   (setf z (ecase rs
    |             (#x11 :d))) ; double-float (64bit)
    |   (:formula
    |     (:= (fpu (ldb (byte 5 6) immu))
    |         (case z
    |           (:d (truncate (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))))))
    |   (fmt "cvt.w.~a src=~a dst=~a" z (fpu (ldb (byte 5 6) immu) :name) (fpu (ldb (byte 5 11) immu) :name)))

    16.9.4 Load/Store instructions
    --------------------------------
    |$(define-inst (lwc1)
    |   (:formula (:= (fpu :rt) (mget-u32b (+ rs imm))))
    |   (fmt "lwc1 [~a+~x] f~a(~x)" :rs imm rt (fpu rt)))

    |$(define-inst (swc1)
    |   (:formula
    |     (progn
    |       (setf x (fpu :rt))
    |       (setf y (case (type-of x)
    |                 (single-float (single-float-to-bits x))
    |                 (double-float (single-float-to-bits (coerce x 'single-float)))
    |                 (cons         (get-single x))
    |                 (t            x)))
    |       (mset-u32b (+ rs imm) y)))
    |   (fmt "swc1 f~a(~x) [~x+~x=~x]=~x" rt x :rs imm (+ (mreg rs) imm) y))

    16.9.5 Aritmetic instructions
    -------------------------------
    |$(define-inst (abs)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd))
    |         (cond
    |           ((integerp x) (:= x (bits-to-single-float x)))
    |           (t x)))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (:= x (logior (fpu :rd) (ash (fpu (1+ :rd)) 32)))
    |         (:= x (bits-to-double-float x))))
    |     (:= y (abs x))
    |     (cond
    |       ((eq z :s)
    |         (:= y (coerce y 'single-float))
    |         (:= (fpu (ldb (byte 5 6) immu)) y))
    |       (t
    |         (:= x (double-float-to-bits y))
    |         (:= (fpu (ldb (byte 5 6) immu))      (logand x (mask 32)))
    |         (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))))))
    |   (fmt "abs.~a f~a f~a (~f)" z rd (ldb (byte 5 6) immu) y))

    |$(define-inst (add.f)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd))
    |         (:= y (fpu :rt))
    |         (if (integerp x) (:= x (bits-to-single-float x)))
    |         (if (integerp y) (:= y (bits-to-single-float y))))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (paranoid (and (integerp (fpu :rt)) (integerp (fpu (1+ :rt)))))
    |         (:= x (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))
    |         (:= y (bits-to-double-float (logior (fpu :rt) (ash (fpu (1+ :rt)) 32))))))
    |     (:= y (+ x y))
    |     (case z (:s
    |               (:= x (single-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu)) x))
    |             (t
    |               (:= x (double-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu))      (logand x (mask 32)))
    |               (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))))))
    |   (fmt "add.~a f~a f~a f~a (~f)" z rt rd (ldb (byte 5 6) immu) y))

    |$(define-inst (div.f)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd))
    |         (:= y (fpu :rt))
    |         (if (integerp x) (:= x (bits-to-single-float x)))
    |         (if (integerp y) (:= y (bits-to-single-float y))))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (paranoid (and (integerp (fpu :rt)) (integerp (fpu (1+ :rt)))))
    |         (:= x (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))
    |         (:= y (bits-to-double-float (logior (fpu :rt) (ash (fpu (1+ :rt)) 32))))))
    |     (:= y (/ x y))
    |     (case z (:s
    |               (:= x (single-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu)) x))
    |             (t
    |               (:= x (double-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu))      (logand x (mask 32)))
    |               (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))))))
    |   (fmt "div.~a f~a f~a f~a (~f)" z rt rd (ldb (byte 5 6) immu) y))

    |$(define-inst (mul.f)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd))
    |         (:= y (fpu :rt))
    |         (if (integerp x) (:= x (bits-to-single-float x)))
    |         (if (integerp y) (:= y (bits-to-single-float y))))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (paranoid (and (integerp (fpu :rt)) (integerp (fpu (1+ :rt)))))
    |         (:= x (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))
    |         (:= y (bits-to-double-float (logior (fpu :rt) (ash (fpu (1+ :rt)) 32))))))
    |     (:= y (* x y))
    |     (case z (:s
    |               (:= x (single-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu)) x))
    |             (t
    |               (:= x (double-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu))      (logand x (mask 32)))
    |               (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))))))
    |   (fmt "mul.~a f~a f~a f~a (~f)" z rt rd (ldb (byte 5 6) immu) y))

    |$(define-inst (sub.f)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd))
    |         (:= y (fpu :rt))
    |         (if (integerp x) (:= x (bits-to-single-float x)))
    |         (if (integerp y) (:= y (bits-to-single-float y))))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (paranoid (and (integerp (fpu :rt)) (integerp (fpu (1+ :rt)))))
    |         (:= x (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))
    |         (:= y (bits-to-double-float (logior (fpu :rt) (ash (fpu (1+ :rt)) 32))))))
    |     (:= y (- x y))
    |     (case z (:s
    |               (:= x (single-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu)) x))
    |             (t
    |               (:= x (double-float-to-bits y))
    |               (:= (fpu (ldb (byte 5 6) immu))      (logand x (mask 32)))
    |               (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) x))))))
    |   (fmt "sub.~a f~a f~a f~a (~f)" z rt rd (ldb (byte 5 6) immu) y))

    |$(define-inst (neg)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)
    |             (#b10001 :d)))
    |   (:formula (progn
    |     (case z
    |       (:s
    |         (:= x (fpu :rd)))
    |       (t
    |         (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |         (:= x (logior (fpu :rd) (ash (fpu (1+ :rd)) 32)))))
    |     (cond
    |       ((integerp x)
    |         (if (eq z :s)
    |           (:= y (logxor (ash 1 31) x))
    |           (:= y (logxor (ash 1 63) x))))
    |       (t (error "float format")))
    |     (:= (fpu (ldb (byte 5 6) immu))      (logand y (mask 32)))
    |     (:= (fpu (1+ (ldb (byte 5 6) immu))) (ldb (byte 32 32) y))))
    |   (fmt "neg.~a f~a f~a (~x)" z rd (ldb (byte 5 6) immu) y))

    16.9.6 Move instructions
    --------------------------------
    |$(define-inst (mov)
    |   (setf z (ecase rs ; format
    |             (#b10000 :s)))
    |   (:formula (:= (fpu (ldb (byte 5 6) immu)) (fpu :rd)))
    |   (fmt "mov.~a ~a ~a" z :rd (ldb (byte 5 6) immu)))

    16.9.7 Compare instructions
    -----------------------------
    |$(macrolet
    |   ((oper (name algo)
    |      `(define-inst (,name)
    |         (setf z (ecase rs ; format
    |                   (#b10000 :s)
    |                   (#b10001 :d)))
    |         (:formula (progn
    |           (case z
    |             (:s
    |               (:= x (fpu :rt))
    |               (:= y (fpu :rd))
    |               (if (integerp x) (:= x (bits-to-single-float x)))
    |               (if (integerp y) (:= y (bits-to-single-float y))))
    |             (t
    |               (paranoid (and (integerp (fpu :rt)) (integerp (fpu (1+ :rt)))))
    |               (paranoid (and (integerp (fpu :rd)) (integerp (fpu (1+ :rd)))))
    |               (:= x (bits-to-double-float (logior (fpu :rt) (ash (fpu (1+ :rt)) 32))))
    |               (:= y (bits-to-double-float (logior (fpu :rd) (ash (fpu (1+ :rd)) 32))))))
    |           (set-flags-z (if ,algo 1 0))))
    |         (fmt "~a.~a f~a(~f) f~a(~f)" ',name z rt x rd y))))
    |   (oper c.lt  (< y x))
    |   (oper c.seq (= y x))
    |   (oper c.ngt (<= y x)))

    16.9.8 Branch instructions
    ----------------------------
    |$(define-inst (bc1x :noload)
    |   (:formula (progn ; rt contain bits: CC(3) ND(1) TF(1) .. we only use TF for now
    |               (assert (< :rt 2)) ; assure no other bits are set, because then we need to check what those do (bits CC and ND).
    |               (cond
    |                 ((and (= :rt 1)      (logbitp 0 (mreg :flags)))  (:= pc (+ -4 pc (ash imm 2))))
    |                 ((and (= :rt 0) (not (logbitp 0 (mreg :flags)))) (:= pc (+ -4 pc (ash imm 2)))))))
    |   (fmt "bc1~a ~a" :rt imm))

