;
;  Monitor
; ----------
;

 7.1 Monitor Overview
 -----------------------
 The monitor is a program that tries to peek and poke at
 the PIE.
 The goal with the monitor is to have a 'gdb' like program
 but that is also sbcl aware, ie maybe more like LDB.

   7.1.1 Why do we need a monitor?
   ----------------------------------
   The monitor can assist in fault isolation by giving an
   easy to use tool during debugging.
   The fundamental tools like memory and hardware access
   is readily available without using a monitor. Just hit
   ctrl-c and type lisp forms in the lisp-hosts debugger.
   | (mget-u8 #x4000) ; get value of the c-args pointer
   The monitor provides compound information by gathering
   data and presenting it in a more useful way.

   7.1.2 Entering the monitor
   -----------------------------
   To start the monitor directly instead of starting the emulation:
   | sbcl --load load mon

   7.1.3 Using the monitor prompt
   ---------------------------------
   When you see the monitor prompt:
   | #|mon>|#
   You can begin typing in stuff. You can either type in monitor
   short commands, or normal lisp forms of list type.

   Example:
   | #|mon>|# (format t "Hi~%")
   | Hi
   | #|mon>|# q
   | quitting monitor...

   7.1.4 A trivial Monitor session
   ----------------------------------
   At the unix prompt sbemu is started in monitor mode:
   | sbcl --load load mon

   When the monitor prompt is reached, we begin by setting up
   the PIE:
   | #|mon>|# i

   The command 'i' will load sbcl into memory and make it ready
   for emulated execution. At this point we can begin to step instructions:
   | #|mon>|# s
   | CPU0.1.0 0x40BA00  XOR.r RBP(1FFFDF70) -> RBP(1FFFDF70) = 0 fr:40:Z

   A little side note, CPU1.2.3 means this instruction was executed on thread 1,
   it was globally the second instruction to run. And within this cpu's current
   RUNT we've consumed three ticks.

   Or we can print the cpu contents of the last runned cpu:
   | #|mon>|# r
   |cpu 0 0
   |  RAX        0
   |  RCX        0
   |...
   |  PC   40BA02
   |  OPC   40BA00
   |  Flags: Z

   We now let the program run until we hit the lisp function REINIT:
   | #|mon>|# g reinit
   | ... Cpu 0 entered lisp function.
   | #|mon>|#

   At this point we check the backtrace:
   | #|mon>|# b
   | just called, will return to 1000026560
   | lisp-backtrace frame/stack: 2207DC0 = 2207EC0,  2207DB0 = 1000026560
   | frame: 2207EC0, return-address: 10000281FB
   | frame: 2207FC0, return-address: 0

 7.2 Monitor Commands
 ----------------------
   7.2.1 Function mon-exit
   --------------------------
   |$(defmon "q" mon-exit (args) (declare (ignore args)) (setf *monitor-state* :exit))

   This expression defines a monitor command that exit the monitor.
   It doesn't take any argument. But instead set the state of the monitor to :EXIT.
   The command is associated to the short-command "q".
   If using this command in a script, the lisp function is named MON-EXIT.

   7.2.2 Function mon-mem
   -------------------------
   Prints the contents of memory. It takes the arguments address and length.
   If the memory isn't paged (realized) it will print NIL.

   Example:

   | #|mon>|# m 65536 8
   | addr 65536 8
   |    10000:  NIL NIL NIL NIL NIL NIL NIL NIL

   The implemenation of the memory command is:

   |$(defmon "m" mon-mem (args)
   |   (let ((addr (maybe-read-from-string (or (first args)  nil)))
   |         (len  (or (maybe-read-from-string (second args)) 48))
   |         (size (or (maybe-read-from-string (third args))  16)))
   |     (labels ((initstr (&optional (acc "") (i 0))
   |                (if (not (zerop (mod (- addr i) size)))
   |                  (initstr (concatenate 'string acc "   ") (1+ i)))))
   |     (macrolet
   |       ((acc () `(if (zerop (mod addr size)) "" (initstr)))
   |        (pri () `(lambda (pair acc)
   |                   (destructuring-bind (addr value) pair
   |                     (concatenate 'string acc
   |                                  (if (zerop (mod addr size))
   |                                    (format nil "~%~8x: " addr) "")
   |                                  (format nil " ~2x" value)))))
   |        (memory () `(loop for i from addr below (+ addr len) collect i))
   |        (memget () `(lambda (addr) (list addr (mget-u8-soft addr)))))
   |       (write-line (foldl (pri) (acc) (map 'list (memget) (memory)))
   |                   *monitor-ostream*)))))

   7.2.3 Function mon-init
   ---------------------------
   Loads the sbcl executable and setup everything (main-args, memory, cpus) ready to run.

   |$(defmon "i" mon-init (args)
   |   (declare (ignore args))
   |   (run-emulated-sbcl nil))

   7.2.4 Function mon-step
   ---------------------------
   Steps execution a number of iterations.

   |$(defmon "s" mon-step (args)
   |   (let ((step (first args)))
   |     (setf step (if step (read-from-string step) 1))
   |     (loop-cpus step)
   |     (force-output common::*log-stream*)))

   7.2.5 Function mon-go
   ---------------------------
   Go until a lisp function is entered

   |$(defmon "g" mon-go (args)
   |   (let ((funname (string-upcase (first args)))
   |         (*cpu-log* nil))
   |     (format t "searching for ~s~%" funname)
   |     (setf *monitor-lisp-function-break* (string-upcase funname))
   |     (loop
   |       (loop-cpus #x4000)
   |       (unless (stringp *monitor-lisp-function-break*)
   |         (format *monitor-ostream* "Cpu ~x entered lisp function." (cpu-tn *monitor-lisp-function-break*))
   |         (return)))
   |     (setf *monitor-lisp-function-break* nil)
   |     (force-output common::*log-stream*)))

   7.2.6 Function mon-regs
   --------------------------
   Print the register values. Both what register and what cpu's can be selected.
   Optional arguments: Registers Cpus
   Where Registers is either a register name or an asterisk to select all registers.
   And Cpus is either a thread-number or asterisk to print all cpu's.
   Specifying no arguments will print all registers for all threads.

   |$(defmon "r" mon-reg (args)
   |   (flet ((print-cpu (cpu reg)
   |            (format *monitor-ostream* "cpu ~a ~a~%" (cpu-tn cpu) (cpu-arch cpu))
   |            (cond
   |              ((or (not reg) (string= "*" reg))
   |                (let ((regs (cpu-regs cpu)))
   |                  (cond
   |                    ((eq (archname x86-64) (cpu-arch cpu))
   |                      (loop for i from 0 below 24 for val across regs do
   |                        (format *monitor-ostream* "  ~a ~8x~%" (register-name i) val))
   |                      (loop for i from 31 below 33 do
   |                        (format *monitor-ostream* "  ~a ~8x~%" (register-name i) (aref regs i)))
   |                      (let ((flags (aref regs 33)))
   |                        (format *monitor-ostream* "  Flags: ~a~a~a~a~a~a~%"
   |                                (if (logbitp 0 flags) "C" "")
   |                                (if (logbitp 3 flags) "P" "")
   |                                (if (logbitp 4 flags) "D" "")
   |                                (if (logbitp 5 flags) "O" "")
   |                                (if (logbitp 6 flags) "Z" "")
   |                                (if (logbitp 7 flags) "S" "")))))))
   |              (t
   |                (setf reg (read-from-string reg))
   |                (format *monitor-ostream* "  ~a ~8x~%" (register-name (register-index reg)) (aref (cpu-regs cpu) (register-index reg)))))))
   |     (let ((reg (first args))
   |           (tn  (second args)))
   |       ; get a list of cpus
   |       (cond
   |         ((not tn) (print-cpu *cpu* reg)) ; thread-number not specified, print last used cpu
   |         ((string= "*" tn) (loop for cpu across *threads* do (print-cpu cpu reg)))
   |         (t (print-cpu (aref *threads* (parse-integer tn)) reg))))))

   7.2.7 Function mon-bt
   --------------------------
   Backtrace.

   |$(defmon "b" mon-bt (args)
   |   (let ((funname (first args))
   |         (cpu *cpu*))
   |     (cond
   |       ((eq (archname x86-64) (cpu-arch cpu))
   |         (let ((sf (aref (cpu-regs cpu) 5))
   |               (sp (aref (cpu-regs cpu) 4)) ret)
   |           (setf ret (mget-u64-soft sp)) ; get return address
   |           (format *monitor-ostream* "just called, will return to ~x~%" ret)
   |           (format *monitor-ostream* "lisp-backtrace frame/stack: ~x = ~x,  ~x = ~x~%" sf (mget-u64-soft sf) sp (mget-u64-soft sp))
   |           (loop repeat 4 do
   |             (setf sf (mget-u64-soft sf))
   |             (setf ret (mget-u64-soft (- sf 8)))
   |             (format *monitor-ostream* "frame: ~x, return-address: ~x~%" sf ret)
   |             (if (or (not ret) (zerop ret)) (return)))))
   |       ((eq (archname mips) (cpu-arch cpu))
   |         (let (;(lra (aref (cpu-regs cpu) 18))
   |               (sf (aref (cpu-regs cpu) 22))
   |               (sp (aref (cpu-regs cpu) 23))
   |               (cfp (aref (cpu-regs cpu) 30))
   |               ret)
   |           (setf ret (+ 5 (mget-u32b-soft (+ sf 4)))) ; get return address
   |           (format *monitor-ostream* "just called, will return to ~x function header at ~x~%" ret cfp)
   |           (format *monitor-ostream* "lisp-backtrace frame/stack: ~x = ~x,  ~x = ~x~%" sf (mget-u32b-soft sf) sp (mget-u32b-soft sp))
   |           ;(format *monitor-ostream* "lra=~x~%" lra) ; may not match sf+4 if function hasn't saved lra there yet.
   |           (loop repeat 4 do
   |             (setf sf (mget-u32b-soft sf))
   |             (setf ret (+ 5 (mget-u32b-soft (+ sf 4)))) ; get return address
   |             (format *monitor-ostream* "frame: ~x, return-address: ~x~%" sf ret)
   |             (if (or (not ret) (zerop ret)) (return)))))
   |       (t
   |         (format *monitor-ostream* "ERROR: backtrace not supported for cpu-arch ~a" (cpu-arch cpu))))))

