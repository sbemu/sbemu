; FIX: move testing into it's own package
(in-package :sbemu)

(defvar *cmem* nil)
(defvar *cpu1* nil)
(defvar *cpu2* nil)
(defvar *asm-comem* nil)
(defvar *asm-fillpointer* nil)

(defmacro aver (cond &rest args)
  `(let ((res ,cond))
     (unless res (error ,@args))))

(defmacro cpu-registers (cpu) `(svref ,cpu 3))

(defun run-cpu (cpu addr len)
  (setf (areg cpu :rpc) addr)
  (disasm cpu len))

(defun psuedo-register? (cpu r)
  (cond
    ((= (archname mips) (cpu-arch cpu)) (and (numberp r) (> r 65)))
    ((= (archname x86-64) (cpu-arch cpu))
      (and (numberp r)
           (or (= r #x1d) ; flags
               (= r #x1e) ; opc
               (= r #x1f) ; pc
               )))))

(defun my-make-memory (len)
  (setf *mem* (make-array len :initial-element nil :fill-pointer len :adjustable t))
  (setf *memw*  (make-array len :initial-element 7 :element-type '(unsigned-byte 8)))
  (setf *memsz* len)
  (setf *cmem* (make-array len :initial-element nil :fill-pointer len :adjustable t)))

(defun my-make-page (mem addr)
  (make-page addr)
  (let* ((page (ash addr (- PAGEBN)))
         (parr (aref mem page)))
    (if (not parr)
      (setf (aref mem page) (make-array (page-size) :element-type '(unsigned-byte 8) :initial-element 0)))))

(defun my-mset-u8 (mem addr data)
  (mset-u8 addr data)
  (let* ((page (ash addr (- PAGEBN)))
         (parr (aref mem page)))
    (if parr
      (setf (aref parr (logand addr PAGEMS)) data)
      ; FIX this is a page fault
      (error "write page-fault at page ~x, address ~x" page addr))
    nil))

(defun my-mget-u8 (mem addr)
  (list (mget-u8 addr)
        (let* ((page (ash addr (- PAGEBN)))
               (parr (aref mem page)))
          (if parr
            (aref parr (logand addr PAGEMS))))))

(defun make-cpu-x86-64 ()
  (make-cpu (get-arch-enum 'x86-64)))

(defun make-cpu-mips ()
  (make-cpu (get-arch-enum 'mips)))

(defun get-arg-value (arg cpu)
  (let ((registers (cpu-registers cpu)))
    (cond
      ((listp arg) (eval arg))
      ((numberp arg) arg)
      ((symbolp arg)
        (case arg
          (pc (reg :pc))
          (t (svref registers (register-index arg)))))
      (t (error "unknown check-oper register mnemonic: ~s" arg)))))

(defun reg-diff (cpu cpu2)
  (let ((reg1 (cpu-registers cpu))
        (reg2 (cpu-registers cpu2))
        res
        (nregs (cond ((= (cpu-arch cpu) (archname x86-64)) 31)
                     ((= (cpu-arch cpu) (archname mips)) 67) ; see mips-util.lisp
                     (t 0))))
    ;(format t "reg-diff cpus: ~s,~s~%" cpu cpu2)
    ; push on register array changes in the interval 0 to nregs
    (loop for i from 0 below nregs do
      ;(format t "check: ~s,~s~%" (svref reg1 i) (svref reg2 i))
      (if (not (equal (svref reg1 i) (svref reg2 i)))
        (push (list i (svref reg2 i)) res)))
    ; push on cpu program-counter, because that is outside the 0-nregs interval
    (if (not (equal (areg cpu :rpc) (areg cpu2 :rpc))) (push (list 'pc (areg cpu2 :rpc)) res))
    res))

(defun check-regs (cpu cpu2 checks)
  ;(format t "~%************ checking registers~%")
  (assert (eq (cpu-arch cpu) (cpu-arch cpu2)))
  (let ((checks (delete nil (loop for check in checks collect
                              (cond
                                ((eq (car check) :reg=) check)
                                ((eq (car check) :mem=) nil)
                                (t
                                  (cond
                                    ((= (cpu-arch cpu) (archname x86-64))
                                      (if (areg :x86-64 (car check)); (ignore-errors (register-index (car check)))
                                        (list* :reg= check)))
                                    ((= (cpu-arch cpu) (archname mips))
                                      (if (areg :mips (car check))
                                        (list* :reg= check)))))))))
        (diffs (reg-diff cpu cpu2)) found)
    ;(format t "checks: ~s~%" checks)
    ;(format t "diffs:  ~s~%" diffs)
    (loop for (ct r1 val1) in checks do
      (if (eq r1 :pc) (setf r1 'pc)) ; workaround
      (if (keywordp r1)
        (cond
          ((= (cpu-arch cpu) (archname x86-64)) (setf r1 (register-index r1)))
          ((= (cpu-arch cpu) (archname mips))   (setf r1 (regnum r1)))))
      ;(format t "checking check: ~s,~s,~s~%" ct r1 val1)
      (setf found nil)
      ;(format t "diffs: ~s~%" diffs)
      (unless (psuedo-register? cpu r1)
        (setf diffs (delete nil (loop for (r2 val2) in diffs collect
                                  (cond
                                    ((psuedo-register? cpu2 r2) nil)
                                    ((equal r1 r2)
                                      (setf found t)
                                      (if (consp val1) (setf val1 (get-bits val1)))
                                      (if (consp val2) (setf val2 (get-bits val2)))
                                      (when (> (abs (- val1 val2)) 0.0001)
                                        (error "mismatch reg-val regs: ~a,~a vals: expected ~x got ~x" r1 r2 val1 val2))
                                      nil)
                                    (t (list r2 val2)))))))
      (unless found (error "ERROR: the check ~x,~x,~x wasn't found in the register diff"
                           ct
                           (if (numberp r1)
                             (cond
                               ((= (cpu-arch cpu) (archname x86-64)) (register-name r1))
                               ((= (cpu-arch cpu) (archname mips))   (regstr r1)))
                             r1)
                           val1)))
    (if diffs (error "ERROR: these register diffs wasn't found in checks: ~x~%" diffs))))

(defun check-mems (cmem checks)
  (let ((checks (delete nil (loop for check in checks collect (if (or (eq (car check) :mem=)) check))))
        diffs found bc)
    ;(format t "~%************ checking memory~%")
    (loop for p from 0
          for pagea across cmem
          for pageb across *mem* do
      (when (and pagea pageb)
        (loop for i from 0
              for ma across pagea
              for mb across pageb do
          (if (/= ma mb) (push (list (+ (ash p PAGEBN) i) mb) diffs)))))
    (loop for check in checks do
      (let ((mem (second check))
            (bytes (cddr check)))
        (setf found nil)
        ;(format t "check ~s ~s~%" mem bytes)
        (setf bc 0)
        (loop for val in bytes do
          (setf diffs (delete nil (loop for (mem2 val2) in diffs collect
                        (cond
                          ((= mem mem2)
                            ;(format t "matching diff ~s/~s ~s/~s~%" mem mem2 val val2)
                            (if (/= val val2) (error "memory mismatch addr: ~x value: expected ~x got ~x" mem val val2))
                            (incf bc)
                            (setf found t)
                            nil)
                          (t (list mem2 val2))))))
          (incf mem))
        ;(format t "diffs: ~s bc: ~s~%" diffs bc)
        (if (not found) (error "ERROR: the check ~x wasn't found in the memory diff~%" check))))
    (if diffs (error "these memorydiffs wasn't checked: ~x~%" diffs))))

(defun test-debug-mem (addr len)
  (format t "#x~x: " addr)
  (loop for i from 0 below len do
    (destructuring-bind (mema memb) (my-mget-u8 *cmem* (+ addr i))
      (if (equal mema memb)
        (format t " ~x" mema)
        (format t " ~x/~x" memb mema)))) ; print old memory to new
  (format t "~%"))

(defun debug-dump-mem (addr len)
  (format t "#x~x: " addr)
  (loop for i from 0 below len do
    (format t " ~x" (mget-u8 (+ addr i))))
  (format t "~%"))

; a very simple assembler to enable compound test cases

(defun asm-init (mem fp)
  (setf *asm-fillpointer* fp)
  (setf *asm-comem* mem))

; store-machine-code
(defun smc (&rest bytes)
  (loop for x in bytes do
    (my-mset-u8 *asm-comem* *asm-fillpointer* x)
    (incf *asm-fillpointer*)))

(defun asm (oper &rest args)
  (ecase oper
    (:raw (apply 'smc args))
    (:mov8
      (destructuring-bind (src dst) args
        (cond
          ((and (numberp src) (keywordp dst))
            (smc #x48
                 (+ #xb8 (logand (register-index dst) #b111))
                 (ldb (byte 8 0)  src)
                 (ldb (byte 8 8)  src)
                 (ldb (byte 8 16) src)
                 (ldb (byte 8 24) src)
                 (ldb (byte 8 32) src)
                 (ldb (byte 8 40) src)
                 (ldb (byte 8 48) src)
                 (ldb (byte 8 56) src)))
          (t (error "asm: unknown operands to :mov")))))
    (:mov ; sign-extend a 32bit value and mov it
      (destructuring-bind (src dst) args
        (cond
          ; (asm mov #x201001b0 :rax) => rex op mrm imm32
          ((and (numberp src) (keywordp dst))
            (let* ((rs (register-index dst))
                   (rsl (logand rs  #b111))
                   (rsh (logand rs #b1000)))
              (smc (logior #x48 (ash rsh -3)) #xc7 (logior #b11000000 rsl)
                   (logand src #xff)
                   (ldb (byte 8 8) src)
                   (ldb (byte 8 16) src)
                   (ldb (byte 8 24) src))))
          (t (error "asm: unknown operands to :mov")))))))

;;; test cases utils
(defun maybe-add-pc-check (checks code code-offset)
  (when (not (loop for check in checks thereis (or (eq (car check) 'pc)
                                                   (and (eq (first check) :reg=)
                                                        (eq (second check) 'pc)))))
    (setf checks (nconc checks `((pc ,(+ (length code) code-offset))))))
  checks)

