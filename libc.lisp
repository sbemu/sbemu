(in-package :sbemu)

(defvar *linux-self* nil)

(defmacro libc-return (arg) `(setf (reg :rax) ,arg))

(defun mget-word (arch addr)
  (ecase arch
    (0 (mget-u64 addr))
    (1 (mget-u32b addr))))

(defun mset-word (arch addr value)
  (ecase arch
    (0 (mset-u64 addr value))
    (1 (mset-u32b addr value))))

; move this to x86-64
;(defmacro reg-s64 (name)
;  `(let ((v (reg ,name)))
;     (if (not (zerop (logand v #x8000000000000000))) (- v #x10000000000000000) v)))

; get the real register indexes using cpu-arch

(defun libc-get-ret (cpu)
  (ecase (cpu-arch cpu)
    (0 +lib-ret-x86-64+)
    (1 +lib-ret-mips+)))

(defun get-libc-arg (arch registers i)
  (cond
    ((eq arch (archname x86-64))
      (cond
        ((>= i (length +lib-args-x86-64+))
          (error "stack argument for x86-64 not implemented"))
        (t (svref registers (nth i +lib-args-x86-64+)))))
    ((eq arch (archname mips))
      (cond
        ((>= i (length +lib-args-mips+))
          (let ((sp (svref registers 29))
                (n (- i (length +lib-args-mips+))))
            (mget-u32b (+ sp (* i 4)))))
        (t (svref registers (nth i +lib-args-mips+)))))))

(defmacro define-stub-funcall ((name cpu registers ret) &body body)
  `(progn
     (format t "loading stub imp ~a~%" ,name)
     (add-stub ,name (lambda (,cpu ,registers ,ret)
                       (declare (ignorable ,registers))
                       ,@body))))

(defmacro define-libcfun ((name (&rest argnames)) &body body)
  (assert (keywordp name))
  (let ((letbody (loop for i from 0 for argname in argnames collect
                   `(,argname (get-libc-arg arch registers ,i)))))
    `(define-stub-funcall (,name cpu registers ret)
       (let ((reti (libc-get-ret cpu))
             (arch (cpu-arch cpu)))
         (let (,@letbody)
           (setf (svref registers reti) (progn ,@body)))
         ret))))

(defun sign-extend64 (v)
  (if (not (zerop (logand v #x8000000000000000))) (- v #x10000000000000000) v))

(defun sign-extend32 (v)
  (setf v (logand v (mask 32)))
  (if (not (zerop (logand v #x80000000))) (- v #x100000000) v))

(defun get-string (addr)
  (let ((str (make-array 0 :fill-pointer 0 :adjustable t :element-type 'character))
        v)
    (loop for i from 0 below 256 do
      (setf v (mget-u8 (+ addr i)))
      (if (= v 0) (return))
      (vector-push-extend (code-char v) str))
    str))

(defun set-string (addr string)
  (loop for a across string
        for i from 0 do
    (mset-u8 (+ addr i) (char-code a)))
  (mset-u8 (+ addr (length string)) 0)
  addr)

(define-stub-funcall (:__libc_start_main@plt cpu registers ret)
  (declare (ignore ret))
  (let ((arch (cpu-arch cpu))
        (registers (cpu-regs cpu))
        (main (areg cpu :arg1)))
    (cond
      ((eq arch (archname x86-64))
        (rset-u64 :rdi (length *c-args*))
        (rset-u64 :rsi *argv*)
        (rset-u64 :rax 0))
      ((eq arch (archname mips))
        (setf (svref registers 25) main) ; set t9
        (setf (svref registers 4) (length *c-args*)) ; a0 = c-argn
        (setf (svref registers 5) *argv*) ; a1 = c-agv
        ))
    (L "transfer control to c-main at ~x (or equivalent) with c-argn=~x c-argv=~x" main (length *c-args*) *argv* :os)
    main))

(defun futex (cpu uaddr op val timeout)
  (ecase op
    ((0 128) (L "syscall futex_wait at address ~x~%" uaddr)
      (let ((cur (mget-u64 uaddr)))
        (cond
          ((= cur val)
            (setf timeout (if (not (zerop timeout))
                            ; take a struct-timespec (second,nanosecond) and convert to
                            ; almost millisecond. 1000 milliseconds is nearly 1024.
                            ; convert the seconds to almost-milli by up 1024, and nanosecond to almost-milly by down 1024*3
                            (logior (ash (mget-u64 timeout) 11) (ash (mget-u64 (+ timeout 8)) (* 11 3)))
                            nil))
            (L "futex timeout set to ~ams~%" timeout)
            (add-futex cpu uaddr op val timeout)))))
    ((1 129) (L "syscall futex_wake at address ~x~%" uaddr)
      (wake-futex uaddr val)))
  0)

(define-libcfun (:syscall@plt (id x y z q))
  (ecase id
    (202
      (futex cpu x y z q))))

(define-libcfun (:getpid@plt ())
  (L "getpid => #xcafe~%" :lib)
  #xcafe)

(define-libcfun (:sigemptyset@plt (sam))
  (L "sigemptyset sa.mask=[~x]~%" sam :lib)
  (mset-u32 sam 0)
  0)

(define-libcfun (:sigaddset@plt (sam sig))
  (let (v)
    (L "rdi=~x, rsi=~x~%" sam sig :lib)
    (setf v (mget-u32 sam))
    (setf v (logior v (ash 1 (1- sig))))
    (mset-u32 sam v)
    (L "sigaddset sa.mask=~x~%" v :lib))
  0)

(define-libcfun (:sigismember@plt (sa sig))
  (let (v)
    (cond
      ((not (zerop sa))
        (ecase (cpu-arch cpu)
          (0 (setf v (mget-u64 sa)))
          (1 (setf v (mget-u32b sa))))
        (L "sigismember *sa=~x sig=~x~%" v sig :lib)
        (if (zerop (logand v (ash 1 (1- sig)))) 0 1))
      (t 0))))

(define-libcfun (:sigaction@plt (sig sa osa))
  (let (fun mask)
    (cond
      ((eq (cpu-arch cpu) (archname x86-64))
        (setf fun  (mget-u64 sa))
        (setf mask (mget-u64 (+ sa 8))))
      ((eq (cpu-arch cpu) (archname mips))
        (setf fun  (mget-u32b (+ sa 4))) ; not a union so skip first word(sa_handler)
        (setf mask (mget-u32b (+ sa 8)))))
    ;(setf flags (mget-u64 (+ sa 12)))
    (L "sigaction sig=~x, sa=~x, osa=~x fun,msk:(~x,~x)~%"
              sig sa osa fun mask :lib)
    (set-signal-handler sig fun mask))
  0)

(define-libcfun (:kill@plt (pid sig))
  (let (sa)
    (L "kill ~a on ~x~%" sig pid :lib)
    (case pid
      (#xcafe
        (setf sa (get-signal-handler sig))
        ; FIX: what if the program hasn't registered any signal handler?
        (if sa (push-cpu-state cpu (list :signal (list sa sig 0 0))))
        (L "when we are done with interrupt , we should return to ~x~%" ret :os)
        0)
      (t
        (L "killing external pid ~a not supported~%" pid :lib)
        1))))

(defun libc-sigprocmask (cpu how sa osa)
  (let ((arch (cpu-arch cpu))
        (sigmask (cpu-sigmask cpu)))
    (if (not (zerop osa)) (mset-word arch osa sigmask))
    (if (= 1 arch) (decf how)) ; seems 32/64 bit libc uses how differently (see /usr/include/bits/sigaction.h)
    (when (not (zerop sa))
      (setf (cpu-sigmask cpu)
            (ecase how
              (0 (logior sigmask (mget-word arch sa))) ; SIG_BLOCK
              (1 (logand sigmask (lognot (mget-word arch sa)))) ; SIG_NOBLOCK
              (2 (mget-word arch sa)))) ; SIG_SETMASK
      (L "new sigmask is ~x~%" (cpu-sigmask cpu) :lib)))
  0)

(define-libcfun (:pthread_sigmask@plt (how sa osa))
  (L "pthread_sigmask how=~a sa=~x osa=~x~%" how sa osa :lib)
  (libc-sigprocmask cpu how sa osa)
  0)

(define-libcfun (:sigprocmask@plt (how sa osa))
  (L "pthread_sigmask how=~a sa=~x osa=~x~%" how sa osa :lib)
  (libc-sigprocmask cpu how sa osa)
  0)

; play fast and loose with the mutexes
(macrolet ((stub (name)
             `(define-libcfun (,name ())
                0)))
  (stub :pthread_mutex_lock@plt)
  (stub :pthread_mutex_unlock@plt)
  (stub :pthread_cond_init@plt)
  (stub :pthread_mutex_init@plt))

(define-libcfun (:pthread_self@plt ())
  (1+ (cpu-tn cpu))) ; KLUDGE: sbcl doesn't allow pthread_t objects with value zero

(define-libcfun (:pthread_kill@plt (pthread sig))
  (L "pthread_kill tn=~x sig=~x~%" pthread sig :lib)
  (let ((cpu (get-thread (1- pthread))))
    (if (get-cpu-state cpu) ; try to NOTE if thread is in any signal state when killed
      (progn            ; most probably we want to push the signal and wait for the current to finish
        (unless (eq (car (get-cpu-state cpu)) :futex) ; futex state is a false positive that we dont want to push, but go on and kill
          (L "NOTE: thread ~x already in exception state ~s" (1- pthread) (get-cpu-state cpu)))))
    (push-cpu-state cpu (list :signal (list (get-signal-handler sig) sig 0 0))))
  0)

(define-libcfun (:pthread_key_create@plt (key))
  (let ((hash (cpu-pthread cpu)))
    (unless hash (setf hash (setf (cpu-pthread cpu) (make-hash-table))))
    (setf (gethash key hash) nil)
    0))

(define-libcfun (:pthread_getspecific@plt (key))
  (if (cpu-pthread cpu) (gethash key (cpu-pthread cpu)))) ; really return nil?

(define-libcfun (:pthread_setspecific@plt (key val))
  (let ((hash (cpu-pthread cpu)))
    (unless hash (setf hash (setf (cpu-pthread cpu) (make-hash-table))))
    (setf (gethash key hash) val)
    0))

; we use our own convention for attrib layout (pthreadtypes.h)
(define-libcfun (:pthread_attr_init@plt (attr))
  (mset-u64 attr 0)       ; address to stack
  (mset-u64 (+ attr 8) 0) ; size of stack
  0)

(define-libcfun (:pthread_attr_setstack@plt (attr stack size))
  (mset-u64 attr stack)       ; address to stack
  (mset-u64 (+ attr 8) size) ; size of stack
  0)

(defun make-libc-thread (parent-cpu address &key pthread-attr pthread pthread-arg)
  (let* ((cpu (make-thread (cpu-arch parent-cpu))) sloti (arch (cpu-arch cpu))
         (registers (cpu-regs cpu)))
    (setf sloti (cpu-tn cpu))
    (setf (cpu-state cpu) nil)
    (setf (cpu-sigmask cpu) (cpu-sigmask parent-cpu))
    (cond
      ((= arch (archname x86-64))
        (setf (svref registers 31) address) ; set pc
        (if pthread (mset-u64 pthread (1+ sloti))) ; store thread reference (cant be zero)
        ; go through any args and set registers
        (when pthread-arg
          (let ((r (car +lib-args-x86-64+)))
            (setf (svref registers r) pthread-arg)))
        (when pthread-attr
          (let ((stack (mget-u64 pthread-attr))
                (size  (mget-u64 (+ pthread-attr 8))))
            (unless (zerop stack)
              (mset-u64 (+ stack size) 0) ; when thread finally returns, steer it to exit-vector
              (setf (svref registers 4) (+ stack size)) ; set rsp
              (setf (svref registers 5) (+ stack size)))))) ; set rbp
      (t (error "make-libc-thread is not supported for arch ~a" arch)))
    cpu))

(define-libcfun (:pthread_create@plt (thread attr routine arg))
  (make-libc-thread cpu routine :pthread-attr attr :pthread thread :pthread-arg arg)
  0)

(define-libcfun (:fprintf@plt (fd fmt))
  (let (v (arg 2) nregs reg-args bz (arch (cpu-arch cpu)))
    (cond
      ((= arch (archname x86-64))
        (setf bz 8)
        (setf reg-args +lib-args-x86-64+))
      ((= arch (archname mips))
        (setf bz 4)
        (setf reg-args +lib-args-mips+)))
    (setf nregs (1- (length reg-args)))
    (flet ((vget (type)
             (let (val)
               (cond
                 ((< arg nregs)
                   (setf val (svref registers (nth arg reg-args)))
                   (L "(register arg=~a:~x)"  (nth arg reg-args) val))
                 (t
                   (L "(stack arg rsp=~x)" (areg cpu :rsp) )
                   (setf val (- (areg cpu :rsp) (* arg bz) (- (* bz 2)))) ; shouldn't we mget the stack-value?
                   (L "(val=~x)" val )))
               (incf arg)
               (when val
                 (case type
                   (:i32 val)
                   (:u32 val)
                   (:str (get-string val)))))))
    (L "fprintf ~a,~a [" fd fmt :lib)
    (loop for i from 0 below 256 do
      (setf v (mget-u8 (+ fmt i)))
      (cond
        ((= v 0) (return))
        ((= v 37) ; %
          (incf i)
          (setf v (mget-u8 (+ fmt i)))
          (cond
            ((= v 105) (L "~a" (vget :i32))) ;%i
            ((= v 120) (L "~x" (vget :u32))) ;%i
            ((= v 115) (L "~s" (vget :str))))) ;%s
        (t
          (L "~a" (code-char v)))))
    (L "]~%")
    0)))

(define-libcfun (:vfprintf@plt (out fmt))
  (L "vfprintf ~a[~a]~%" out (get-string fmt))
  0)

(define-libcfun (:fwrite@plt (fmt fd))
  (let (v)
    (L "fwrite ~x,~x [" fd fmt :lib)
    (loop for i from 0 below 256 do
      (setf v (mget-u8 (+ fmt i)))
      (if (= v 0) (return))
      (L "~a" (code-char v)))
    (L "]~%")
    0))

(define-libcfun (:malloc@plt (sz))
  (L "malloc ~x bytes~%" sz :lib)
  (let ((v (make-malloc sz)))
    ;(L "malloc ~x bytes at ~x~%" sz v :lib)
    (L "malloc ~x bytes at ~x~%" sz v :lib)
    v))

(define-libcfun (:calloc@plt (item nums))
  (let (v)
    (setf v (* item nums))
    (setf v (make-malloc v))
    (L "calloc ~xx~x(~x) bytes at ~x~%" item nums (* item nums) v :lib)
    v))

(define-libcfun (:free@plt (addr))
  (L "free ~x~%" addr :lib)
  (os-free addr)
  0)

(define-libcfun (:memcpy@plt (dst src len))
  (let (v)
    (L "memcpy from ~x -> ~x, ~x bytes" dst src len)
    (loop for i from 0 below len do
      (setf v (mget-u8 (+ src i)))
      (mset-u8 (+ dst i) v))
    dst))

(define-libcfun (:strcpy@plt (dst src))
  (let (v (n 0))
    (L "strcpy from [~x]" (get-string src) :lib)
    (mset-u8 dst 0)
    (loop for i from 0 below 1024 do ; dont copy forever
      (setf v (mget-u8 (+ src i)))
      (if (= v 0) (return))
      (incf n)
      (mset-u8 (+ dst i) v))
    (mset-u8 (+ dst n) 0)
    (L " to [~x] len=~x~%" (get-string dst) n)
    dst))

(define-libcfun (:__strdup@plt (src))
  (let ((str (get-string src)))
    (L "strdup [~x]~%" str :2 :lib)
    (set-string (make-malloc (1+ (length str))) str)))

(define-libcfun (:memmove@plt (dst src len))
  (let (x)
    (loop for i from 0 below len do
      (setf x (mget-u8 (+ src i)))
      (mset-u8 (+ dst i) x)))
  dst)

(define-libcfun (:strcmp@plt (dst src))
  (let ((x (get-string dst))
        (y (get-string src)))
    (L "strcmp [~x<->~x]" x y :lib)
    (if (string= x y)
      0
      dst)))

(define-libcfun (:strlen@plt (str))
  (let (v (n 0))
    (L "strlen ~x [" str :lib)
    (loop for i from 0 below 256 do
      (setf v (mget-u8 (+ str i)))
      (if (= v 0) (return))
      (incf n)
      (L "~a" (code-char v)))
    (L "]~%")
    n))

(define-libcfun (:strstr@plt ()) ; args are haystack needle
  ;(L "strstr find [~a] in [~a]~%" (get-string needle) (get-string haystack) :lib) ; FIX-SBCL bug
  #xdead) ; just return assuming we found something

(defun strtol (src)
  (let ((str (make-array 0 :fill-pointer 0 :adjustable t :element-type 'character)) v n)
    (loop for i from 0 below 16 do
      (setf v (mget-u8 (+ src i)))
      (if (zerop v) (return))
      (vector-push-extend (code-char v) str))
    (setf n (parse-integer str :junk-allowed t))
    (L "strtol: ~s => ~a~%" str n :lib)
    n))

(define-libcfun (:strtol@plt (src))
  (assert (eq (cpu-arch cpu) (archname x86-64)))
  (strtol src)) ; return in R12 sometimes?

(define-libcfun (:__strtol_internal@plt (src))
  (strtol src))

(define-libcfun (:atoi@plt (src)) ; llvm uses this
  (strtol src))

(define-libcfun (:strchr@plt (str chr))
  (let (v n)
    (L "strchr" :lib)
    (loop for i from 0 below 256 do
      (setf v (mget-u8 (+ str i)))
      (if (= v 0) (return))
      (if (not n) (setf n 0))
      (if (= chr v) (return))
      (incf n))
    (if n
      (+ str n)
      0)))

(define-libcfun (:sprintf@plt (dst fmt va1 va2))
  (let ((str (get-string fmt)) n)
    (mset-u8 dst 0)
    (setf fmt (get-string fmt))
    (cond
      ((string= fmt "%s%s")
        (setf va1 (get-string va1))
        (loop for i from 0 below 256
              for a across va1
              do
          (mset-u8 (+ dst i) (char-code a)))
        (setf n (length va1))
        (setf va2 (get-string va2))
        (loop for i from 0 below 256
              for a across va2
              do
          (mset-u8 (+ dst i n) (char-code a)))
        (mset-u8 (+ dst n (length va2)) 0))
      (t
        (L "sprintf WARNING: fmt [~s] not implemented" str)))
    (L "sprintf [~s]~%" (get-string dst) :lib))
  0)

(define-stub-funcall (:pow@plt cpu registers ret)
  (declare (ignore cpu))
  (let ((bas (get-double (reg :xmm0)))
        (exp (get-double (reg :xmm1))))
    (L "bas=~f exp=~f~%" bas exp)
    (L "raw regs: ~x,~x~%" (reg :xmm0) (reg :xmm1))
    (setf (reg :xmm0) (make-doublefloat :double (expt bas exp)))
    (libc-return 0)
    ret))

; FIX: move to libc-extras.lisp (where is that file anyway?)
(macrolet
  ((math (name formula)
     `(define-stub-funcall (,name cpu registers ret)
        (declare (ignore cpu))
        (cond
          ((= (cpu-arch cpu) (archname x86-64))
            (let ((x (get-double (reg :xmm0))))
              (L "oper ~a raw regs: ~x,~x cpui=~x~%" ',name x (reg :xmm0) *cpui* :lib)
              (setf (reg :xmm0) (make-doublefloat :double ,formula))))
          ((= (cpu-arch cpu) (archname mips))
            (let ((x (fpu :f12))  ; mips+hard-float calling-convention
                  (y (fpu :f13)) z)
              (L "oper ~a raw regs: ~x,~x cpui=~x~%" ',name x y *cpui* :lib)
              (setf x (bits-to-double-float (logior (ash y 32) x)))
              (setf z ,formula)
              (setf z (double-float-to-bits z))
              (setf (fpu :f0) (logand z (mask 32)))
              (setf (fpu :f1) (ldb (byte 32 32) z)))))
        (libc-return 0)
        ret)))
  (math :exp@plt (exp x))
  (math :log@plt (log x)) ; FIX: log can return non-double
  (math :cos@plt (cos x))
  (math :sin@plt (sin x))
  (math :tan@plt (tan x))
  (math :acos@plt (acos x))
  (math :asin@plt (asin x))
  (math :atan@plt (atan x))
  (math :cosh@plt (cosh x))
  (math :sinh@plt (sinh x))
  (math :tanh@plt (tanh x))
  (math :acosh@plt (acosh x))
  (math :asinh@plt (asinh x))
  (math :atanh@plt (atanh x)))

(define-libcfun (:uname@plt (utsname))
  ; set release field in utsname struct 
  (loop for i from 0 for a across "2.6.123" do
    (mset-u8 (+ utsname i (* 65 2)) (char-code a)))
  (mset-u8 (+ utsname 7 (* 65 2)) 0)
  0)

(define-libcfun (:readlink@plt (file str sz))
  (let ((filename (get-string file)))
    (L "readlink [~a]~%" filename)
    (if (string= filename "/proc/self/exe") (setf filename (or *linux-self* (first *c-args*))))
    (L "readlink new filename is [~a]~%" filename)
    (cond
      ((ignore-errors (probe-file filename))
        (L "  doing truename on [~a]~%" filename)
        (let ((newfile (format nil "~a" (truename filename))))
          (L "readlink ~x ~s -> ~s at ~x,~x~%" file (get-string file) newfile str sz :lib :3)
          (loop for i from 0 below sz
                for c across newfile do
            (mset-u8 (+ str i) (char-code c)))
          (length newfile)))
      (t #xffffffff))))

(define-libcfun (:confstr@plt (conf))
  (ecase conf
    (3 ; _CS_GNU_LIBPTHREAD_VERSION
      (L "confstr _CS_GNU_LIBPTHREAD_VERSION~%" :lib)
      ))
    ;(loop for i from 0 below sz
    ;      for c across sbcl do
    ;  (mset-u8 (+ str i) (char-code c)))
  1)

(define-libcfun (:mmap@plt (addr len prot flags fd offset))
  (let ((offset (logand offset (mask 32))))
    (setf fd (sign-extend32 fd))
    (mmap addr len prot flags fd offset)))

(define-libcfun (:mmap64@plt (addr len prot flags))
  ; mips: (addr len prot flags fd ??? offsethi offset)
  ; For some reason the abi is different on mips than x86-64.
  ; It seems that a 64bit value occupies two parameters.
  (let ((arch (cpu-arch cpu)) fd offset)
    (cond
      ((eq arch (archname x86-64))
        (setf fd (reg :r8))       ; arg5
        (setf offset (reg :r9)))  ; arg6
      ((eq arch (archname mips))
        (setf fd     (get-libc-arg arch registers 4))
        (setf offset (get-libc-arg arch registers 7))))
    (setf offset (logand offset (mask 32)))
    (setf fd (sign-extend32 fd))
    (mmap addr len prot flags fd offset)))

(define-libcfun (:mprotect@plt (addr len prot))
  (mprotect addr len prot)
  0)

(define-libcfun (:mprotect64@plt (addr len prot))
  (mprotect addr len prot)
  0)

(define-libcfun (:open@plt (file flags))
  (setf file (get-string file))
  (open-file file flags))

(define-libcfun (:open64@plt (file flags))
  (setf file (get-string file))
  (open-file file flags))

(define-libcfun (:close@plt (fd))
  (close-file fd)
  0)

(define-libcfun (:read@plt (fd buf len))
  (L "read: try reading ~x bytes from fd ~x~%" len fd :lib)
  (let* ((lst (read-file fd len))
         (ll (length lst)))
    (setf len (length lst))
    (loop for i from 0 below ll
          for x in lst do
      (mset-u8 (+ buf i) x))
    (L "return length ~a~%" ll)
    ll))

(define-libcfun (:write@plt (fd buf len))
  (write-file fd buf len))

(define-libcfun (:lseek@plt (fd off whe))
  (setf fd (sign-extend32 fd))
  (setf off (sign-extend32 off))
  (setf whe (sign-extend32 whe))
  (seek-file fd off whe))

(define-libcfun (:lseek64@plt (fd off whe))
  (setf fd (sign-extend32 fd))
  (setf off (sign-extend32 off))
  (setf whe (sign-extend32 whe))
  (L "lseek fd=~x, off=~x, whence=~x~%" fd off whe)
  (cond
    ((and (>= whe 0) (< whe 3)) (seek-file fd off whe))
    (t 0))) ; whence=-1 seen on mips

; FIX: setup a dl open/close handle-hash,
; for now just return something non-zero (it's an opaque handle)
(define-libcfun (:dlopen@plt (file flags))
  (if (zerop file)
    (L "dlopen file=0, flags=~a~%" flags)
    (L "dlopen file=~a, flags=~a~%" (get-string file) flags))
  *libc*) ; just return something, it is an opaque handle anyway

(define-libcfun (:dlclose@plt (handle))
  (L "dlclose ~a~%" handle)
  0)

(define-libcfun (:getcwd@plt (buf size))
  (let ((cwd (format nil "~a" (truename "./"))))
    (setf cwd (subseq cwd 0 (1- (length cwd))))
    (cond
      ((zerop buf)
        (setf buf (make-malloc (1+ (length cwd))))
        (loop for a across cwd
              for i from 0 do
          (mset-u8 (+ buf i) (char-code a)))
        (mset-u8 (+ buf (length cwd)) 0))
      (t (mset-u8 buf 0)
        (loop for a across cwd
              for i from 0 below size do
           (mset-u8 (+ buf i) (char-code a)))
        (mset-u8 (+ buf (length cwd)) 0)))
    (L "getcwd copied ~s to location ~x~%" cwd buf :os))
  buf)

; FIX: test this routine has UTF/UNICODE issues
(define-libcfun (:opendir@plt (fileaddr))
  (let* ((file (get-string fileaddr))
         (files (directory (format nil "~a*.*" file)))
         bulk (totsz 8) cur) ; totsz 8 make room for end-of-array marker
    (L "searched dir [~a] found ~a files" (format nil "~a*.*" file) (length files) :2 :lib)
    (flet ((add-file (file)
             (assert file)
             (incf totsz (+ (length file) 1 8 8 2 1)) ; length of dirent struct
             file))
      (setf files (nconc (list (add-file ".") (add-file ".."))
                         (loop for path in files collect
                           ; FIX: portability issues, assuming '.' for example.
                           (let ((file (or (let ((name (pathname-name path))
                                                 (type (pathname-type path)))
                                             (if name (concatenate 'string name (if type  "." "") (if type type ""))))
                                           (car (last (pathname-directory path))))))
                             (add-file file))))))
    (L "opendir file list: [~s]~%" files :2 :lib)
    (setf cur (setf bulk (make-malloc totsz)))
    (loop for file in files do
      (L "  opendir add file [~a] at direntry ~x~%" file cur :4 :lib)
      ; see /usr/include/bits/dirent.h
      (mset-u64 cur #xbeefcafedeadfeed) (incf cur 8)          ; inode-number
      (mset-u64 cur (+ 8 8 2 1 1 (length file))) (incf cur 8) ; offset to the next dirent
      (mset-u16 cur (+ 8 8 2 1 1 (length file))) (incf cur 2) ; length of this record
      (mset-u8  cur 0) (incf cur 1)                         ; type of file
      (set-string cur file) (incf cur (1+ (length file))))  ; filename
    (mset-u64 cur 0) ; mark next dirent structure as end-of-array
    bulk))

(define-libcfun (:closedir@plt (pointers))
  (L "closedir handle ~x~%" pointers :lib)
  (os-free pointers)
  0)

(define-libcfun (:readdir@plt (bulk))
  (let ((ret 0))
    (loop
      (let ((inod (mget-u64 bulk))
            (next (mget-u32 (+ bulk 8)))
            (type (mget-u8 (+ bulk 8 8 2))))
        (L "readdir: next dirent is at bulk+offset=~x+~x, this dirent has type ~x and inode ~x~%" bulk next type inod :lib)
        (cond
          ((zerop inod) (return)) ; we've reached the end of the dirent array
          ((zerop type)
            (mset-u8 (+ bulk 8 8 2) 1) ; mark this dirent as consumed
            (setf ret bulk)
            (L "  found new dirent at offset ~x~%" bulk :lib)
            (return))
          (t ; get next dirent slot
            (incf bulk next)))))
    ret))

; stat mess according to LSB binary standard
; stat (path stat_buf) = __xstat64 (3 path stat_buf)
; lstat(path stat_buf) = __lxstat64(3 path stat_buf)
; fstat(fd stat_buf)   = __fxstat64(3 fd stat_buf)

(defun lsb-version-assert (ver be)
  (unless (and (integerp ver) (= ver be))
    (L "WARNING: stat requested, but LSB version found is ~a, wanted ~a" ver be :lib)
    ; according to LSB 2/3 it gives unspecified result to have version /= 3, but stat.h says ver-1 is x86-64
    ; see more: http://refspecs.linuxbase.org/LSB_2.0.0/LSB-Core/LSB-Core/baselib-xstat-1.html
    #+nil(error "bad LSB version ~a instead of ~a" ver be)))

(defun libc-stat (who arch file buf)
  (L "~a doing stat on file [~x] to buf ~x~%" who file buf :lib)
  (let ((file (cond
                ((and (stringp file) (string= file "tty")) "/dev/tty") ; IO has tty mapped to a regular file
                ((stringp file) file)
                (t (get-string file))))
        (i 0) flen mode)
    (L "~a: [~a]~%" who file :lib)
    (cond
      ((probe-file file)
        (ecase arch
          (0
            ;(setf flen (file-length file))
            (setf mode (logior (if (is-directory file) #8r40000 #8r100000)))
            (setf flen #x309)
            (mset-u64 (+ buf i) #x111) (incf i 8) ; dev      -a0
            (mset-u64 (+ buf i) #x222) (incf i 8) ; inode    -98
            (mset-u64 (+ buf i) #x444) (incf i 8) ; nlink    -90
            (mset-u32 (+ buf i) mode)  (incf i 4) ; mode     -88
            (mset-u32 (+ buf i) #x3e8) (incf i 4) ; uid      -84
            (mset-u64 (+ buf i) #x3e8) (incf i 8) ; gid      -80
            (mset-u64 (+ buf i) #x666) (incf i 8) ; rdev     -78
            (mset-u64 (+ buf i) flen)  (incf i 8) ; size     -70
            (mset-u64 (+ buf i) 4096) (incf i 8)  ; blksize  -68
            (mset-u64 (+ buf i) (1+ (truncate (/ flen 4096)))) (incf i 8) ; blocks   -60
            (mset-u64 (+ buf i) #xf1) (incf i 8)  ; atime    -58
            (mset-u64 (+ buf i) #xf2) (incf i 8)  ;
            (mset-u64 (+ buf i) #xf3) (incf i 8)  ; mtime    -48
            (mset-u64 (+ buf i) #xf4) (incf i 8)  ;
            (mset-u64 (+ buf i) #xf5) (incf i 8)  ; ctime    -38
            (mset-u64 (+ buf i) #xf6) (incf i 8))  ;
          (1 ; mips-sim-abi32/mips-sim-nabi32 version of stat (see asm/stat.h)
            ;(setf flen (file-length file))
            (setf mode (logior (if (is-directory file) #8r40000 #8r100000)))
            (setf flen #x309)
            (mset-u32b (+ buf i) #x801) (incf i 4) ; dev      0 (0=word-offset)
            (incf i (* 4 3))                       ; padding for network id 1-3
            (mset-u32b (+ buf i) #x000) (incf i 4) ; inode    4 
            (mset-u32b (+ buf i) mode)  (incf i 4) ; mode     5
            (mset-u32b (+ buf i) mode)  (incf i 4) ; nlink    6 real mode?
            (mset-u32b (+ buf i) #x555) (incf i 4) ; uid      7
            (mset-u32b (+ buf i) #x777) (incf i 4) ; gid      8
            (mset-u32b (+ buf i) #x666) (incf i 4) ; rdev     9
            (incf i (* 4 3))                       ; padding  a-c
            (mset-u32b (+ buf i) flen)  (incf i 4) ; size     d
            (mset-u32b (+ buf i) 0) (incf i 4) ; (1+ (truncate (/ flen 4096)))) (incf i 4) ; blocks 10
            (mset-u32b (+ buf i) 4096)  (incf i 4) ; blksize  e
            (mset-u32b (+ buf i) #x40000000) (incf i 4)  ; atime    15
            (mset-u32b (+ buf i) #xf2) (incf i 4)  ;
            (mset-u32b (+ buf i) #x40000000) (incf i 4)  ; mtime    17
            (mset-u32b (+ buf i) #xf4) (incf i 4)  ;
            (mset-u32b (+ buf i) #x40000000) (incf i 4)  ; ctime    19
            (mset-u32b (+ buf i) #xf6) (incf i 4)
            (mset-u32b (+ buf i) 4096)  (incf i 4) ; blksize
            (mset-u32b (+ buf i) (1+ (truncate (/ flen 4096)))) (incf i 4) ; blocks
            ))
        0)
      (t
        (mset-u32 *libc-errno* 2) ; set errno = file not found
        #xffffffff)))) ; return -1

(defun libc-fdstat (who arch fd buf)
  (let ((file (first (aref *fdi* fd)))) ; get the filename (special if fd < 3 or tty) FIX: wont work with sbemu-io-over-socket feature
    (L "fdstat: [~a] from fd ~a~%" file fd :lib)
    (libc-stat who arch file buf)))

; FIX: test this on mips
(define-libcfun (:__xstat@plt    (ver file buf)) (lsb-version-assert ver 1) (libc-stat   :__xstat  (cpu-arch cpu) file buf))
(define-libcfun (:__lxstat@plt   (ver file buf)) (lsb-version-assert ver 1) (libc-stat   :__lxstat (cpu-arch cpu) file buf))
(define-libcfun (:__fxstat@plt   (ver fd   buf)) (lsb-version-assert ver 1) (libc-fdstat :__fxstat (cpu-arch cpu) fd   buf))
(define-libcfun (:__xstat64@plt  (ver file buf)) (lsb-version-assert ver 1) (libc-stat   :__xstat64  (cpu-arch cpu) file buf))
(define-libcfun (:__lxstat64@plt (ver file buf)) (lsb-version-assert ver 1) (libc-stat   :__lxstat64 (cpu-arch cpu) file buf))
(define-libcfun (:__fxstat64@plt (ver fd   buf)) (lsb-version-assert ver 1) (libc-fdstat :__fxstat64 (cpu-arch cpu) fd   buf))

(define-libcfun (:getuid@plt ())
  1001)

(define-libcfun (:realpath@plt (path retpath))
  (let ((path (get-string path)))
    (cond
      ((probe-file path)
        (setf path (format nil "~a" (truename path)))
        (let ((n (length path)))
          (when (> n 1)
            (if (char= (char path (1- n)) #\/)
              (setf path (subseq path 0 (1- n))))))
        (L "realpath [~a]~%" path :lib)
        (cond
          (path
            (set-string retpath path)
            retpath)
          (t 0)))
      (t #xffffffff))))

(define-libcfun (:getenv@plt (what))
  (let ((str (get-string what)))
    (L "getenv: find key [~a]~%" str :lib)
    (cond
      ((string= str "SBCL_HOME")
        (set-string *libc* +lib-env-sbcl-home+)
        *libc*)
      ((string= str "HOME")
        (set-string *libc* +lib-env-home+)
        *libc*)
      (t 0))))

(define-libcfun (:gettimeofday@plt (tv))
  (L "gettimeofday stored at ~x~%" tv :lib)
  (let ((utc (get-universal-time)))
    (ecase (cpu-arch cpu)
      (0 (mset-u64 tv (- utc 2208988800))
         (mset-u64 (+ tv 8) 0))
      (1 (mset-u32b tv (- utc 2208988800))
         (mset-u32b (+ tv 4) 0)))
    0))

(define-libcfun (:nl_langinfo@plt ())
  (loop for i from 0 for a across "UTF-8" do
    (mset-u8 (+ *libc* i) (char-code a)))
  (mset-u8 (+ *libc* (length "UTF-8")) 0)
  *libc*)

(define-libcfun (:select@plt (nfds readfds writefds errorfds timeout))
  (L "select ~a:[~x,~x,~x] -> " nfds readfds writefds errorfds :lib)
  (let ((rbits (if (not (zerop readfds))  (mget-u64 readfds)))
        (wbits (if (not (zerop writefds)) (mget-u64 writefds)))
        (ebits (if (not (zerop errorfds)) (mget-u64 errorfds)))
        (tout  (if (not (zerop timeout))  (mget-u32 timeout)))
        (n 0))
    (if rbits (loop for i from 0 below 32 do (if (logbitp i rbits) (incf n))))
    (if wbits (loop for i from 0 below 32 do (if (logbitp i wbits) (incf n))))
    (if ebits (loop for i from 0 below 32 do (if (logbitp i ebits) (incf n))))
    (L "bits: ~x,~x,~x timeout: ~x bits: ~a~%" rbits wbits ebits tout n)
    (sleep 1)
    n))

; return number of structures that has nonzero revents fields (ie events or errors)
; or return 0 if timeout was reached
(define-libcfun (:poll@plt (fds nfds timeout))
  (L "poll ~a fds, timeout ~a" nfds timeout)
  (cond
    ((not (zerop fds))
      (let ()
        (mset-u16 (+ fds 4 2) 1) ; set revents field to 1(POLLIN)
        )))
  1)

(define-libcfun (:localtime_r@plt ())
  *libc*)

(define-libcfun (:gmtime_r@plt ())
  *libc*)

(define-libcfun (:__errno_location@plt ())
  *libc-errno*)

(define-libcfun (:exit@plt ())
  (push-cpu-state cpu (list :thread-exit nil))
  (setf ret 0)) ; set pc to exit-vector (just in case we dont quit)

