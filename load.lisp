; sbcl emulator

; setup compilation configuration

(require :asdf)

; load information on compile profile for sbemu
; this information is picked up from config.lisp
; and command-line.
(asdf:oos 'asdf:load-op :sbemu-config)
(sbemu::config-parse-cli)

; after configuration is read, load any extra packages that
; configuration has requested
(sbemu::load-by-config)

; ok, now everything is setup for compilation of sbemu
(asdf:oos 'asdf:load-op :sbemu)

; now that we've compiled sbemu it is time to do
; run-time configuration, this configuration comes from
; config.lisp, site.lisp and the command-line

(in-package :sbemu)

(defvar *site* "site.lisp")

; first a bunch of helper functions
(defun set-log-level (level)
  (setf common::*log-level* level))

(defun set-config ()
  ;(setf common::*log-level* 3) ; set an apropiate default log-level
  (loop for form in *config-late* do
    (format t "setting config ~a~%" form)
    (eval form)))

(defun load-and-run-rtest ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (load "r/load")
  (funcall 'rtest))

(defun load-and-run-gcctest ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (load "t/gcc/load")
  (funcall 'gcctest))

(defun load-and-run-test ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (funcall 'test))

(defun cli-parse-log (arg)
  (format t "logfile: ~a~%" arg)
  (setf common::*log-filename* arg))

(defun cli-parse-verbose (arg) (setf *verbose* (parse-integer arg)))
(defun cli-parse-thaw    (arg) (setf *ice* arg))
(defun cli-parse-site    (arg)
  (format t "**** setting *site* to ~a~%" arg)
  (setf *site* arg))
(defun cli-parse-cpu-freeze  (arg) (setf *cpu-freeze* (read-from-string (format nil "#x~a" arg))))
(defun cli-maxrun (arg) (setf *maxrun* (read-from-string (format nil "#x~a" arg))))

(let (cmd)
  (loop for arg in #+sbcl (cdr sb-ext:*posix-argv*)
                   #+clisp ext:*args*
                   do
    (format t "check arg [~a]~%" arg)
    (cond
      ((string= arg "-v") (decf *verbose*))
      ((string= arg "+v") (incf *verbose*))
      ; the purpose of the core command is to dump a core
      ; that behaves as sbcl, but is emulated. This means
      ; using a logfile instead of stdio etc.
      ((string= arg "core") (setf cmd :core))
      ((string= arg "run") (setf cmd :run))
      ((string= arg "mon") (setf cmd :mon))
      ((string= arg "gdb") (setf cmd :gdb))
      ((string= arg "help") (setf cmd :help))
      ((string= arg "test") (setf cmd :test))
      ((string= arg "rtest") (setf cmd :rtest))
      ((string= arg "gcctest") (setf cmd :gcctest))
      ((string= arg "load") (setf cmd :load))
      ((not (search "--" arg)) (setf *cmdarg* (nconc *cmdarg* (list arg))))
      ((search "--maxrun=" arg) (cli-maxrun (subseq arg (1+ (search "=" arg)))))
      ((search "--site=" arg) (cli-parse-site (subseq arg (1+ (search "=" arg)))))
      ((search "--log=" arg) (cli-parse-log (subseq arg (1+ (search "=" arg)))))
      ((search "--thaw=" arg) (cli-parse-thaw (subseq arg (1+ (search "=" arg)))))
      ((search "--cpu-freeze=" arg) (cli-parse-cpu-freeze (subseq arg (1+ (search "=" arg)))))
      ((search "--verbose=" arg) (cli-parse-verbose (subseq arg (1+ (search "=" arg)))))
      ((search "--paranoid=" arg)) ; rest are parsed by config-cli.lisp
      ((search "--pagesize=" arg))
      ((search "--config=" arg))
      (t (error "unknown command line argument: ~a~%" arg))))
  (format t "cmd = [~a] arg = [~a]~%" cmd *cmdarg*)
  (when (not (zerop *verbose*))
    (format t "compile configuration:~%")
    (format t "page-size: ~x~%" (page-size))
    (format t "paranoid-limit: ~x~%" PARANOID-LIMIT))
  (set-config)
  (when (probe-file *site*)
    (format t "loading site file ~a~%" *site*)
    (let ((*package* (find-package "SBEMU")))
      (load *site*)))
  (case cmd
    (:run (start t))
    (:mon (monitor :init t :greet t))
    (:gdb
      (load "gdb")
      (funcall 'start-gdb-concert (start nil)))
    (:test    (load-and-run-test))
    (:rtest   (load-and-run-rtest))
    (:gcctest (load-and-run-gcctest))
    (:help (help))
    (:core (core))
    (:load
      (in-package :sbemu)
      (format t "Loading file ~a~%" (first *cmdarg*))
      (if *cmdarg* (load (first *cmdarg*))))
    (:nil)
    (t (help))))

