
; Try to lookup symbol in a lisp function
; if we are outside a lisp function,
; call the normal Info-follow-nearest-node.
(defun my-info-lookup-symbol ()
  (interactive)
  (let ((line (thing-at-point 'line))
        (c (current-column))
        f b n a len cont word node nodename)
    (setf f c)
    (setf b c)
    (setf len (length line))
    (setf n (string-match " |" line))
    (if n
      (progn
        ; look forward
        (setf cont t)
        (setf f (+ f 1))
        (while (and  cont (<= f len))
          (setf a (substring line (- f 1) f))
          (if (not (or (string-equal a "-")
                       (string-equal a "?")
                       (string-equal a "_")
                       (and (string< "/" a)
                            (string< a ":"))
                       (and (string< "@" a)
                            (string< a "["))
                       (and (string< "`" a)
                            (string< a "{"))))
            (progn (setf cont nil) (setf f (- f 1)))
            (setf f (+ f 1))))
        ; look backward
        (setf cont t)
        (setf b (- b 1))
        (while (and  cont (/= b 0))
          (setf a (substring line b (+ b 1)))
          (if (not (or (string-equal a "-")
                       (string-equal a "?")
                       (string-equal a "_")
                       (and (string< "/" a)
                            (string< a ":"))
                       (and (string< "@" a)
                            (string< a "["))
                       (and (string< "`" a)
                            (string< a "{"))))
            (progn (setf cont nil) (setf b (+ b 1)))
            (setf b (- b 1))))
        (setf word (substring line b f))
        (setf nodename (concatenate 'string "Function " word))

        (Info-find-node nil nodename))
      (Info-follow-nearest-node))))

(add-hook 'Info-mode-hook '(lambda ()
                             (local-set-key (kbd "RET") 'my-info-lookup-symbol)))

