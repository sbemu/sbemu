(in-package :sbemu)

(defun help ()
  (format t "SBCL Emulator~%")
  (format t "  version: FIX~%")
  (format t "  usage: sbcl --load load <options> <command>~%")
  (format t "  options: -v or +v to change verbosity~%")
  (format t "  command: run/test/help~%"))

