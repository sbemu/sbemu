; 4.6 Float handling

  4.6.1 Float and bits conversions
  ----------------------------------
  The native cpu handles float as a string of bits.
  For example lets take a look at how @math{\pi} looks
  in bitformat after being converted to a single-float:

  | (sb-kernel:single-float-bits (coerce pi 'single-float))
  | => 1000000010010010000111111011011

  This bit string can be divided into the three parts:
  Sign, Exponent and Fraction.

  | SExponentFraction..............
  | 1000000010010010000111111011011

  FIX: CLHS's venerable INTEGER-DECODE-FLOAT and DECODE-FLOAT emits
  these three parts, but not in cpu-native form.

  4.6.1.1 From bits to Float
  -----------------------------
  We here take $note of the functions, bits-to-single-float and bits-to-double-float.
  They will convert a bitstring (represented as an integer type)
  to a float of either single or double type.
  We need to decompose the bitstring into the three parts
  Sign, Exponent and Fraction.
  After that we will look at the Exponent as to detect NaN or Inf.
  Finally we will use an algorithm that take the three parts and
  outputs a host-lisp native float of single or double type.

  |$(macrolet
  |   ((bits-to-float (name type exps fras)

  Because we need one function for either type, we have tangled them
  together using a MACROLET. When the macro-expansion function is called
  it will emit a DEFUN. The macro-expansion function is named
  bits-to-float and takes four parameters:
  name -- the DEFUN function name.
  type -- wheter the code should deal with single or double float
  exps -- number of bits in the exponent bitstring field
  fras -- number of bits in the fraction bitstring field

  |      (let ((exp-mask (1- (ash 1 exps))))
  |      `(defun ,name (x)
  |         (assert (integerp x)) ; if result is infinity we must return the bits in x as integer
  |         (let ((exponent (ldb (byte ,exps ,fras) x))
  |               (fraction (ldb (byte ,fras 0) x))
  |               (z 0))

  Do a little bit of setup where exp-mask will be a number of same bitlength as the exponent
  with all bits set to one.
  Exponent and Fraction variable corresponds to those parts picked out of the bitstring.
  What we do next is to test the exponent and fraction field for special cases like
  Nan, Inf and lowest and biggest float numbers.

  |           $(check-for-special-floats)

  Finally we can take the three parts and put them together using the standard float algoritm.

  |           $(algoritm)

  We didn't use the Sign bit in the algoritm above, it is used in the final stage where we
  coerce the float number into the correct type.

  |           (* (coerce z ',type)
  |              (if (logbitp ,(+ exps fras) x)
  |                ,(if (eq type 'single-float) -1f0 -1d0)
  |                ,(if (eq type 'single-float)  1f0  1d0))))))))

  |   (bits-to-float bits-to-single-float single-float  8 23)
  |   (bits-to-float bits-to-double-float double-float 11 52))

  We've now looked at the overview, now lets dive into the meat
  parts of the expansion function.

  |$(check-for-special-floats
  |   ; FIX: check if exponent is 255/??? (+inf,-inf and nan)
  |   (setf x (logand x ,(if (eq type 'single-float) #xffffffff #xffffffffffffffff)))
  |   (cond
  |     ; FIX: how large does x in (coerce 1/x 'single-float) have to be to coerce to zero?
  |     ,@(if (eq type 'single-float)
  |         `(((= x #x7f7fffff) most-positive-single-float))
  |         `(((= x #x7f7fffffffffffff) most-positive-double-float)))
  |     ,@(if (eq type 'single-float)
  |         `(((= x #xff7fffff) most-negative-single-float))
  |         `(((= x #xff7fffffffffffff) most-negative-double-float)))
  |     ((= x 1) ,(if (eq type 'single-float)
  |                 `(return-from ,name least-positive-single-float)
  |                 `(return-from ,name least-positive-double-float)))
  |     ,@(if (eq type 'single-float)
  |         `(((= x #x80000001) least-negative-single-float))
  |         `(((= x #x8000000000000001) least-negative-double-float)))
  We cant use the algoritm to calculate the lowest/biggest float because that would take us to
  overflow error, so instead we try to detect it instead and emit those extreme numbers manually.

  Also below we detect for infinity and NaN. Notice the convention used:
  If the number is inifnity we will return the input number, ie an integer.
  If the number is NaN we will return NIL.

  |     ((and (= exponent ,exp-mask) ; infinity
  |           (zerop fraction))
  |       (return-from ,name x))
  |     ; FIX: we should distinguish between quiet and signalling NaNs
  |     ((= exponent ,exp-mask) (return-from ,name))))


  |$(algoritm
  |   (if (not (zerop exponent)) (setf z 1))
  |   ;(format t "exponent=~x~%" exponent)
  |   ;(format t "fraction=~x [~a.~23b]~%" fraction z fraction)
  |   (loop for i from 0 below ,fras do
  |     (when (logbitp i fraction)
  |       ;(format t "bit: ~a [~f] is set~%" i (ash 1 (- 23 i)))
  |       (incf z (/ 1 (ash 1 (- ,fras i)))))) ;(incf z (expt 2 (- (+ i 1))))
  |   (setf z (* z (expt 2 (- exponent ,(1- (ash 1 (1- exps))))))))

