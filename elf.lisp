; references:
; /usr/include/elf.h 

(in-package :sbemu)

(defstruct elf
  arch
  bitwidth
  endian
  pagefun
  stubfun)

(defstruct ph ; program header entry
  type
  flags
  off
  vad
  pad
  fsz
  msz
  align)

(defun anyref (array offset size endian)
  (ecase endian
    (l (ecase size
          (8  (aref-u8 array offset))
          (16 (aref-u16 array offset))
          (32 (aref-u32 array offset))
          (64 (aref-u64 array offset))))
    (b (ecase size
          (8  (aref-u8 array offset))
          (16 (aref-u16be array offset))
          (32 (aref-u32be array offset))
          (64 (aref-u64be array offset))))))

(defun load-psh (bin ph)
  (let ((off (ph-off ph))
        (vad (ph-vad ph)))
    (L "  load-program-header at file offset ~a file-size ~a to mem ~x~%" off (ph-fsz ph) vad :elf)
    (loop for i from 0 below (ph-fsz ph) do
      (let ((c (aref bin (+ off i))))
        (make-page (+ vad i))
        (mset-u8 (+ vad i) c)))))

(defun maybe-load-psh (bin ph end)
  (if (= (ph-type ph) 1) ; 1 = type PT_LOAD
    (load-psh bin ph)))

(defun print-psh (bin off end bit)
  (let ((ph (make-ph)))
    (cond
      ((= bit 64)
        (setf (ph-type ph)  (aref-u32 bin off)) (incf off 4)
        (setf (ph-flags ph) (aref-u32 bin off)) (incf off 4)
        (setf (ph-off ph)   (aref-u64 bin off)) (incf off 8)
        (setf (ph-vad ph)   (aref-u64 bin off)) (incf off 8)
        (setf (ph-pad ph)   (aref-u64 bin off)) (incf off 8)
        (setf (ph-fsz ph)   (aref-u64 bin off)) (incf off 8)
        (setf (ph-msz ph)   (aref-u64 bin off)) (incf off 8)
        (setf (ph-align ph) (aref-u64 bin off)))
      (t
        (setf (ph-type ph)  (anyref bin off 32 end)) (incf off 4)
        (setf (ph-off ph)   (anyref bin off 32 end)) (incf off 4)
        (setf (ph-vad ph)   (anyref bin off 32 end)) (incf off 4)
        (setf (ph-pad ph)   (anyref bin off 32 end)) (incf off 4)
        (setf (ph-fsz ph)   (anyref bin off 32 end)) (incf off 4)
        (setf (ph-msz ph)   (anyref bin off 32 end)) (incf off 4)
        (setf (ph-flags ph) (anyref bin off 32 end)) (incf off 4)
        (setf (ph-align ph) (anyref bin off 32 end))))
    (L "(~x)~a " (ph-type ph)
            (case (ph-type ph)
              (0 "PT_NULL")
              (1 "PT_LOAD")
              (2 "PT_DYNAMIC")
              (3 "PT_INTERP")
              (4 "PT_NOTE")
              (6 "PT_PHDR")
              (#x6474e550 "PT_GNU_EH_FRAME")
              (#x6474e551 "PT_GNU_STACK")
              (#x70000000 "PT_LOPROC")
              (t "???")) :elf)
    (L "~x ~x ~x ~x ~x ~x ~x~%"
            (ph-flags ph) (ph-off ph)
            (ph-vad ph)   (ph-pad ph)
            (ph-fsz ph)   (ph-msz ph)
            (ph-align ph))
    ph))

(defun elf-sh-name (bin name shin)
  (let ((str "") (i (+ shin name)) c)
    (loop
      (setf c (aref bin i))
      (incf i)
      (if (= c 0) (return))
      (setf str (format nil "~a~a" str (code-char c))))
    str))

(defun get-sh-symbol (bin offs shsize esz sym-name end bit)
  (L "  searching for offs:~x sz:~x sym-name: ~a" offs shsize sym-name :elf)
  (let ((o offs) name value size sname)
    (loop
      (setf name (anyref bin o 32 end))
      (cond
        ((= bit 64) ; the Elf64_Sym looks too irregular to use anyref
          (setf value (aref-u64 bin (+ o  8)))
          (setf size  (aref-u32 bin (+ o 16))))
        (t
          (setf value (aref-u32be bin (+ o 4)))
          (setf size  (aref-u32be bin (+ o 8)))))
      (incf o esz)
      (setf sname (elf-sh-name bin (+ offs shsize) name))
      (when (string= sname sym-name)
        (L "symbol-name: ~x, value: ~x, size: ~x string-name: ~a~%" name value size sname :elf)
        (return-from get-sh-symbol (values value size)))
      (if (>= o (+ offs shsize)) (return)))))

(defun elf-get-dynsym-string (bin dynsym dynstr symindex)
  ;(destructuring-bind (offs size esz addr) dynsym
  ;  (declare (ignore esz addr))
  ;  (L "dynsym: off=~x, size=~x~%" offs size))
  ;(destructuring-bind (offs size esz addr) dynstr
  ;  (declare (ignore esz addr))
  ;  (L "dynstr: off=~x, size=~x~%" offs size))
  (destructuring-bind (offs size esz addr) dynsym
    (declare (ignore size addr))
    ;(L "symname at ~x index=~x esz=~x~%" (+ offs (* esz symindex)) symindex esz)
    ;(L "symname is ~x~%" (aref-u32 bin (+ offs (* esz symindex))))
    ;(L "dynstr at ~x~%" (first dynstr))
    (let ((symname (aref-u32 bin (+ offs (* esz symindex)))))
      (elf-sh-name bin symname (first dynstr)))))

(defun read-reltab (elf bin offs shsize esz addr dynsym dynstr)
  (if (zerop esz) (error "the size of an entry in a relocatable-table cant be zero"))
  (L "  read-rel-section searching for offs:~x sz:~x addr:~x~%" offs shsize addr :elf)
  (let ((n 0) (end (elf-endian elf)) (bit (elf-bitwidth elf)))
    (loop for i from 0 do
      (let* ((addr (anyref bin (+ n offs) bit end))
             (info (anyref bin (+ n offs  (ash bit -3)) bit end))
             ;(adde (aref-u64 bin (+ n offs 16)))
             ;(symt (logand #xff info))
             (symi (ash info -32))
             (sname (elf-get-dynsym-string bin dynsym dynstr symi)))
        (L " addr=~x, info=~x symi=~x [~s]~%" addr info symi sname)
        (funcall (elf-stubfun elf) addr sname :dynfun (elf-arch elf))
        (incf n esz)
        (if (>= n shsize) (return-from read-reltab i))))))

(defun read-dynsym (elf bin dynsym dynstr)
  (L "  read-dynsym~%" :elf)
  (destructuring-bind (offs size esz addr) dynsym
    (declare (ignore addr))
    (let ((n 0) (end (elf-endian elf)))
      (loop for i from 0 do
        (let ((symname (anyref bin (+ offs (* esz i)) 32 end))
              (addr    (anyref bin (+ offs (* esz i)  4) 32 end))
              (symsz   (anyref bin (+ offs (* esz i)  8) 32 end))
              (info    (anyref bin (+ offs (* esz i) 12)  8 end))
              (ndx     (anyref bin (+ offs (* esz i) 14) 16 end)))
          (when (not (zerop symname))
            (let ((name (elf-sh-name bin symname (first dynstr))))
            (cond
              ((and (= info #x12) (zerop ndx))
                (funcall (elf-stubfun elf) addr name :dynfun (elf-arch elf))))))
          (incf n esz)
          (if (>= n size) (return-from read-dynsym i)))))))

(defun elf-section-type-name (type)
  (case type
    (2 :symtab)
    (3 :strtab)
    (11 :dynsym)))

(defun elf-resolve-dynamic-symbols (elf bin dynsym dynstr rela-plt)
  (let (num-rel-read)
    (cond
      (rela-plt ; linux abi
        (destructuring-bind (offs size esz addr) rela-plt
          (setf num-rel-read (read-reltab elf bin offs size esz addr dynsym dynstr))
          (L "read ~a relocatable symbols" num-rel-read)))
      (t ; system-v
        (read-dynsym elf bin dynsym dynstr)))))

(defun elf-make-bss-room (bss)
  (destructuring-bind (offs size esz addr) bss
    (declare (ignore offs esz))
    (L "bss section: addr=~x size=~x~%" addr size)
    (loop for i from 0 do
      (make-page (+ addr i))
      (if (> i size) (return))
      (incf i PAGESZ))))

(defun elf-load-rld (elf rld)
  (destructuring-bind (offs size esz addr) rld
    (declare (ignore size esz))
    (funcall (elf-pagefun elf) addr)
    (L "loading RLD section from file-off ~x, to mem ~x~%" offs addr :elf)))

(defun elf-load-got (elf bin got dynstr)
  (destructuring-bind (offs size esz addr) got
    (let ((n (1+ (truncate size #x1000)))) ; number of pages
      (loop for i from 0 below n do
        (L "make got page ~x~%" addr)
        (funcall (elf-pagefun elf) addr)
        (incf addr #x1000)))))


(defun parse-elf (elf bin off shin)
  (let ((o off) (end (elf-endian elf)) (bit (elf-bitwidth elf)) name sname type flag addr offs size link info alig esz)
    (cond
      ((= bit 64)
        (setf name (aref-u32 bin o)) (incf o 4)
        (setf type (aref-u32 bin o)) (incf o 4)
        (setf flag (aref-u64 bin o)) (incf o 8)
        (setf addr (aref-u64 bin o)) (incf o 8)
        (setf offs (aref-u64 bin o)) (incf o 8)
        (setf size (aref-u64 bin o)) (incf o 8)
        (setf link (aref-u32 bin o)) (incf o 4)
        (setf info (aref-u32 bin o)) (incf o 4)
        (setf alig (aref-u64 bin o)) (incf o 8)
        (setf esz  (aref-u64 bin o)) (incf o 8)
        (setf sname (elf-sh-name bin name shin)))
      (t
        (setf name (anyref bin o 32 end)) (incf o 4)
        (setf type (anyref bin o 32 end)) (incf o 4)
        (setf flag (anyref bin o 32 end)) (incf o 4)
        (setf addr (anyref bin o 32 end)) (incf o 4)
        (setf offs (anyref bin o 32 end)) (incf o 4)
        (setf size (anyref bin o 32 end)) (incf o 4)
        (setf link (anyref bin o 32 end)) (incf o 4)
        (setf info (anyref bin o 32 end)) (incf o 4)
        (setf alig (anyref bin o 32 end)) (incf o 4)
        (setf esz  (anyref bin o 32 end)) (incf o 4)
        (setf sname (elf-sh-name bin name shin))))
    (L "section name ~a, offset ~x size ~x type ~a~%" (elf-sh-name bin name shin) offs size (or (elf-section-type-name type) type) :0 :elf)
    (if (not (zerop size))
      (progn
      (L "section-header: addr=~x name=~a file-offset=~x size=~x entry-size=~x~%" addr sname offs size esz :elf)
      (cond
        ((string= sname ".dynstr")   (list :dynstr (list offs size esz addr)))
        ((string= sname ".dynsym")   (list :dynsym (list offs size esz addr)))
        ((string= sname ".symtab")   (list :main (get-sh-symbol bin offs size esz "main" end bit)))
        ((string= sname ".rela.plt") (list :rela-plt (list offs size esz addr)))
        ((string= sname ".got")      (list :got    (list offs size esz addr)))
        ((string= sname ".bss")      (list :bss    (list offs size esz addr)))
        ((string= sname ".rld_map")  (list :rld    (list offs size esz addr)))
        (t (list nil nil))))
      (list nil nil))))

(defun elf-sections (elf bin shnum shoff shsz shix)
  (let* ((bit (elf-bitwidth elf)) (end (elf-endian elf))
         (shin (anyref bin (+ shoff (* shsz shix) 8 (ash bit -2)) bit end))
         main dynstr dynsym rela-plt bss)
    (L "get shin(string-table) at address: ~x~%" (+ shoff (* shsz shix) 8 (ash bit -2)) :elf)
    (L "shin(.shstrtab table at address): ~x~%" shin :elf)
    (L "Section header entries:~%" :elf)
    (loop for i from 0 below shnum do
      (let ((offset (+ shoff (* shsz i))))
        (destructuring-bind (name val) (parse-elf elf bin offset shin)
          (cond
            ((eq name :bss)    (setf bss val))
            ((eq name :main)   (setf main val))
            ((eq name :dynsym) (setf dynsym val))
            ((eq name :dynstr) (setf dynstr val))
            ((eq name :rela-plt) (setf rela-plt val))
            ((eq name :got) (elf-load-got elf bin val dynstr))
            ((eq name :rld) (elf-load-rld elf val))))))
    (elf-resolve-dynamic-symbols elf bin dynsym dynstr rela-plt)
    (elf-make-bss-room bss)
    main))

(defun elf-parse (bin pagefun stubfun)
  (let ((elf (make-elf))
        type arch ver entry phoff shoff phnum shnum phsz shsz shix end bit)
    (setf (elf-pagefun elf) pagefun)
    (setf (elf-stubfun elf) stubfun)
    ; ensure ELF file type
    (if (not (and (= (aref bin 0) #x7f) ; check magic
                  (= (aref bin 1) #x45)
                  (= (aref bin 2) #x4c)
                  (= (aref bin 3) #x46)))
      (error "runtime is not of ELF type (bad magic)"))
    (setf type (aref-u16 bin 16))
    (cond
      ((= type 2)   (setf end 'l) (setf bit 64))
      ((= type 512) (setf end 'b) (setf bit 32))
      (t (error "ELF-type is not an executable, it is ~a" type)))
    (setf (elf-bitwidth elf) bit)
    (setf (elf-endian   elf) end)
    (cond
      ((= bit 64)
        (setf arch  (anyref bin 18 16 end))
        (setf ver   (anyref bin 20 32 end))
        (setf entry (anyref bin 24 64 end)) ; program entry point (not main)
        (setf phoff (anyref bin 32 64 end)) ; program-header-offset
        (setf shoff (anyref bin 40 64 end)) ; section-header-offset
        (setf phsz  (anyref bin 54 16 end)) ; program-header-size
        (setf phnum (anyref bin 56 16 end)) ; number-of-program-headers
        (setf shsz  (anyref bin 58 16 end)) ; section-header-size
        (setf shnum (anyref bin 60 16 end)) ; number-of-section-headers
        (setf shix  (anyref bin 62 16 end))) ; section-header-string-table-index
      (t
        (setf arch  (anyref bin 18 16 end))
        (setf ver   (anyref bin 20 32 end))
        (setf entry (anyref bin 24 32 end)) ; program entry point (not main)
        (setf phoff (anyref bin 28 32 end)) ; program-header-offset
        (setf shoff (anyref bin 32 32 end)) ; section-header-offset
        (setf phsz  (anyref bin 42 16 end)) ; program-header-size
        (setf phnum (anyref bin 44 16 end)) ; number-of-program-headers
        (setf shsz  (anyref bin 46 16 end)) ; section-header-size
        (setf shnum (anyref bin 48 16 end)) ; number-of-section-headers
        (setf shix  (anyref bin 50 16 end)))) ; section-header-string-table-index
    (cond
      ((= arch 62) (setf arch 'x86-64))
      ((= arch 8)  (setf arch 'mips)) ; mips-be-r3000
      (t (error "Unsupported ELF-arch: ~a" arch)))
    (setf (elf-arch elf) arch) ; elf files can contain arch specific tables
    (if (/= ver  1) (error "ELF-version is not current, it is ~a" ver))
    (L "type: ~a~%" type :elf)
    (L "arch: ~a~%" arch :elf)
    (L "ver: ~a~%" ver :elf)
    (L "entry: #x~x~%" entry :elf)
    (L "phnum,phsz,phoff: ~a ~a #x~x~%" phnum phsz phoff :elf)
    (L "shnum,shsz,shoff: ~a ~a #x~x~%" shnum shsz shoff :elf)
    (L "shix: ~a~%" shix :elf)
    ; dump program-header's, assume sbcl, where PH's is assumed to be low
    (if (> phnum 16) (error "Number of program-headers looks too large: ~a" phnum))
    (L "Program header entries (phnum=~a):~%" phnum :elf)
    (L "type flags offset vaddr paddr filesz memsz align~%" :elf)
    (loop for i from 0 below phnum do
      (let ((offset (+ phoff (* phsz i))) ph)
        (setf ph (print-psh bin offset end bit))
        (maybe-load-psh bin ph end)))
    (L "loading elf sections~%" :elf)
    (elf-sections elf bin shnum shoff shsz shix)
    ;(if (not main) (error "couldn't locate main()"))
    ;(L "entry-point (main): ~x~%" main :elf)
    (L "elf loading done, entry=~x, arch=~a~%" entry arch :elf)
    (values entry arch)))

