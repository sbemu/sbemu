(in-package :sbemu)

(defvar *monitor-istream* nil)
(defvar *monitor-ostream* nil)
(defvar *monitor-prompt* nil)
(defvar *monitor-state* nil)
(defvar *monitor-funs* (make-hash-table :test 'equal))

(defun print-monitor-prompt ()
  (terpri *monitor-ostream*)
  (when *monitor-prompt*
    (write-string *monitor-prompt* *monitor-ostream*))
  (force-output *monitor-ostream*))

(defun monitor-eval-as-lisp (form)
  (let ((*package* (find-package :sbemu)))
    (print (eval (read-from-string form)) *monitor-ostream*)))

(defun monitor-eval-as-command (line)
  (let (fun-sym
        (args (split " " line)))
    (maphash (lambda (name sym)
               (when (zerop (or (search name (car args)) 1))
                 (setf fun-sym sym)
                 ;(return-maphash)
                 ))
             *monitor-funs*)
    (if fun-sym
      (funcall fun-sym (cdr (split " " line)))
      (format *monitor-ostream* "ERROR: no such monitor command."))))

(defun eval-monitor (form)
  (when (> (length form) 0)
    (loop while (char= (char form 0) #\Space) do
      (setf form (subseq form 1))))
  (when (> (length form) 0)
    (cond
      ((char= (char form 0) #\() (monitor-eval-as-lisp form))
      (t (monitor-eval-as-command form)))))

(defun run-monitor ()
  (print-monitor-prompt)
  (loop for line = (read-line *monitor-istream* nil) while line do
    (eval-monitor line)
    (print-monitor-prompt)
    (if (eq *monitor-state* :exit) (return)))
  (format t "Exiting monitor.~%"))

(defun monitor-start (&key init
                           (prompt "#|mon>|# ")
                           (input-stream *standard-input*)
                           (output-stream *standard-output*))
  (if init (log-open))
  (setf *monitor-istream* input-stream)
  (setf *monitor-ostream* output-stream)
  (setf *monitor-prompt* prompt))

(defun monitor-stop (&key init)
  (if init (log-close)))

(defun monitor-init ()
  (load-arch-config)
  (load-runtime (first *c-args*))
  (os-init *heap-size*))

(defun monitor (&key init
                     greet
                     (prompt "#|mon>|# ")
                     (input-stream *standard-input*)
                     (output-stream *standard-output*))
  (monitor-start :init init
                 :prompt prompt
                 :input-stream input-stream
                 :output-stream output-stream)
  (when init
    (monitor-init))
  (if greet (format t "Welcome to monitor!~%"))
  ; initialize som default commands
  (run-monitor)
  (monitor-stop :init init))

(defmacro defmon (cmd name &body defun-body)
  `(progn
     (setf (gethash ,cmd *monitor-funs*) ',name)
     (defun ,name ,@defun-body)))

