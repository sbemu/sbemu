;
;  Heap
; ------
;

  4.7.1 Lazy pages
  -------------------
  A guest page is implemented as an simple-array. These pages(arrays) are stored in the array *mem*.
  There is another *memw* array that keeps flags-bits, which are notes about the status of each page.
  If a page is realized through, for example mmap, the host-lisp will not use MAKE-ARRAY yet. Instead a note will be kept
  about this page being lazy. The first time the guest request a write to the page, it will be 
  realized by the host-lisp by using MAKE-ARRAY and stored in the *mem*.

  If the guest request a read against a page that is lazy, that is most probable a bug in the guest software,
  and is therefor forbidden.

  When a page is being realized, the array that is created is filled with zeros.
  Therefor there is no difference between a zeroed page or an lazy page.
  This means the host-lisp can reclaim some memory by converting zero pages into lazy pages.
  Over time there could be situations where the guest has alot of zero pages. If so a relazy operation
  could be beneficial.

  4.7.1.1 Function heap-relazy
  ------------------------------

  |$(defun maybe-relazy-page (page-num page)
  |   (cond
  |     ((and page (loop for a across page thereis (not (zerop a)))) nil)
  |     (page
  |       (setf (aref *mem* page-num) nil)
  |       (setf (aref *memw* page-num) (logior (aref *memw* page-num) OS-PROT-LAZY))
  |       t)))
  |
  |$(defun heap-relazy ()
  |   (let ((tot 0))
  |     (loop for i from 0 below (length *mem*) do
  |       (if (maybe-relazy-page i (get-page-num i)) (incf tot)))
  |     (L "number of pages, relazied: ~x~%" tot)))

  4.7.2 Heap compressor 
  -----------------------
  Do make the host lisp utilize physical memory more sparsely, compression of guest pages can be enabled.

  4.7.2.1 Function page-compress
  --------------------------------

  |$(defun page-compress (page)
  |   (let ((out (make-array 0 :fill-pointer 0 :adjustable t)) o (n 0))
  |     (flet ((output ()
  |              (cond
  |                ((zerop n)
  |                  (vector-push-extend o out)
  |                  (when (= o #xff) ; escape
  |                    (vector-push-extend o out)))
  |                ((or (and (/= o #xff) (< n 3)) (and (= o #xff) (< n 1)))
  |                  (incf n)
  |                  (if (= o #xff) (setf n (* n 2)))
  |                  (loop repeat n do (vector-push-extend o out))
  |                  (setf n 0))
  |                (t
  |                  (multiple-value-bind (chunks rest) (truncate (1+ n) 6)
  |                    (loop repeat chunks do
  |                      (vector-push-extend #xff out)
  |                      (vector-push-extend 6 out)
  |                      (vector-push-extend o out))
  |                    (when (not (zerop rest))
  |                      (vector-push-extend #xff out)
  |                      (vector-push-extend rest out)
  |                      (vector-push-extend o out)))
  |                  (setf n 0)))))
  |       (loop for a across page do
  |         (if o
  |            (cond
  |              ((= a o)
  |                (incf n))
  |              (t
  |                (output))))
  |         (setf o a)
  |         )
  |       (output)
  |       out)))

  4.7.2.2 Function page-decompress
  ----------------------------------

  |$(defun page-decompress (page)
  |   (let ((out (make-array PAGESZ)) (i 0) x y a)
  |     (flet ((output (a)
  |              (setf (aref out i) a)
  |              (incf i)))
  |       (loop for n from 0 below (length page) do
  |         (setf a (aref page n))
  |         (cond
  |           ((= a #xff)
  |             (setf x (aref page (1+ n)))
  |             (cond
  |               ((= x #xff) (incf n) (output #xff)) ; escape
  |               (t
  |                 (setf y (aref page (+ 2 n)))
  |                 (incf n 2)
  |                 (loop repeat x do (output y)))))
  |           (t
  |             (output a)))))
  |     out))

  4.7.2.3 Function heap-size
  --------------------------------
  |$(defun heap-size ()
  |   (loop for page across *mem*
  |         summing (cond
  |                   ((and page (listp page)) (+ 16 16 (length (car page)))) ; cost of cons 16, array-header 16, content
  |                   (page (+ 16 PAGESZ)) ; array-header and content cost
  |                   (t 8)) ; nil costs 8 bytes
  |         into sum finally (return sum)))

  4.7.2.4 Function heap-compress
  --------------------------------
  |$(defun heap-compress ()
  |   (L "heap physical size before compression: ~a~%" (heap-size))
  |   (loop for i from 0
  |         for page across *mem* do
  |     (when (and page (not (listp page)))
  |       (setf (aref *mem* i) (cons (page-compress page) nil))))
  |   (L "heap physical size after compression: ~a~%" (heap-size)))
  |

  4.7.2.5 Testing the page compressor
  ------------------------------------

  A harness is also given below, to be used for testing.

  |$(defun test-page-codec (page)
  |   (format t "testing page: ~a~%" page)
  |   (let ((codec (page-decompress (page-compress page))))
  |     (loop for i from 0 below PAGESZ do
  |       (if (/= (aref page i) (aref codec i))
  |         (return-from test-page-codec)))
  |     t))

  |$(defun test-page-codec-random ()
  |   (let ((page (make-array PAGESZ)))
  |     (loop for i from 0 below PAGESZ do (setf (aref page i)
  |                                                 (if (zerop (random 3))
  |                                                   255
  |                                                   (random 256))))
  |     (format t "test ~a~%" (test-page-codec page))))

  To test the page compressor/decompressor try:

  | (loop repeat 10 do (test-page-codec-random))
 
  FIX: also make test cases out of this.

