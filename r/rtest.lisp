
(in-package :sbemu)

(defun make-c-file (call code)
  (with-open-file (s "r/foo.c" :direction :output :if-exists :supersede :if-does-not-exist :create)
    (format s "
#include <stdio.h>
#include <string.h>
#include <stdlib.h>~%~%")
(loop for line in code do (write-line line s))
(format s "~%
int main (int argc, char **argv)
{
  if(~a == 123){
    exit(0);
  }else{
    exit(1);
  }
}" call)))

(defun compile-run-c ()
  (if (probe-file "r/foo") (delete-file "r/foo"))
  (sb-ext:run-program "/usr/bin/gcc" '("-Wall" "-O0" "r/foo.c" "-o" "r/foo") :wait t)
  (let ((proc (sb-ext:run-program "./r/foo" nil :wait t)))
    (if (/= (sb-ext:process-exit-code proc) 0)
      (error "The test case in foo.c doesn't return 123 (atleast the program foo doesn't return 0 or may not even exist)"))))

(defun run-objdump ()
  (let ((proc (sb-ext:run-program "/usr/bin/objdump" '("-S" "r/foo") :wait t :output :stream))
        lines record)
    (loop for line = (read-line (sb-ext:process-output proc) nil) while line do
      (cond
        ((search " <foo>:" line) (setf record 1))
        ((and record (= (length line) 0)) (return))
        ((and record (= record 1)) (setf lines (nconc lines (list line))))))
    lines))

(defun parse-bytes (asm)
  (let ((code (make-array 0 :fill-pointer 0 :adjustable t)) n)
    (loop for line in asm do
      (setf n (search ":" line))
      (when n
        (setf line (subseq line (1+ n)))
        (format t "~s~%" line)
        (let (a b (s 0) c)
        (loop for x across line do
          (cond
            ((or (char= x #\Space) (char= x #\Tab)) ; s is consecutive whitespace counter
              (incf s))                                ; s helps to find end of hex list
            (t (setf s 0)))
          (if (and c (> s 1)) (return)) ; we've reached end of hex list, now comes disassembly
          (setf x (char-code x))
          (setf x (cond ((and (>= x 97) (<= x 102)) (- x 97 -10)) ;a-f
                        ((and (>= x 48) (<= x 57)) (- x 48)))) ;0-9
          (cond
            (x (setf a b) (setf b x))
            (t (setf a nil) (setf b nil)))
          (when (and a b)
            (setf c t)
            (vector-push-extend (+ (ash a 4) b) code)
            (setf a (setf b nil)))
          ;(format t "~a: ~s [~a,~a]~%" s x a b)
          ))))
    ;(format t "code: ")
    ;(loop for c across code do (format t "~x " c))
    ;(format t "~%")
    code))

(defun test-form (name code-offset max-gcc arg1 arg2 call-string function-body)
  (format t "run random-test ~a~%" name)
  (let (asm code)
    (make-c-file call-string function-body)
    (compile-run-c)
    (setf asm (run-objdump))
    (setf code (parse-bytes asm))
    ;;
    ;; setup environment and call disasm, collect return value in register rax
    ;;
    (let* ((*print-pretty* nil)
           (cpu (make-cpu))
           (registers (svref cpu 3)))
      (setf *mem* (make-array #x10000 :initial-element nil :fill-pointer #x10000 :adjustable t))
      (setf *memw* (make-array #x10000 :initial-element 7))
      (setf *memsz* #x10000)
      (make-page code-offset)
      ; initialize code memory to both real and test memory
      (loop for i from 0 for x across code do (mset-u8 (+ i code-offset) x))
      ; initialize stacks
      (make-page #x1000) ; stack mapping
      (make-page (- #x1000 1)) ; stack mapping
      (rset-u64 :rsp #x1000)
      (rset-u64 :rbp #x1008)
      ;(mset-u64 #x1008 #xdeadbeef)
      (mset-u64 #x1000 #xdeadbeef)
      ;(mset-u64 #xff8  #xdeadbeef)
      ; create somewhat unique values that fills the whole register
      ; FIX: setup stack to contain a bogus return value, we will stop cpu when return PC contains this value
      (rset-u64 :rdi (logand arg1 (mask 32)))
      (rset-u64 :rsi (logand arg2 (mask 32)))
      (setf cpu (disasm code-offset max-gcc cpu :stop-pc #xdeadbeef))
      (format t "pc: ~x, rax=~x (~a)~%" (svref cpu 0) (reg :rax) (reg :rax))
      (assert (= (reg :rax) 123)))))

(defun rtest ()
  (in-package :sbemu)
  (load "r/tests"))

