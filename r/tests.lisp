; 1) generate a c-code file that contains a function named foo that will return 123
; 2) extract the dissasembly and bytes of the foo function using an external tool
; 3) run the bytes through the emulator and check that it also returns 123
;
; if a test fails it is possible to compare it side by side with gdb.
;
; TEST-FORM
; test name
; code-blit-address
; run-max-instructions
; argument-1
; argument-2
; c-function-call-string
; c-function-body-list)

(in-package :sbemu)

; a simple and static test to begin with
(test-form :a0 8 40 100 23
  "foo(100,23, 'A', \"hej\")"
  '("int foo (int a, int b, char c, char *d)
    {
      return a + b;
    }"))

(let* ((a (random 100))
       (b (random 100))
       (c (+ 123 a b)))
  (test-form :a1 8 20 a b
    (format nil "foo(~a,~a, 'A', \"hej\")" a b)
    (list (format nil "int foo (int a, int b, char c, char *d)
                       {
                         return ~a - a - b;
                       }" c))))

(let* ((a (- (random (ash 1 32)) (ash 1 31)))
       (b (- (random (ash 1 32)) (ash 1 31)))
       (c (+ 123 a b)))
  (test-form :a3 8 20 a b
    (format nil "foo(~a,~a, 'A', \"hej\")" a b)
    (list (format nil "int foo (int a, int b, char c, char *d)
                       {
                         return ~a - a - b;
                       }" c))))

(let* ((n (1+ (random 10)))
       (a (- (random (ash 1 32)) (ash 1 31)))
       (b (- (random (ash 1 32)) (ash 1 31))))
  (test-form :a3 8 20 a 0
    (format nil "foo(~a, ~a)" a b)
    (list (format nil "int min (int a, int b) { return a - b; }
                       int plu (int a, int b) { return a + b; }
                       int dii (int a, int b) { return a / b; }
                       int mul (int a, int b) { return a * b; }
                       int bar (int a) {
                         int i, x = 0;
                         for(i = 0; i < ~a; i++){
                           x = min(x, a);
                           x = plu(x, i);
                           x = mul(x, i);
                           x = dii(x, 2);
                         }
                         return x;
                       }
                       int foo (int a, int b)
                       {
                         int x = 0;
                         x += bar(a);
                         x -= bar(b);
                         x -= bar(a);
                         x += bar(b);
                         return x + 123;
                       }" n))))

