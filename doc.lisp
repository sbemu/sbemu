;;;; Communicate Emmanuelle to Texinfo and sbemu-sources

(defun top ()
  (topdoc #+sbcl (second sb-ext:*posix-argv*))
  (sb-ext:exit))

; FIX: load this routine from common.lisp instead
(defun split (del line &optional acc)
  (let ((n (search del line)))
    (cond (n (split del (subseq line (1+ n))
                        (nconc acc (list (subseq line 0 n)))))
          (t (nconc acc (list line))))))

(defmacro f (s fmt &rest args) ; When you are to lazy to type ormat.
  `(format ,s ,fmt ,@args))

(defmacro with-open-file-read  ((stream file) &body body)
  `(with-open-file (,stream ,file :direction :input)
     ,@body))

(defmacro with-open-file-write ((stream file) &body body)
  `(with-open-file (,stream ,file :direction :output :if-exists :supersede :if-does-not-exist :create)
     ,@body))

(defun smatch (pat line)
  (loop while (and (> (length line) 0) (char= (char line 0) #\Space)) do
    (setf line (subseq line 1)))
  (= (or (search pat line) -1) 0))

; To avoid duplicates in the input textfile two hashes keeps track of
; what menus and menunumbers we've seen so far.
(defvar *menu-num* (make-hash-table :test #'equal))
(defvar *menu-name* (make-hash-table :test #'equal))
; When parsing a textfile, there is an optional associated lispfile where
; code lands in this. The STREAM object of that file is held here.
(defvar *lispfile* nil)
(defvar *lispfile-name* nil)
; an output stream for lisp sentences, these are closed automatically
; either when stumbling upon next sentence or when reaching next chapter.
(defvar *lispfile-sen* nil)
(defvar *lispfile-sen-name* nil)
; before first chapter in first document, we are in intro-mode (preamble).
(defvar *intro-done* nil)
(defvar *vars* (make-hash-table :test #'equal)) ; document variables
; After first massage of textfile an IR is generated that is stored here.
(defvar *code* nil)
; This is the only function which may put stuff on *code*.
(defun emit (type &rest args)
  (setf *code* (nconc *code* (list (list type args)))))

(defun parse-and-dump-lisp (lisp stream)
  ;(format t "  dumping lisp form ~s to stream ~s~%" lisp stream)
  (let ((p 0) o f (out "") forms codes macros macronames)
    ; first load in all top-level forms marked by $()
    (loop for a across lisp do
      (cond
        ((> p 0)
          (if (char= a #\() (incf p))
          (if (char= a #\)) (decf p))
          (setf out (concatenate 'string out (format nil "~c" a)
                                 (if (= p 0) (format nil "~%") "")))
          (when (= p 0)
            (setf forms (nconc forms (list out)))
            (setf out "")
            ))
        ((and o (char= a #\() (char= o #\$))
          ;(format t " begin top $form~%")
          (incf p)
          (setf out (concatenate 'string out (format nil "~c" a)))))
      (setf o a))
    ; now order all forms into code and macros
    (loop for form in forms do
      (let ((body form))
        (loop
          (let ((n (search "$(" body)) name)
            (cond
              (n
                (setf name (subseq body (+ n 2)))
                (setf name (subseq name 0 (search ")" name)))
                ;(format t "   Found macro call: ~a~%" name)
                (push name macronames)
                (setf body (subseq body (+ n (length name)))))
              (t (return))))))
      (let ((n (search " " form)) name)
        (when n
          (setf name (subseq form 0 n))
          (assert (char= (char name 0) #\()) ; could be caused by forms such as ( foobar) instead of (foobar)
          (if (search ";" name) (setf name (subseq name 0 (search ";" name)))) ; dont include lisp comment
          (setf name (subseq name 1))
          (setf name (delete #\Space name))
          (setf name (delete #\Tab name))
          (setf name (delete #\Return name))
          (setf name (delete #\Newline name))
          ;(format t "toplevel form name [~a]~%" name)
          ; check wheter this form is a macro or code
          (cond
            ((loop for macro in macronames thereis (string= macro name))
              ;(format t "a macro~%")
              (push (list name form) macros))
            (t
              ;(format t "code~%")
              (setf codes (nconc codes (list form))))))))
    ; finally output codes and do macro substitution
    (loop for code in codes do
      (let ((form code) (out ""))
        (loop
          (let ((n (search "$(" form)) name)
            (cond
              (n
                (setf out (concatenate 'string out (subseq form 0 (+ n))))
                (setf name (subseq form (+ n 2)))
                (setf name (subseq name 0 (search ")" name)))
                ;(format t "macro name is ~a~%" name)
                ;(format t "  replace macro ~a~%" name)
                (assert (loop for (macroname macro) in macros thereis (string= macroname name)))
                (loop for (macroname macro) in macros do
                  (if (string= macroname name)
                    (let ((macro (subseq macro  (1+ (search " " macro))))) ; unwrap the left paren and symbol
                      (setf macro (subseq macro 0 (search ")" macro :from-end t))) ; remove right side paren
                      (setf out (concatenate 'string out macro)))))
                (setf form (subseq form (+ n 3 (length name)))))
              (t (return)))))
        (setf out (concatenate 'string out form))
        (write-string out stream)
        (terpri stream)))))

(defun texi-header (so name file shortname cat shortdesc)
  (f so "\\input texinfo    @c -*-texinfo-*-
@c %**start of header
@setfilename ~a
@settitle ~a
@c %**end of header

@c for install-info
@dircategory ~a
@direntry
* ~a: (~a).           ~a
@end direntry

" file name cat shortname shortname shortdesc))

(defun texi-footer (so)
  #+nil(f so "
@node Index
@unnumbered Index
@printindex cp~%")
  (f so "@bye~%"))

(defun label? (line)
  (let ((n (search " ---" line)))
    (when n
      (setf line (subseq line 0 n))
      (loop for i from 0 below n do
        (if (char/= (char line i) #\Space) (return-from label?)))
      (return-from label? t))))

(defun block? (line)
  (let ((n (search " |" line)))
    (when n
      (loop for i from 0 below n do
        (if (char/= (char line i) #\Space) (return-from block?)))
      (return-from block? (subseq line (if (>= (+ n 2) (length line)) (length line) (+ n 2)))))))

(defun parse-label (line)
  (loop
    (cond
      ((char= (char line 0) #\Space) (setf line (subseq line 1)))
      (t (return))))
  (let ((num (subseq line 0 (search " " line)))
        (lab (subseq line (1+ (search " " line))))
        (type 0))
    (loop for a across num do (if (char= a #\.) (incf type)))
    (values (case type
              (0 :chapter)
              (1 :section)
              (2 :subsection)
              (3 :subsubsection))
            num lab)))

(defun maybe-emit-block (block)
  (cond
    (block
      (emit :block block)
      nil)
    (t block)))

(defun emit-load (args)
  (let ((textfile (first args))
        (lispfile (second args)))
    (format t "load text file ~a output code to ~a~%" textfile lispfile)
    (with-open-file-read (si textfile)
      (emit :lispfile lispfile)
      (parse-top si)
      (emit :endlispfile))))

(defun emit-command (line)
  (let ((n (search " " line)) cmd args)
    (cond
      (n (setf cmd (subseq line 0 n))
         (setf args (subseq line (1+ n))))
      (t (setf cmd line)))
    (cond
      ((string= cmd "load") (emit-load (split " " args)))
      (t (error "unknown command ~a~%" cmd)))))

(defun split-english (line)
  (let (o sens str e c)
    (loop for a across line do
      (setf c nil)
      (cond
        ((char= a #\")
          (setf e (if e nil t))
          (setf c t))
        ((or (and (not e) o (char= o #\.) (char= a #\Space))
             (and (not e) o (char= o #\.) (char= a #\Newline)))
          (setf sens (nconc sens (list str)))
          (setf str ""))
        (t
          (setf c t)))
      (if c (setf str (concatenate 'string str (format nil "~c" a))))
      (setf o a))
    (if str (setf sens (nconc sens (list str))))
    sens))

(defun emit-text-sentence (line)
  (flet ((get-last-word-undot (line)
           (let ((str "") e c)
             (loop for i from (1- (length line)) downto 0 do
               (setf c (char line i))
               (cond
                 ((char= c #\") (setf e (if e nil t)))
                 ((and (not e) (char= c #\Space)) (return)))
               (setf str (concatenate 'string str (format nil "~c" c))))
             (if (char= (char str 0) #\.) (setf str (subseq str 1)))
             (if (char= (char str 0) #\") (setf str (subseq str 1)))
             (setf str (reverse str))
             (if (char= (char str 0) #\") (setf str (subseq str 1)))
             str))
         (get-key-words (line)
           (let ((words (reverse (split " " (subseq line 0 (1- (length line)))))) (expect-and t) keys)
             (push (car words) keys)
             (format t "Processing key words [~s]~%" words)
             (loop for word in (cdr words) do
               (cond
                 ((and expect-and (string= "and" word)) (setf expect-and nil))
                 ((and (not expect-and) (not (search "," word)))
                   (push word keys)
                   (setf expect-and t))
                 (t (return))))
             keys)))
    (let ((n (length line)) (cmd (string-downcase line)) dot)
      (setf dot (char line (1- n)))
      (cond
        ((= 0 (or (search "$describe" cmd) -1))
          (unless dot (error "Command given but sentence not ending in a dot"))
          (format t "Processing english sentence [~a]~%" line)
          (let ((file (get-last-word-undot line)))
            (format t "  Processing resulted in IR :sentence-file with args [~a]~%" file)
            (emit :sentence-file file)))
        ((= 0 (or (search "$note" cmd) -1))
          (unless dot (error "Command given but sentence not ending in a dot"))
          (let ((notes (get-key-words line)))
            (format t "Found notes ~s~%" notes)
            (loop for note in notes do
              (emit :note note))))
        ((= 0 (or (search "$ref " cmd) -1))
          (unless dot (error "Command given but sentence not ending in a dot"))
          (let ((refs (get-key-words line)))
            (format t "Found references to ~s~%" refs)
            (loop for ref in refs do
              (emit :ref ref))))
        ((and (= 0 (or (search "$" cmd) -1)) (search " " cmd))
          (let ((cmd (subseq cmd 1 (search " " cmd)))
                (word (get-last-word-undot line)))
            (format t "Variable [~a]=[~a]~%" cmd word)
            (setf (gethash cmd *vars*) word))))))
  (emit :text line))

(defun emit-text-english (line)
  (let ((n (search "$" line)))
    (cond
      (n ; here we handle $english sentence
        (let (a b c)
          (setf a (subseq line 0 n))
          (setf b (subseq line n))
          (emit-text-english a)
          (emit-text-sentence b)))
      (t
        (emit :text line)))))

; top-level text handler
(defun emit-text-and-more (line)
  (let ((n (search "$(" line)))
    (cond
      (n ; here we handle $(form)
        (let (a b c)
          (setf a (subseq line 0 n))
          (setf b (subseq line (+ 2 n)))
          (setf n (search ")" b))
          (setf c (subseq b (1+ n)))
          (setf b (subseq b 0 n))
          (format t "Parts: [~a] [~a] [~a]~%" a b c)
          (emit-text-and-more a)
          (emit-command b)
          (emit-text-and-more c)))
      (t
        (cond
          ((= (or (search ";" line) -1) 0)) ; dont emit text comments
          ((search "$" line)
            (loop for sent in (split-english line) do
              (emit-text-english sent)))
          (t
            (emit :text line)))))))

(defun parse-top (si)
  (format t "begin parsing text at top-level~%")
  (let (old blo)
    (flet ((parse-line (line)
             (cond
               ((label? line)
                 (unless *intro-done*
                   (format t "Document preamble is done.~%")
                   (setf *intro-done* t))
                 (setf blo (maybe-emit-block blo))
                 (multiple-value-bind (type num title) (parse-label old)
                   (format t "found label ~a / ~a [~a]~%" num type title)
                   (let ((n 0))
                     (loop for a across line do (if (char= a #\-) (incf n)))
                     (if (< n (+ 3 (length num) (length title)))
                       (format t "WARNING: label ~a looks bad~%" old)))
                   (assert (not (gethash num *menu-num*)))
                   (assert (not (gethash title *menu-name*)))
                   (setf (gethash num *menu-num*) t)
                   (setf (gethash title *menu-name*) t)
                   (emit :label num type title)
                   nil))
               ((block? old) (setf blo (nconc blo (list (block? old)))) line)
               (old
                 (setf blo (maybe-emit-block blo))
                 (emit-text-and-more old)
                 line)
               (t line))))
      (loop for line = (read-line si nil) while line do
        (setf old (parse-line line)))
      (parse-line old)
      (maybe-emit-block blo)
      (format t "end parsing text at top-level~%"))))

; check if num looks like an appendix
(defun appendix? (num)
  (loop for a across num thereis
    (let ((x (char-code a)))
      (and (not (= x #x2e))
           (or (< x #x30)
               (> x #x39))))))

(defun dump-texi-intro (so base title shortname cat shortdesc)
  (assert (stringp title))
  (let ((info (format nil "~a.info" base)))
    (format t "begin dump of intro~%")
    (texi-header so title info shortname cat shortdesc)
    (f so "
@titlepage
@title ~a
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents
" title)
    (f so "
@ifnottex
@node Top
@comment  node-name,  next,  previous,  up
@top ~a

" shortname)
    ; dump the menu, we need to iterate the *code* again to pick out the menu items
    (f so "@menu~%")
    (loop for item in *code* do
      (when (eq (car item) :label)
        (destructuring-bind (num type name) (second item)
          (f so "~a~a~a~%"
             (ecase type
               (:chapter "* ")
               (:section       (concatenate 'string "  " num ".... "))
               (:subsection    (concatenate 'string "  " num "...... "))
               (:subsubsection (concatenate 'string "  " num "........ ")))
             (if (appendix? num)
               (concatenate 'string num " " name)
               name)
             (case type (:chapter ":: ") (t ""))))))
    (f so "@end menu
@end ifnottex~%~%")
    (f t "end dump of intro~%")))

(defun type-lte (a b)
  (setf a (ecase a (:chapter 1)
                   (:section 2)
                   (:subsection 3)
                   (:subsubsection 4)))
  (setf b (ecase b (:chapter 1)
                   (:section 2)
                   (:subsection 3)
                   (:subsubsection 4)))
  (>= a b))

(defun dump-node-menu (so at curtype)
  (let (menu item)
    (loop for i from at do
      (setf item (nth i *code*))
      (if (not item) (return))
      (when (eq (car item) :label)
        (destructuring-bind (num type name) (second item) num
          ; if we reach a node of type equal or below current node, we've reach the end of the scope to menuize on
          (if (type-lte curtype type) (return))
          (cond
            ((and (eq curtype :chapter)    (eq type :section))       (setf menu (nconc menu (list name))))
            ((and (eq curtype :section)    (eq type :subsection))    (setf menu (nconc menu (list name))))
            ((and (eq curtype :subsection) (eq type :subsubsection)) (setf menu (nconc menu (list name))))))))
    (when menu
      (f so "@menu~%")
      (loop for item in menu do (f so "* ~a::~%" item))
      (f so "@end menu~%~%"))))

(defun dump-info-text-lines (text so)
  (let (fix extra)
    (loop for txt in text do
      (setf extra nil)
      (cond
        ((and (> (length txt) 0) (smatch "FIX: " txt))
          (setf fix t)
          (f so "~%"))
        (t
          (if fix (setf extra t)))) ; leaving FIX section, put a extra newline to make a paragraph-style in info
      (f so "~a~%" txt)
      (if extra (f so "~%")))))

(defun write-lispfile-prefix (s forms)
  (loop for form in forms do
    (print form s)
    (terpri s))
  (terpri s))

(defun dump-texi (si)
  (format t "~%Lets begin translating IR to texinfo.~%")
  (let (at title text lisp
        (shortname (gethash "projname" *vars*))
        (base (gethash "filebase" *vars*))
        (cat (gethash "software-category"  *vars*))
        (shdesc (gethash "short-description" *vars*))
        ; When generating a lispfile it will only contain functions, defvars etc..
        ; This is not suitable for direct asdf consumption, because asdf needs
        ; information about which package to put this code in.
        ; these forms is written as prefix to each opened lispfile.
        (file-prefix (read-from-string (gethash "file-prefix" *vars*))))
    (assert base)
    (assert shortname)
    (flet
      ((open-lispfile-sen (lispfile)
         (format t "open sentence-file [~a]~%" lispfile)
         (setf lisp nil)
         (setf *lispfile-sen-name* lispfile)
         (setf *lispfile-sen* (open lispfile :direction :output :if-exists :supersede :if-does-not-exist :create))
         (write-lispfile-prefix *lispfile-sen* file-prefix))
       (close-lispfile-sen ()
         (format t "close sentence-file ~a~%" *lispfile-sen-name*)
         (parse-and-dump-lisp lisp *lispfile-sen*)
         (setf lisp nil)
         (close *lispfile-sen*)
         (setf *lispfile-sen* nil)))

  (with-open-file (so (format nil "~a.texi" base) :direction :output :if-exists :supersede :if-does-not-exist :create)
    (loop for i from 0 for old in *code* do
      (ecase (car old)
        (:label
          (if (eq at :intro) (dump-texi-intro so base title shortname cat shdesc))
          (setf at :label)
          (destructuring-bind (num type name) (second old)
            (format t "label-op: ~a,~a,~a~%" num type name)
            (when (eq type :chapter)
              (when *lispfile-sen*
                (parse-and-dump-lisp lisp *lispfile-sen*)
                (format t "closing sentence-file ~a~%" *lispfile-sen-name*)
                (close *lispfile-sen*)
                (setf  *lispfile-sen* nil)))
            (let* ((appendix (appendix? num))
                   (nodename (if appendix (concatenate 'string num " " name) name)))
              (f so "@node ~a~%" nodename)
              (f so "@comment node-name,  next,  previous,  up~%")
              (f so "@~a~%" (if appendix
                            (concatenate 'string "unnumbered " nodename)
                            (concatenate 'string (string-downcase (string type)) " " nodename))))
            (dump-node-menu so (1+ i) type)))
        (:intro (setf at :intro))
        (:block
          (setf at :block)
          (if (eq at :intro) (dump-texi-intro so base title shortname cat shdesc))
          (setf text (car (second old)))
          ;(format t "block-op~%")
          (f so "@verbatim~%")
          (loop for line in text do
            ;(f t "[~a]~%" line)
            (f so "    |  ~a~%" line)
            (setf lisp (concatenate 'string lisp line (f nil "~%"))))
          (f so "@end verbatim~%~%"))
        (:text
          (setf text (second old))
          ;(format t "text-op: ~s~%" text)
          (cond
            ((eq at :intro)
              (when (and (not (zerop (length (car text))))
                         (not title))
                (setf title (car text))
                (format t "found title [~a]~%" title)
                ))
            (t
              (dump-info-text-lines text so))))
        (:ref
          (setf text (car (second old)))
          (format so "@xref{Function ~a}" text))
        (:note
          (setf text (car (second old)))
          (format so "@anchor{~a}~%" text))
        (:sentence-file
          (let ((lispfile (car (second old))))
            (if *lispfile-sen* (close-lispfile-sen))
            (open-lispfile-sen lispfile)))
        (:lispfile
          (let ((lispfile (car (second old))))
            (if *lispfile-sen* (close-lispfile-sen))
            (format t "open lispfile [~a]~%" lispfile)
            (assert (not *lispfile*))
            (setf lisp nil)
            (setf *lispfile-name* lispfile)
            (setf *lispfile* (open lispfile :direction :output :if-exists :supersede :if-does-not-exist :create))
            (write-lispfile-prefix *lispfile* file-prefix)))
        (:endlispfile
          (format t "close lispfile ~a~%" *lispfile-name*)
          (assert *lispfile*)
          ; dump any lisp forms gathered
          (parse-and-dump-lisp lisp *lispfile*)
          (close *lispfile*)
          (setf *lispfile* nil)
          (setf lisp nil))
        (:nop)))
    (if *lispfile-sen* (close-lispfile-sen))
    (assert (not *lispfile*))
    (texi-footer so)
    (format t "end texdump~%")))))

(defun topdoc (infile)
  (if (not (search ".txt" infile)) (error "bad input filename [~a]" infile))
  (with-open-file (si infile  :direction :input)
    (format t "compiling input to IR~%")
    (emit :intro nil) ; we need a special block taking care of the top text in input file (because input file top has no header)
    (parse-top si)
    (cond
      ((loop for item in sb-ext:*posix-argv* thereis (string= item "--syntax"))) ; check IR
      ((loop for item in sb-ext:*posix-argv* thereis (string= item "--ir")) ; dump IR
        (loop for item in *code* do
          (print item)))
      (t
        (dump-texi si)))))

(top)

