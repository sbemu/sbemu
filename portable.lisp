; fix: due to package prefix, need to break this file out into portable-sbcl.lisp and portable-cl-store.lisp

(in-package :sbemu)

;;; GC inhibit

(defmacro maybe-inhibit-gc (&body body)
  (cond
    ((feature :daa)
      #+sbcl`(sb-sys:without-gcing ,@body)
      #-sbcl (error "Direct-array-access specified but host-lisp type is not found"))
    (t
      `(progn ,@body))))

;;; DAA

(defmacro set-vector-u32 (vector address data)
  #+sbcl  `(sb-kernel:%set-sap-ref-32 (sb-sys:vector-sap ,vector) ,address ,data)
  #+clisp `(error "DAA is not supported ~a,~a,~a" vector address data))

(defmacro set-vector-u64 (vector address data)
  #+sbcl  `(sb-kernel:%set-sap-ref-64 (sb-sys:vector-sap ,vector) ,address ,data)
  #+clisp `(error "DAA is not supported ~a,~a,~a" vector address data))

(defmacro get-vector-u32 (vector address)
  #+sbcl  `(sb-kernel::sap-ref-32 (sb-sys:vector-sap ,vector) ,address)
  #+clisp `(error "DAA is not supported ~a,~a,~a" vector address))

(defmacro get-vector-u64 (vector address)
  #+sbcl  `(sb-kernel::sap-ref-64 (sb-sys:vector-sap ,vector) ,address)
  #+clisp `(error "DAA is not supported ~a,~a,~a" vector address))

;;; CL-STORE

#-cl-store
(defun store (object stream)
  (declare (ignore object stream))
  (format t "stub: no store interface~%"))
#-cl-store
(defun restore (stream)
  (declare (ignore stream))
  (format t "stub: no restore interface~%"))

#+cl-store
(defun store (object stream)
  (format t "cl-store object~%")
  ;(cl-store:store object stream)
  )

#+cl-store
(defun restore (stream)
  ;(cl-store:restore stream)
  )


; FIX: not portable, or atleast very fragile
(defun is-directory (file)
  (let ((res (format nil "~a" (car (directory file)))))
    (= (1- (length res)) (search "/" res :from-end t))))

(defun lisp-exit ()
  #+sbcl (sb-ext:exit)
  #+clisp (ext:quit))

(defun lisp-chdir (dir)
  (if (keywordp dir)
    (cond
      ((eq dir :pwd)
        (setf *default-pathname-defaults* (truename "."))))))

; single-float-to-bits
; bits-to-single-float
; double-float-to-bits
; bits-to-double-float
#+sbcl
(defun single-float-to-bits (f)
  (sb-kernel:single-float-bits (coerce f 'single-float)))

#-sbcl
(defun single-float-to-bits (f)
  (multiple-value-bind (fraction exponent sign) (integer-decode-float (coerce f 'single-float))
    (logior (ash sign 31)
            (ash (+ exponent 127) 23) ; something is wrong here
            (logand fraction (1- (ash 1 23))) ; dont include the implicit MSB bit
            )))

#+sbcl
(defun double-float-to-bits (f)
  (setf f (coerce f 'double-float))
  (let* ((abs (abs f))
         (hi (sb-kernel:double-float-high-bits abs))
         (lo (sb-kernel:double-float-low-bits abs))
         (exp (ldb sb-vm:double-float-exponent-byte hi))
         (fra (logior (ash (ldb sb-vm:double-float-significand-byte hi) 32)
                      lo))
         (sig (if (minusp (float-sign f)) 1 0)))
    (logior (ash sig 63)
            (ash exp 52)
            fra)))
#-sbcl
(defun single-float-to-bits (f)
  (multiple-value-bind (fraction exponent sign) (integer-decode-float (coerce f 'single-float))
    (logior (ash sign 63)
            (ash (+ exponent 127) 52) ; something is wrong here
            (logand fraction (1- (ash 1 52))) ; dont include the implicit MSB bit
            )))

