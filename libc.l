;
;  Libc implementation
; ------------------------
;

 12.1 Libc Overview
 --------------------
  The emulator is focused on emulating user-code in the users program.
  There are therefor limits where the emulation of user-code is instead done
  by host-lisp functions. (Sole) Examples are kernel-interrupts and the whole libc library.

  The emulated libc functions are called by the host to perform
  more or less equivalent work of what a native libc would do. Again focus is more
  about the user-code emulation so quite many libc functions acts likes stubs (doing minimal work).

 12.2 How a libc function is defined
 -------------------------------------
  All libc function that the user-code can call is defined by a macro named
  DEFINE-LIBCFUN. This macro takes care of the calling-convention by mapping
  input registers and stack during runtime and matching those to the parameter
  list given by the definition. The return value is handled in a similar way
  by looking up the architecture at runtime and inserting the return value in
  the appropiate cpu register.

  An libc function looks like this code:

  | (define-libcfun (name argument-list)
  |   body)

  The name must match a string found in the ELF sections. Else the emulators "dl.so" equivalent
  wont be able to lookup this function at runtime.
  The argument-list is mapped to the registers and stack which contains input.

   12.3 Libc Functions
   ---------------------

     12.3.1 Function getrusage
     ---------------------------
     This function sets the C-struct timeval being
     the number of emulated instructions executed sofar.
     This is the approximation that one emulated instruction takes
     one microsecond.

     |$(define-libcfun (:getrusage@plt (who rusage))
     |   (L "getrusage: ~x,~x~%" who rusage :lib)
     |   ; set c-struct-timeval =~ number-of-instructions-executed-sofar
     |   (multiple-value-bind (sec usec) (truncate *cpui* 1000000)
     |     (let ((arch (cpu-arch cpu)))
     |       (ecase arch
     |         (0 $(set-timeval-x86-64))
     |         (1 $(set-timeval-mips))))

     We uncoditionally return 0 to the user-code (caller of this libc function).

     |     0))

     In the above code snippet we have the following substitutions.
     Set-timeval-x86-64 that stores the second and microsecond values
     in a timeval struct of little-endian format.

     | $(set-timeval-x86-64
     |    (mset-u64 (+ rusage  0) sec)
     |    (mset-u64 (+ rusage  8) usec)
     |    (mset-u64 (+ rusage 16) sec)
     |    (mset-u64 (+ rusage 24) usec))

     And set-timeval-mips stores the second and microsecond values
     in an big-endian format of timeval struct found in the address rusage.

     | $(set-timeval-mips
     |    (mset-u32b (+ rusage  0) sec)
     |    (mset-u32b (+ rusage  4) usec)
     |    (mset-u32b (+ rusage  8) sec)
     |    (mset-u32b (+ rusage 12) usec))

     That concludes the implementation of getrusage libc function. If you
     would like to see the function in lisp-only form please take a look
     in the libc-aux.lisp file, but dont edit that! Edit here.

     12.3.2 Function nanosleep
     ---------------------------

     |$(define-libcfun (:nanosleep@plt (req)) ; FIX: use reminder argument when interrupted sleep
     |   (let* ((sec (mget-u64 req))
     |          (nsec (mget-u64 (+ req 8)))
     |          (ticks (microseconds-to-ticks (+ (* sec 1000000) (truncate (/ nsec 1000))))))

     First we convert the second and microsecond argument into ticks.

     |     (L "nanosleep for ~x ticks~%" ticks)
     |     (push-cpu-state cpu (list :tickwait (+ *cpui* ticks)))

     Then we set the cpu into :tickwait state.

     |     0))

     See also $ref microseconds-to-ticks.

