;
; heap usage:
; ram size  #x1000000000
; bank      #xAA--------
; address   #x--BBBBBBBB        
;
; banks: $00 - $10 
; address range in each bank: $00000000 - $ffffffff
;
;
(in-package :sbemu)

(defvar *heap-size* nil)              ; defined in config
(defvar *malloc-heap*     #x10000000) ; start of malloc-heap, where segments get allocated
(defvar *malloc-heap-size* #x2100000) ; memory region size in bytes reserverd for malloc usage
(defvar *mmap-heap*       #x14000000) ; mmap give away anonymous memory blocks
(defvar *mmap-heap-head* nil)         ; pointer to last free memory
(defvar *argv* #x3800)
(defvar *libc* #x4000) ; one page worth of libc cruft
(defvar *libc-errno* #x4f00)
(defvar *libc-stdout* #x3f00)
(defvar *libc-stderr* #x3f08)
(defvar *stub-vector* #x5000)
(defvar *stub-load-index* 0)
(defvar *stubs* (make-hash-table))
(defvar *stubs-name* (make-hash-table))
(defvar *signal-handlers* (make-hash-table))
(defvar *malloc* (make-hash-table :test 'equal)) ; location to current allocated segments
(defvar *sbcl-heap*     #x1fffe608) ; FIX: rip from executable
(defvar *c-stack*       #x1fffe000) ; FIX: put this in its own bank
(defvar *page-fault* nil) ; what to do if page-fault occurs
(defvar *rmu* 0)
(defvar *c-args* nil)
;defvar *mem*         #x1000000000

; addr -- place to write data
; data -- what data to write
; type -- signed/unsigned
(defun page-fault (page addr asz data type)
  (L "page-fault page=~x, addr=~x size=~x data=~x~%" page addr asz data :os)
  (cond
    ((not *page-fault*) (error "page-fault is forbidden"))
    (t ; default is to create the page and continue
      (make-page addr)
      (if (/= (ash (+ addr asz) (- PAGEBN)) page)
        (progn
        (make-page (+ addr asz))))
      (if data
        (mset addr asz data)
        (progn
        (mget addr asz type))))))

(defun check-read-page (page)
  ;(if (zerop (logand (aref *memw* page) OS-PROT-READ))
  ;  (L "[r ~x=~x] " page (aref *memw* page)))
  (let ((prot (aref *memw* page)))
    ; first check if page is lazy and needs creation
    (if (not (zerop (logand prot OS-PROT-LAZY)))
      (make-prot-page page (logxor OS-PROT-LAZY prot))) ; create page without lazy flag
    ; check if we are doing a read-page-fault
    (when (zerop (logand prot OS-PROT-READ))
      (error "time to read SIGSEGV"))))

(defun check-write-page (page)
  ;(if (zerop (logand (aref *memw* page) OS-PROT-WRITE))
  ;  (L "[w ~x=~x] " page (aref *memw* page)))
  (let ((prot (aref *memw* page)))
    ; first check if page is lazy and needs creation
    (if (not (zerop (logand prot OS-PROT-LAZY)))
      (make-prot-page page (logxor OS-PROT-LAZY prot))) ; create page without lazy flag
    (when (zerop (logand prot OS-PROT-WRITE))
      (L "write SIGSEGV at page ~x, handler ~s~%" page (get-signal-handler 11))
      (setf *cpu-signal* (list :signal (list (get-signal-handler 11) 11 page 0)))
      t)))

(defun dead_protect-page (addr prot) ; not sure why this fun is unused
  (let* ((page (ash addr (- PAGEBN)))
         (oprot (aref *memw* page)))
    (L "protect-page ~x:~x ~x->~x~%" page addr oprot prot :os)
    ;(if oprot (setf prot (logior prot (logxor OS-PROT-LAZY oprot)))) ; keep lazy bit
    (setf (aref *memw* page) prot)))

(defun add-stub (name fun)
  (setf (gethash name *stubs-name*) fun))

; emulate a c-call
; FIX: decouple this into the LOOP-CPU state-machine instead
(defun ccall3 (addr orig-cpu arg1 arg2 arg3)
  (let ((cpu (copy-seq orig-cpu)))
    (setf (svref cpu 3) (copy-seq (svref orig-cpu 3)))
    (let ((registers (svref cpu 3)) v)
      (L "OS: ccall3 ~x rsp=~x rbp=~x~%" addr (reg :rsp) (reg :rbp))
      (decf (reg :rsp) 8)
      (setf v (reg :rsp))
      (setf (reg :rdi) arg1)
      (setf (reg :rsi) arg2)
      (setf (reg :rdx) arg3)
      (mset-u64 v #xcafe0000)) ; store the end-of-ccall-marker (FIX: jump to an vector with escape/break?)
    (disasm cpu addr #x200000) ; FIX: 20000 is arbitrary (in case routine is faulty dont log too much)
    (let ((registers (svref cpu 3)))
      (L "OS: ccall3 done ~x rsp=~x rbp=~x~%" addr (reg :rsp) (reg :rbp)))))

(defun set-signal-handler (sig fun mask)
  (setf (gethash sig *signal-handlers*) (list fun mask)))

(defun get-signal-handler (sig)
  (gethash sig *signal-handlers*))

(defun malloc-info ()
  (let (loans)
    (maphash (lambda (addr size) (setf loans (nconc loans (list (list addr size)))))
             *malloc*)
    (setf loans (sort loans (lambda (a b) (< (car a) (car b)))))
    (L "OS: current malloc: ")
    (loop for (addr size) in loans do (L " [~x ~x]" addr size))
    (L "~%")
    (L "OS: malloc memory usage: ~d Mb~%"
            (let ((used 0))
              (loop for (addr size) in loans do (incf used size))
              (ash used -20)))))

(defun bless (addr size as)
  (let ((page (ash addr (- PAGEBN)))
        (endpage (ash (+ addr size -1) (- PAGEBN))))
    (L "blessing address range ~x-~x as ~x~%" addr (+ addr size -1) as :os)
    (loop while (<= page endpage) do
      ;(L "malloc bless! page ~x = ~x~%" page as :os)
      (setf (aref *memw* page) as)
      (incf page))))

;;; a malloc implementation interface

; how to walk over all malloced loans
(defmacro loop-malloc-loans ((addr size) loans &body body)
  `(loop for (,addr ,size) in ,loans do
     ,@body))

; get a list/array of current malloc loans
(defun get-malloc-loans ()
  (let (loans)
    (maphash (lambda (addr size) (setf loans (nconc loans (list (list addr size)))))
             *malloc*)
    (sort loans (lambda (a b) (< (car a) (car b))))))

; number of current malloced loans
(defun count-malloc-loans (loans)
  (length loans))

(defun add-malloc-loan (addr size)
  (setf (gethash addr *malloc*) size)
  (bless addr size (logior OS-PROT-LAZY OS-PROT-READ OS-PROT-WRITE))
  (if (not (< (+ addr size) (+ *malloc-heap* *malloc-heap-size*)))
    (L "malloc-out-of-memory at ~x" (+ addr size)))
  (assert (< (+ addr size) (+ *malloc-heap* *malloc-heap-size*)))
  ; FIX-SBCL: there is a bug in sbcl util.c that assume memory is zero initialized
  (loop for i from addr below (+ addr size) do
    (if (get-page i)
      (mset-u8 i 0)))
  addr)

;;; the malloc user interface

(defun make-malloc (size)
  (assert (and (> size 0)
               (< size MALLOC-MAX)))
  (let ((loans (get-malloc-loans)) d e last)
    ;(format t "make-malloc ~a ~x~%" size loans)
    (L "about to malloc size ~x, current loans: " size :2 :os)
    (loop for (addr size) in loans do (L " [~x ~x]" addr size))
    (L "~%")
    (L "malloc memory usage: ~d MB~%"
       (let ((used 0))
         (loop-malloc-loans (addr size) loans (incf used size))
         (ash used -20)) :os)
    ; first check if we have no loans
    (when (zerop (count-malloc-loans loans))
      ;(format t "first malloc at ~x ~x~%" *malloc-heap* size)
      (return-from make-malloc (add-malloc-loan *malloc-heap* size)))
    ; if we have loans, try to squeeze in wanted loan between any current loans
    ; |<-- left allocation -->| possibly-enough-free-space |<-- right allocation -->|
    (loop-malloc-loans (a s) loans
      (when d ; d is the byte after the left allocation
        (setf e (- a 1))   ; get the byte before the right allocation
        (when (>= (- e d) size) ; check if the space between these active loans is enough to hold the wanted loan
          ;(format t "squeeze malloc at ~x ~x~%" d size)
          (return-from make-malloc (add-malloc-loan d size))))
      (setf last (setf d (+ a s)))) ; last will finally point to outside all loans (extend the active malloc heap)
    ; if we get here, there was no region between alloc large enough (or only one active alloc)
    ;(format t "last malloc at ~x ~x~%" *malloc-heap* size)
    (add-malloc-loan last size)))

; FIX: would be great for debugging to have malloc objects on their own pages.
; On free page is removed, buggy read/writes are detected
(defun os-free (addr)
  ;(format t "try to free ~a~%" addr)
  (maphash (lambda (addr2 size)
             (when (= addr2 addr)
               (L "free ~a of size ~a~%" addr size :os)
               ;(format t "free ~a of size ~a~%" addr size)
               (remhash addr *malloc*)))
           *malloc*))

(defun vassert (cond desc)
  (unless cond
    (L "VASSERT fail: ~a~%" desc)
    (error "VASSERT fail description: ~a" desc)))

(defun mmap (addr len prot flags fd offset)
  (L "mmap addr=~x size=~x prot=~x flags=~x fd=~x offset=~x" addr len prot flags fd offset :os)
  (let (pagea pageb
        (prot prot))
    (if (< fd 0) (setf prot (logior prot OS-PROT-LAZY)))
    (if (not (zerop addr)) ; ensure address is not within mmap/malloc region
      (vassert (not (and (> addr *mmap-heap*) (< addr (+ *malloc-heap* *malloc-heap-size*))))
               (format nil "mmap for address ~x is within mmap/malloc region" addr)))
    (when (zerop addr)
      (setf addr *mmap-heap-head*)
      (incf *mmap-heap-head* len)
      (when (> *malloc-heap* *mmap-heap*) ; make sure we dont run into malloc-heap
        (if (not(< (+ addr len) *malloc-heap*)) (L "malloc-heap at ~x overrun by mmap at ~x" *malloc-heap* (+ addr len)))
        (assert (< (+ addr len) *malloc-heap*))))
    (setf pagea (ash addr (- PAGEBN)))
    (setf pageb (ash (+ addr len -1) (- PAGEBN)))
    (when (> fd -1) ; map in a file
      (let ()
        (io-file-position fd offset)
        (let ((pageo pagea))
          (make-prot-page pageo 7)
          (loop for i from 0 below len
                for a from addr
                for b = (io-read-byte fd) do ; FIX: use io-read-sequence
            (setf pageb (ash a (- PAGEBN)))
            (when (/= pageb pageo)
              (setf pageo pageb)
              (make-prot-page pageb 7))
            (mset-u8 a b)))))
    (L "mmap: page range ~x - ~x " pagea pageb)
    (loop for page from pagea to pageb do
      (setf (aref *memw* page) prot))
    (L "done mmap addr=~x size=~x" addr len :os)
    addr))

(defun mprotect (addr len prot)
  (let ((pagea (ash addr (- PAGEBN)))
        (pageb (ash (+ addr len -1) (- PAGEBN))))
    (assert (zerop (mod len PAGESZ))) ; no support for sub-page granilarity
    (L "mprotect addr=~x len=~x pages=~x-~x prot=~x" addr len pagea pageb prot :os)
    (loop for page from pagea to pageb do
      ; set the new protection bits, but leave lazy-bit alone
      (setf (aref *memw* page) (logior prot
                                       (logand (or (aref *memw* page) 0) OS-PROT-LAZY))))))

;;; futex

(defun add-futex (cpu uaddr op val timeout)
  (assert (not (cpu-futex cpu))) ; cpu-futex is not a list, so we cant hold several recursive futexes
  (setf (cpu-futex cpu) (list uaddr op val timeout))
  (push-cpu-state cpu (list :futex (cpu-tn cpu))))

(defun clear-futex (cpu)
  (L "futex place has changed or timeout, exiting futex state")
  (assert (cpu-futex cpu))
  (setf (cpu-futex cpu) nil)
  ; This will not work if the cpu is in any signal-handler.
  ; We should traverse the cpu-state list and pop off our futex.
  (assert (eq (caar (cpu-state cpu)) :futex))
  (pop-cpu-state cpu))

(defun wake-futex (uaddr max)
  (let ((n 0))
    (loop for cpu across *threads* do
      (if (>= n max) (return-from wake-futex))
      (when cpu
        (when (cpu-futex cpu)
          (when (= uaddr (first (cpu-futex cpu)))
            (incf n)
            (L "wake-futex thread ~a~%" (cpu-tn cpu))
            (clear-futex cpu)))))))

;;; file io

(defun open-file (file flags)
  (or (io-open-file file flags)
      (error "can open file ~a" file)))

(defun close-file (fd)
  (io-close-file fd))

(defun read-file (fd len)
  (L "read fd=~a len ~x~%" fd len :os)
  (io-read-file fd len))

(defun write-file (fd buf len)
  (io-write-file fd buf len))

(defun seek-file (fd off whe)
  (L "seek-file fd=~x off=~x whence=~x~%" fd off whe :os)
  (ecase whe
    (0 (io-file-position fd off))
    (1 (let ((cur (io-file-position fd nil)))
         (io-file-position fd (+ cur off))))
    (2 (let ((end (io-file-length fd)))
         (io-file-position fd (+ end off))))))

(defun os-call ()
  (loop for cpu across *threads* do
    (when cpu
      (when (cpu-futex cpu)
        (destructuring-bind (uaddr op val timeout) (cpu-futex cpu)
          (declare (ignore op))
          (let ((now (mget-u64 uaddr)))
            (L "checking futex [~x]=~x == ~x" uaddr now val)
            (cond
              ((/= now val)
                (clear-futex cpu))
              (timeout
                (decf timeout)
                (if (minusp timeout)
                  (clear-futex cpu))))))))))

