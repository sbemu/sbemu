; run gdb in concert with emulator

(in-package :sbemu)

(defvar *gdb* '("/usr/bin/gdb" "-i" "mi" "--args" "sbcl/src/runtime/sbcl" "--core" "sbcl/output/sbcl.core"))
(defvar *gdb-proc* nil)

(defun split (del line &optional acc)
  (let ((n (search del line)))
    (cond (n (split del (subseq line (1+ n))
                        (nconc acc (list (subseq line 0 n)))))
          (t (nconc acc (list line))))))

(defun get-gdb-lines ()
  (let ((pty (sb-ext:process-pty *gdb-proc*)))
    (loop for line = (read-line pty nil)
          while (and line (not (search "(gdb)" line)))
          collect line)))

(defun send-gdb (&rest cmds)
  (let ((pty (sb-ext:process-pty *gdb-proc*)))
    (car (loop for cmd in cmds collect
           (progn (format pty cmd) (force-output pty)
                  (get-gdb-lines))))))

(defun start-gdb ()
 (let ((proc (sb-ext:run-program (car *gdb*) (cdr *gdb*)
                                 :input  :stream
                                 :output :stream
                                 :error :stream
                                 :pty :stream
                                 :wait nil))
        pty out)
    (setf *gdb-proc* proc)
    (format t "process-p0 ~s~%" (sb-ext:process-alive-p proc))
    (sleep 0.2) ; give gdb time to wake up
    (format t "process-p1 ~s~%" (sb-ext:process-alive-p proc))
    (setf pty (sb-ext:process-pty proc))
    (setf out (sb-ext:process-output proc))
    ; do some preamble stuff, setup gdb
    (get-gdb-lines)
    (send-gdb "set confirm off~%"
              "display/i $pc~%"
              "handle SIGUSR1 pass noprint nostop~%"
              "handle SIGSEGV pass noprint nostop~%")))

(defun stop-gdb ()
  (let ((pty (sb-ext:process-pty *gdb-proc*)))
    (format pty "q~%")
    (force-output pty)
    (sb-ext:process-wait *gdb-proc*) ; FIX: is there an order which to do two below
    (sb-ext:process-close *gdb-proc*)))

(defun start-gdb-concert (cmain-entry-point)
  (let ((regname (make-array 256))
        (cpu (make-cpu))
        (pc cmain-entry-point)
        registers ni)
    ; setup the cpu
    (setf registers (svref cpu 3))
    (rset-u64 :rbp (- #x1fffe000 (* 16 11))) ; FIX: move the stack to it's own bank (upper part in malloc bank?)
    (rset-u64 :rsp (- #x1fffe668 (* 16 11))); #x650 8))
    (rset-u64 :rdi 3) ; this is argc
    (rset-u64 :rsi *argv*)
    (let ((c-args (list "/home/shrek/sbemu/sbcl/src/runtime/sbcl" "--core" "sbcl/output/sbcl.core"))
          (z 0) (n 512)) ; room for 512/8 args
      (loop for arg in c-args do
        (mset-u64 (+ *argv* z) (+ *argv* n)) (incf z 8)
        (loop for a across arg do
          (mset-u8 (+ *argv* n) (char-code a)) (incf n))
        (mset-u8 (+ *argv* n) 0) (incf n)))
    ;
    (format t "call ~a~%" cmain-entry-point)
    (start-gdb)
    (format t "getting register names~%")
    (let ((regs (send-gdb "-data-list-register-names~%")))
      (loop for line in regs do
        (when (search "register-names=[" line)
          (setf line (subseq line (+ 16 (search "register-names=[" line))))
          (setf line (subseq line 0 (search "]" line)))
          (setf line (split ","  line))
          (loop for i from 0
                for reg in line do
            (setf (svref regname i) reg)
            (format t " [reg ~a: ~a]" i reg)))))
    (terpri)
    (send-gdb "break call_into_lisp_first_time~%")
    (send-gdb "run~%")
    (format t "~a~%" (get-gdb-lines))
    (disasm pc nil cpu :log '(#x417f92 #x417fa8) :stop-pc #x421204) ; need to sync the stop-pc with the first gdb-si command, for now manually sync
    (setf pc (reg :pc))
    (loop for i from 0 below 100000 do
      (format t "******** iteration ~a *********~%" i)
      (let ((nisi (send-gdb (if ni "ni~%" "si~%"))) ; we get a prompt back as command receipt
            (inst (get-gdb-lines)) ; then we get a prompt back after a step-inst
            ;(regs (send-gdb "-data-list-changed-registers~%"))
            pca)
        (disasm pc (if ni 2 1) cpu :log 0)
        (setf pc (svref cpu 0))
        (setf pca (string-downcase (format nil "~x" pc)))
        (setf ni nil)
        (loop for line in inst do
          (when (search "=>" line)
            (format t "should we ni? ~a~%" line)
            (if (search "@plt" line) (setf ni t))
            (setf line (subseq line (+ 4 (search ">" line))))
            ;(format t "2) ~a~%" line)
            (setf line (subseq line 0 (search " " line)))
            (if (search ":" line) (setf line (subseq line 0 (search ":" line))))
            (format t "~a : ~a~%" line pca)
            (assert (string= line pca))))
        ;(loop for line in regs do
        ;  (when (search "changed-registers" line)
        ;    (format t "~a~%" line)))
        ))
    (stop-gdb)))

