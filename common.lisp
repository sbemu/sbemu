(in-package :sbemu)

(defvar *syms* (make-hash-table))
(defvar *verbose* 3)
(defvar *cmdarg* nil)

;(defmacro L (&rest args)
;  `(progn
;     (format t ,@args)
;     (terpri)))

;(defmacro fmt (&rest args)
;  `(L ,@args))

(defmacro paranoid (cond &optional (paranoid 1))
  (if (not (and paranoid (>= paranoid PARANOID-LIMIT)))
    `(if (not ,cond) (error "paranoid ~s failed" ',cond))))

(defun update-casetree (origtree levels item)
  (flet ((find-tree (key tree)
           (find key tree :test (lambda (a b) (equal a (car b))))))
  (let ((tree (cons nil origtree)) ret)
    (setf ret tree)
    (loop for level in levels do
      (if (not (find-tree level (cdr tree))) (push (cons level nil) (cdr tree)))
      (setf tree (find-tree level (cdr tree))))
    (setf (cdr tree) (list :casetreeleaf item))
    (cdr ret))))

(defun make-ecasebody (tree levels)
  (append (list 'ecase (car levels))
          (loop for (item . next) in tree collect
            (list item
                  (if (eq (car next) :casetreeleaf)
                    (second next)
                    (make-ecasebody next (cdr levels)))))))

(defun symbolicate (&rest args)
  (let ((args (loop for arg in args collect
                (cond
                  ((symbolp arg) (string arg))
                  ((stringp arg) (string-upcase arg))
                  ((numberp arg) (format nil "~a" arg))))))
    (intern (apply 'concatenate 'string args))))

(defun split (del line &optional acc)
  (let ((n (search del line)))
    (cond (n (split del (subseq line (1+ n))
                        (nconc acc (list (subseq line 0 n)))))
          (t (nconc acc (list line))))))

(defun maybe-read-from-string (str) (if (stringp str) (read-from-string str)))

(defun %foldl (fun acc lst)
  (loop for item in lst do
    (setf acc (funcall fun item acc)))
  acc)

(defmacro foldl (fun acc lst)
  `(%foldl ,fun ,acc ,lst))

(defun treemap (fun tree)
  (cond
    ((and tree (listp tree))
      (cons (treemap fun (car tree))
            (treemap fun (cdr tree))))
    (t (funcall fun tree))))

