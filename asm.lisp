
(in-package :sbemu)

(defvar *runts* #x10000)
(defvar *ips* nil)
(defvar *ipsutc* nil)
(defvar *ipscur* nil)
(defvar *cpui* 0)      ; move into cpu
(defvar *cpu-log* nil)
(defvar *cpu-nolog* nil)
(defvar *cpu-debug-lisp-fun* nil)
(defvar *cpu-watch* nil)
(defvar *cpu-freeze* nil)

(defvar *monitor-lisp-function-break* nil)
(defvar *cpu-signal* nil) ; external signal to cpu
(defvar *cpu* nil) ; currently bound cpu

(defun make-cpu (arch)
  (let ((cpu (make-array 10))
        (registers (make-array (cond
                                 ((= arch (archname x86-64)) 34) ; from x86-64.lisp
                                 ((= arch (archname mips  )) 69) ; from mips-util.lisp and site.lisp
                                 (t (error "unknown arch ~a" arch)))
                               :initial-element 0)))
    (setf (cpu-pthread cpu) nil) ; thread private variables
    (setf (cpu-regs cpu) registers)
    (setf (cpu-fpucw cpu) 0)   ; clear fpu control word
    (setf (cpu-futex cpu) nil) ; clear futex
    (setf (cpu-sigmask cpu) 0) ; clear sigmask
    (setf (cpu-state cpu) nil) ; clear state
    (setf (cpu-arch cpu) arch)
    (setf (cpu-misc cpu) nil)  ; slush stuff
    (when (= arch (archname x86-64)) ; on x86-64 the registers 16-29 is used as xmms registers
      (loop for i from 16 below 29 do
        (setf (svref registers i) (cons 0 nil))))
    cpu))

(defun calc-ipscur ()
  (incf *ipscur*)
  (when (> *ipscur* 100000)
    (let ((utc (get-universal-time)))
      (setf *ips* (- utc *ipsutc*))
      (setf *ipsutc* utc)
      (setf *ipscur* 0))))

(defun check-watch (cpui pc watch watchvalue)
  (cond
    (watchvalue
      (cond
        ((/= watchvalue (or (mget-u8-soft watch) #xdead))
          (L "CPU.~x.~x.~x: watch ~x changed from ~x to ~x~%" *cpui* cpui pc watch watchvalue (mget-u8-soft watch))
          (mget-u8-soft watch))
        (t watchvalue)))
    (t (mget-u8-soft watch))))

(defun disasm (cpu stopoffset &key stop-pc decode-only quit-on-branch)
  (if (or (not stopoffset) (zerop stopoffset)) (setf stopoffset #xffffffff))
  (setf *ipsutc* (get-universal-time))
  (setf *ipscur* 0)
  (setf *cpu* cpu)
  (cond
    ((= (cpu-arch cpu) (archname x86-64))
      (disasm-x86-64 cpu stopoffset stop-pc decode-only :quit-on-branch quit-on-branch))
    ((= (cpu-arch cpu) (archname mips))
      (disasm-mips cpu stopoffset stop-pc decode-only))))

