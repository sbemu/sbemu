;
;  Operating System emulation
; -----------------------------
;

 13.1 OS Overview
 ------------------
  The operating system is the glue between the kernel and libc.
  In the emulator the OS will take care of OS and Kernel dutys.

  13.1.1 OS initialization routine
  ----------------------------------

  13.1.1.1 Function os-init
  ---------------------------
  Before any user-code is emulated, we need to setup memory, page-protection, I/O handling etc.

  |$(defun os-init (size)
  |   (L "setting up read-write heap of size ~x pages~%" size :os)
  |   (setf *mem* (make-array size :initial-element nil))
  |   (L "setting up write-protected heap of size ~x pages~%" size :os)
  |   ; only two bits are needed, but use a whole byte for now (the bits doesnt fit)
  |   (setf *memw* (make-array size :initial-element 0      ; OS-PROT-READ(logior OS-PROT-READ OS-PROT-WRITE)
  |                                 :element-type '(unsigned-byte 8)))
  |   (setf *memsz* size)
  |   (L "done setting up heap~%" :os)
  |   (loop for i from 0 below 2 do
  |     (L "init libc-cruft-heap page ~x~%" (+ i (ash *libc* (- PAGEBN))))
  |     (make-prot-page (+ i (ash *libc* (- PAGEBN))) (logior OS-PROT-READ OS-PROT-WRITE OS-PROT-LAZY)))
  |   (let ((cca *cpu-context-area*)
  |         (isa *interrupt-stack*))
  |     (loop for i from 0 below 8 do
  |       (L "bless interrupt pages~%" (+ i (ash isa (- PAGEBN))))
  |       (make-prot-page (- (ash isa (- PAGEBN)) i) (logior OS-PROT-READ OS-PROT-WRITE OS-PROT-LAZY))
  |       (make-prot-page (+ (ash cca (- PAGEBN)) i) (logior OS-PROT-READ OS-PROT-WRITE OS-PROT-LAZY))))
  |   (setf *mmap-heap-head* *mmap-heap*) ; reset mmap head
  |   (io-init))


  13.2 Call/Escape mechanism
  ----------------------------
  To give a quick overview of the machinery and what the functions do.
  In a native program this would partly be taken care of by the "dl.so" program

  The overall phases when emulation a program would be:
  1) Load parts of the program from the file into memory

  2) Resolve LAZY references (ie a call to "strlen" is binded to a host-lisp function)

  3) run the program, here the user-code will call the elf-references. In a native OS
     the "dl.so" will during runtime resolve the reference and reassembly the code.
     But the emulator does this instead during load-phase (step 2).

  4) final, here a native program would unmap and close any foreign libraries.

  The first two steps are pretty much governed by the ELF protocol.
  The lisp file elf.lisp will run the ELF protocol doing these two steps.
  To perform step 2, register a lazy reference to a host-lisp function
  the MAKE-STUB-ARCH is used.

  The following functions handles the call/escape mechanism that emulates how
  a "lazy symbol" is resolved and called by user-code.

  | make-stub-x86-64 -- see below
  | make-stub-mips   -- see below
  | make-stub-arch   -- reference a host-lisp function with a elf-reference
  |                     This functions just dispatch into arch dependent versions above
  | call-stub-escape -- a user-code call into a elf-reference is gone through this function

  13.2.1 Function make-stub-x86-64
  ----------------------------------
  | $(defun make-stub-x86-64 (addr name)
  |    (let ((stubaddr (+ *stub-vector* (incf *stub-load-index* 8)))
  |          (code (gethash name *stubs-name*))) ; see if we have any stub implementation loaded

  Stubaddr is a slot in the jump-vector table. We will set this slot to point to an
  instruction that throws the cpu to the stub/escape mechanism.

  Code is the function designator if any, depending on if the emulated libc has the function in
  question.
  The whole section below is an uninteresting blob where we manipulate pages and protection.
  The only interesting line is the one where stubaddr is written to addr.

  |      (let* ((pn (pageno addr)) ; FIX use mset-u64-soft instead
  |             (pb (aref *memw* pn))
  |             (page (get-page-num pn)))
  |        (cond
  |          ((not page) (L "failed to make-stub ~a, no page at address ~x" name addr))
  |          (t (setf (aref *memw* pn) OS-PROT-WRITE)
  |             (make-page stubaddr) ; FIX: it would be better to make the stub-vector pages prior emulation-start
  |             (mset-u64 addr stubaddr)
  |             (setf (aref *memw* pn) pb))))

  ASM-STUB is the "assemble stub" psuedo mnemonic. But it isn't arch independent and is therefor x86-64 only.
  This function is the one that writes the stub/escape instruction.
  |      (asm-stub stubaddr)

  To summarise up whats been done so far:

  We've wrote the stubaddr to addr, ie this will make the cpu jump to stubaddr address.
  Then at stubaddress we wrote the stub/escape instruction which will be executed by the cpu.

  We are almost done, finish off by storing either the function or name in the *STUBS*.
  This is used during call to get at the code (a function designator). Or the caller will
  get the string which atleast informs the user about what function was inteded to call,
  but instead is a true stub ( will just return zero in the return-register ).

  |      (cond
  |        (code
  |          (L "add stub ~s (with code) addr ~x -> stubaddr ~x~%" name addr stubaddr :0 :os)
  |          (setf (gethash stubaddr *stubs*) code))
  |        (t
  |          (L "add stub ~s addr ~x -> ~x~%" name addr stubaddr :0 :os)
  |          (setf (gethash stubaddr *stubs*) name)))
  |      (setf (gethash stubaddr *stubs-name*) name)))


  13.2.2 Function make-stub-mips
  --------------------------------

  We begin this function by getting the emulated implementation of the function as either a
  stringname or a function designator.

  | $(defun make-stub-mips (addr name)
  |    (let ((code (gethash name *stubs-name*))) ; see if we have any stub implementation loaded

  The following section has only one interesting line, the rest is just page protection workaround and page handling.
  The interesting line is when we write #xff to addr, that will write the stub/escape instruction.

  |      (let* ((pn (pageno addr)) ; FIX use mset-u64-soft instead
  |             (pb (aref *memw* pn))
  |             (page (get-page-num pn)))
  |        (cond
  |          ((not page) (L "failed to make-stub ~a, no page at address ~x" name addr))
  |          (t (setf (aref *memw* pn) OS-PROT-WRITE)
  |             (make-page addr) ; FIX: it would be better to make the stub-vector pages prior emulation-start
  |             (mset-u8 addr #xff)
  |             (setf (aref *memw* pn) pb))))


  Finish off by storing either the function designator or the stringname of the function.

  |      (cond
  |        (code
  |          (L "add mips stub ~s (with code) addr ~x~%" name addr :0 :os)
  |          (setf (gethash addr *stubs*) code))
  |        (t
  |          (L "add mips stub ~s addr ~x~%" name addr :0 :os)
  |          (setf (gethash addr *stubs*) name)))
  |      (setf (gethash addr *stubs-name*) name)))

  13.2.3 Function make-stub-arch
  --------------------------------
  This function is passed to the elf-loader to resolv symbolic references
  ( ie references not pointed to by a address number, but by a text-string ).
  Each architecture handles resolving a little different.

  | $(defun make-stub-arch (addr name arch)
  |    (ecase arch
  |      (x86-64 (make-stub-x86-64 addr name))
  |      (mips   (make-stub-mips addr name))))

  Lets take an overview description of how a stub is resolved.
  First, lets look at what is passed to MAKE-STUB-ARCH.

  * addr -- This is a address that when jumped to, will eventually lead to an external library function (like a libc call).
            More specifically this is the address that the user-code has been compiled by GCC to call when it wants to call
            this particular external function.

  * name -- This is a string containing the C-name of the function. This name is suffixed with ATplt
            to distinguish it from other stuff.

  * arch -- The architecture that we are supposed to emulate.

  With this information it is enough to bind the address that the user-code calls, with the host-lisp libc
  function with the corresponding name.

  The main differences between x86-64 and mips are those: x86-64 addr is a table entry where we should
  write the jump-address into a libc-routine (or other external mmaped function address).
  The emulator will write a address that points to a "stubslot". This stubslot is just a single
  instruction that will cause the CPU to enter the emulator-specific stub/escape mechanism.

  On mips, the room pointed to by addr should instead contain an instruction. So here we just
  write the stub/escape instruction (instead of jumping to the instruction as x86-64).

  @xref{Function make-stub-x86-64}, tells you more about how x86-64 stubs are created.

  @xref{Function make-stub-mips}, tells you more about how mips stubs are created.


  13.2.4 Function call-stub-escape
  ----------------------------------
  | $(defun call-stub-escape (pc cpu registers)
  |    (L "~x STUB/ESC called." *cpui* :os)
  |    (let ((fun (gethash pc *stubs*))
  |          (ret (mget-u64 (reg :rsp))))
  |      (L " load return ~x from ~x" ret (reg :rsp))
  |      (cond
  |        ((functionp fun)
  |          (L " calling stub function ~s~%" (gethash pc *stubs-name*) :os :6)
  |          (prog1
  |            (funcall fun cpu registers ret)
  |            (incf (reg :rsp) 8))) ; emulate a LEAVE (we are called by CALL)
  |        (t
  |          (L " WARNING: no stub function for ~x, got ~a, just returning~%" pc fun :os :2)
  |          (setf (reg :rax) 0)
  |          (incf (reg :rsp) 8)
  |          ret))))

  13.2.5 Function call-stub-escape-mips
  ---------------------------------------
  | $(defun call-stub-escape-mips (pc cpu registers)
  |    (let ((fun (gethash pc *stubs*))
  |          (ret (svref registers 31))) ; ra
  |      (L "~x STUB/ESC called, return to ~x~%" *cpui* ret :os)
  |      (cond
  |        ((functionp fun)
  |          (L " calling stub function ~s~%" (gethash pc *stubs-name*) :os :6)
  |          (funcall fun cpu registers ret))
  |        (t
  |          (L " WARNING: no stub function for ~x, got ~a, just returning~%" pc fun :os :2)
  |          (setf (svref registers 2) 0)
  |          ret))))

  13.3 Misc OS functions
  ------------------------

  13.3.1 Function microseconds-to-ticks
  ---------------------------------------
  Given a timespan this functions gives an approximation how
  many ticks would be executed during the timespan.
  Preferably this would be calculated during startup and then adjusted due to host-cpu load.
  But for now just equal one emulated instruction taking 1/10 microsecond

  |$(defun microseconds-to-ticks (us)
  |   (* 10 us))

