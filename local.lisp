(in-package :sbemu)

(defvar *core* nil)
(defvar *memsz* 0)
(defvar *mem* nil)  ; memory current available for reading and writing
(defvar *memw* nil) ; write protected memory (sigsegv)

(defmacro pageno (addr) `(ash ,addr (- PAGEBN)))

(defmacro aref-u8 (arr off)
  `(aref ,arr ,off))

(defmacro aref-u16 (arr off)
  `(+ (aref ,arr ,off) (ash (aref ,arr (1+ ,off)) 8)))

(defmacro aref-u32 (arr off)
  `(+ (aref ,arr ,off) (ash (aref ,arr (1+ ,off)) 8)
                       (ash (aref ,arr (+ 2 ,off)) 16)
                       (ash (aref ,arr (+ 3 ,off)) 24)))

(defmacro aref-u64 (arr off)
  `(+ (aref ,arr ,off) (ash (aref ,arr (1+ ,off)) 8)
                       (ash (aref ,arr (+ 2 ,off)) 16)
                       (ash (aref ,arr (+ 3 ,off)) 24)
                       (ash (aref ,arr (+ 4 ,off)) 32)
                       (ash (aref ,arr (+ 5 ,off)) 40)
                       (ash (aref ,arr (+ 6 ,off)) 48)
                       (ash (aref ,arr (+ 7 ,off)) 56)))

(defmacro aref-u16be (arr off)
  `(+ (ash (aref ,arr ,off) 8) (aref ,arr (1+ ,off))))

(defmacro aref-u32be (arr off)
  `(+ (ash (aref ,arr ,off) 24)
      (ash (aref ,arr (1+ ,off)) 16)
      (ash (aref ,arr (+ 2 ,off)) 8)
      (aref ,arr (+ 3 ,off))))

(defmacro aref-u64be (arr off)
  `(+ (ash (aref ,arr ,off) 56)
      (ash (aref ,arr (1+ ,off)) 48)
      (ash (aref ,arr (+ 2 ,off)) 40)
      (ash (aref ,arr (+ 3 ,off)) 32)
      (ash (aref ,arr (+ 4 ,off)) 24)
      (ash (aref ,arr (+ 5 ,off)) 16)
      (ash (aref ,arr (+ 6 ,off)) 8)
      (aref ,arr (+ 7 ,off))))

(defun get-page (addr) ; get page array by address
  (let* ((pn (pageno addr))
         (page (aref *mem* pn)))
    (cond
      ((and page (listp page))
        (setf (aref *mem* pn) (page-decompress (car page))))
      (t page))))

(defun get-page-num (pn) ; get page array by page-number
  (let ((page (aref *mem* pn)))
    (cond
      ((and page (listp page))
        (setf (aref *mem* pn) (page-decompress (car page))))
      (t page))))

; FIX: merge below two functions into prot being optional
(defun make-page (addr)
  (let* ((page (ash addr (- PAGEBN)))
         (parr (aref *mem* page)))
    (when (not parr)
      (setf (aref *mem* page) (make-array PAGESZ :element-type '(unsigned-byte 8) :initial-element 0))
      (setf (aref *memw* page) (logior OS-PROT-READ OS-PROT-WRITE OS-PROT-EXEC)))
    nil))

(defun make-prot-page (page prot)
  (setf (aref *memw* page) prot)
  (let ((parr (aref *mem* page)))
    (if (not parr)
      (setf (aref *mem* page) (make-array PAGESZ :element-type '(unsigned-byte 8) :initial-element 0)))
    nil))

(defun mget (addr size type)
  (let ((val 0))
    (loop for i from 0 below size do
      (let ((parr (get-page (+ addr i)))) ; get the page (a byte-array)
        (setf val (logior val (ash (aref parr (logand (+ addr i) PAGEMS)) (* i 8))))))
    (if type
      (if (not (zerop (logand val (ash 1 (1- (* size 8)))))) (- val (ash 1 (* size 8))) val)
      val)))

(defun mset (addr size data)
  (loop for i from 0 below size do
    (let ((parr (get-page (+ addr i)))) ; get the page (a byte-array)
      (setf (aref parr (logand (+ addr i) PAGEMS)) (logand (ash data (* i 8)) #xff)))))

(defun mget-u8-soft (addr)
  (let ((parr (get-page addr)))
    (if parr
      (aref parr (logand addr PAGEMS)))))

(defmacro mget-u8 (addr)
  `(let* ((page (pageno ,addr))
          (parr (get-page-num page)))
     (check-read-page page)
     (if parr
       (aref parr (logand ,addr PAGEMS))
       (page-fault page ,addr 1 nil nil))))

(defmacro mget-u16 (addr0)
  `(let* ((addr1 (1+ ,addr0))
          (page0 (ash ,addr0 (- PAGEBN)))
          (page1 (ash addr1 (- PAGEBN)))
          (parr0 (get-page-num page0)))
     (cond
       ((/= page0 page1)
         (check-read-page page0)
         (check-read-page page1)
         (L "u16:word split on page, bad address?~%" :cpu)
         (let ((parr1 (get-page-num page1)))
           (cond
             ((and parr0 parr1)
               (+ (aref parr0 (logand ,addr0 PAGEMS))
                  (ash (aref parr1 (logand addr1 PAGEMS)) 8)))
             (t
               (page-fault page0 ,addr0 2 nil nil)))))
       (t
         (check-read-page page0)
         (if parr0
           (+ (aref parr0 (logand ,addr0 PAGEMS))
              (ash (aref parr0 (logand addr1 PAGEMS)) 8))
           (page-fault page0 ,addr0 2 nil nil))))))

(defmacro mget-u16b (addr0)
  `(let* ((addr1 (1+ ,addr0))
          (page0 (ash ,addr0 (- PAGEBN)))
          (page1 (ash addr1 (- PAGEBN)))
          (parr0 (get-page-num page0)))
     (cond
       ((/= page0 page1)
         (check-read-page page0)
         (check-read-page page1)
         (L "u16:word split on page, bad address?~%" :cpu)
         (let ((parr1 (get-page-num page1)))
           (cond
             ((and parr0 parr1)
               (+ (aref parr1 (logand addr1 PAGESZ))
                  (ash (aref parr0 (logand ,addr0 PAGESZ)) 8)))
             (t
               (page-fault page0 ,addr0 2 nil nil)))))
       (t
         (check-read-page page0)
         (if parr0
           (+ (aref parr0 (logand addr1 PAGEMS))
              (ash (aref parr0 (logand ,addr0 PAGEMS)) 8))
           (page-fault page0 ,addr0 2 nil nil))))))

(eval
`(defun mget-u32b (addr0)
  (let* ((addr1 (+ addr0 1))
         (addr2 (+ addr0 2))
         (addr3 (+ addr0 3))
         (page0 (pageno addr0))
         (page3 (pageno addr3))
         parr0)
    (check-read-page page0)
    (setf parr0 (get-page-num page0))
    (cond
      ((/= page0 page3)
        (check-read-page page3)
        (let* ((page1 (pageno addr1))
               (page2 (pageno addr2))
               (parr1 (get-page-num page1))
               (parr2 (get-page-num page2))
               (parr3 (get-page-num page3)))
          (cond
            ((and parr0 parr1 parr2 parr3)
              (+ (aref parr3      (logand addr3 PAGEMS))
                 (ash (aref parr2 (logand addr2 PAGEMS)) 8)
                 (ash (aref parr1 (logand addr1 PAGEMS)) 16)
                 (ash (aref parr0 (logand addr0 PAGEMS)) 24)))
            (t
              (page-fault page0 addr0 4 nil nil)))))
      (t
        (if parr0
          ,@(cond
              ((feature :daa)
                `((get-vector-u32 parr0 (logand addr0 PAGEMS))
                  (error "must implement big-endian get-vector-u32")))
              (t
                `((+ (aref parr0 (logand addr3 PAGEMS))
                     (ash (aref parr0 (logand addr2 PAGEMS)) 8)
                     (ash (aref parr0 (logand addr1 PAGEMS)) 16)
                     (ash (aref parr0 (logand addr0 PAGEMS)) 24)))))
          (page-fault page0 addr0 4 nil nil)))))))
(eval
`(defun mget-u32b-soft (addr0)
  (let* ((addr1 (+ addr0 1))
         (addr2 (+ addr0 2))
         (addr3 (+ addr0 3))
         (page0 (pageno addr0))
         (page3 (pageno addr3))
         parr0)
    (setf parr0 (get-page-num page0))
    (when parr0
    (cond
      ((/= page0 page3)
        (let* ((page1 (pageno addr1))
               (page2 (pageno addr2))
               (parr1 (get-page-num page1))
               (parr2 (get-page-num page2))
               (parr3 (get-page-num page3)))
          (cond
            ((and parr0 parr1 parr2 parr3)
              (+ (aref parr3      (logand addr3 PAGEMS))
                 (ash (aref parr2 (logand addr2 PAGEMS)) 8)
                 (ash (aref parr1 (logand addr1 PAGEMS)) 16)
                 (ash (aref parr0 (logand addr0 PAGEMS)) 24))))))
      (t
        (if parr0
          ,@(cond
              ((feature :daa)
                `((get-vector-u32 parr0 (logand addr0 PAGEMS))
                  (error "must implement big-endian get-vector-u32")))
              (t
                `((+ (aref parr0 (logand addr3 PAGEMS))
                     (ash (aref parr0 (logand addr2 PAGEMS)) 8)
                     (ash (aref parr0 (logand addr1 PAGEMS)) 16)
                     (ash (aref parr0 (logand addr0 PAGEMS)) 24))))))))))))

(eval
`(defun mget-u32 (addr0)
  (let* ((addr1 (+ addr0 1))
         (addr2 (+ addr0 2))
         (addr3 (+ addr0 3))
         (page0 (pageno addr0))
         (page3 (pageno addr3))
         parr0)
    (check-read-page page0)
    (setf parr0 (get-page-num page0))
    (cond
      ((/= page0 page3)
        (check-read-page page3)
        (let* ((page1 (pageno addr1))
               (page2 (pageno addr2))
               (parr1 (get-page-num page1))
               (parr2 (get-page-num page2))
               (parr3 (get-page-num page3)))
          (cond
            ((and parr0 parr1 parr2 parr3)
              (+ (aref parr0      (logand addr0 PAGEMS))
                 (ash (aref parr1 (logand addr1 PAGEMS)) 8)
                 (ash (aref parr2 (logand addr2 PAGEMS)) 16)
                 (ash (aref parr3 (logand addr3 PAGEMS)) 24)))
            (t
              (page-fault page0 addr0 4 nil nil)))))
      (t
        (if parr0
          ,@(cond
              ((feature :daa)
                `((get-vector-u32 parr0 (logand addr0 PAGEMS))))
              (t
                `((+ (aref parr0 (logand addr0 PAGEMS))
                     (ash (aref parr0 (logand addr1 PAGEMS)) 8)
                     (ash (aref parr0 (logand addr2 PAGEMS)) 16)
                     (ash (aref parr0 (logand addr3 PAGEMS)) 24)))))
          (page-fault page0 addr0 4 nil nil)))))))

; 0x7fffffffe758 is stored as 58 e7 ff ff ff 7f 00 00
;(defmacro mget-u64 (addr)
;  `(let* ((page (ash ,addr (- PAGEBN)))
;          (parr (aref *mem* page)))
;     (if parr
;       (aref parr (logand ,addr PAGEMS))
;       (error "read page-fault at page ~x, address ~x" page ,addr))))

(defun mset-u8 (addr data)
  (let ((page (ash addr (- PAGEBN))) parr a)
    (setf a (check-write-page page))
    (setf parr (get-page-num page))
    (if (not a)
      (if parr
        (setf (aref parr (logand addr PAGEMS)) data)
        (page-fault page addr 1 data nil)))
    nil))

(defmacro mset-u16 (addr data)
  `(let ((page0 (ash ,addr (- PAGEBN)))
         (page1 (ash (1+ ,addr) (- PAGEBN))))
     (check-write-page page0)
     (if (/= page0 page1) (check-write-page page1))
     (let ((parr0 (get-page-num page0))
           (parr1 (get-page-num page1)))
       (cond
         ((and parr0 parr1)
           (setf (aref parr0 (logand ,addr PAGEMS)) (logand ,data #xff))
           (setf (aref parr1 (logand (1+ ,addr) PAGEMS)) (ash (logand ,data #xff00) -8)))
         (t (if (not parr0) (page-fault page0 ,addr 2 ,data nil))
            (if (not parr1) (page-fault page1 ,addr 2 ,data nil))))
       nil)))

(eval
`(defun mset-u32 (addr data)
  (let* ((addr1 (+ addr 1))
          (addr2 (+ addr 2))
          (addr3 (+ addr 3))
          (page0 (ash addr (- PAGEBN)))
          (page3 (ash addr3 (- PAGEBN)))
          parr0)
     (check-write-page page0)
     (if (/= page0 page3) (check-write-page page3))
     (setf parr0 (get-page-num page0))
     (cond
       ((/= page0 page3)
         (let* ((page1 (ash addr1 (- PAGEBN)))
                (page2 (ash addr2 (- PAGEBN)))
                (parr1 (get-page-num page1))
                (parr2 (get-page-num page2))
                (parr3 (get-page-num page3)))
           (cond
             ((and parr0 parr1 parr2 parr3)
               (setf (aref parr0 (logand addr PAGEMS)) (logand data #xff))
               (setf (aref parr1 (logand addr1 PAGEMS)) (ash (logand data #xff00) -8))
               (setf (aref parr2 (logand addr2 PAGEMS)) (ash (logand data #xff0000) -16))
               (setf (aref parr3 (logand addr3 PAGEMS)) (ash (logand data #xff000000) -24)))
             (t
               (page-fault page0 addr 4 nil nil)))))
       (t
         (cond
           (parr0
             ,@(cond
                 ((feature :daa)
                   `((set-vector-u32 parr0 (logand addr PAGEMS) (logand data #xffffffff))))
                 (t
                   `((setf (aref parr0 (logand addr PAGEMS))  (logand data #xff))
                     (setf (aref parr0 (logand addr1 PAGEMS)) (ash (logand data #xff00) -8))
                     (setf (aref parr0 (logand addr2 PAGEMS)) (ash (logand data #xff0000) -16))
                     (setf (aref parr0 (logand addr3 PAGEMS)) (ash (logand data #xff000000) -24))))))
           (t (page-fault page0 addr 4 nil nil)))))
     nil)))

(eval
`(defun mset-u32b (addr data)
  (let* ((addr1 (+ addr 1))
          (addr2 (+ addr 2))
          (addr3 (+ addr 3))
          (page0 (ash addr (- PAGEBN)))
          (page3 (ash addr3 (- PAGEBN)))
          parr0)
     (check-write-page page0)
     (if (/= page0 page3) (check-write-page page3))
     (setf parr0 (get-page-num page0))
     (cond
       ((/= page0 page3)
         (let* ((page1 (ash addr1 (- PAGEBN)))
                (page2 (ash addr2 (- PAGEBN)))
                (parr1 (get-page-num page1))
                (parr2 (get-page-num page2))
                (parr3 (get-page-num page3)))
           (cond
             ((and parr0 parr1 parr2 parr3)
               (setf (aref parr3 (logand addr3 PAGEMS)) (logand data #xff))
               (setf (aref parr2 (logand addr2 PAGEMS)) (ash (logand data #xff00) -8))
               (setf (aref parr1 (logand addr1 PAGEMS)) (ash (logand data #xff0000) -16))
               (setf (aref parr0 (logand addr  PAGEMS)) (ash (logand data #xff000000) -24)))
             (t
               (page-fault page0 addr 4 nil nil)))))
       (t
         (cond
           (parr0
             ,@(cond
                 ((feature :daa)
                   `((set-vector-u32 parr0 (logand addr PAGEMS) (logand data #xffffffff))
                     (error "daa set-vector-u32 is little-endian")))
                 (t
                   `((setf (aref parr0 (logand addr3 PAGEMS))  (logand data #xff))
                     (setf (aref parr0 (logand addr2 PAGEMS)) (ash (logand data #xff00) -8))
                     (setf (aref parr0 (logand addr1 PAGEMS)) (ash (logand data #xff0000) -16))
                     (setf (aref parr0 (logand addr  PAGEMS)) (ash (logand data #xff000000) -24))))))
           (t (page-fault page0 addr 4 nil nil)))))
     nil)))

(eval
`(defun mset-u64 (addr data)
  (let ((pagea (ash addr (- PAGEBN)))
        (pageb (ash (+ addr 7) (- PAGEBN))) a b)
    (setf a (check-write-page pagea))
    (if (/= pagea pageb) (setf b (check-write-page pageb)))
    (cond
      ,@(if (feature :daa)
          `(((and (not (or a b)) (= pagea pageb))
              (set-vector-u64 (get-page-num pagea) (logand addr PAGEMS) (logand data #xffffffffffffffff)))))
      ((not (or a b)) ; quadword crosses page boundary, do slow method
        (loop for i from 0 below 8 do
          (let* ((page (ash (+ addr i) (- PAGEBN))) ; make page address
                 (parr (get-page-num page))) ; get the page (a byte-array)
            (cond
              (parr ; page exist, write byte
                (setf (aref parr (logand (+ addr i) PAGEMS)) (logand (ash data (- (* i 8))) #xff)))
              (t
                (page-fault page (+ addr i) 8 data nil))))))
      (t
        (L "cant write to addr ~x~%" addr :cpu))))))

(eval
`(defun mget-u64 (addr)
  (let ((val 0)
        (pagea (pageno addr))
        (pageb (pageno (+ addr 7))))
    (check-read-page pagea)
    (cond
      ,@(if (feature :daa)
          `(((= pagea pageb)
              (setf val (get-vector-u64 (get-page-num pagea) (logand addr PAGEMS))))))
      (t
        (if (/= pagea pageb) (check-read-page pageb))
        (loop for i from 0 below 8 do
          (let* ((page (ash (+ addr i) (- PAGEBN))) ; make page address
                 (parr (get-page-num page))) ; get the page (a byte-array)
            (cond
              (parr ; page exist, write byte
                (setf val (logior val (ash (aref parr (logand (+ addr i) PAGEMS)) (* i 8)))))
              (t
                (page-fault page (+ addr i) 8 nil nil)))))))
    val)))

(defun mget-u64-soft (addr)
  (let ((val 0) parr)
    (loop for i from 0 below 8 do
      (setf parr (get-page (+ addr i)))
      (cond
        (parr ; page exist, get byte
          (setf val (logior val (ash (aref parr (logand (+ addr i) PAGEMS)) (* i 8)))))
        (t (return-from mget-u64-soft))))
     val))

; signed versions

(macrolet
  ((mget-signed (name uname signbit)
     (let ((conv (ash 1 signbit)))
       `(defmacro ,name (addr)
          `(let ((x (,,uname ,addr)))
             (if (logbitp ,,(1- signbit) x) (- x ,,conv) x))))))
  (mget-signed mget-s8  'mget-u8   8)
  (mget-signed mget-s16 'mget-u16 16)
  (mget-signed mget-s32 'mget-u32 32)
  (mget-signed mget-s64 'mget-u64 64))

