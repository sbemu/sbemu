
(require :asdf)
(require :sb-posix)

(defun lisp-quit ()
  #+sbcl (sb-ext:exit)
  #+clisp (ext:quit))

(unless (ignore-errors (truename "./sbemu.asd"))
  (format t "You must be in sbemu source directory to slam.~%")
  (lisp-quit))

; setup compilation configuration
; load information on compile profile for sbemu
; this information is picked up from config.lisp
; and command-line.
(asdf:oos 'asdf:load-op :sbemu-config)
(sbemu::config-parse-cli)

; after configuration is read, load any extra packages that
; configuration has requested
(sbemu::load-by-config)

(defvar *asdf-fasl-cache* nil)
; FIX: can we utilize more functions in ASDF/UIOP instead of cooking our own pathname?
(let ((pathname-components (map 'list #'namestring
                                ; FIX: test if uiop is available on Clisp
                                (list (uiop/configuration:resolve-absolute-location uiop/configuration:*user-cache*
                                                                                    :ensure-directory t
                                                                                    :wilden nil)
                                      (user-homedir-pathname)
                                      "sbemu"))))
  (setf *asdf-fasl-cache* (apply #'concatenate 'string pathname-components)))
; FIX check prerequisite: directory exist, else prompt for user .cache/common-lisp directory
(assert (probe-file *asdf-fasl-cache*))
(format t "asdf-fasl-cache: ~s~%" *asdf-fasl-cache*)

; sources is a list of source file together with a number.
; The number tells how to bring in the file:
; 0 -- load the file by the fasl file or compile if source is newer
; 1 -- force compile the source file, move the resulting fasl to the asdf-fasl-cache and load it from there
(defvar *sources*
        '((0 "packages")
          ;(0 "config-loader" ); definitions usable by compile-time configuration
          ;(0 "config" )       ; compile-time configuration
          ;(0 "config-cli" )   ; get compile-time configuration from command-line
          (0 "logger")
          (0 "portable" ); routines that differs between CL-implementation
          (0 "portable-aux" )
          (0 "common" )  ; routines that are general and is not sbemu specific
          (0 "arch"    ) ; architecture support common
          (0 "local"   ) ; local routines to sbemu that are used here and there
          (0 "asm"      ); common assembler/disassembler
          (0 "x86-64"  ) ; arch specific instructions
          (0 "asm-x86-64" ); x86-64 converter
          (0 "mips-util")
          (0 "mips")
          (0 "asm-mips")
          (0 "cpu")   ; cpu handling
          (0 "os")    ; OS stubs and such
          (0 "os-aux")
          (0 "heap")  ; heap manipulation
          (0 "io")    ; io-handling
          (0 "libc")  ; system calls
          (0 "libc-aux")
          (0 "elf")   ; executable linking format
          (0 "ice")   ; thaw/freeze emulator cores
          (0 "emu")   ; misc emulator stuff
          (0 "debug") ; debug support
          (0 "mon")   ; monitor
          (0 "mon-aux") ; monitor commands
          (0 "lisp-code")
          (0 "frag")
          (0 "help")))

(defun utime (file) (let* ((stat (sb-posix:stat file))) (sb-posix:stat-mtime stat)))

(unless (ignore-errors (truename *asdf-fasl-cache*))
  (format t "Try to load sbemu by normal asdf load. Then slam it.~%")
  (lisp-quit))
(unless (string= (package-name *package*) "SBEMU")
  (format t "Strange, asdf didn't change to SBEMU package, maybe something went wrong when asdf-loading sbemu-config or calling load-by-config~%")
  (lisp-quit))

(defun compile-and-load (base file)
  (handler-bind
    ((style-warning (lambda (c) (format t "~s~%" c) nil)))
    (multiple-value-bind (outfile warning failure) (compile-file file)
      (declare (ignore outfile))
      (when (or failure)
      ;(format t "SLAM: times ~s -> ~s~%" stime ftime)
        (lisp-quit))))
  (sb-ext:run-program "/bin/mv" (list (format nil "~a.fasl" file) base))
  (load (format nil "~a/~a.fasl" base file)))

(defun compile/load-using-asdf (file)
  (declare (ignore base))
  (let ((compop (make-instance 'asdf:compile-op))
        (loadop (make-instance 'asdf:load-op))
        (source (make-instance 'asdf:cl-source-file)))
    ;... fill in the slots or something
    (asdf:perform compopile-op file)
    (asdf:perform loadop file)))

(format t "sbcl-home according to slam: ~s~%" sbemu::+lib-env-sbcl-home+)

; ok, now everything is setup for compilation of sbemu
(let ((base (format nil "~a" *asdf-fasl-cache*))) ; FIX: how do we stringify a PATHNAME?
  (loop for (com file) in *sources* do
    ;(format t "SLAM: file: ~a~%" file)
    (let* ((fasl-file (format nil "~a/~a.fasl" base file))
           (fasl-p    (probe-file fasl-file))
           (stime     (utime (format nil "~a.lisp" file)))
           (ftime     (if fasl-p (utime fasl-file))))
      ;(format t "SLAM: times ~s -> ~s~%" stime ftime)
      (cond
        ; compile the source and load its result if either
        ((or (= com 1)       ; the force-flag is set by the user
             (> stime ftime) ; the file modification time of source is newer than fasl
             (not (probe-file fasl-file))) ; the fasl doesn' exist.
          (format t "SLAM: compiling file ~a~%" file)
          (compile-and-load base file)
          #+nil(compile-and-load-using-asdf base file))
        (t
          (format t "SLAM: loading fasl ~a~%" fasl-file)
          (load fasl-file))))))

; now that we've compiled sbemu it is time to do
; run-time configuration, this configuration comes from
; config.lisp, site.lisp and the command-line

(in-package :sbemu)


(defvar *site* "site.lisp")

; first a bunch of helper functions
(defun set-log-level (level)
  (setf common::*log-level* level))

(defun set-config ()
  ;(setf common::*log-level* 3) ; set an apropiate default log-level
  (loop for form in *config-late* do
    (format t "setting config ~a~%" form)
    (eval form)))

(defun load-and-run-rtest ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (load "r/load")
  (funcall 'rtest))

(defun load-and-run-gcctest ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (load "t/gcc/load")
  (funcall 'gcctest))

(defun load-and-run-test ()
  (asdf:oos 'asdf:load-op :sbemu-test)
  (funcall 'test))

(defun cli-parse-log (arg)
  (format t "logfile: ~a~%" arg)
  (setf common::*log-filename* arg))

(defun cli-parse-verbose (arg) (setf *verbose* (parse-integer arg)))
(defun cli-parse-thaw    (arg) (setf *ice* arg))
(defun cli-parse-site    (arg)
  (format t "**** setting *site* to ~a~%" arg)
  (setf *site* arg))
(defun cli-parse-cpu-freeze  (arg) (setf *cpu-freeze* (read-from-string (format nil "#x~a" arg))))
(defun cli-maxrun (arg) (setf *maxrun* (read-from-string (format nil "#x~a" arg))))

(let (cmd)
  (loop for arg in (cdr sb-ext:*posix-argv*) do
    (format t "check arg [~a]~%" arg)
    (cond
      ((string= arg "-v") (decf *verbose*))
      ((string= arg "+v") (incf *verbose*))
      ; the purpose of the core command is to dump a core
      ; that behaves as sbcl, but is emulated. This means
      ; using a logfile instead of stdio etc.
      ((string= arg "core") (setf cmd :core))
      ((string= arg "run") (setf cmd :run))
      ((string= arg "mon") (setf cmd :mon))
      ((string= arg "gdb") (setf cmd :gdb))
      ((string= arg "help") (setf cmd :help))
      ((string= arg "test") (setf cmd :test))
      ((string= arg "rtest") (setf cmd :rtest))
      ((string= arg "gcctest") (setf cmd :gcctest))
      ((string= arg "load") (setf cmd :load))
      ((not (search "--" arg)) (setf *cmdarg* (nconc *cmdarg* (list arg))))
      ((search "--maxrun=" arg) (cli-maxrun (subseq arg (1+ (search "=" arg)))))
      ((search "--site=" arg) (cli-parse-site (subseq arg (1+ (search "=" arg)))))
      ((search "--log=" arg) (cli-parse-log (subseq arg (1+ (search "=" arg)))))
      ((search "--thaw=" arg) (cli-parse-thaw (subseq arg (1+ (search "=" arg)))))
      ((search "--cpu-freeze=" arg) (cli-parse-cpu-freeze (subseq arg (1+ (search "=" arg)))))
      ((search "--verbose=" arg) (cli-parse-verbose (subseq arg (1+ (search "=" arg)))))
      ((search "--paranoid=" arg)) ; rest are parsed by config-cli.lisp
      ((search "--pagesize=" arg))
      ((search "--config=" arg))
      (t (error "unknown command line argument: ~a~%" arg))))
  (format t "cmd = [~a] arg = [~a]~%" cmd *cmdarg*)
  (when (not (zerop *verbose*))
    (format t "compile configuration:~%")
    (format t "page-size: ~x~%" (page-size))
    (format t "paranoid-limit: ~x~%" PARANOID-LIMIT))
  (set-config)
  (when (probe-file *site*)
    (format t "loading site file ~a~%" *site*)
    (load *site*))
  (case cmd
    (:run (start t))
    (:mon (monitor :init t :greet t))
    (:gdb
      (load "gdb")
      (funcall 'start-gdb-concert (start nil)))
    (:test    (load-and-run-test))
    (:rtest   (load-and-run-rtest))
    (:gcctest (load-and-run-gcctest))
    (:help (help))
    (:core (core))
    (:load
      (in-package :sbemu)
      (format t "Loading file ~a~%" (first *cmdarg*))
      (if *cmdarg* (load (first *cmdarg*))))
    (t (help))))

