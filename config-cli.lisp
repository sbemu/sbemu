(in-package :sbemu)

; a bit ugly, but we need to get command-line arguments before the
; compilation starts

(defun cli-parse-paranoid (arg) (eval `(define-paranoid ,(parse-integer arg))))
(defun cli-parse-pagesize (arg) (eval `(define-pagesize ,(parse-integer arg))))
(defun cli-parse-config   (arg)
  (let ((*package* (find-package "SBEMU")))
    (load arg)))

(defun config-parse-cli ()
  (in-package :sbemu)
  (loop for arg in #+sbcl (cdr sb-ext:*posix-argv*)
                   #+clisp ext:*args*
                   do
    (format t "config check arg [~a]~%" arg)
    (let ((subarg (subseq arg (1+ (or (search "=" arg) -1)))))
      (cond
        ((search "--paranoid=" arg) (cli-parse-paranoid subarg))
        ((search "--pagesize=" arg) (cli-parse-pagesize subarg))
        ((search "--config="   arg) (cli-parse-config   subarg))))))

; load extra packages
(defun load-by-config ()
  (when (gethash :cl-store *config-features*)
    (asdf:oos 'asdf:load-op :cl-store)
    (push :cl-store *features*)))

